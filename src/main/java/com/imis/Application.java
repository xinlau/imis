package com.imis;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * 启动类
 *
 * @author IMIS
 */
@Slf4j
@EnableSwagger2WebMvc
@SpringBootApplication(exclude = {
        DruidDataSourceAutoConfigure.class
})
public class Application {

    /*
     * DruidDataSourceAutoConfigure会注入一个DataSourceWrapper，
     * 其会在原生的spring.datasource下找url,username,password等。
     * 而我们动态数据源的配置路径是变化的，要排除DruidDataSourceAutoConfigure
     */

    /**
     * 启动方法
     *
     * @param args -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/3 11:15
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
