package com.imis.module.quartz;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * <p>
 * QuartzJobTest<br>
 * 定时任务测试类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年06月13日 18:17
 */
@Slf4j
public class QuartzJobTest extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("Hello Word ！！！我是一个定时任务测试类，我要搞事情！！！");
    }

    /**
     * 定时任务执行类
     * @param context
     */
    private void doQuartzJob(JobExecutionContext context){
        System.out.println("略略略");
    }

}
