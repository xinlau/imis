package com.imis.module.quartz;

import com.imis.base.constant.enums.CommonResponseEnum;
import com.imis.base.globle.exception.BusinessException;
import com.imis.base.globle.response.BaseResponse;
import com.imis.module.system.bus.SysQuartzJobBus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * <p>
 * QuartzJobInitialization<br>
 * 定时任务初始化
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年06月15日 09:41
 */
@Slf4j
@Component
@Order(value = 2)
public class QuartzJobInitialization implements CommandLineRunner {

    /**
     * 定时任务 业务处理类
     */
    private SysQuartzJobBus sysQuartzJobBus;

    @Autowired
    public void setSysQuartzJobBus(SysQuartzJobBus sysQuartzJobBus) {
        this.sysQuartzJobBus = sysQuartzJobBus;
    }

    /**
     * 定时任务启动初始化
     *
     * @param args -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/15 9:46
     */
    @Override
    public void run(String... args) throws BusinessException {
        log.debug("定时任务自动初始化......");
        BaseResponse result = sysQuartzJobBus.loadSysJobToQuartz();
        CommonResponseEnum.ERROR_500.assertIsTrue(CommonResponseEnum.SUCCESS.getCode().equals(result.getCode()));
        log.debug("定时任务自动初始化全部完成：" + result);
    }

}
