package com.imis.module.quartz;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * <p>
 * QuartzJobTest<br>
 * 带参数定时任务测试类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年06月13日 18:17
 */
@Slf4j
public class QuartzJobParameterTest extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext context) {
        log.info("Hello Word ！！！我是一个携带参数的定时任务测试类，我要搞事情！！！");
        doQuartzJob(context);
    }

    /**
     * 定时任务执行类
     *
     * @param context - 定时任务执行上下文
     */
    private void doQuartzJob(JobExecutionContext context) {

        JobDataMap jobDataMap = context.getMergedJobDataMap();
        String toJsonString = JSONObject.toJSONString(jobDataMap);
        System.out.println(toJsonString);

        doTestForQuartzJobParameter(
                jobDataMap.getIntegerFromString("integer"),
                jobDataMap.getCharFromString("c"),
                jobDataMap.getLongValueFromString("l"),
                jobDataMap.getFloatValueFromString("f"),
                jobDataMap.getDoubleValueFromString("d"),
                jobDataMap.getString("string"),
                jobDataMap.getBooleanValueFromString("b")
        );
    }

    private void doTestForQuartzJobParameter(Integer integer, char c, Long l, Float f, Double d, String string, Boolean b) {
        System.out.println("Integer :" + integer.toString());
        System.out.println("char :" + c);
        System.out.println("Long :" + l.toString());
        System.out.println("Float :" + f.toString());
        System.out.println("Double :" + d.toString());
        System.out.println("String :" + string);
        System.out.println("Boolean :" + b.toString());
    }

}
