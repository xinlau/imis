package com.imis.module.base.impl;

import com.imis.module.base.BaseMapper;
import com.imis.module.base.BaseEntity;
import com.imis.module.base.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * Service基类 服务实现类（泛型：M 是 Mapper 对象、 T 是实体对象 ）
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
@Slf4j
public abstract class BaseServiceImpl<M extends BaseMapper<T>, T extends BaseEntity> extends com.baomidou.mybatisplus.extension.service.impl.ServiceImpl<M, T> implements BaseService<T> {

    /**
     * 基于传值泛型注入Mapper实现，此处加入 required = false 是为了避免IDEA对泛型注入的检查，其他地方不可以像这样注入
     */
    @Autowired(required = false)
    protected M mapper;
}
