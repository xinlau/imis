package com.imis.module.base;

/**
 * <p>
 * Mapper 接口基类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
public interface BaseMapper<T> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {
}
