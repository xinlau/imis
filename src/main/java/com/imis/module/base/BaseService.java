package com.imis.module.base;

/**
 * <p>
 * Service基类 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
public interface BaseService<T> extends com.baomidou.mybatisplus.extension.service.IService<T> {
}
