package com.imis.module.base;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.imis.base.util.ConvertUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * SysPageRo<br>
 * 分页条件RO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月11日 15:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BasePageDTO implements Serializable {

    /**
     * 页码
     */
    @ApiModelProperty(value = "页码", required = true)
    private Long pageNumber;
    public Long getPageNumber() {
        return this.pageNumber = ConvertUtils.isEmpty(pageNumber) ? 1L : pageNumber;
    }

    /**
     * 页长
     */
    @ApiModelProperty(value = "页长", required = true)
    private Long pageSize;
    public Long getPageSize() {
        return this.pageSize = ConvertUtils.isEmpty(pageSize) ? 10L : pageSize;
    }

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private Long createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间-起")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTimeStart;
    @ApiModelProperty(value = "创建时间-始")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTimeEnd;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private Long updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间-起")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime updateTimeStart;
    @ApiModelProperty(value = "更新时间-始")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime updateTimeEnd;

    /**
     * 排序字段
     */
    @ApiModelProperty(value = "排序字段")
    private List<SortField> sortFieldList;

    /**
     * 超级查询参数 这个参数方便自定义SQL条件查询（要考虑SQL注入，查询条件与数据权限冲突）
     */
    @ApiModelProperty(value = "超级查询参数", hidden = true)
    private String superQueryParams;

}
