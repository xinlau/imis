package com.imis.module.base;

import com.imis.base.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 通用 业务处理类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
@Slf4j
public abstract class BaseBus {

    /**
     * Redis 工具类
     */
    @Autowired
    protected RedisUtil redisUtil;

}
