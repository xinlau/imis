package com.imis.module.system.bus;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.enums.ArgumentResponseEnum;
import com.imis.base.constant.enums.CommonResponseEnum;
import com.imis.base.constant.enums.QuartzJobStatusEnum;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.base.globle.response.ErrorResponse;
import com.imis.base.util.ConvertUtils;
import com.imis.module.base.BaseBus;
import com.imis.module.system.model.converter.SysQuartzJobConverter;
import com.imis.module.system.model.po.SysQuartzJob;
import com.imis.module.system.model.ro.PagingQueryQuartzJobDTO;
import com.imis.module.system.model.ro.SysQuartzJobAddRO;
import com.imis.module.system.model.ro.SysQuartzJobUpdateRO;
import com.imis.module.system.model.vo.SysQuartzJobVO;
import com.imis.module.system.service.ISysQuartzJobService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 定时任务 业务处理类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
@Slf4j
@Service
public class SysQuartzJobBus extends BaseBus {

    /**
     * 定时任务 服务类
     */
    private ISysQuartzJobService serviceBySysQuartzJobService;

    @Autowired
    public void setSysQuartzJobService(ISysQuartzJobService serviceBySysQuartzJobService) {
        this.serviceBySysQuartzJobService = serviceBySysQuartzJobService;
    }

    /**
     * 定时任务 调度程序
     */
    private Scheduler scheduler;

    @Autowired(required = false)
    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * 添加定时任务重复校验
     *
     * @param sysQuartzJobAdd - 定时任务添加对象
     * @return SysQuartzJob
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/9 9:47
     */
    private SysQuartzJob quartzJobAddVerification(SysQuartzJobAddRO sysQuartzJobAdd) {
        // 1.校验定时任务名称重复
        SysQuartzJob sysQuartzJob = this.serviceBySysQuartzJobService.queryQuartzJobByName(sysQuartzJobAdd.getJobName());
        // 根据定时任务信息查询，该定时任务名存在
        ArgumentResponseEnum.MENU_ADD_ERR_REPEAT.assertIsNull(sysQuartzJob, sysQuartzJobAdd.getJobName());
        return SysQuartzJobConverter.INSTANCE.getAddEntity(sysQuartzJobAdd);
    }

    /**
     * 确定具有给定标识符的作业是否已存在于计划程序中
     *
     * @param jobKey - 定时任务 唯一标识
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 15:59
     */
    private Boolean checkQuartzJobExists(final JobKey jobKey) {
        boolean checkExists;
        try {
            // 1.确定具有给定标识符的作业是否已存在于计划程序中
            checkExists = this.scheduler.checkExists(jobKey);
        } catch (SchedulerException e) {
            checkExists = Boolean.FALSE;
        }
        return checkExists;
    }

    /**
     * 删除定时任务校验
     *
     * @param quartzJobIdentification - 定时任务对象编号
     * @return SysQuartzJob - 定时任务对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/9 9:47
     */
    private SysQuartzJob quartzJobDeleteVerification(final Long quartzJobIdentification) {
        SysQuartzJob sysQuartzJob = this.serviceBySysQuartzJobService.getById(quartzJobIdentification);
        // 1.校验定时任务存在
        ArgumentResponseEnum.QUARTZ_JOB_DELETE_ERR_NON.assertNotNull(sysQuartzJob);
        JobKey jobKey = new JobKey(sysQuartzJob.getJobName(), sysQuartzJob.getJobGroup());
        log.debug(jobKey.toString());
        // 2.已存在于计划程序中
        ArgumentResponseEnum.QUARTZ_JOB_DELETE_ERR_ASSIGNED.assertIsTrue(this.checkQuartzJobExists(jobKey), sysQuartzJob.getJobName());
        return sysQuartzJob;
    }

    /**
     * 更新定时任务校验
     *
     * @param sysQuartzJobUpdate - 定时任务更新对象
     * @return SysQuartzJob - 定时任务
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/9 9:47
     */
    private SysQuartzJob quartzJobUpdateVerification(final SysQuartzJobUpdateRO sysQuartzJobUpdate) {
        // 1.校验定时任务存在
        SysQuartzJob quartzJob = this.serviceBySysQuartzJobService.getById(sysQuartzJobUpdate.getId());
        ArgumentResponseEnum.QUARTZ_JOB_UPDATE_ERR_NON.assertNotNull(quartzJob, sysQuartzJobUpdate.getJobName());
        // 2.校验定时任务存在于计划程序中
        JobKey jobKey = new JobKey(quartzJob.getJobName(), quartzJob.getJobGroup());
        log.debug(jobKey.toString());
        ArgumentResponseEnum.QUARTZ_JOB_UPDATE_ERR_ASSIGNED.assertIsTrue(this.checkQuartzJobExists(jobKey), sysQuartzJobUpdate.getJobName());
        // 3.校验同一个任务分组定时任务名称重复
        if (!quartzJob.getJobName().equals(sysQuartzJobUpdate.getJobName())) {
            SysQuartzJob quartzJobName = this.serviceBySysQuartzJobService.getOne(Wrappers.<SysQuartzJob>lambdaQuery().eq(SysQuartzJob::getJobName, sysQuartzJobUpdate.getJobName()).eq(SysQuartzJob::getJobGroup, sysQuartzJobUpdate.getJobGroup()), Boolean.FALSE);
            ArgumentResponseEnum.QUARTZ_JOB_UPDATE_ERR_REPEAT.assertIsNull(quartzJobName, sysQuartzJobUpdate.getJobName());
        }
        SysQuartzJobConverter.INSTANCE.getUpdateEntity(quartzJob, sysQuartzJobUpdate);
        return quartzJob;
    }

    /**
     * 构建定时任务校验
     *
     * @param quartzJobIdentification - 定时任务对象编号
     * @return SysQuartzJob - 定时任务对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/9 9:47
     */
    private SysQuartzJob schedulerQuartzJobVerification(final Long quartzJobIdentification) {
        SysQuartzJob quartzJob = this.serviceBySysQuartzJobService.getById(quartzJobIdentification);
        // 1.校验定时任务存在
        ArgumentResponseEnum.QUARTZ_JOB_SCHEDULER_ERR_JOB_NON.assertNotNull(quartzJob);
        // 2.排除数据库中状态为  正常、暂停、完成、出错、阻塞、具有给定标识符的作业已存在于计划程序中 的定时任务
        JobKey key = new JobKey(quartzJob.getJobName(), quartzJob.getJobGroup());
        boolean scheduler = !quartzJob.getJobState().equals(QuartzJobStatusEnum.NONE.getKey()) || this.checkQuartzJobExists(key);
        ArgumentResponseEnum.QUARTZ_JOB_SCHEDULER_ERR_JOB_REPEAT.assertIsFalse(scheduler, quartzJobIdentification);
        return quartzJob;
    }

    /**
     * 暂停定时任务校验
     *
     * @param quartzJobIdentification - 定时任务对象编号
     * @return SysQuartzJob - 定时任务对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/9 9:47
     */
    private SysQuartzJob pauseQuartzJobVerification(final Long quartzJobIdentification) {
        SysQuartzJob quartzJob = this.serviceBySysQuartzJobService.getById(quartzJobIdentification);
        // 1.校验定时任务存在
        ArgumentResponseEnum.QUARTZ_JOB_PAUSE_ERR_NON.assertNotNull(quartzJob);
        // 2.排除数据库中状态为 不存在、具有给定标识符的作业已不存在于计划程序中 的定时任务
        JobKey key = new JobKey(quartzJob.getJobName(), quartzJob.getJobGroup());
        boolean pause = quartzJob.getJobState().equals(QuartzJobStatusEnum.NONE.getKey()) || !this.checkQuartzJobExists(key);
        ArgumentResponseEnum.QUARTZ_JOB_PAUSE_ERR_STATE_ERR.assertIsFalse(pause, quartzJobIdentification);
        return quartzJob;
    }

    /**
     * 恢复定时任务校验
     *
     * @param quartzJobIdentification - 定时任务对象编号
     * @return SysQuartzJob - 定时任务对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/9 9:47
     */
    private SysQuartzJob resumeQuartzJobVerification(final Long quartzJobIdentification) {
        SysQuartzJob quartzJob = this.serviceBySysQuartzJobService.getById(quartzJobIdentification);
        // 1.校验定时任务存在
        ArgumentResponseEnum.QUARTZ_JOB_RESUME_ERR_JOB_NON.assertNotNull(quartzJob);
        // 2.数据库中状态为  暂停、出错、具有给定标识符的作业已不存在于计划程序中 的定时任务
        JobKey key = new JobKey(quartzJob.getJobName(), quartzJob.getJobGroup());
        boolean resume = quartzJob.getJobState().equals(QuartzJobStatusEnum.PAUSED.getKey())
                || quartzJob.getJobState().equals(QuartzJobStatusEnum.ERROR.getKey())
                || this.checkQuartzJobExists(key);
        ArgumentResponseEnum.QUARTZ_JOB_RESUME_ERR_JOB_STATE_ERR.assertIsTrue(resume, quartzJobIdentification);
        return quartzJob;
    }

    /**
     * 移除定时任务校验
     *
     * @param quartzJobIdentification - 定时任务对象编号
     * @return SysQuartzJob - 定时任务对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/9 9:47
     */
    private SysQuartzJob removeQuartzJobVerification(final Long quartzJobIdentification) {
        SysQuartzJob quartzJob = this.serviceBySysQuartzJobService.getById(quartzJobIdentification);
        // 1.校验定时任务存在
        ArgumentResponseEnum.QUARTZ_JOB_REMOVE_ERR_JOB_NON.assertNotNull(quartzJob);
        // 2.数据库中状态为  暂停 并且 具有给定标识符的作业已不存在于计划程序中 的定时任务
        JobKey key = new JobKey(quartzJob.getJobName(), quartzJob.getJobGroup());
        boolean remove = quartzJob.getJobState().equals(QuartzJobStatusEnum.PAUSED.getKey()) && this.checkQuartzJobExists(key);
        ArgumentResponseEnum.QUARTZ_JOB_REMOVE_ERR_STATE_ERR.assertIsFalse(remove, quartzJobIdentification);
        return quartzJob;
    }

    /**
     * 执行定时任务校验
     *
     * @param quartzJobIdentification - 定时任务对象编号
     * @return SysQuartzJob - 定时任务对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/9 9:47
     */
    private SysQuartzJob triggerQuartzJobVerification(final Long quartzJobIdentification) {
        SysQuartzJob quartzJob = this.serviceBySysQuartzJobService.getById(quartzJobIdentification);
        // 1.校验定时任务不存在
        ArgumentResponseEnum.QUARTZ_JOB_QUERY_ERR.assertNotNull(quartzJob);
        // 2.排除数据库中状态为 不存在、具有给定标识符的作业已不存在于计划程序中 的定时任务
        JobKey key = new JobKey(quartzJob.getJobName(), quartzJob.getJobGroup());
        boolean trigger = quartzJob.getJobState().equals(QuartzJobStatusEnum.NONE.getKey()) || !this.checkQuartzJobExists(key);
        ArgumentResponseEnum.QUARTZ_JOB_TRIGGER_ERR.assertIsTrue(trigger, quartzJob.getJobName());
        return quartzJob;
    }

    /**
     * 修改定时任务执行状态
     *
     * @param sysQuartzJob - 定时任务
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/23 10:10
     */
    private BaseResponse updateQuartzJobState(final SysQuartzJob sysQuartzJob) {
        try {
            // TriggerState = NONE, NORMAL, PAUSED, COMPLETE, ERROR, BLOCKED
            Trigger.TriggerState triggerState = this.scheduler.getTriggerState(TriggerKey.triggerKey(sysQuartzJob.getJobName(), sysQuartzJob.getJobGroup()));
            // 修改定时任务执行状态
            sysQuartzJob.setJobState(QuartzJobStatusEnum.valueOf(triggerState.name()).getKey());
            boolean update = this.serviceBySysQuartzJobService.updateById(sysQuartzJob);
            ArgumentResponseEnum.QUARTZ_JOB_UPDATE_ERR.assertIsTrue(update, sysQuartzJob.getJobName());
        } catch (SchedulerException e) {
            ArgumentResponseEnum.QUARTZ_JOB_UPDATE_ERR.assertFail(sysQuartzJob.getJobName());
        }
        return new CommonResponse();
    }

    /**
     * 分页查询定时任务
     *
     * @param pagingQueryQuartzJobDTO - 定时任务分页查询对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    public CommonResponse<Page<SysQuartzJobVO>> pagingQuerySysQuartzJobListByParameter(final PagingQueryQuartzJobDTO pagingQueryQuartzJobDTO) {
        Page<SysQuartzJobVO> sysQuartzJobListByParameter = this.serviceBySysQuartzJobService.pagingQuerySysQuartzJobListByParameter(pagingQueryQuartzJobDTO);
        return new CommonResponse<>(sysQuartzJobListByParameter);
    }

    /**
     * 定时任务添加
     *
     * @param sysQuartzJobAdd - 定时任务添加对象
     * @return null -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 9:59
     */
    public BaseResponse addQuartzJob(final SysQuartzJobAddRO sysQuartzJobAdd) {
        //  1.校验定时任务重复
        SysQuartzJob sysQuartzJob = this.quartzJobAddVerification(sysQuartzJobAdd);
        // 2.新创建定时任务，定时任务计划 内状态为不存在
        sysQuartzJob.setJobState(QuartzJobStatusEnum.NONE.getKey());
        boolean saveQuartzJob = this.serviceBySysQuartzJobService.saveQuartzJob(sysQuartzJob);
        // 3.判断保存成功
        ArgumentResponseEnum.QUARTZ_JOB_ADD_ERR.assertIsTrue(saveQuartzJob, sysQuartzJobAdd.getJobName());
        return new CommonResponse<>();
    }

    /**
     * 定时任务删除
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return null -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 9:59
     */
    public BaseResponse deleteQuartzJob(final Long quartzJobIdentification) {
        // 1.删除定时任务校验
        SysQuartzJob sysQuartzJob = this.quartzJobDeleteVerification(quartzJobIdentification);
        // 2.删除定时任务
        boolean deleteQuartzJob = this.serviceBySysQuartzJobService.deleteQuartzJob(quartzJobIdentification);
        // 3.判断删除成功
        ArgumentResponseEnum.QUARTZ_JOB_DELETE_ERR.assertIsTrue(deleteQuartzJob, quartzJobIdentification);
        return new CommonResponse<>();
    }

    /**
     * 定时任务查看
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 9:59
     */
    public CommonResponse<SysQuartzJobVO> queryQuartzJobById(final Long quartzJobIdentification) {
        SysQuartzJob sysQuartzJob = this.serviceBySysQuartzJobService.getById(quartzJobIdentification);
        ArgumentResponseEnum.QUARTZ_JOB_QUERY_ERR.assertNotNull(sysQuartzJob, quartzJobIdentification);
        return new CommonResponse<>(SysQuartzJobConverter.INSTANCE.getReturnValue(sysQuartzJob));
    }

    /**
     * 定时任务更新
     *
     * @param sysQuartzJobUpdate - 定时任务更新对象
     * @return null -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 9:59
     */
    public BaseResponse updateQuartzJob(final SysQuartzJobUpdateRO sysQuartzJobUpdate) {
        // 1.修改定时任务校验
        SysQuartzJob sysQuartzJob = this.quartzJobUpdateVerification(sysQuartzJobUpdate);
        // 2.定时任务修改
        boolean updateQuartzJob = this.serviceBySysQuartzJobService.updateByQuartzJobIdentification(sysQuartzJob);
        // 3.判断保存成功
        ArgumentResponseEnum.QUARTZ_JOB_UPDATE_ERR.assertIsTrue(updateQuartzJob, sysQuartzJobUpdate.getJobName());
        return new CommonResponse<>();
    }

    /**
     * 开启构建定时任务
     *
     * @param quartzJob - 定时任务
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/15 10:57
     */
    public BaseResponse schedulerQuartzJob(final SysQuartzJob quartzJob) {
        try {
            // 1.定时任务执行类类名
            Class aClass = Class.forName(quartzJob.getJobClassName());
            // 2.定时任务构建器
            JobBuilder jobBuilder = JobBuilder.newJob(aClass)
                    .withIdentity(quartzJob.getJobName(), quartzJob.getJobGroup())
                    .withDescription(quartzJob.getDescription());
            if (ConvertUtils.isNotEmpty(quartzJob.getJobDataMap())) {
                // 3.构建定时任务参数
                JobDataMap jobDataMap = JSONObject.parseObject(quartzJob.getJobDataMap(), JobDataMap.class);
                jobBuilder.setJobData(jobDataMap);
            }
            // 4.检验定时任务执行类是否存在
            JobDetail jobDetail = jobBuilder.build();
            // 5.1触发时间点
            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(quartzJob.getCronExpression().trim());
            // 5.2触发器生成
            Trigger trigger = TriggerBuilder.newTrigger().withIdentity(quartzJob.getJobName(), quartzJob.getJobGroup())
                    .startNow().withSchedule(cronScheduleBuilder).build();
            // 6.交由Scheduler安排触发
            this.scheduler.scheduleJob(jobDetail, trigger);
            // 7.修改定时任务状态
            return this.updateQuartzJobState(quartzJob);
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage());
            return new BaseResponse(CommonResponseEnum.ERROR_500);
        } catch (SchedulerException e) {
            // 修改定时任务状态
            this.updateQuartzJobState(quartzJob);
            log.error(e.getMessage());
            return new BaseResponse(CommonResponseEnum.ERROR_500);
        }
    }

    /**
     * 定时任务构建接口
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 16:28
     */
    public BaseResponse schedulerQuartzJobByQuartzJobIdentification(final Long quartzJobIdentification) {
        SysQuartzJob sysQuartzJob = this.schedulerQuartzJobVerification(quartzJobIdentification);
        return this.schedulerQuartzJob(sysQuartzJob);
    }

    /**
     * 定时任务执行接口
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 16:28
     */
    public BaseResponse triggerQuartzJobByQuartzJobIdentification(final Long quartzJobIdentification) {
        // 1.定时任务验证
        SysQuartzJob sysQuartzJob = this.triggerQuartzJobVerification(quartzJobIdentification);
        try {
            // 2.定时任务执行 执行一次，但是不更新状态
            JobKey key = new JobKey(sysQuartzJob.getJobName(), sysQuartzJob.getJobGroup());
            this.scheduler.triggerJob(key);
            return new CommonResponse<>();
        } catch (SchedulerException e) {
            // 修改定时任务状态
            this.updateQuartzJobState(sysQuartzJob);
            log.error(e.getMessage());
            return new BaseResponse(CommonResponseEnum.ERROR_500);
        }
    }

    /**
     * 定时任务暂停接口
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 16:28
     */
    public BaseResponse pauseQuartzJobByQuartzJobIdentification(final Long quartzJobIdentification) {
        SysQuartzJob sysQuartzJob = this.pauseQuartzJobVerification(quartzJobIdentification);
        try {
            // 3.定时任务暂停
            JobKey key = new JobKey(sysQuartzJob.getJobName(), sysQuartzJob.getJobGroup());
            this.scheduler.pauseJob(key);
            // 4.修改定时任务状态
            return this.updateQuartzJobState(sysQuartzJob);
        } catch (SchedulerException e) {
            // 修改定时任务状态
            this.updateQuartzJobState(sysQuartzJob);
            log.error(e.getMessage());
            return new BaseResponse(CommonResponseEnum.ERROR_500);
        }
    }

    /**
     * 定时任务恢复接口
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 16:28
     */
    public BaseResponse resumeQuartzJobByQuartzJobIdentification(final Long quartzJobIdentification) {
        // 1、恢复定时任务校验
        SysQuartzJob sysQuartzJob = this.resumeQuartzJobVerification(quartzJobIdentification);
        try {
            // 2.定时任务恢复
            JobKey key = new JobKey(sysQuartzJob.getJobName(), sysQuartzJob.getJobGroup());
            this.scheduler.resumeJob(key);
            // 3.修改定时任务状态
            return this.updateQuartzJobState(sysQuartzJob);
        } catch (SchedulerException e) {
            // 修改定时任务状态
            this.updateQuartzJobState(sysQuartzJob);
            log.error(e.getMessage());
            return new BaseResponse(CommonResponseEnum.ERROR_500);
        }
    }

    /**
     * 定时任务移除接口
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 16:28
     */
    public BaseResponse removeQuartzJobByQuartzJobIdentification(final Long quartzJobIdentification) {
        SysQuartzJob sysQuartzJob = this.removeQuartzJobVerification(quartzJobIdentification);
        ArgumentResponseEnum.QUARTZ_JOB_QUERY_ERR.assertNotNull(sysQuartzJob);
        try {
            JobKey key = new JobKey(sysQuartzJob.getJobName(), sysQuartzJob.getJobGroup());
            TriggerKey triggerKey = TriggerKey.triggerKey(sysQuartzJob.getJobName(), sysQuartzJob.getJobGroup());
            // 3.定时任务停止
            this.scheduler.pauseTrigger(triggerKey);
            // 4.移除触发器
            this.scheduler.unscheduleJob(triggerKey);
            // 5.删除任务
            this.scheduler.deleteJob(key);
            // 6.修改定时任务状态
            return this.updateQuartzJobState(sysQuartzJob);
        } catch (SchedulerException e) {
            // 修改定时任务状态
            this.updateQuartzJobState(sysQuartzJob);
            return new ErrorResponse(ArgumentResponseEnum.QUARTZ_JOB_REMOVE_ERR);
        }
    }

    /**
     * 获取定时任务状态
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return CommonResponse<String> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 16:28
     */
    public CommonResponse<String> getQuartzJobStateByQuartzJobIdentification(final Long quartzJobIdentification) {
        SysQuartzJob sysQuartzJob = this.serviceBySysQuartzJobService.getById(quartzJobIdentification);
        ArgumentResponseEnum.QUARTZ_JOB_QUERY_ERR.assertNotNull(sysQuartzJob);
        Trigger.TriggerState trigger = Trigger.TriggerState.NONE;
        if (sysQuartzJob != null) {
            TriggerKey triggerKey = TriggerKey.triggerKey(sysQuartzJob.getJobName(), sysQuartzJob.getJobGroup());
            try {
                // TriggerState = NONE, NORMAL, PAUSED, COMPLETE, ERROR, BLOCKED
                trigger = this.scheduler.getTriggerState(triggerKey);
            } catch (SchedulerException e) {
                ArgumentResponseEnum.QUARTZ_JOB_GET_STATE_ERR.assertFail(e);
            }
        }
        return new CommonResponse<>(trigger.toString());
    }

    /**
     * 定时任务启动初始化
     *
     * @return CommonResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/23 10:46
     */
    public BaseResponse loadSysJobToQuartz() {
        List<SysQuartzJob> sysQuartzJobList = this.serviceBySysQuartzJobService.list();
        for (SysQuartzJob sysQuartzJob : sysQuartzJobList) {
            // 开启构建定时任务
            schedulerQuartzJob(sysQuartzJob);
            log.debug(ArgumentResponseEnum.QUARTZ_JOB_TRIGGER_OK.getMessage(), sysQuartzJob.getJobName());
            if (QuartzJobStatusEnum.PAUSED.getKey().equals(sysQuartzJob.getJobState())) {
                // 暂停定时任务
                pauseQuartzJobByQuartzJobIdentification(sysQuartzJob.getId());
                log.debug(ArgumentResponseEnum.QUARTZ_JOB_PAUSE_OK.getMessage(), sysQuartzJob.getJobName());
            }
        }
        return new CommonResponse();
    }

}