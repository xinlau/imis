package com.imis.module.system.bus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DictCodeConstant;
import com.imis.base.constant.enums.ArgumentResponseEnum;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseBus;
import com.imis.module.system.model.converter.SysWebsocketMessageConverter;
import com.imis.module.system.model.po.SysRichText;
import com.imis.module.system.model.po.SysWebsocketMessage;
import com.imis.module.system.model.ro.PagingQueryWebsocketMessageDTO;
import com.imis.module.system.model.ro.SysWebsocketMessageAddRO;
import com.imis.module.system.model.vo.SysWebsocketMessageInfoVO;
import com.imis.module.system.model.vo.SysWebsocketMessageVO;
import com.imis.module.system.service.ISysDictService;
import com.imis.module.system.service.ISysRichTextService;
import com.imis.module.system.service.ISysWebsocketMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * WebSocket信息传递记录 业务处理类
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
@Slf4j
@Service
public class SysWebsocketMessageBus extends BaseBus {

    /**
     * WebSocket信息传递记录 服务类
     */
    private ISysWebsocketMessageService sysWebsocketMessageService;

    @Autowired
    public void setSysWebsocketMessageService(ISysWebsocketMessageService sysWebsocketMessageService) {
        this.sysWebsocketMessageService = sysWebsocketMessageService;
    }

    /**
     * 字典项 服务类
     */
    private ISysDictService sysDictService;

    @Autowired
    public void setSysDictService(ISysDictService sysDictService) {
        this.sysDictService = sysDictService;
    }

    /**
     * 富文本 服务类
     */
    private ISysRichTextService sysRichTextService;

    @Autowired
    public void setSysRichTextService(ISysRichTextService sysRichTextService) {
        this.sysRichTextService = sysRichTextService;
    }

    /**
     * 批量保存WebsocketMessages记录
     *
     * @param websocketMessageAddArrayList - WebSocket信息传递记录
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 17:32
     */
    private Boolean batchSaveWebsocketMessages(final List<SysWebsocketMessageAddRO> websocketMessageAddArrayList) {
        // 信息文本
        String content = websocketMessageAddArrayList.get(0).getContent();
        SysRichText sysRichText = new SysRichText();
        sysRichText.setContent(content);
        boolean saveRichText = this.sysRichTextService.save(sysRichText);
        ArgumentResponseEnum.WEBSOCKET_MESSAGE_SEND_ERR_RICH_TEXT_SAVE_ERR.assertIsTrue(saveRichText);
        List<SysWebsocketMessage> sysWebsocketMessageList = SysWebsocketMessageConverter.INSTANCE.getAddEntity(websocketMessageAddArrayList, sysRichText.getId());
        return this.sysWebsocketMessageService.saveBatchSysWebsocketMessage(sysWebsocketMessageList);
    }

    /**
     * 分页查询WebSocket信息接口
     *
     * @param pagingQueryWebsocketMessageDTO - WebSocket信息传递记录查询对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:39
     */
    public CommonResponse<Page<SysWebsocketMessageVO>> pagingQuerySysWebsocketListByParameter(final PagingQueryWebsocketMessageDTO pagingQueryWebsocketMessageDTO) {
        Page<SysWebsocketMessageVO> sysWebsocketMessageListByParameter = this.sysWebsocketMessageService.pagingQuerySysWebsocketListByParameter(pagingQueryWebsocketMessageDTO);
        return new CommonResponse(sysWebsocketMessageListByParameter);
    }

    /**
     * WebSocket信息查看接口
     *
     * @param websocketIdentification - WebSocket信息传递记录标识
     * @return null -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:48
     */
    public CommonResponse<SysWebsocketMessageInfoVO> queryWebsocketById(final Long websocketIdentification) {
        SysWebsocketMessageInfoVO sysWebsocketMessage = this.sysWebsocketMessageService.queryWebsocketById(websocketIdentification);
        ArgumentResponseEnum.WEBSOCKET_QUERY_ERR.assertNotNull(sysWebsocketMessage);
        String messageStateText = this.sysDictService.queryDictTextByKey(DictCodeConstant.MESSAGE_STATE, sysWebsocketMessage.getMessageState().toString());
        sysWebsocketMessage.setMessageStateText(messageStateText);
        return new CommonResponse<>(sysWebsocketMessage);
    }

    /**
     * 批量保存WebsocketMessages记录
     *
     * @param websocketMessageAddArrayList - WebsocketMessages记录数组
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 17:24
     */
    @Transactional(rollbackFor = Exception.class)
    public Boolean batchAddWebsocketMessages(final List<SysWebsocketMessageAddRO> websocketMessageAddArrayList) {
        return this.batchSaveWebsocketMessages(websocketMessageAddArrayList);
    }

}