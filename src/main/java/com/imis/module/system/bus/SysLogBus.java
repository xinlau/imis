package com.imis.module.system.bus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseBus;
import com.imis.module.system.model.ro.PagingQueryLogDTO;
import com.imis.module.system.model.vo.SysLogVO;
import com.imis.module.system.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * SysLogBus<br>
 * 日志相关功能业务处理类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月10日 09:18
 */
@Service
public class SysLogBus extends BaseBus {

    /**
     * 日志记录表 服务类
     */
    private ISysLogService serviceByLogService;

    @Autowired
    public void setServiceBySysLogService(ISysLogService serviceByLogService) {
        this.serviceByLogService = serviceByLogService;
    }

    /**
     * 分页查询系统日志
     *
     * @param pagingQueryLogDTO - 系统日志查询对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/10 15:32
     */
    public CommonResponse<Page<SysLogVO>> pagingQuerySysLogListByParameter(final PagingQueryLogDTO pagingQueryLogDTO) {
        Page<SysLogVO> sysLogSelectRoPage = this.serviceByLogService.pagingQuerySysLogListByParameter(pagingQueryLogDTO);
        return new CommonResponse<>(sysLogSelectRoPage);
    }

}
