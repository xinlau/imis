package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysWebsocketMessageBus;
import com.imis.module.system.model.ro.PagingQueryWebsocketMessageDTO;
import com.imis.module.system.model.vo.SysWebsocketMessageInfoVO;
import com.imis.module.system.model.vo.SysWebsocketMessageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * WebSocket信息传递记录 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
@Slf4j
@RestController
@RequestMapping(path = "/system/websocket")
@Api(tags = {"WebSocket信息传递记录"})
@ApiSort(8)
public class SysWebsocketMessageController extends BaseController<SysWebsocketMessageBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询WebSocket信息接口", notes = "分页查询WebSocket信息传递记录接口")
    @RequiresPermissions(PermissionsValueConstant.WEBSOCKET_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysWebsocketMessageVO>> pagingQuerySysWebsocketListByParameter(PagingQueryWebsocketMessageDTO pagingQueryWebsocketMessageDTO) {
        return service.pagingQuerySysWebsocketListByParameter(pagingQueryWebsocketMessageDTO);
    }

    @GetMapping(path = "/queryWebsocketById")
    @ApiOperation(value = "WebSocket信息查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.WEBSOCKET_QUERY_BY_ID)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "WebSocket信息标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public CommonResponse<SysWebsocketMessageInfoVO> queryWebsocketById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryWebsocketById(id);
    }

}