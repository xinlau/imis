package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysRoleBus;
import com.imis.module.system.model.ro.*;
import com.imis.module.system.model.vo.SysRoleVO;
import com.imis.module.system.model.vo.SysUserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 系统角色表-权限组 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/role")
@Api(tags = {"系统角色"})
@ApiSort(4)
public class SysRoleController extends BaseController<SysRoleBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询角色信息接口", notes = "分页查询角色信息接口")
    @RequiresPermissions(PermissionsValueConstant.ROLE_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysRoleVO>> pagingQuerySysRoleListByParameter(PagingQueryRoleDTO pagingQueryRoleDTO) {
        return service.pagingQuerySysRoleListByParameter(pagingQueryRoleDTO);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "角色添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.ROLE_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addRole(@RequestBody @Valid SysRoleAddRO sysRoleAdd) {
        return service.addRole(sysRoleAdd);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "角色删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.ROLE_PAGE, PermissionsValueConstant.ROLE_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "角色标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse deleteRole(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.deleteRole(id);
    }

    @GetMapping(path = "/queryRoleById")
    @ApiOperation(value = "角色查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.ROLE_QUERY_BY_ID)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "角色标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public CommonResponse<SysRoleVO> queryRoleById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryRoleById(id);
    }

    @GetMapping(path = "/pagingQueryUserByRoleId")
    @ApiOperation(value = "根据角色分页查询用户对象接口", notes = "根据角色分页查询用户对象")
    @RequiresPermissions(PermissionsValueConstant.USER_PAGE_BY_ROLE_ID)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<Page<SysUserVO>> pagingQueryUserByRoleId(PagingQueryUserByRoleIdDTO pagingQueryUserByRoleIdDTO) {
        return service.pagingQuerySysUserListByRoleIdParameter(pagingQueryUserByRoleIdDTO);
    }

    @GetMapping(path = "/pagingQueryUnauthorizedUser")
    @ApiOperation(value = "根据角色分页查询未授权用户对象接口", notes = "根据角色分页查询未授权用户对象")
    @RequiresPermissions(PermissionsValueConstant.UNAUTHORIZED_USER_PAGE_BY_ROLE_ID)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public CommonResponse<Page<SysUserVO>> pagingQueryUnauthorizedUserByRoleId(PagingQueryUserByRoleIdDTO pagingQueryUserByRoleIdDTO) {
        return service.pagingQueryUnauthorizedSysUserListByRoleIdParameter(pagingQueryUserByRoleIdDTO);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "角色修改接口", notes = "单条修改")
    @RequiresPermissions(PermissionsValueConstant.ROLE_UPDATE)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public BaseResponse updateRole(@RequestBody @Valid SysRoleUpdateRO sysRoleUpdate) {
        return service.updateRole(sysRoleUpdate);
    }

    @PostMapping(path = "/pagingQueryGrantRoleListByParameter")
    @ApiOperation(value = "分页查询已授权角色", notes = "根据用户ID分页查找已经授权的角色")
    @RequiresPermissions(PermissionsValueConstant.GRANT_ROLE_PAGE_BY_USER)
    @ApiOperationSupport(order = 8, author = "XinLau")
    public CommonResponse<Page<SysRoleVO>> pagingQueryGrantRoleListByParameter(@RequestBody PagingQueryUserRoleDTO sysUserRoleSelect) {
        return service.pagingQueryGrantRoleListByParameter(sysUserRoleSelect);
    }

    @PostMapping(path = "/pagingQueryNotGrantRoleListByParameter")
    @ApiOperation(value = "分页查询未授权角色", notes = "根据条件分页查询还未授权的角色")
    @RequiresPermissions(PermissionsValueConstant.UN_GRANT_ROLE_PAGE_BY_USER)
    @ApiOperationSupport(order = 9, author = "XinLau")
    public CommonResponse<Page<SysRoleVO>> pagingQueryNotGrantRoleListByParameter(@RequestBody PagingQueryUserRoleDTO sysUserRoleSelect) {
        return service.pagingQueryNotGrantRoleListByParameter(sysUserRoleSelect);
    }

    @PostMapping(path = "/grantMenuPermissionsToRole")
    @ApiOperation(value = "为角色授权功能菜单权限", notes = "为角色授权功能菜单权限")
    @RequiresPermissions(PermissionsValueConstant.GRANT_MENU_TO_ROLE)
    @ApiOperationSupport(order = 10, author = "XinLau")
    public BaseResponse grantMenuPermissionsToRole(@RequestBody @Valid SysGrantMenuPermissionToRoleRO sysGrantMenuPermissionToRole) {
        return service.grantMenuPermissionsToRole(sysGrantMenuPermissionToRole);
    }

    @GetMapping(value = "/terminateAllMenuPermissionsToRole")
    @ApiOperation(value = "撤销角色功能菜单权限", notes = "撤销角色所有功能菜单权限")
    @RequiresPermissions(PermissionsValueConstant.TERMINATE_ALL_MENU_TO_ROLE)
    @ApiImplicitParam(name = "roleId", value = "角色标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 11, author = "XinLau")
    public BaseResponse terminateAllMenuPermissionsToRole(@RequestParam(name = "roleId", required = true) Long roleId) {
        return service.terminateAllMenuPermissionsToRole(roleId);
    }

    @PostMapping(value = "/authorizingRoleToUsers")
    @ApiOperation(value = "角色授权用户", notes = "将角色授权给用户")
    @RequiresPermissions(PermissionsValueConstant.AUTHORIZING_ROLE_TO_USER)
    @ApiOperationSupport(order = 12, author = "XinLau")
    public BaseResponse authorizingRoleToUsers(@RequestBody @Valid SysGrantRoleToUsersRO sysGrantRoleToUsers) {
        return service.authorizingRoleToUsers(sysGrantRoleToUsers);
    }

    @PostMapping(path = "/unAuthorizingRoleToUsers")
    @ApiOperation(value = "取消角色授权用户", notes = "取消对用户的角色授权")
    @RequiresPermissions(PermissionsValueConstant.UN_AUTHORIZING_ROLE_TO_USER)
    @ApiOperationSupport(order = 13, author = "XinLau")
    public BaseResponse unAuthorizingRoleToUsers(@RequestBody @Valid SysUnGrantRoleToUsersRO sysUnGrantRoleToUsers) {
        return service.unAuthorizingRoleToUsers(sysUnGrantRoleToUsers);
    }

}