package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysQuartzJobBus;
import com.imis.module.system.model.ro.PagingQueryQuartzJobDTO;
import com.imis.module.system.model.ro.SysQuartzJobAddRO;
import com.imis.module.system.model.ro.SysQuartzJobUpdateRO;
import com.imis.module.system.model.vo.SysQuartzJobVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 定时任务 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/quartz")
@Api(tags = {"定时任务"})
@ApiSort(7)
public class SysQuartzJobController extends BaseController<SysQuartzJobBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询定时任务接口", notes = "分页查询定时任务接口")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysQuartzJobVO>> pagingQuerySysQuartzJobListByParameter(PagingQueryQuartzJobDTO pagingQueryQuartzJobDTO) {
        return service.pagingQuerySysQuartzJobListByParameter(pagingQueryQuartzJobDTO);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "定时任务添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addQuartzJob(@RequestBody @Valid SysQuartzJobAddRO sysQuartzJobAddRo) {
        return service.addQuartzJob(sysQuartzJobAddRo);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "定时任务删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.QUARTZ_PAGE, PermissionsValueConstant.QUARTZ_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "定时任务标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse deleteQuartzJob(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.deleteQuartzJob(id);
    }

    @RequestMapping(path = "/queryQuartzJobById", method = RequestMethod.GET)
    @ApiOperation(value = "定时任务查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_QUERY_BY_ID)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "定时任务标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public CommonResponse<?> queryQuartzJobById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryQuartzJobById(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "定时任务修改接口", notes = "单条修改")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_UPDATE)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public BaseResponse updateQuartzJob(@RequestBody @Valid SysQuartzJobUpdateRO sysQuartzJobUpdateRo) {
        return service.updateQuartzJob(sysQuartzJobUpdateRo);
    }

    @GetMapping(path = "/scheduler")
    @ApiOperation(value = "定时任务构建接口", notes = "单条构建")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_SCHEDULER)
    @ApiImplicitParam(name = "quartzJobIdentification", value = "定时任务标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public BaseResponse schedulerQuartzJob(@RequestParam(name = "quartzJobIdentification", required = true) Long quartzJobIdentification) {
        return service.schedulerQuartzJobByQuartzJobIdentification(quartzJobIdentification);
    }

    @GetMapping(path = "/trigger")
    @ApiOperation(value = "定时任务执行接口", notes = "单条执行")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_TRIGGER)
    @ApiImplicitParam(name = "quartzJobIdentification", value = "定时任务标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public BaseResponse triggerQuartzJob(@RequestParam(name = "quartzJobIdentification", required = true) Long quartzJobIdentification) {
        return service.triggerQuartzJobByQuartzJobIdentification(quartzJobIdentification);
    }

    @GetMapping(path = "/pause")
    @ApiOperation(value = "定时任务暂停接口", notes = "单条暂停")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_PAUSE)
    @ApiImplicitParam(name = "quartzJobIdentification", value = "定时任务标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 8, author = "XinLau")
    public BaseResponse pauseQuartzJob(@RequestParam(name = "quartzJobIdentification", required = true) Long quartzJobIdentification) {
        return service.pauseQuartzJobByQuartzJobIdentification(quartzJobIdentification);
    }

    @GetMapping(path = "/resume")
    @ApiOperation(value = "定时任务恢复接口", notes = "单条恢复")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_RESUME)
    @ApiImplicitParam(name = "quartzJobIdentification", value = "定时任务标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 9, author = "XinLau")
    public BaseResponse resumeQuartzJob(@RequestParam(name = "quartzJobIdentification", required = true) Long quartzJobIdentification) {
        return service.resumeQuartzJobByQuartzJobIdentification(quartzJobIdentification);
    }

    @GetMapping(path = "/remove")
    @ApiOperation(value = "定时任务移除接口", notes = "单条移除")
    @RequiresPermissions(PermissionsValueConstant.QUARTZ_REMOVE)
    @ApiImplicitParam(name = "quartzJobIdentification", value = "定时任务标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 10, author = "XinLau")
    public BaseResponse removeQuartzJob(@RequestParam(name = "quartzJobIdentification", required = true) Long quartzJobIdentification) {
        return service.removeQuartzJobByQuartzJobIdentification(quartzJobIdentification);
    }

    @GetMapping(path = "/getQuartzJobState")
    @ApiOperation(value = "获取定时任务状态", notes = "单条获取")
    @ApiImplicitParam(name = "quartzJobIdentification", value = "定时任务标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 11, author = "XinLau")
    public CommonResponse<String> getQuartzJobState(@RequestParam(name = "quartzJobIdentification", required = true) Long quartzJobIdentification) {
        return service.getQuartzJobStateByQuartzJobIdentification(quartzJobIdentification);
    }

}