package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysOrganizationBus;
import com.imis.module.system.model.ro.*;
import com.imis.module.system.model.vo.SysOrganizationTreeVO;
import com.imis.module.system.model.vo.SysOrganizationVO;
import com.imis.module.system.model.vo.SysUserOrganizationVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 组织机构 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-04-22
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/organization")
@Api(tags = {"组织机构"})
@ApiSort(6)
public class SysOrganizationController extends BaseController<SysOrganizationBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询组织机构接口", notes = "分页查询组织机构接口")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysOrganizationTreeVO>> pagingQuerySysOrganizationParentListByParameter(PagingQueryOrganizationDTO pagingQueryOrganizationDTO) {
        return service.pagingQuerySysOrganizationParentListByParameter(pagingQueryOrganizationDTO);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "组织机构添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addOrganization(@RequestBody @Valid SysOrganizationAddRO sysOrganizationAdd) {
        return service.addOrganization(sysOrganizationAdd);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "组织机构删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.ORGANIZATION_PAGE, PermissionsValueConstant.ORGANIZATION_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "组织机构标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse deleteOrganization(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.deleteOrganization(id);
    }

    @GetMapping(path = "/queryOrganizationById")
    @ApiOperation(value = "组织机构查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_QUERY_BY_ID)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "组织机构标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public CommonResponse<SysOrganizationVO> queryOrganizationById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryOrganizationById(id);
    }

    @GetMapping(path = "/tree")
    @ApiOperation(value = "查询组织机构树接口", notes = "查询组织机构树接口")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_TREE)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<List<SysOrganizationTreeVO>> querySysOrganizationTreeList() {
        return service.querySysOrganizationTreeList();
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "组织机构修改接口", notes = "单条修改")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_UPDATE)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public BaseResponse updateOrganization(@RequestBody @Valid SysOrganizationUpdateRO sysOrganizationUpdate) {
        return service.updateOrganization(sysOrganizationUpdate);
    }

    @GetMapping(path = "/getTreeByParentId")
    @ApiOperation(value = "根据父组织机构标识查询组织机构树接口", notes = "根据父组织机构标识查询组织机构树接口")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_QUERY_BY_PARENT_ID)
    @ApiImplicitParam(name = "parentId", value = "组织机构标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public CommonResponse<List<SysOrganizationTreeVO>> queryOrganizationTreeByParentId(@RequestParam(name = "parentId", required = true) Long parentId) {
        return service.queryOrganizationTreeByParentId(parentId);
    }

    @GetMapping(path = "/getOrganizationByUserIdentification")
    @ApiOperation(value = "根据用户标识查询用户拥有的组织机构接口", notes = "根据用户标识查询用户拥有的组织机构接口")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_QUERY_BY_USER_ID)
    @ApiImplicitParam(name = "userIdentification", value = "用户标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 8, author = "XinLau")
    public CommonResponse<List<SysUserOrganizationVO>> queryOrganizationByUserIdentification(@RequestParam(name = "userIdentification", required = true) Long userIdentification) {
        return service.queryOrganizationByUserIdentification(userIdentification);
    }

    @PostMapping(path = "/grantOrganizationToUser")
    @ApiOperation(value = "用户授权组织机构", notes = "单人多个组织机构授权")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_GRANT_TO_USER)
    @ApiOperationSupport(order = 9, author = "XinLau")
    public BaseResponse grantOrganizationToUser(@RequestBody @Valid SysGrantOrganizationToUserRO sysGrantOrganizationToUser) {
        return service.grantOrganizationToUser(sysGrantOrganizationToUser);
    }

    @GetMapping(path = "/terminateAllOrganizationToUser")
    @ApiOperation(value = "取消用户组织机构授权", notes = "为用户取消所有组织机构授权")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_TERMINATE_GRANT_TO_USER)
    @ApiImplicitParam(name = "userIdentification", value = "用户标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 10, author = "XinLau")
    public BaseResponse terminateAllOrganizationToUser(@RequestParam(name = "userIdentification", required = true) Long userIdentification) {
        return service.terminateAllOrganizationToUser(userIdentification);
    }

    @PostMapping(path = "/authorizingUserToOrganization")
    @ApiOperation(value = "组织机构分配用户", notes = "单个组织机构分配多个用户")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_AUTHORIZING_USER_TO_ORGANIZATION)
    @ApiOperationSupport(order = 11, author = "XinLau")
    public BaseResponse authorizingUserToOrganization(@RequestBody @Valid SysGrantUserToOrganizationRO sysGrantUserToOrganization) {
        return service.authorizingUserToOrganization(sysGrantUserToOrganization);
    }

    @PostMapping(path = "/unAuthorizingUserToOrganization")
    @ApiOperation(value = "组织机构取消分配用户", notes = "单个组织机构取消分配多个用户")
    @RequiresPermissions(PermissionsValueConstant.ORGANIZATION_UN_AUTHORIZING_USER_TO_ORGANIZATION)
    @ApiOperationSupport(order = 12, author = "XinLau")
    public BaseResponse unAuthorizingUserToOrganization(@RequestBody @Valid SysGrantUserToOrganizationRO sysGrantUserToOrganization) {
        return service.unAuthorizingUserToOrganization(sysGrantUserToOrganization);
    }


}