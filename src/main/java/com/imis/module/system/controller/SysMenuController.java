package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysMenuBus;
import com.imis.module.system.model.ro.PagingQueryMenuDTO;
import com.imis.module.system.model.ro.QueryMenuDTO;
import com.imis.module.system.model.ro.SysMenuAddRO;
import com.imis.module.system.model.ro.SysMenuUpdateRO;
import com.imis.module.system.model.vo.SysMenuTreeVO;
import com.imis.module.system.model.vo.SysMenuVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 功能菜单表 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-03-17
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/menu")
@Api(tags = {"功能菜单"})
@ApiSort(5)
public class SysMenuController extends BaseController<SysMenuBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询功能菜单接口", notes = "分页查询功能菜单接口")
    @RequiresPermissions(PermissionsValueConstant.MENU_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysMenuTreeVO>> pagingQuerySysMenuParentListByParameter(PagingQueryMenuDTO pagingQueryMenuDTO) {
        return service.pagingQuerySysMenuParentListByParameter(pagingQueryMenuDTO);
    }

    @GetMapping(path = "/tree")
    @ApiOperation(value = "查询功能菜单树接口", notes = "查询功能菜单树接口")
    @RequiresPermissions(PermissionsValueConstant.MENU_TREE)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public CommonResponse<List<SysMenuTreeVO>> querySysMenuTreeListByParameter(QueryMenuDTO queryMenuDTO) {
        return service.querySysMenuTreeListByParameter(queryMenuDTO);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "功能菜单添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.MENU_ADD)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse addMenu(@RequestBody @Valid SysMenuAddRO sysMenuAdd) {
        return service.addMenu(sysMenuAdd);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "功能菜单删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.MENU_PAGE, PermissionsValueConstant.MENU_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "功能菜单标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse deleteMenu(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.deleteMenu(id);
    }

    @GetMapping(path = "/queryMenuById")
    @ApiOperation(value = "功能菜单查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.MENU_QUERY_BY_ID)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "功能菜单标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<SysMenuVO> queryMenuById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryMenuById(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "功能菜单修改接口", notes = "单条修改")
    @RequiresPermissions(PermissionsValueConstant.MENU_UPDATE)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public BaseResponse updateMenu(@RequestBody @Valid SysMenuUpdateRO sysMenuUpdate) {
        return service.updateMenu(sysMenuUpdate);
    }

    @GetMapping(path = "/queryMenuByRoleId")
    @ApiOperation(value = "查看角色拥有功能菜单接口", notes = "根据角色查看功能菜单")
    @RequiresPermissions(PermissionsValueConstant.MENU_QUERY_BY_ROLE_ID)
    @ApiImplicitParam(name = "roleId", value = "角色标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public CommonResponse<List<SysMenuTreeVO>> queryMenuByRoleId(@RequestParam(name = "roleId", required = true) Long roleId) {
        return service.querySysMenuTreeListByRoleId(roleId);
    }

}