package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.api.model.ro.UserRegisterDTO;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysUserBus;
import com.imis.module.system.model.ro.*;
import com.imis.module.system.model.vo.SysUserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * SysUserController<br>
 * 用户信息模块
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月10日 09:14
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/user")
@Api(tags = {"用户管理"})
@ApiSort(3)
public class SysUserController extends BaseController<SysUserBus> {


    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询用户信息接口", notes = "分页查询用户信息接口")
    @RequiresPermissions(PermissionsValueConstant.USER_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysUserVO>> pagingQuerySysUserListByParameter(PagingQueryUserDTO pagingQueryUserDTO){
        return service.pagingQuerySysUserListByParameter(pagingQueryUserDTO);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "用户添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.USER_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addUser(@RequestBody @Valid UserRegisterDTO sysRegister) {
        return service.addUser(sysRegister);
    }

    @RequestMapping(path = "/freeze", method = RequestMethod.GET)
    @ApiOperation(value = "用户冻结接口", notes = "单条冻结")
    @RequiresPermissions(value = {PermissionsValueConstant.USER_PAGE, PermissionsValueConstant.USER_FREEZE}, logical = Logical.AND)
    @ApiImplicitParam(name = "identification", value = "用户标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse freezeUserByIdentification(@RequestParam(name = "identification", required = true) Long identification) {
        return service.freezeUserByIdentification(identification);
    }

    @GetMapping(value = "/unFreeze")
    @ApiOperation(value = "用户解冻接口", notes = "单条解冻")
    @RequiresPermissions(value = {PermissionsValueConstant.USER_PAGE, PermissionsValueConstant.USER_UN_FREEZE}, logical = Logical.AND)
    @ApiImplicitParam(name = "identification", value = "用户标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse unFreezeUserByIdentification(@RequestParam(name = "identification", required = true) Long identification) {
        return service.unFreezeUserByIdentification(identification);
    }

    @GetMapping(path = "/queryUserById")
    @ApiOperation(value = "用户查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.USER_QUERY_BY_ID)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "用户标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<SysUserVO> queryUserById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryUserById(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "用户信息更新接口", notes = "单条更新")
    @RequiresPermissions(PermissionsValueConstant.USER_UPDATE)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public BaseResponse updateById(@RequestBody @Valid SysUserUpdateRO sysUserUpdateRo) {
        return service.updateById(sysUserUpdateRo);
    }

    @PostMapping(path = "/grantRoleToUser")
    @ApiOperation(value = "用户授权角色", notes = "单人多个角色授权")
    @RequiresPermissions(value = {PermissionsValueConstant.USER_PAGE, PermissionsValueConstant.USER_GRANT_ROLE_TO_USER}, logical = Logical.AND)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public BaseResponse grantRoleToUser(@RequestBody @Valid SysGrantRolesToUserRO sysGrantUserToRole) {
        return service.grantRoleToUser(sysGrantUserToRole);
    }

    @GetMapping(path = "/terminateAllRoleToUser")
    @ApiOperation(value = "取消用户角色授权", notes = "为用户取消所有角色授权")
    @RequiresPermissions(value = {PermissionsValueConstant.USER_PAGE, PermissionsValueConstant.USER_TERMINATE_ALL_ROLE_TO_USER}, logical = Logical.AND)
    @ApiImplicitParam(name = "userId", value = "用户标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 8, author = "XinLau")
    public BaseResponse terminateAllRoleToUser(@RequestParam(name = "userId", required = true) Long userId) {
        return service.terminateAllRoleToUser(userId);
    }

    @PostMapping(path = "/changeUserPassword")
    @ApiOperation(value = "修改密码", notes = "为用户修改密码")
    @RequiresPermissions(value = {PermissionsValueConstant.USER_PAGE, PermissionsValueConstant.USER_CHANGE_USER_PASSWORD}, logical = Logical.AND)
    @ApiOperationSupport(order = 9, author = "XinLau")
    public BaseResponse changeUserPassword(@RequestBody @Valid SysChangeUserPasswordRO sysUserChangePassword) {
        return service.changeUserPassword(sysUserChangePassword);
    }

    @GetMapping(path = "/pagingQueryUserByOrganizationIdentification")
    @ApiOperation(value = "查询组织机构下的用户接口", notes = "根据组织机构标识查询组织机构下的用户接口")
    @RequiresPermissions(PermissionsValueConstant.USER_PAGE_USER_BY_ORGANIZATION_ID)
    @ApiOperationSupport(order = 10, author = "XinLau")
    public CommonResponse<Page<SysUserVO>> pagingQueryUserByOrganizationIdentification(PagingQueryUserByOrganizationIdDTO pagingQueryUserByOrganizationIdDTO) {
        return service.pagingQueryUserByOrganizationIdentification(pagingQueryUserByOrganizationIdDTO);
    }

    @GetMapping(path = "/pagingQueryUnauthorizedUser")
    @ApiOperation(value = "查询组织机构下没有的用户接口", notes = "根据组织机构标识查询组织机构下没有的用户接口")
    @RequiresPermissions(PermissionsValueConstant.USER_PAGE_UNAUTHORIZED_USER_BY_ORGANIZATION_ID)
    @ApiOperationSupport(order = 11, author = "XinLau")
    public CommonResponse<Page<SysUserVO>> pagingQueryUnauthorizedUser(PagingQueryUserByOrganizationIdDTO pagingQueryUserByOrganizationIdDTO) {
        return service.pagingQueryUnauthorizedUser(pagingQueryUserByOrganizationIdDTO);
    }

}
