package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysLogBus;
import com.imis.module.system.model.ro.PagingQueryLogDTO;
import com.imis.module.system.model.vo.SysLogVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * SysLogController<br>
 * 系统日志查看
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月10日 09:14
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/log")
@Api(tags = {"系统日志"})
@ApiSort(1)
public class SysLogController extends BaseController<SysLogBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询系统日志接口", notes = "分页查询系统日志接口")
    @RequiresPermissions(PermissionsValueConstant.LOG_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysLogVO>> pagingQuerySysLogListByParameter(PagingQueryLogDTO pagingQueryLogDTO) {
        return service.pagingQuerySysLogListByParameter(pagingQueryLogDTO);
    }

}
