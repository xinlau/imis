package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysDataSourceBus;
import com.imis.module.system.model.ro.PagingQueryDataSourceDTO;
import com.imis.module.system.model.ro.SysDataSourceAddRO;
import com.imis.module.system.model.ro.SysDataSourceUpdateRO;
import com.imis.module.system.model.vo.SysDataSourceVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 系统数据源管理 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-12-30
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/datasource")
@Api(tags = {"系统数据源管理"})
@ApiSort(9)
public class SysDataSourceController extends BaseController<SysDataSourceBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询数据源信息接口", notes = "分页查询数据源信息接口")
    @RequiresPermissions(PermissionsValueConstant.DATASOURCE_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysDataSourceVO>> pagingQueryDataSourceListByParameter(PagingQueryDataSourceDTO pagingQueryDataSource){
        return service.pagingQueryDataSourceListByParameter(pagingQueryDataSource);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "数据源添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.DATASOURCE_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addDataSource(@RequestBody @Valid SysDataSourceAddRO sysDataSourceAdd) {
        return service.addDataSource(sysDataSourceAdd);
    }

    @RequestMapping(path = "/freeze", method = RequestMethod.GET)
    @ApiOperation(value = "数据源冻结接口", notes = "单条冻结")
    @RequiresPermissions(value = {PermissionsValueConstant.DATASOURCE_PAGE, PermissionsValueConstant.DATASOURCE_FREEZE}, logical = Logical.AND)
    @ApiImplicitParam(name = "identification", value = "数据源标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse freezeDataSourceByIdentification(@RequestParam(name = "identification", required = true) Long identification) {
        return service.freezeDataSourceByIdentification(identification);
    }

    @GetMapping(value = "/unFreeze")
    @ApiOperation(value = "数据源解冻接口", notes = "单条解冻")
    @RequiresPermissions(value = {PermissionsValueConstant.DATASOURCE_PAGE, PermissionsValueConstant.DATASOURCE_UN_FREEZE}, logical = Logical.AND)
    @ApiImplicitParam(name = "identification", value = "数据源标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse unFreezeDataSourceByIdentification(@RequestParam(name = "identification", required = true) Long identification) {
        return service.unFreezeDataSourceByIdentification(identification);
    }

    @GetMapping(path = "/queryUserById")
    @ApiOperation(value = "数据源查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.DATASOURCE_QUERY_BY_ID)
    @ApiImplicitParam(name = "id", value = "数据源标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<SysDataSourceVO> queryDataSourceById(@RequestParam(name = "id", required = true) Long id) {
        return service.queryDataSourceById(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "数据源信息更新接口", notes = "单条更新")
    @RequiresPermissions(PermissionsValueConstant.DATASOURCE_UPDATE)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public BaseResponse updateById(@RequestBody @Valid SysDataSourceUpdateRO sysDataSourceUpdate) {
        return service.updateById(sysDataSourceUpdate);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "数据源删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.DATASOURCE_PAGE, PermissionsValueConstant.DATASOURCE_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = "id", value = "数据源标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public BaseResponse deleteDataSource(@RequestParam(name = "id", required = true) Long id) {
        return service.deleteDataSource(id);
    }

    @GetMapping(path = "/decryption")
    @ApiOperation(value = "数据源加密串解密接口", notes = "数据源加密串解密")
    @RequiresPermissions(value = {PermissionsValueConstant.DATASOURCE_PAGE, PermissionsValueConstant.DATASOURCE_DECRYPTION}, logical = Logical.AND)
    @ApiImplicitParam(name = "encryptionString", value = "数据源加密串", dataType = "String", dataTypeClass = String.class, required = true)
    @ApiOperationSupport(order = 8, author = "XinLau")
    public CommonResponse<String> decryptionDataSourceEncryptionString(@RequestParam(name = "id", required = true) String encryptionString) {
        return service.decryptionDataSourceEncryptionString(encryptionString);
    }

}