package com.imis.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.bus.SysDictBus;
import com.imis.module.system.model.ro.*;
import com.imis.module.system.model.vo.SysDictItemVO;
import com.imis.module.system.model.vo.SysDictVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 字典管理前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-03-19
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/dict")
@Api(tags = {"字典管理"})
@ApiSort(2)
public class SysDictController extends BaseController<SysDictBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询字典项接口", notes = "分页查询字典项接口")
    @RequiresPermissions(PermissionsValueConstant.DICT_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<SysDictVO>> pagingQuerySysDictListByParameter(PagingQueryDictDTO pagingQueryDictDTO) {
        return service.pagingQuerySysDictListByParameter(pagingQueryDictDTO);
    }

    @PostMapping(path = "/addDict")
    @ApiOperation(value = "字典项添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.DICT_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addDict(@RequestBody @Valid SysDictAddRO sysDictAdd) {
        return service.addDict(sysDictAdd);
    }

    @GetMapping(path = "/deleteDict")
    @ApiOperation(value = "字典项删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.DICT_PAGE, PermissionsValueConstant.DICT_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "字典项标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse deleteDict(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.deleteDict(id);
    }

    @GetMapping(path = "/queryDictById")
    @ApiOperation(value = "字典值查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.DICT_QUERY_BY_ID)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "字典项标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse queryDictById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryDictById(id);
    }

    @PutMapping(path = "/updateDict")
    @ApiOperation(value = "字典项修改接口", notes = "单条修改")
    @RequiresPermissions(PermissionsValueConstant.DICT_UPDATE)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public BaseResponse updateDict(@RequestBody @Valid SysDictUpdateRO sysDictUpdate) {
        return service.updateDict(sysDictUpdate);
    }

    @GetMapping(path = "/getDictItemByDict")
    @ApiOperation(value = "根据字典项获取字典值", notes = "获取字典值")
    @RequiresPermissions(PermissionsValueConstant.DICT_ITEM_QUERY_BY_DICT)
    @ApiImplicitParam(name = "dictIdentification", value = "字典项标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public CommonResponse<List<SysDictItemVO>> queryDictItemByDict(@RequestParam(name = "dictIdentification", required = true) Long dictIdentification) {
        return service.queryDictItemByDict(dictIdentification);
    }

    @GetMapping(path = "/pagingQueryDictItem")
    @ApiOperation(value = "分页查询字典值", notes = "分页查询字典值")
    @RequiresPermissions(PermissionsValueConstant.DICT_ITEM_PAGE)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public CommonResponse<Page<SysDictItemVO>> pagingQuerySysDictItemListByParameter(PagingQueryDictItemDTO pagingQueryDictItemDTO) {
        return service.pagingQuerySysDictItemListByParameter(pagingQueryDictItemDTO);
    }

    @PostMapping(path = "/addDictItem")
    @ApiOperation(value = "字典值添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.DICT_ITEM_ADD)
    @ApiOperationSupport(order = 8, author = "XinLau")
    public BaseResponse addDictItem(@RequestBody @Valid SysDictItemAddRO sysDictItemAdd) {
        return service.addDictItem(sysDictItemAdd);
    }

    @GetMapping(path = "/deleteDictItem")
    @ApiOperation(value = "字典值删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.DICT_ITEM_PAGE, PermissionsValueConstant.DICT_ITEM_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "字典项标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 9, author = "XinLau")
    public BaseResponse deleteDictItem(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.deleteDictItem(id);
    }

    @GetMapping(path = "/queryDictItemById")
    @ApiOperation(value = "字典值查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.DICT_ITEM_QUERY_BY_ID)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "字典项标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 10, author = "XinLau")
    public CommonResponse<SysDictItemVO> queryDictItemById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryDictItemById(id);
    }

    @PutMapping(path = "/updateDictItem")
    @ApiOperation(value = "字典值修改接口", notes = "单条修改")
    @RequiresPermissions(PermissionsValueConstant.DICT_ITEM_UPDATE)
    @ApiOperationSupport(order = 11, author = "XinLau")
    public BaseResponse updateDictItem(@RequestBody @Valid SysDictItemUpdateRO sysDictItemUpdate) {
        return service.updateDictItem(sysDictItemUpdate);
    }

}