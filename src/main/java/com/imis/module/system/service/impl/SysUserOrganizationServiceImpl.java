package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.system.model.converter.SysOrganizationConverter;
import com.imis.module.system.model.converter.SysUserConverter;
import com.imis.module.system.model.po.SysUser;
import com.imis.module.system.model.po.SysUserOrganization;
import com.imis.module.system.dao.SysUserOrganizationMapper;
import com.imis.module.system.model.ro.PagingQueryUserByOrganizationIdDTO;
import com.imis.module.system.model.ro.SysGrantUserToOrganizationRO;
import com.imis.module.system.model.vo.SysUserVO;
import com.imis.module.system.service.ISysUserOrganizationService;
import com.imis.module.base.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * <p>
 * 用户-组织机构关联表 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysUserOrganizationServiceImpl extends BaseServiceImpl<SysUserOrganizationMapper, SysUserOrganization> implements ISysUserOrganizationService {

    /**
     * 查询组织机构下的用户接口
     *
     * @param pagingQueryUserByOrganizationIdDTO - 根据组织机构查询用户对象
     * @return Page<SysUserVO> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/18 16:23
     */
    @Override
    public Page<SysUserVO> pagingQueryUserByOrganizationIdentification(final PagingQueryUserByOrganizationIdDTO pagingQueryUserByOrganizationIdDTO) {
        // 1.页码、页长
        Page<SysUser> sysUserPage = new Page<>(pagingQueryUserByOrganizationIdDTO.getPageNumber(), pagingQueryUserByOrganizationIdDTO.getPageSize());
        // 2.排序字段
        sysUserPage.addOrder(OrderItem.descs("su.create_time", "su.update_time"));
        // 3.条件分页查询
        sysUserPage = mapper.pagingQueryUserByOrganizationIdentification(sysUserPage, pagingQueryUserByOrganizationIdDTO);
        // 4.PO分页对象转VO实体分页对象
        return SysUserConverter.INSTANCE.getReturnValue(sysUserPage);
    }

    /**
     * 查询组织机构下没有的用户接口
     *
     * @param pagingQueryUserByOrganizationIdDTO - 根据组织机构查询用户对象
     * @return Page<SysUserVO> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/18 16:23
     */
    @Override
    public Page<SysUserVO> pagingQueryUnauthorizedUser(final PagingQueryUserByOrganizationIdDTO pagingQueryUserByOrganizationIdDTO) {
        // 1.页码、页长
        Page<SysUser> sysUserPage = new Page<>(pagingQueryUserByOrganizationIdDTO.getPageNumber(), pagingQueryUserByOrganizationIdDTO.getPageSize());
        // 2.排序字段
        sysUserPage.addOrder(OrderItem.descs("su.create_time", "su.update_time"));
        // 3.条件分页查询
        sysUserPage = mapper.pagingQueryUnauthorizedUser(sysUserPage, pagingQueryUserByOrganizationIdDTO);
        // 4.PO分页对象转VO实体分页对象
        return SysUserConverter.INSTANCE.getReturnValue(sysUserPage);
    }

    /**
     * 组织机构分配用户
     *
     * @param sysGrantUserToOrganization - 组织机构分配用户
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/20 9:45
     */
    @Override
    public Boolean authorizingUserToOrganization(final SysGrantUserToOrganizationRO sysGrantUserToOrganization) {
        // 对象转换
        List<SysUserOrganization> sysUserOrganizationList = SysOrganizationConverter.INSTANCE.grantUserToOrganization(sysGrantUserToOrganization);
        // 2.授权处理
        int authorizing = mapper.authorizingUserToOrganization(sysUserOrganizationList);
        return authorizing > 0;
    }

    /**
     * 组织机构取消分配用户
     *
     * @param sysGrantUserToOrganization - 组织机构分配用户
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/20 9:45
     */
    @Override
    public Boolean unAuthorizingUserToOrganization(final SysGrantUserToOrganizationRO sysGrantUserToOrganization) {
        // 对象转换
        List<SysUserOrganization> sysUserOrganizationList = SysOrganizationConverter.INSTANCE.grantUserToOrganization(sysGrantUserToOrganization);
        // 2.授权处理
        int unAuthorizing = mapper.unAuthorizingUserToOrganization(sysUserOrganizationList);
        return unAuthorizing > 0;
    }

}