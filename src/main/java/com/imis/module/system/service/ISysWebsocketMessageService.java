package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysWebsocketMessage;
import com.imis.module.system.model.ro.PagingQueryWebsocketMessageDTO;
import com.imis.module.system.model.vo.SysWebsocketMessageInfoVO;
import com.imis.module.system.model.vo.SysWebsocketMessageVO;
import com.imis.module.websocket.dto.PagingQueryMessageDTO;

import java.util.List;

/**
 * <p>
 * WebSocket信息传递记录 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
public interface ISysWebsocketMessageService extends BaseService<SysWebsocketMessage> {

    /**
     * 分页查询WebSocket信息接口
     *
     * @param pagingQueryWebsocketMessageDTO - WebSocket信息传递记录查询对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:39
     */
    Page<SysWebsocketMessageVO> pagingQuerySysWebsocketListByParameter(final PagingQueryWebsocketMessageDTO pagingQueryWebsocketMessageDTO);

    /**
     * WebSocket信息查看接口
     *
     * @param websocketIdentification - WebSocket信息传递记录标识
     * @return SysWebsocketMessageInfoVO -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:48
     */
    SysWebsocketMessageInfoVO queryWebsocketById(final Long websocketIdentification);

    /**
     * 分页查询WebSocket历史信息接口
     *
     * @param pagingQueryMessageDTO - 分页WebSocket信息对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:39
     */
    Page<SysWebsocketMessageInfoVO> pagingQueryHistoricalMassages(final PagingQueryMessageDTO pagingQueryMessageDTO);

    /**
     * 批量保存WebSocket信息
     *
     * @param sysWebsocketMessageList - WebSocket信息
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/25 11:10
     */
    Boolean saveBatchSysWebsocketMessage(final List<SysWebsocketMessage> sysWebsocketMessageList);

}