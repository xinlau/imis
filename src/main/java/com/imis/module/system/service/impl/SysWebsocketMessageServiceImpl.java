package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.system.dao.SysWebsocketMessageMapper;
import com.imis.module.system.model.converter.SysWebsocketMessageConverter;
import com.imis.module.system.model.po.SysWebsocketMessage;
import com.imis.module.system.model.ro.PagingQueryWebsocketMessageDTO;
import com.imis.module.system.model.vo.SysWebsocketMessageInfoVO;
import com.imis.module.system.model.vo.SysWebsocketMessageVO;
import com.imis.module.system.service.ISysWebsocketMessageService;
import com.imis.module.websocket.dto.PagingQueryMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * WebSocket信息传递记录 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysWebsocketMessageServiceImpl extends BaseServiceImpl<SysWebsocketMessageMapper, SysWebsocketMessage> implements ISysWebsocketMessageService {

    /**
     * 分页查询WebSocket信息接口
     *
     * @param pagingQueryWebsocketMessageDTO - WebSocket信息传递记录查询对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:39
     */
    @Override
    public Page<SysWebsocketMessageVO> pagingQuerySysWebsocketListByParameter(final PagingQueryWebsocketMessageDTO pagingQueryWebsocketMessageDTO) {
        // 1.页码、页长
        Page<SysWebsocketMessage> sysWebsocketMessagePage = new Page<>(pagingQueryWebsocketMessageDTO.getPageNumber(), pagingQueryWebsocketMessageDTO.getPageSize());
        // 2.排序字段
        sysWebsocketMessagePage.addOrder(OrderItem.desc(DataBaseConstant.CREATE_TIME_FIELD), OrderItem.desc(DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        sysWebsocketMessagePage = mapper.pagingQuerySysWebsocketListByParameter(sysWebsocketMessagePage, pagingQueryWebsocketMessageDTO);
        // 4.PO分页对象转VO实体分页对象
        Page<SysWebsocketMessageVO> sysQuartzJobPage = SysWebsocketMessageConverter.INSTANCE.getReturnValue(sysWebsocketMessagePage);
        // 5.返回值
        return sysQuartzJobPage;
    }

    /**
     * WebSocket信息查看接口
     *
     * @param websocketIdentification - WebSocket信息传递记录标识
     * @return SysWebsocketMessageInfoVO -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:48
     */
    @Override
    public SysWebsocketMessageInfoVO queryWebsocketById(final Long websocketIdentification) {
        SysWebsocketMessageInfoVO sysWebsocketMessageInfoVO = mapper.queryWebsocketById(websocketIdentification);
        return sysWebsocketMessageInfoVO;
    }

    /**
     * 分页查询WebSocket历史信息接口
     *
     * @param pagingQueryMessageDTO - 分页WebSocket信息对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:39
     */
    @Override
    public Page<SysWebsocketMessageInfoVO> pagingQueryHistoricalMassages(final PagingQueryMessageDTO pagingQueryMessageDTO) {
        // 1.页码、页长
        Page<SysWebsocketMessageInfoVO> sysWebsocketMessagePage = new Page<>(pagingQueryMessageDTO.getPageNumber(), pagingQueryMessageDTO.getPageSize());
        // 2.排序字段
        sysWebsocketMessagePage.addOrder(OrderItem.desc("swm.create_time"), OrderItem.desc("swm.update_time"));
        // 3.条件分页查询
        sysWebsocketMessagePage = mapper.pagingQueryHistoricalMassages(sysWebsocketMessagePage, pagingQueryMessageDTO);
        return sysWebsocketMessagePage;
    }

    /**
     * 批量保存WebSocket信息
     *
     * @param sysWebsocketMessageList - WebSocket信息
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/25 11:10
     */
    @Override
    public Boolean saveBatchSysWebsocketMessage(final List<SysWebsocketMessage> sysWebsocketMessageList) {
        int insertBatch = mapper.saveBatchSysWebsocketMessage(sysWebsocketMessageList);
        return insertBatch == sysWebsocketMessageList.size();
    }

}