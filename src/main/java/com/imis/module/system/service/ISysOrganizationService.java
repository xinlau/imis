package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysOrganization;
import com.imis.module.system.model.ro.PagingQueryOrganizationDTO;
import com.imis.module.system.model.ro.SysGrantOrganizationToUserRO;
import com.imis.module.system.model.vo.DropDownBoxTreeVO;
import com.imis.module.system.model.vo.SysOrganizationTreeVO;
import com.imis.module.system.model.vo.SysOrganizationVO;
import com.imis.module.system.model.vo.SysUserOrganizationVO;

import java.util.List;

/**
 * <p>
 * 组织机构 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-04-22
 */
public interface ISysOrganizationService extends BaseService<SysOrganization> {

    /**
     * 分页查询组织机构
     *
     * @param pagingQueryOrganizationDTO - 组织机构查询对象
     * @return List<SysOrganizationTreeVO> - 分页组织机构
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    Page<SysOrganizationTreeVO> pagingQuerySysOrganizationParentListByParameter(final PagingQueryOrganizationDTO pagingQueryOrganizationDTO);

    /**
     * 根据组织机构名查询组织机构
     *
     * @param organizationName - 组织机构名称
     * @return SysMenu - 组织机构
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    SysOrganization queryOrganizationByName(final String organizationName);

    /**
     * 根据组织机构标识查询用户编号
     *
     * @param organizationIdentification - 组织机构标识
     * @return Long[] 用户编号
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Long[] queryUserIdentificationByOrganizationIdentification(final Long organizationIdentification);

    /**
     * 根据父级组织机构编号查询子级组织机构
     *
     * @param parentId - 父级组织机构标识
     * @return List<SysMenuVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<SysOrganizationVO> selectSysOrganizationListByParentId(final Long parentId);

    /**
     * 根据组织机构编号逻辑删除组织机构
     *
     * @param organizationIdentification - 父级组织机构标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Boolean deleteOrganizationById(final Long organizationIdentification);

    /**
     * 根据组织机构编号修改组织机构信息
     *
     * @param sysOrganization - 组织机构修改对象
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Boolean updateByOrganizationIdentification(final SysOrganization sysOrganization);

    /**
     * 查询功能菜单
     *
     * @return List<SysMenuTreeVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    List<SysOrganization> querySysOrganizationList();

    /**
     * 查询组织机构树
     *
     * @return List<SysOrganizationTreeVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    List<SysOrganizationTreeVO> querySysOrganizationTreeList();

    /**
     * 根据父组织机构查询组织机构树
     *
     * @param parentSysOrganization - 父组织机构
     * @return List<SysOrganizationTreeVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    List<SysOrganizationTreeVO> queryOrganizationTreeByParent(final SysOrganization parentSysOrganization);

    /**
     * 根据用户ID查询用户拥有的组织机构
     *
     * @param userId - 用户ID
     * @return List<SysUserOrganizationVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/6 14:54
     */
    List<SysUserOrganizationVO> queryOrganizationByUserIdentification(final Long userId);

    /**
     * 用户授权组织机构
     *
     * @param sysGrantOrganizationToUser - 用户授权组织机构
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    Boolean grantOrganizationToUser(final SysGrantOrganizationToUserRO sysGrantOrganizationToUser);

    /**
     * 撤销用户组织机构授权
     *
     * @param userId - 用户编号
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/20 14:22
     */
    Boolean terminateAllOrganizationToUser(final Long userId);

    /**
     * 根据组织机构名称获取下拉
     *
     * @param organizationName - 组织机构名称
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    List<DropDownBoxTreeVO> queryOrganizationTreeByOrganizationName(final String organizationName);

}