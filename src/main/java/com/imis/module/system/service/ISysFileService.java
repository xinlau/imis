package com.imis.module.system.service;


import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysFile;
import com.imis.module.system.model.ro.DuplicateCheckDTO;

import java.util.List;

/**
 * <p>
 * 文件存放 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
public interface ISysFileService extends BaseService<SysFile> {

    /**
     * 数据重复校验
     *
     * @param duplicateCheck - 重复校验数据模型
     * @return Boolean -
     * @author XinLau
     * @since 2020/6/29 18:03
     * @creed The only constant is change ! ! !
     */
    Boolean doDuplicateCheck(final DuplicateCheckDTO duplicateCheck);

    /**
     * 批量保存文件
     *
     * @param sysFileList - 文件
     * @return Boolean -
     * @author XinLau
     * @since 2020/6/29 18:03
     * @creed The only constant is change ! ! !
     */
    Boolean saveBatchFile(final List<SysFile> sysFileList);

}