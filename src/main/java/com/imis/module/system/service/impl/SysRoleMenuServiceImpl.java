package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.system.model.po.SysRoleMenu;
import com.imis.module.system.dao.SysRoleMenuMapper;
import com.imis.module.system.service.ISysRoleMenuService;
import com.imis.module.base.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 角色-菜单权限关联表 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysRoleMenuServiceImpl extends BaseServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}