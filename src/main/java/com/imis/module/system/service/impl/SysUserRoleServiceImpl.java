package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.system.dao.SysUserRoleMapper;
import com.imis.module.system.model.converter.SysRoleConverter;
import com.imis.module.system.model.converter.SysUserConverter;
import com.imis.module.system.model.po.SysUser;
import com.imis.module.system.model.po.SysUserRole;
import com.imis.module.system.model.ro.PagingQueryUserByRoleIdDTO;
import com.imis.module.system.model.ro.SysGrantRoleToUsersRO;
import com.imis.module.system.model.ro.SysUnGrantRoleToUsersRO;
import com.imis.module.system.model.vo.SysUserVO;
import com.imis.module.system.service.ISysUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户角色关联表 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

    /**
     * 根据角色标识查看用户
     *
     * @param pagingQueryUserByRoleIdDTO - 根据角色查询用户对象
     * @return Page<SysUser> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:26
     */
    @Override
    public Page<SysUserVO> pagingQuerySysUserListByRoleIdParameter(final PagingQueryUserByRoleIdDTO pagingQueryUserByRoleIdDTO) {
        // 1.页码、页长
        Page<SysUser> sysUserPage = new Page<>(pagingQueryUserByRoleIdDTO.getPageNumber(), pagingQueryUserByRoleIdDTO.getPageSize());
        // 2.排序字段
        sysUserPage.addOrder(OrderItem.descs(DataBaseConstant.CREATE_TIME_FIELD, DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        sysUserPage = mapper.pagingQuerySysUserListByRoleIdParameter(sysUserPage, pagingQueryUserByRoleIdDTO);
        // 4.PO分页对象转VO实体分页对象
        return SysUserConverter.INSTANCE.getReturnValue(sysUserPage);
    }

    /**
     * 根据角色标识查看未授权用户
     *
     * @param pagingQueryUserByRoleIdDTO - 根据角色查询用户对象
     * @return Page<SysUser> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:26
     */
    @Override
    public Page<SysUserVO> pagingQueryUnauthorizedSysUserListByRoleIdParameter(final PagingQueryUserByRoleIdDTO pagingQueryUserByRoleIdDTO) {
        // 1.页码、页长
        Page<SysUser> sysUserPage = new Page<>(pagingQueryUserByRoleIdDTO.getPageNumber(), pagingQueryUserByRoleIdDTO.getPageSize());
        // 2.排序字段
        sysUserPage.addOrder(OrderItem.descs("su.create_time", "su.update_time"));
        // 3.条件分页查询
        sysUserPage = mapper.pagingQueryUnauthorizedSysUserListByRoleIdParameter(sysUserPage, pagingQueryUserByRoleIdDTO);
        // 4.PO分页对象转VO实体分页对象
        return SysUserConverter.INSTANCE.getReturnValue(sysUserPage);
    }

    /**
     * 角色授权用户
     *
     * @param sysGrantRoleToUsers - 角色授权用户
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    @Override
    //    @HashCacheEvict(hashIdentification = CacheConstant.PREFIX_SYS_USER_ROLE, hashKey = "'USER_ID_' + #sysGrantRoleToUsersRO.userIdList")
    public Boolean authorizingRoleToUsers(SysGrantRoleToUsersRO sysGrantRoleToUsers) {
        try {
            // 1.对象转换
            List<SysUserRole> sysUserRoleList = SysRoleConverter.INSTANCE.grantRoleToUsers(sysGrantRoleToUsers);
            // 2.授权处理
            int grant = mapper.grantRoleToUser(sysUserRoleList);
            return grant > 0;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Boolean.FALSE;
        }
    }

    /**
     * 取消角色授权用户
     *
     * @param sysUnGrantRoleToUsers - 取消角色授权用户
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean unAuthorizingRoleToUsers(final SysUnGrantRoleToUsersRO sysUnGrantRoleToUsers) {
        try {
            // 1.校验数量
            List<Long> userIdList = sysUnGrantRoleToUsers.getUserIdList();
            List<SysUserRole> sysUserRoleList = mapper.selectList(Wrappers.<SysUserRole>lambdaQuery()
                    .eq(SysUserRole::getRoleId, sysUnGrantRoleToUsers.getRoleId())
                    .in(SysUserRole::getUserId, userIdList));
            if (userIdList.size() != sysUserRoleList.size()) {
                return Boolean.FALSE;
            }
            List<Long> arrayList = new ArrayList();
            for (SysUserRole sysUserRole : sysUserRoleList) {
                arrayList.add(sysUserRole.getId());
            }
            int deleteBatchIds = mapper.deleteBatchIds(arrayList);
            if (deleteBatchIds == sysUserRoleList.size()) {
                return Boolean.TRUE;
            }
            throw new Exception();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Boolean.FALSE;
        }
    }

}