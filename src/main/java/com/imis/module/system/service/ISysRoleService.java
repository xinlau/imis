package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysRole;
import com.imis.module.system.model.ro.PagingQueryRoleDTO;
import com.imis.module.system.model.ro.PagingQueryUserRoleDTO;
import com.imis.module.system.model.ro.SysGrantMenuPermissionToRoleRO;
import com.imis.module.system.model.vo.SysRoleVO;

import java.util.List;

/**
 * <p>
 * 系统角色表-权限组 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public interface ISysRoleService extends BaseService<SysRole> {

    /**
     * 分页查询角色信息
     *
     * @param pagingQueryRoleDTO - 角色查询对象
     * @return Page<SysRoleVO> - 角色信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Page<SysRoleVO> pagingQuerySysRoleListByParameter(final PagingQueryRoleDTO pagingQueryRoleDTO);

    /**
     * 根据角色名查询角色信息
     *
     * @param roleName - 角色名
     * @return SysRole - 角色
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:30
     */
    SysRole queryRoleByName(final String roleName);

    /**
     * 根据角色编码查询角色信息
     *
     * @param roleCode - 角色编码
     * @return SysRole - 角色
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:30
     */
    SysRole queryRoleByCode(final String roleCode);

    /**
     * 创建角色信息
     *
     * @param sysRole - 角色
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:30
     */
    Boolean saveRole(final SysRole sysRole);

    /**
     * 删除角色信息
     *
     * @param roleIdentification - 角色标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:30
     */
    Boolean deleteRole(final Long roleIdentification);

    /**
     * 清除角色权限信息
     *
     * @param roleIdentification - 角色标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:30
     */
    Boolean cleanRolePermissionsById(final Long roleIdentification);

    /**
     * 根据角色编号查询用户编号
     *
     * @param roleIdentification - 角色标识
     * @return List<Long>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<Long> selectSysUserIdArrayByRoleId(final Long roleIdentification);

    /**
     * 根据用户编号查询角色编号
     *
     * @param userIdentification - 用户标识
     * @return List<Long>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<Long> selectSysRoleIdArrayByUserId(final Long userIdentification);

    /**
     * 根据用户名查询角色编号
     *
     * @param userName - 用户名
     * @return List<Long>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<Long> selectSysRoleIdArrayByUserName(final String userName);

    /**
     * 根据角色编号修改角色信息
     *
     * @param sysRole - 角色修改对象
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Boolean updateByRoleIdentification(final SysRole sysRole);

    /**
     * 分页查找已授权角色
     *
     * @param sysUserRoleSelect - 授权角色查询对象
     * @return Page<SysRoleVO> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/20 15:33
     */
    Page<SysRoleVO> pagingQueryGrantRoleListByParameter(final PagingQueryUserRoleDTO sysUserRoleSelect);

    /**
     * 分页查找未授权角色
     *
     * @param sysUserRoleSelect - 授权角色查询对象
     * @return Page<SysRoleVO> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/20 15:33
     */
    Page<SysRoleVO> pagingQueryNotGrantRoleListByParameter(final PagingQueryUserRoleDTO sysUserRoleSelect);

    /**
     * 为角色授权功能菜单权限
     *
     * @param sysGrantMenuPermissionToRole - 角色授权功能菜单
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/21 10:59
     */
    Boolean grantMenuPermissionsToRole(final SysGrantMenuPermissionToRoleRO sysGrantMenuPermissionToRole);

    /**
     * 根据用户编号查询角色信息
     *
     * @param userIdentification - 用户标识
     * @return List<SysRole> - 角色信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/29 10:59
     */
    List<SysRoleVO> selectSysRoleListByUserId(final Long userIdentification);

}