package com.imis.module.system.service;

import com.imis.module.system.model.po.SysRichText;
import com.imis.module.base.BaseService;

/**
 * <p>
 * 富文本 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
public interface ISysRichTextService extends BaseService<SysRichText> {

}