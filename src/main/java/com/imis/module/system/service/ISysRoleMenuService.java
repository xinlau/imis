package com.imis.module.system.service;

import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysRoleMenu;

/**
 * <p>
 * 角色-菜单权限关联表 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
public interface ISysRoleMenuService extends BaseService<SysRoleMenu> {

}