package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysDictItem;
import com.imis.module.system.model.ro.PagingQueryDictItemDTO;
import com.imis.module.system.model.vo.SysDictItemVO;

import java.util.List;

/**
 * <p>
 * 字典值 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
public interface ISysDictItemService extends BaseService<SysDictItem> {

    /**
     * 通过字典项编号获取字典值
     *
     * @param dictId - 字典项编号
     * @return List<SysDictItem>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    List<SysDictItem> selectSysDictItemListByDictId(final Long dictId);

    /**
     * 通过字典项编号、字典值文本获取字典值
     *
     * @param itemText - 字典值文本
     * @param dictId   - 字典项编号
     * @return SysDictItem
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    SysDictItem selectDictItemByTextAndDict(final String itemText, final Long dictId);

    /**
     * 通过字典项编号、字典值值获取字典值
     *
     * @param itemValue - 字典值值
     * @param dictId    - 字典项编号
     * @return SysDictItem
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    SysDictItem selectDictItemByValueAndDict(final String itemValue, final Long dictId);

    /**
     * 保存字典值
     *
     * @param sysDictItem - 字典值
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    Boolean saveDictItem(final SysDictItem sysDictItem);

    /**
     * 删除字典值
     *
     * @param dictItemIdentification - 字典值标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    Boolean deleteDictItem(final Long dictItemIdentification);

    /**
     * 更新字典值
     *
     * @param sysDictItem - 字典值更新对象
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    Boolean updateByDictItemIdentification(final SysDictItem sysDictItem);

    /**
     * 分页查询字典值
     *
     * @param pagingQueryDictItemDTO - 字典值查询对象
     * @return Page<SysDictItemVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    Page<SysDictItemVO> pagingQuerySysDictItemListByParameter(final PagingQueryDictItemDTO pagingQueryDictItemDTO);

}