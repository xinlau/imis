package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.system.dao.SysDataSourceMapper;
import com.imis.module.system.model.converter.SysDataSourceConverter;
import com.imis.module.system.model.po.SysDataSource;
import com.imis.module.system.model.ro.PagingQueryDataSourceDTO;
import com.imis.module.system.model.vo.SysDataSourceVO;
import com.imis.module.system.service.ISysDataSourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统数据源管理 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-12-30
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysDataSourceServiceImpl extends BaseServiceImpl<SysDataSourceMapper, SysDataSource> implements ISysDataSourceService {

    /**
     * 分页查询数据源信息
     *
     * @param pagingQueryDataSource - 系统数据源查询对象
     * @return Page<SysDataSourceVO> - 分页数据源信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 15:47
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_MASTER)
    public Page<SysDataSourceVO> pagingQueryDataSourceListByParameter(final PagingQueryDataSourceDTO pagingQueryDataSource) {
        // 1.页码、页长
        Page<SysDataSource> sysDataSourcePage = new Page<>(pagingQueryDataSource.getPageNumber(), pagingQueryDataSource.getPageSize());
        // 2.排序字段
        sysDataSourcePage.addOrder(OrderItem.descs(DataBaseConstant.CREATE_TIME_FIELD, DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        sysDataSourcePage = mapper.pagingQueryDataSourceListByParameter(sysDataSourcePage, pagingQueryDataSource);
        // 4.PO分页对象转VO实体分页对象
        return SysDataSourceConverter.INSTANCE.getReturnValue(sysDataSourcePage);
    }

}