package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysUserRole;
import com.imis.module.system.model.ro.PagingQueryUserByRoleIdDTO;
import com.imis.module.system.model.ro.SysGrantRoleToUsersRO;
import com.imis.module.system.model.ro.SysUnGrantRoleToUsersRO;
import com.imis.module.system.model.vo.SysUserVO;

/**
 * <p>
 * 用户角色关联表 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
public interface ISysUserRoleService extends BaseService<SysUserRole> {

    /**
     * 根据角色标识查看用户
     *
     * @param pagingQueryUserByRoleIdDTO - 根据角色查询用户对象
     * @return Page<SysUserVO> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:26
     */
    Page<SysUserVO> pagingQuerySysUserListByRoleIdParameter(final PagingQueryUserByRoleIdDTO pagingQueryUserByRoleIdDTO);

    /**
     * 根据角色标识查看未授权用户
     *
     * @param pagingQueryUserByRoleIdDTO - 根据角色查询用户对象
     * @return Page<SysUserVO> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:26
     */
    Page<SysUserVO> pagingQueryUnauthorizedSysUserListByRoleIdParameter(final PagingQueryUserByRoleIdDTO pagingQueryUserByRoleIdDTO);

    /**
     * 角色授权用户
     *
     * @param sysGrantRoleToUsers - 角色授权用户
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    Boolean authorizingRoleToUsers(final SysGrantRoleToUsersRO sysGrantRoleToUsers);

    /**
     * 取消角色授权用户
     *
     * @param sysUnGrantRoleToUsers - 取消角色授权用户
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    Boolean unAuthorizingRoleToUsers(final SysUnGrantRoleToUsersRO sysUnGrantRoleToUsers);

}