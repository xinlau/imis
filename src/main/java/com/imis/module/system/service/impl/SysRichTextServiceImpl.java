package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.system.model.po.SysRichText;
import com.imis.module.system.dao.SysRichTextMapper;
import com.imis.module.system.service.ISysRichTextService;
import com.imis.module.base.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 富文本 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysRichTextServiceImpl extends BaseServiceImpl<SysRichTextMapper, SysRichText> implements ISysRichTextService {

}