package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.api.model.ro.DictItemsByCodeQueryDTO;
import com.imis.module.api.model.ro.DictItemsQueryDTO;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysDict;
import com.imis.module.system.model.ro.PagingQueryDictDTO;
import com.imis.module.system.model.ro.QueryDictTreeDTO;
import com.imis.module.system.model.vo.DictVO;
import com.imis.module.system.model.vo.DropDownBoxTreeVO;
import com.imis.module.system.model.vo.SysDictVO;

import java.util.List;

/**
 * <p>
 * 字典项 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
public interface ISysDictService extends BaseService<SysDict> {

    /**
     * 通过查询指定code 获取字典值text
     *
     * @param code - 字典Code
     * @param key  - 字典Key
     * @return String
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    String queryDictTextByKey(final String code, final String key);

    /**
     * 通过查询指定表（table）的字段（text、code） 获取字典值text
     *
     * @param table - 表名
     * @param text  - 字段名
     * @param code  - 字段值
     * @param key   - 子表key
     * @return String
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:55
     */
    String queryTableDictTextByKey(final String table, final String text, final String code, final String key);

    /**
     * 分页查询字典项
     *
     * @param pagingQueryDictDTO - 字典项查询对象
     * @return Page<SysDictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    Page<SysDictVO> pagingQuerySysDictListByParameter(final PagingQueryDictDTO pagingQueryDictDTO);

    /**
     * 查询字典项
     *
     * @param dictName - 字典名称
     * @return SysDict
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    SysDict queryDictByName(final String dictName);

    /**
     * 查询字典项
     *
     * @param dictCode - 字典编码
     * @return SysDict
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    SysDict queryDictByCode(final String dictCode);

    /**
     * 保存字典项
     *
     * @param sysDict - 字典项
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    Boolean saveDict(final SysDict sysDict);

    /**
     * 删除字典项
     *
     * @param dictIdentification - 字典项标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    Boolean deleteDict(final Long dictIdentification);

    /**
     * 更新字典项
     *
     * @param sysDict - 字典项修改对象
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    Boolean updateByDictIdentification(final SysDict sysDict);

    /**
     * 根据字典编码（code）获取下拉
     *
     * @param dictCode - 字典编码（code）
     * @return List<DictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    List<DictVO> queryDictItemsByDictCode(final String dictCode);

    /**
     * 验证表、表字段是否存在
     *
     * @param table      表名
     * @param fieldArray 字段数组
     * @return String
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    Boolean verifyTable(final String table, final String[] fieldArray);

    /**
     * 通过查询指定表（table）的 字段（text、code）进行条件SQL语句（filterSql）检索查询数据成为字典结果
     *
     * @param sysDictItemsQuery 指定表、字段的下拉查询对象
     * @return List<DictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    List<DictVO> queryTableDictItemsByParameter(final DictItemsQueryDTO sysDictItemsQuery);

    /**
     * 通过查询指定表（table）的 字段（text、code）进行指定内容（code）的检索查询数据成为字典结果
     *
     * @param sysDictItemsByCodeQuery 指定表、字段的指定内容下拉查询对象
     * @return List<DictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    List<DictVO> queryTableDictItemsByCode(final DictItemsByCodeQueryDTO sysDictItemsByCodeQuery);

    /**
     * 条件查询字典树
     *
     * @param queryDictTree - 字典树查询对象
     * @return List<DropDownBoxTreeVO> - 字典树
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/3 16:39
     */
    List<DropDownBoxTreeVO> queryDictTreeListByParameter(final QueryDictTreeDTO queryDictTree);

}