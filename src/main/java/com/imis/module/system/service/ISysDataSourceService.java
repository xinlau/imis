package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysDataSource;
import com.imis.module.system.model.ro.PagingQueryDataSourceDTO;
import com.imis.module.system.model.vo.SysDataSourceVO;

/**
 * <p>
 * 系统数据源管理 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-12-30
 */
public interface ISysDataSourceService extends BaseService<SysDataSource> {

    /**
     * 分页查询数据源信息
     *
     * @param pagingQueryDataSource - 系统数据源查询对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 15:47
     */
    Page<SysDataSourceVO> pagingQueryDataSourceListByParameter(final PagingQueryDataSourceDTO pagingQueryDataSource);

}