package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.system.dao.SysQuartzJobMapper;
import com.imis.module.system.model.converter.SysQuartzJobConverter;
import com.imis.module.system.model.po.SysQuartzJob;
import com.imis.module.system.model.ro.PagingQueryQuartzJobDTO;
import com.imis.module.system.model.vo.SysQuartzJobVO;
import com.imis.module.system.service.ISysQuartzJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysQuartzJobServiceImpl extends BaseServiceImpl<SysQuartzJobMapper, SysQuartzJob> implements ISysQuartzJobService {

    /**
     * 分页查询定时任务
     *
     * @param pagingQueryQuartzJobDTO - 定时任务查询对象
     * @return List<SysQuartzJobVO> - 分页定时任务
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    @Override
    public Page<SysQuartzJobVO> pagingQuerySysQuartzJobListByParameter(final PagingQueryQuartzJobDTO pagingQueryQuartzJobDTO) {
        // 1.页码、页长
        Page<SysQuartzJob> sysQuartzJobPage = new Page<>(pagingQueryQuartzJobDTO.getPageNumber(), pagingQueryQuartzJobDTO.getPageSize());
        // 2.排序字段
        sysQuartzJobPage.addOrder(OrderItem.desc(DataBaseConstant.CREATE_TIME_FIELD), OrderItem.desc(DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        sysQuartzJobPage = mapper.pagingQuerySysQuartzJobListByParameter(sysQuartzJobPage, pagingQueryQuartzJobDTO);
        // 4.PO分页对象转VO实体分页对象 返回值
        return SysQuartzJobConverter.INSTANCE.getReturnValue(sysQuartzJobPage);
    }

    /**
     * 根据定时任务名字查询定时任务
     *
     * @param jobName - 定时任务名称
     * @return SysQuartzJob - 定时任务
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    @Override
    public SysQuartzJob queryQuartzJobByName(final String jobName) {
        return mapper.selectOne(Wrappers.<SysQuartzJob>lambdaQuery().eq(SysQuartzJob::getJobName, jobName));
    }

    /**
     * 添加定时任务
     *
     * @param sysQuartzJob - 定时任务
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    @Override
    public Boolean saveQuartzJob(final SysQuartzJob sysQuartzJob) {
        int insert = mapper.insert(sysQuartzJob);
        return insert == 1;
    }

    /**
     * 定时任务删除
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    @Override
    public Boolean deleteQuartzJob(final Long quartzJobIdentification) {
        int deleteById = mapper.deleteById(quartzJobIdentification);
        return deleteById == 1;
    }

    /**
     * 更新定时任务
     *
     * @param sysQuartzJob - 定时任务更新对象
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    @Override
    public Boolean updateByQuartzJobIdentification(final SysQuartzJob sysQuartzJob) {
        int update = mapper.updateById(sysQuartzJob);
        return update > 0;
    }

}