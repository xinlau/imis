package com.imis.module.system.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.api.model.ro.UserRegisterDTO;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysUser;
import com.imis.module.system.model.ro.PagingQueryUserDTO;
import com.imis.module.system.model.ro.SysGrantRolesToUserRO;
import com.imis.module.system.model.vo.SysUserVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public interface ISysUserService extends BaseService<SysUser> {

    /**
     * 重复注册验证
     *
     * @param register - 用户信息注册对象
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    List<SysUser> duplicateRegistrationVerification(final UserRegisterDTO register);

    /**
     * 分页查询用户信息
     *
     * @param pagingQueryUserDTO - 用户信息查询对象
     * @return Page<SysUserVO> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    Page<SysUserVO> pagingQuerySysUserListByParameter(final PagingQueryUserDTO pagingQueryUserDTO);

    /**
     * 通过用户账号查询用户信息
     *
     * @param username - 用户账号
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/6 16:13
     */
    SysUser queryUserByName(final String username);

    /**
     * 创建用户信息
     *
     * @param sysUser - 用户信息
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/6 16:13
     */
    Boolean saveUser(final SysUser sysUser);

    /**
     * 冻结用户
     *
     * @param identification - 用户唯一标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/5 17:25
     */
    Boolean freezeUserByIdentification(final Long identification);

    /**
     * 解冻用户
     *
     * @param identification - 用户唯一标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/5 17:25
     */
    Boolean unFreezeUserByIdentification(final Long identification);

    /**
     * 用户授权角色
     *
     * @param sysGrantUserToRole - 用户授权角色
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    Boolean grantRoleToUser(final SysGrantRolesToUserRO sysGrantUserToRole);

    /**
     * 撤销用户角色授权
     *
     * @param userId - 用户编号
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/20 14:22
     */
    Boolean terminateAllRoleToUser(final Long userId);

    /**
     * 通过用户编号获取用户角色集合
     *
     * @param userId - 用户编号
     * @return Set<String> - 用户角色集合
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    Set<String> queryUserRolesSetByUserId(final Long userId);

    /**
     * 通过用户名获取用户权限集合
     *
     * @param userId - 用户编号
     * @return Set<String> - 权限集合
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    Set<String> queryUserPermissionsSetByUserId(final Long userId);

    /**
     * 根据手机号查询用户信息
     *
     * @param phone - 手机号
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/21 16:00
     */
    SysUser queryUserByPhone(final String phone);

    /**
     * 根据邮箱查询用户信息
     *
     * @param email - 邮箱
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/21 16:00
     */
    SysUser queryUserByEmail(final String email);

    /**
     * 修改密码
     *
     * @param username    - 用户名
     * @param newPassword - 新密码
     * @return Boolean - 成功标志
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    Boolean changeUserPassword(final String username, final String newPassword);

}
