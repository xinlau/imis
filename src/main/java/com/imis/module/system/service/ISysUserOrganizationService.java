package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysUserOrganization;
import com.imis.module.system.model.ro.PagingQueryUserByOrganizationIdDTO;
import com.imis.module.system.model.ro.SysGrantUserToOrganizationRO;
import com.imis.module.system.model.vo.SysUserVO;

/**
 * <p>
 * 用户-组织机构关联表 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
public interface ISysUserOrganizationService extends BaseService<SysUserOrganization> {

    /**
     * 查询组织机构下的用户接口
     * @param pagingQueryUserByOrganizationIdDTO - 根据组织机构查询用户对象
     * @return Page<SysUserVO> -
     * @author XinLau
     * @since 2020/7/18 16:23
     * @creed The only constant is change ! ! !
     */
    Page<SysUserVO> pagingQueryUserByOrganizationIdentification(final PagingQueryUserByOrganizationIdDTO pagingQueryUserByOrganizationIdDTO);

    /**
     * 查询组织机构下没有的用户接口
     * @param pagingQueryUserByOrganizationIdDTO - 根据组织机构查询用户对象
     * @return Page<SysUserVO> -
     * @author XinLau
     * @since 2020/7/18 16:23
     * @creed The only constant is change ! ! !
     */
    Page<SysUserVO> pagingQueryUnauthorizedUser(final PagingQueryUserByOrganizationIdDTO pagingQueryUserByOrganizationIdDTO);

    /**
     * 组织机构分配用户
     * @param sysGrantUserToOrganization - 组织机构分配用户
     * @return Boolean -
     * @author XinLau
     * @since 2020/7/20 9:45
     * @creed The only constant is change ! ! !
     */
    Boolean authorizingUserToOrganization(final SysGrantUserToOrganizationRO sysGrantUserToOrganization);

    /**
     * 组织机构取消分配用户
     * @param sysGrantUserToOrganization - 组织机构分配用户
     * @return Boolean -
     * @author XinLau
     * @since 2020/7/20 9:45
     * @creed The only constant is change ! ! !
     */
    Boolean unAuthorizingUserToOrganization(final SysGrantUserToOrganizationRO sysGrantUserToOrganization);

}