package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysLog;
import com.imis.module.system.model.ro.PagingQueryLogDTO;
import com.imis.module.system.model.vo.SysLogVO;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
public interface ISysLogService extends BaseService<SysLog> {

    /**
     * 插入一条记录（选择字段，策略插入）
     * 重写保存方法,以便扩展多数据源
     *
     * @param entity 实体对象
     * @return boolean
     */
    @Override
    boolean save(final SysLog entity);

    /**
     * 分页查询系统日志
     *
     * @param pagingQueryLogDTO - 系统日志查询对象
     * @return Page<SysLog> - 系统日志
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/11 15:45
     */
    Page<SysLogVO> pagingQuerySysLogListByParameter(final PagingQueryLogDTO pagingQueryLogDTO);

}
