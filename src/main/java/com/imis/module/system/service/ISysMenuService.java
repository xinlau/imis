package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysMenu;
import com.imis.module.system.model.ro.PagingQueryMenuDTO;
import com.imis.module.system.model.ro.QueryMenuDTO;
import com.imis.module.system.model.vo.DropDownBoxTreeVO;
import com.imis.module.system.model.vo.SysMenuTreeVO;
import com.imis.module.system.model.vo.SysMenuVO;

import java.util.List;

/**
 * <p>
 * 功能菜单表 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-17
 */
public interface ISysMenuService extends BaseService<SysMenu> {

    /**
     * 分页查询功能菜单
     *
     * @param pagingQueryMenuDTO - 功能菜单查询对象
     * @return Page<SysMenuTreeVO> - 分页功能菜单
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    Page<SysMenuTreeVO> pagingQuerySysMenuParentListByParameter(final PagingQueryMenuDTO pagingQueryMenuDTO);

    /**
     * 根据功能菜单名查询功能菜单
     *
     * @param menuName - 功能菜单名称
     * @return SysMenu - 功能菜单
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    SysMenu queryMenuByName(final String menuName);

    /**
     * 根据功能菜单编号查询角色编号
     *
     * @param menuIdentification - 功能菜单标识
     * @return Long[]
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Long[] selectSysRoleIdArrayByMenuId(final Long menuIdentification);

    /**
     * 根据父级功能菜单编号查询子级功能菜单
     *
     * @param parentId - 父级功能菜单标识
     * @return List<SysMenuVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<SysMenuVO> selectSysMenuListByParentId(final Long parentId);

    /**
     * 根据功能菜单编号逻辑删除功能菜单
     *
     * @param menuIdentification - 父级功能菜单标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Boolean deleteMenuById(final Long menuIdentification);

    /**
     * 根据功能菜单编号修改功能菜单信息
     *
     * @param sysMenu - 功能菜单修改对象
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Boolean updateByMenuIdentification(final SysMenu sysMenu);

    /**
     * 查询功能菜单树
     *
     * @param queryMenuDTO - 功能菜单查询对象
     * @return List<SysMenuTreeVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    List<SysMenuTreeVO> querySysMenuTreeListByParameter(final QueryMenuDTO queryMenuDTO);

    /**
     * 查询指定角色的功能菜单树
     *
     * @param roleIdListByUserId - 角色编号
     * @return List<SysMenuTreeVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/23 10:50
     */
    List<SysMenuTreeVO> querySysMenuTreeListByRoleIdList(final List<Long> roleIdListByUserId);

    /**
     * 查询指定角色的功能菜单树
     *
     * @param roleIdentification - 角色编号
     * @return List<SysMenuTreeVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/23 10:50
     */
    List<SysMenuTreeVO> querySysMenuTreeListByRoleId(final Long roleIdentification);

    /**
     * 根据功能菜单名称获取下拉
     *
     * @param menuName - 功能菜单名称
     * @return List<DropDownBoxTreeVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    List<DropDownBoxTreeVO> queryMenuTreeByMenuName(final String menuName);

}