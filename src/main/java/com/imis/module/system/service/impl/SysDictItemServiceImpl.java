package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.system.dao.SysDictItemMapper;
import com.imis.module.system.model.converter.SysDictConverter;
import com.imis.module.system.model.po.SysDictItem;
import com.imis.module.system.model.ro.PagingQueryDictItemDTO;
import com.imis.module.system.model.vo.SysDictItemVO;
import com.imis.module.system.service.ISysDictItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 字典项 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
@Service
@Slf4j
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysDictItemServiceImpl extends BaseServiceImpl<SysDictItemMapper, SysDictItem> implements ISysDictItemService {

    /**
     * 通过字典项编号获取字典值
     *
     * @param dictId - 字典项编号
     * @return List<SysDictItem>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    @Override
    public List<SysDictItem> selectSysDictItemListByDictId(final Long dictId) {
        return baseMapper.selectList(Wrappers.<SysDictItem>lambdaQuery().eq(SysDictItem::getDictId, dictId).orderByAsc(SysDictItem::getSortOrder));
    }

    /**
     * 通过字典项编号、字典值文本获取字典值
     *
     * @param itemText - 字典值文本
     * @param dictId   - 字典项编号
     * @return SysDictItem
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    @Override
    public SysDictItem selectDictItemByTextAndDict(final String itemText, final Long dictId) {
        return baseMapper.selectOne((Wrappers.<SysDictItem>lambdaQuery().eq(SysDictItem::getItemText, itemText).eq(SysDictItem::getDictId, dictId)));
    }

    /**
     * 通过字典项编号、字典值值获取字典值
     *
     * @param itemValue - 字典值值
     * @param dictId    - 字典项编号
     * @return SysDictItem
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    @Override
    public SysDictItem selectDictItemByValueAndDict(final String itemValue, final Long dictId) {
        return baseMapper.selectOne((Wrappers.<SysDictItem>lambdaQuery().eq(SysDictItem::getItemValue, itemValue).eq(SysDictItem::getDictId, dictId)));
    }

    /**
     * 保存字典值
     *
     * @param sysDictItem - 字典值
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    @Override
    public Boolean saveDictItem(final SysDictItem sysDictItem) {
        // 影响行数
        int insert = baseMapper.insert(sysDictItem);
        return insert > 0;
    }

    /**
     * 删除字典值
     *
     * @param dictItemIdentification - 字典值标识
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    @Override
    public Boolean deleteDictItem(final Long dictItemIdentification) {
        // 影响行数
        int delete = mapper.deleteById(dictItemIdentification);
        return delete > 0;
    }

    /**
     * 更新字典值
     *
     * @param sysDictItem - 字典值更新对象
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 9:56
     */
    @Override
    public Boolean updateByDictItemIdentification(final SysDictItem sysDictItem) {
        // 影响行数
        int update = mapper.updateById(sysDictItem);
        return update > 0;
    }

    /**
     * 分页查询字典值
     *
     * @param pagingQueryDictItemDTO - 字典值查询对象
     * @return Page<SysDictItemVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    @Override
    public Page<SysDictItemVO> pagingQuerySysDictItemListByParameter(final PagingQueryDictItemDTO pagingQueryDictItemDTO) {
        // 1.页码、页长
        Page<SysDictItem> sysDictItemPage = new Page<>(pagingQueryDictItemDTO.getPageNumber(), pagingQueryDictItemDTO.getPageSize());
        // 2.排序字段
        sysDictItemPage.addOrder(OrderItem.asc("sort_order"), OrderItem.desc(DataBaseConstant.CREATE_TIME_FIELD), OrderItem.desc(DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        sysDictItemPage = mapper.pagingQuerySysDictItemListByParameter(sysDictItemPage, pagingQueryDictItemDTO);
        // 4.PO分页对象转VO实体分页对象
        return SysDictConverter.INSTANCE.getDictItemReturnValueList(sysDictItemPage);
    }

}