package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.system.dao.SysFileMapper;
import com.imis.module.system.model.po.SysFile;
import com.imis.module.system.model.ro.DuplicateCheckDTO;
import com.imis.module.system.service.ISysFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 文件存放 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
@Service
@Slf4j
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class SysFileServiceImpl extends BaseServiceImpl<SysFileMapper, SysFile> implements ISysFileService {

    /**
     * 数据重复校验
     *
     * @param duplicateCheck - 重复校验数据模型
     * @return Boolean -
     * @author XinLau
     * @since 2020/6/29 18:03
     * @creed The only constant is change ! ! !
     */
    @Override
    public Boolean doDuplicateCheck(final DuplicateCheckDTO duplicateCheck) {
        Long number = mapper.doDuplicateCheck(duplicateCheck);
        return number > 0;
    }

    /**
     * 批量保存文件
     *
     * @param sysFileList - 文件
     * @return Boolean -
     * @author XinLau
     * @since 2020/6/29 18:03
     * @creed The only constant is change ! ! !
     */
    @Override
    public Boolean saveBatchFile(final List<SysFile> sysFileList) {
        int saveBatch = mapper.saveBatchFile(sysFileList);
        return saveBatch == sysFileList.size();
    }

}