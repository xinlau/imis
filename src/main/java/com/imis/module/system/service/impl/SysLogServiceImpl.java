package com.imis.module.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.system.dao.SysLogMapper;
import com.imis.module.system.model.converter.SysLogConverter;
import com.imis.module.system.model.po.SysLog;
import com.imis.module.system.model.ro.PagingQueryLogDTO;
import com.imis.module.system.model.vo.SysLogVO;
import com.imis.module.system.service.ISysLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
@Service("sysLogService")
@Slf4j
@DS(DataBaseConstant.DATA_SOURCE_RECORD)
public class SysLogServiceImpl extends BaseServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

    /**
     * 扩展系统日志的写入策略
     *
     * @return
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_RECORD)
    public boolean save(SysLog entity) {
        return super.save(entity);
    }

    /**
     * 分页查询系统日志
     *
     * @param pagingQueryLogDTO - 系统日志查询对象
     * @return Page<SysLogVO> - 系统日志
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/10 15:32
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_RECORD)
    public Page<SysLogVO> pagingQuerySysLogListByParameter(final PagingQueryLogDTO pagingQueryLogDTO) {
        // 1.页码、页长
        Page<SysLog> sysLogPage = new Page<>(pagingQueryLogDTO.getPageNumber(), pagingQueryLogDTO.getPageSize());
        // 2.排序字段
        sysLogPage.addOrder(OrderItem.descs(DataBaseConstant.CREATE_TIME_FIELD, DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        sysLogPage = mapper.pagingQuerySysLogListByParameter(sysLogPage, pagingQueryLogDTO);
        // 4.PO分页对象转VO实体分页对象
        return SysLogConverter.INSTANCE.getReturnValueList(sysLogPage);
    }

}
