package com.imis.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseService;
import com.imis.module.system.model.po.SysQuartzJob;
import com.imis.module.system.model.ro.PagingQueryQuartzJobDTO;
import com.imis.module.system.model.vo.SysQuartzJobVO;


/**
 * <p>
 * 定时任务 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
public interface ISysQuartzJobService extends BaseService<SysQuartzJob> {

    /**
     * 分页查询定时任务
     *
     * @param pagingQueryQuartzJobDTO - 定时任务查询对象
     * @return List<SysQuartzJobVO> - 分页定时任务
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    Page<SysQuartzJobVO> pagingQuerySysQuartzJobListByParameter(final PagingQueryQuartzJobDTO pagingQueryQuartzJobDTO);

    /**
     * 根据定时任务名字查询定时任务
     *
     * @param jobName - 定时任务名称
     * @return SysQuartzJob - 定时任务
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    SysQuartzJob queryQuartzJobByName(final String jobName);

    /**
     * 添加定时任务
     *
     * @param sysQuartzJob - 定时任务
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    Boolean saveQuartzJob(final SysQuartzJob sysQuartzJob);

    /**
     * 定时任务删除
     *
     * @param quartzJobIdentification - 定时任务标识
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    Boolean deleteQuartzJob(final Long quartzJobIdentification);

    /**
     * 添加定时任务
     *
     * @param sysQuartzJob - 定时任务更新对象
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    Boolean updateByQuartzJobIdentification(final SysQuartzJob sysQuartzJob);

}