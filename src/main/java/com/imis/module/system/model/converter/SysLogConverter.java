package com.imis.module.system.model.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.system.model.po.SysLog;
import com.imis.module.system.model.vo.SysLogVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 系统日志 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月11日 11:52
 */
@Mapper
public interface SysLogConverter {

    /**
     * 系统日志 实例
     */
    SysLogConverter INSTANCE = Mappers.getMapper(SysLogConverter.class);

    /**
     * PO 转 VO
     *
     * @param entity - 系统日志
     * @return SysLogVO
     */
    @Mappings({})
    SysLogVO getReturnValue(SysLog entity);

    /**
     * PO 转 VO
     *
     * @param entityList - 系统日志
     * @return List<SysLogVO>
     */
    @Mappings({})
    List<SysLogVO> getReturnValueList(List<SysLog> entityList);

    /**
     * 分页PO转分页VO
     *
     * @param entityPage - 系统日志
     * @return Page<SysLogVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)

    })
    Page<SysLogVO> getReturnValueList(Page<SysLog> entityPage);

}
