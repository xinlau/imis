package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * <p>
 * SysLog<br>
 * 系统日志表
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年04月15日 11:54
 */
@Data
@Slf4j
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_log")
public class SysLog extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 耗时
     */
    @TableField("cost_time")
    private Long costTime;

    /**
     * IP
     */
    @TableField("ip")
    private String ip;

    /**
     * 请求参数
     */
    @TableField("request_param")
    private String requestParam;

    /**
     * 请求类型
     */
    @TableField("request_type")
    private String requestType;

    /**
     * 请求路径
     */
    @TableField("request_url")
    private String requestUrl;

    /**
     * 请求方法
     */
    @TableField("method")
    private String method;

    /**
     * 操作人用户名称
     */
    @TableField("username")
    private String username;

    /**
     * 操作人用户账户
     */
    @TableField("user_id")
    private String userId;

    /**
     * 操作详细日志
     */
    @TableField("log_content")
    private String logContent;

    /**
     * 日志类型（0成功 1失败）
     */
    @TableField("log_type")
    private Integer logType;

    /**
     * 操作类型（@RequiresPermissions）
     */
    @TableField("operate_type")
    private String operateType;

    /**
     * 错误内容
     */
    @TableField("error_content")
    private String errorContent;

}
