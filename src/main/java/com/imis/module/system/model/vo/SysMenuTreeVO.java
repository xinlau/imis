package com.imis.module.system.model.vo;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.imis.base.annotation.Dict;
import com.imis.base.constant.DictCodeConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * 功能菜单表 VO
 *
 * @author XinLau
 * @since 2020-03-18
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class SysMenuTreeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 父记录id
     */
    @ApiModelProperty(value = "父记录id")
    private String parentId;
    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;
    /**
     * 路径
     */
    @ApiModelProperty(value = "路径")
    private String url;
    /**
     * 组件
     */
    @ApiModelProperty(value = "组件")
    private String component;
    /**
     * 菜单类型（0：一级菜单，1：子菜单，2按钮权限）
     */
    @Dict(dicCode = DictCodeConstant.MENU_TYPE)
    @ApiModelProperty(value = "菜单类型（0：一级菜单，1：子菜单，2按钮权限）")
    private Integer menuType;
    /**
     * 菜单权限编码
     */
    @ApiModelProperty(value = "菜单权限编码")
    private String perms;
    /**
     * 权限策略1显示2禁用
     */
    @ApiModelProperty(value = "权限策略1显示2禁用")
    private Integer permsType;
    /**
     * 菜单排序
     */
    @ApiModelProperty(value = "菜单排序")
    private Double sortNo;
    /**
     * 菜单图标
     */
    @ApiModelProperty(value = "菜单图标")
    private String icon;
    /**
     * 是否叶子节点:    1:是   0:不是
     */
    @ApiModelProperty(value = "是否叶子节点:    1:是   0:不是")
    private Integer leafType;
    /**
     * 菜单描述
     */
    @ApiModelProperty(value = "菜单描述")
    private String description;
    /**
     * 删除状态 0正常 1已删除
     */
    @ApiModelProperty(value = "删除状态 0正常 1已删除")
    private Integer delFlag;
    /**
     * 按钮权限状态(0无效1有效)
     */
    @Dict(dicCode = DictCodeConstant.STATUS)
    @ApiModelProperty(value = "按钮权限状态(0无效1有效)")
    private Integer status;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTime;
    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime updateTime;
    /**
     * 子级功能菜单
     */
    @ApiModelProperty(value = "子级功能菜单")
    private List<SysMenuTreeVO> childMenu;
}
