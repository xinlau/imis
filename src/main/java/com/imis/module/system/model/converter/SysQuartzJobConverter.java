package com.imis.module.system.model.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.system.model.po.SysQuartzJob;
import com.imis.module.system.model.ro.SysQuartzJobAddRO;
import com.imis.module.system.model.ro.SysQuartzJobUpdateRO;
import com.imis.module.system.model.vo.SysQuartzJobVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 定时任务 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
@Mapper
public interface SysQuartzJobConverter {

    /**
     * 定时任务 转换器实例
     */
    SysQuartzJobConverter INSTANCE = Mappers.getMapper(SysQuartzJobConverter.class);

    /**
     * 定时任务 RO转VO
     *
     * @param add
     * @return SysQuartzJob
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "jobDataMap", expression = "java(com.alibaba.fastjson.JSONObject.toJSONString(add.getJobDataMap()))"),
            @Mapping(target = "jobState", ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysQuartzJob getAddEntity(SysQuartzJobAddRO add);

    /**
     * 定时任务 RO转VO
     *
     * @param sysQuartzJob - 定时任务
     * @param update       - 定时任务
     * @return SysQuartzJob
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget SysQuartzJob sysQuartzJob, SysQuartzJobUpdateRO update);

    /**
     * 定时任务 PO转VO
     *
     * @param entity
     * @return SysQuartzJobVO
     */
    @Mappings({})
    SysQuartzJobVO getReturnValue(SysQuartzJob entity);

    /**
     * 定时任务 PO转VO
     *
     * @param entityList
     * @return List<SysQuartzJobVO>
     */
    @Mappings({})
    List<SysQuartzJobVO> getReturnValue(List<SysQuartzJob> entityList);

    /**
     * 定时任务 分页PO转分页VO
     *
     * @param entityPage
     * @return Page<SysQuartzJobVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysQuartzJobVO> getReturnValue(Page<SysQuartzJob> entityPage);

}
