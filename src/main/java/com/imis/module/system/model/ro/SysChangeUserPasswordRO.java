package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * SysChangeUserPasswordRO<br>
 * 管理员密码修改对象
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月26日 14:23
 */
@Data
@ApiModel(value="管理员密码修改对象", description="管理员密码修改对象")
public class SysChangeUserPasswordRO {

    @ApiModelProperty(value = "用户编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long userId;

    @ApiModelProperty(value = "新密码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String newPassword;

    @ApiModelProperty(value = "验证码")
    private String captcha;

    @ApiModelProperty(value = "验证码标识")
    private String verificationCodeIdentification;

}
