package com.imis.module.system.model.converter;

import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.system.model.po.SysRichText;
import com.imis.module.system.model.ro.SysRichTextRO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * <p>
 * 富文本 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
@Mapper
public interface SysRichTextConverter {

    /**
     * 富文本 转换器实例
     */
    SysRichTextConverter INSTANCE = Mappers.getMapper(SysRichTextConverter.class);

    /**
     * 富文本 RO转PO
     *
     * @param entityRo
     * @return SysRichText
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysRichText ro2Po(SysRichTextRO entityRo);

}
