package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 字典值 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-03-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_dict_item")
public class SysDictItem extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典id
     */
    @TableField("dict_id")
    private Long dictId;

    /**
     * 字典项文本
     */
    @TableField("item_text")
    private String itemText;

    /**
     * 字典项值
     */
    @TableField("item_value")
    private String itemValue;

    /**
     * 描述
     */
    @TableField("description")
    private String description;

    /**
     * 排序
     */
    @TableField("sort_order")
    private Double sortOrder;

    /**
     * 状态（1启用 0不启用）
     */
    @TableField("status")
    private Integer status;

}
