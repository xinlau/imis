package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 字典项 RO
 *
 * @author XinLau
 * @since 2020-05-12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="字典项修改对象", description="字典项修改对象")
public class SysDictUpdateRO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 字典项ID
     */
    @ApiModelProperty(value = "字典项编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;
    /**
     * 字典名称
     */
    @ApiModelProperty(value = "字典名称")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String dictName;
    /**
     * 字典编码
     */
    @ApiModelProperty(value = "字典编码")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String dictCode;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 字典类型 （0为string,1为number）
     */
    @ApiModelProperty(value = "字典类型 （0为string,1为number）")
    private Integer type;
    /**
     * 删除状态 0正常 1已删除
     */
    @ApiModelProperty(value = "删除状态 0正常 1已删除")
    private Integer delFlag;

}
