package com.imis.module.system.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 字典项表 VO
 *
 * @author XinLau
 * @since 2020-05-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "字典项表返回值对象", description = "字典项表返回值对象")
public class SysDictVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典项ID
     */
    @ApiModelProperty(value = "字典项编号")
    private String id;

    /**
     * 字典名称
     */
    @ApiModelProperty(value = "字典名称")
    private String dictName;

    /**
     * 字典编码
     */
    @ApiModelProperty(value = "字典编码")
    private String dictCode;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;

    /**
     * 字典类型 （0为string,1为number）
     */
    @ApiModelProperty(value = "字典类型 （0为string, 1为number）")
    private Integer type;

    /**
     * 删除状态
     */
    @ApiModelProperty(value = "删除状态")
    private Integer delFlag;

}
