package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 功能菜单表 RO
 *
 * @author XinLau
 * @since 2020-03-18
*/
@Data
@ApiModel(value="功能菜单更新对象", description="功能菜单表")
@EqualsAndHashCode(callSuper = false)
public class SysMenuUpdateRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 功能菜单ID
     */
    @ApiModelProperty(value = "功能菜单编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;
    /**
     * 父记录id
     */
    @ApiModelProperty(value = "父记录id")
    private Long parentId;
    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String name;
    /**
     * 路径
     */
    @ApiModelProperty(value = "路径")
    private String url;
    /**
     * 组件
     */
    @ApiModelProperty(value = "组件")
    private String component;
    /**
     * 菜单类型（0：一级菜单，1：子菜单，2按钮权限）
     */
    @ApiModelProperty(value = "菜单类型（0：一级菜单，1：子菜单，2按钮权限）")
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Integer menuType;
    /**
     * 菜单权限编码
     */
    @ApiModelProperty(value = "菜单权限编码")
    private String perms;
    /**
     * 权限策略1显示2禁用
     */
    @ApiModelProperty(value = "权限策略1显示2禁用")
    private Integer permsType;
    /**
     * 菜单排序
     */
    @ApiModelProperty(value = "菜单排序")
    private Double sortNo;
    /**
     * 菜单图标
     */
    @ApiModelProperty(value = "菜单图标")
    private String icon;
    /**
     * 是否叶子节点:    1:是   0:不是
     */
    @ApiModelProperty(value = "是否叶子节点:    1:是   0:不是")
    private Integer leafType;
    /**
     * 菜单描述
     */
    @ApiModelProperty(value = "菜单描述")
    private String description;
    /**
     * 删除状态 0正常 1已删除
     */
    @ApiModelProperty(value = "删除状态 0正常 1已删除")
    private Integer delFlag;
    /**
     * 按钮权限状态(0无效1有效)
     */
    @ApiModelProperty(value = "按钮权限状态(0无效1有效)")
    private Integer status;

}
