package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 定时任务 RO
 *
 * @author XinLau
 * @since 2020-06-15
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="定时任务更新对象", description="定时任务")
public class SysQuartzJobUpdateRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 定时任务ID
     */
    @ApiModelProperty(value = "定时任务编号")
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;
    /**
     * 任务名称
     */
    @ApiModelProperty(value = "任务名称")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String jobName;
    /**
     * 任务分组
     */
    @ApiModelProperty(value = "任务分组")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String jobGroup;
    /**
     * 定时任务执行类类名
     */
    @ApiModelProperty(value = "定时任务执行类类名")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String jobClassName;
    /**
     * cron表达式
     */
    @ApiModelProperty(value = "cron表达式")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String cronExpression;
    /**
     * 定时任务参数
     */
    @ApiModelProperty(value = "定时任务参数")
    private String jobDataMap;
    /**
     * 任务状态
     */
    @ApiModelProperty(value = "任务状态")
    private Integer jobState;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
}
