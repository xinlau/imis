package com.imis.module.system.model.ro;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * 字典树查询 RO
 *
 * @author XinLau
 * @since 2020-05-12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="字典树查询对象", description="字典树查询对象")
public class QueryDictTreeDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 表名称
     */
    @ApiModelProperty(value = "表名称", required = true)
    private String tableName;
    /**
     * 名-字段
     */
    @ApiModelProperty(value = "名-字段", required = true)
    private String textField;
    /**
     * 编号-字段
     */
    @ApiModelProperty(value = "编号-字段", required = true)
    private String codeField;
    /**
     * 父级-字段
     */
    @ApiModelProperty(value = "父级-字段", required = true)
    private String parentField;
    /**
     * 父级编号
     */
    @ApiModelProperty(value = "父级编号")
    private String parentId;
    /**
     * 拥有子节点标识字段
     */
    @ApiModelProperty(value = "拥有子节点标识字段（0-无，1-有）")
    private String hasChildField;
    /**
     * 条件（key-表列名，value-表列值）
     */
    @ApiModelProperty(value = "条件（key-表列名，value-表列值）")
    Map<String, String> parameterMap;

}
