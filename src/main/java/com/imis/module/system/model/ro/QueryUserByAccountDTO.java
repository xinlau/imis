package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * QueryUserByAccountDTO<br>
 * 自助密码修改-用户查询对象
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月26日 14:23
 */
@Data
@ApiModel(value="自助密码修改-用户查询对象", description="自助密码修改-用户查询对象")
@EqualsAndHashCode(callSuper = false)
public class QueryUserByAccountDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "登录账号", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String account;

    @ApiModelProperty(value = "验证码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String captcha;

    @ApiModelProperty(value = "验证码标识", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String verificationCodeIdentification;

}
