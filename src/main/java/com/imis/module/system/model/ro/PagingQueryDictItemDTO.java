package com.imis.module.system.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 字典值分页查询 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
@Data
@ApiModel(value="字典值查询对象", description="字典值查询")
@EqualsAndHashCode(callSuper = false)
public class PagingQueryDictItemDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典值ID
     */
    @ApiModelProperty(value = "字典值编号")
    private Long dictId;
    /**
     * 字典项文本
     */
    @ApiModelProperty(value = "字典项文本")
    private String itemText;
    /**
     * 字典项值
     */
    @ApiModelProperty(value = "字典项值")
    private String itemValue;
    /**
     * 状态（1启用 0不启用）
     */
    @ApiModelProperty(value = "字典项状态")
    private Integer status;

}
