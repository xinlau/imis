package com.imis.module.system.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 系统角色表-权限组 RO
 *
 * @author XinLau
 * @since 2020-03-12
*/
@Data
@ApiModel(value="角色查询对象", description="系统角色表-权限组-角色查询")
@EqualsAndHashCode(callSuper = false)
public class PagingQueryRoleDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色编号")
    private Long id;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String roleName;
    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码")
    private String roleCode;
    /**
     * 角色描述
     */
    @ApiModelProperty(value = "角色描述")
    private String description;

}
