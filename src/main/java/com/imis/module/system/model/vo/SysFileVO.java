package com.imis.module.system.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 文件存放 VO
 *
 * @author XinLau
 * @since 2020-03-27
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class SysFileVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文件ID
     */
    @ApiModelProperty(value = "文件编号")
    private String id;
    /**
     * 文件名称
     */
    @ApiModelProperty(value = "文件名称")
    private String fileName;
    /**
     * 文件上传名
     */
    @ApiModelProperty(value = "文件上传名")
    private String realName;
    /**
     * 文件类型
     */
    @ApiModelProperty(value = "文件类型")
    private String fileType;
    /**
     * 文件地址
     */
    @ApiModelProperty(value = "文件地址")
    private String fileUrl;
    /**
     * 文件大小
     */
    @ApiModelProperty(value = "文件大小")
    private Long fileSize;
    /**
     * 文件描述
     */
    @ApiModelProperty(value = "文件描述")
    private String description;
    /**
     * 删除状态 （0正常 1已删除）
     */
    @ApiModelProperty(value = "删除状态 （0正常 1已删除）")
    private Integer delFlag;
}
