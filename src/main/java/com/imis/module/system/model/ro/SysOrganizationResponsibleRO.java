package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 组织机构 RO
 *
 * @author XinLau
 * @since 2020-04-22
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="组织机构负责对象", description="组织机构")
public class SysOrganizationResponsibleRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组织机构id
     */
    @ApiModelProperty(value = "组织机构id", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long organizationId;

    /**
     * 是否负责部门（0-不负责，1-负责）
     */
    @ApiModelProperty(value = "是否负责部门（0-不负责，1-负责）")
    private Integer isResponsible;

}
