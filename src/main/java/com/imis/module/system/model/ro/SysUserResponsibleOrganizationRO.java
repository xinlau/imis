package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 用户负责组织机构 RO
 *
 * @author XinLau
 * @since 2020-04-22
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="用户负责组织机构对象", description="用户负责组织机构对象")
public class SysUserResponsibleOrganizationRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long userId;

    /**
     * 是否负责部门（0-不负责，1-负责）
     */
    @ApiModelProperty(value = "是否负责部门（0-不负责，1-负责）")
    private Integer isResponsible;

}
