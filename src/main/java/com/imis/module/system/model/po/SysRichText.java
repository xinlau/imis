package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 富文本 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_rich_text")
public class SysRichText extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文本类型
     */
    @TableField("text_type")
    private Integer textType;

    /**
     * 主表id
     */
    @TableField("primary_table_id")
    private Long primaryTableId;

    /**
     * 文本内容
     */
    @TableField("content")
    private String content;

}
