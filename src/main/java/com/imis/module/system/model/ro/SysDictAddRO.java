package com.imis.module.system.model.ro;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 *
 * 字典项 RO
 *
 * @author XinLau
 * @since 2020-05-12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="字典项添加对象", description="字典项添加对象")
public class SysDictAddRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典名称
     */
    @ApiModelProperty(value = "字典名称", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String dictName;
    /**
     * 字典编码
     */
    @ApiModelProperty(value = "字典编码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String dictCode;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 字典类型 （0为string,1为number）
     */
    @ApiModelProperty(value = "字典类型 （0为string,1为number）")
    private Integer type;
    /**
     * 删除状态 0正常 1已删除
     */
    @ApiModelProperty(hidden = true)
    private Integer delFlag = CommonConstant.DEL_FLAG_NOT_DELETE;

}
