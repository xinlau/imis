package com.imis.module.system.model.ro;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 字典值 RO
 *
 * @author XinLau
 * @since 2020-03-24
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="字典值添加对象", description="字典值")
public class SysDictItemAddRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典项id
     */
    @ApiModelProperty(value = "字典项编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long dictId;
    /**
     * 字典项文本
     */
    @ApiModelProperty(value = "字典值文本", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String itemText;
    /**
     * 字典项值
     */
    @ApiModelProperty(value = "字典值值", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String itemValue;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Double sortOrder;
    /**
     * 状态（1启用 0不启用）
     */
    @ApiModelProperty(value = "状态")
    private Integer status = CommonConstant.STATUS_OK;
}
