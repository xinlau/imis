package com.imis.module.system.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * 组织机构 VO
 *
 * @author XinLau
 * @since 2020-04-22
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="组织机构对象", description="组织机构")
public class SysOrganizationVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 父级编号
     */
    @ApiModelProperty(value = "父级编号")
    private String parentId;
    /**
     * 组织机构名称
     */
    @ApiModelProperty(value = "组织机构名称")
    private String organizationName;
    /**
     * 组织机构编号
     */
    @ApiModelProperty(value = "组织机构编号")
    private String organizationCode;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 排序号
     */
    @ApiModelProperty(value = "排序号")
    private Integer sortOrder;
    /**
     * 删除状态(0-正常,1-已删除)
     */
    @ApiModelProperty(value = "删除状态(0-正常,1-已删除)")
    private Integer delFlag;
}
