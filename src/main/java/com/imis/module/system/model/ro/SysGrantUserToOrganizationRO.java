package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * SysGrantUserToOrganizationRO<br>
 * 用户组织机构关联
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月19日 12:50
 */
@Data
@ApiModel(value="组织机构分配用户", description="组织机构分配用户")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysGrantUserToOrganizationRO {

    /**
     * 组织机构id
     */
    @ApiModelProperty(value = "组织机构id", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long organizationId;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户编号数组", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private List<SysUserResponsibleOrganizationRO> userIdList;

}
