package com.imis.module.system.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 系统数据源管理分页查询 RO
 *
 * @author XinLau
 * @since 2020-12-30
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="DataSource分页查询对象", description="系统数据源管理")
public class PagingQueryDataSourceDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库类型
     */
    @ApiModelProperty(value = "数据库类型")
    private String type;
    /**
     * 连接池名称
     */
    @ApiModelProperty(value = "连接池名称")
    private String poolName;
    /**
     * JDBC driver
     */
    @ApiModelProperty(value = "JDBC driver")
    private String driverClassName;
    /**
     * JDBC url 地址 IP
     */
    @ApiModelProperty(value = "JDBC url 地址 IP")
    private String ip;
    /**
     * JDBC url 地址 port
     */
    @ApiModelProperty(value = "JDBC url 地址 port")
    private Integer portNumber;
    /**
     * JDBC url 地址 数据库名称
     */
    @ApiModelProperty(value = "JDBC url 地址 数据库名称")
    private String databaseName;
    /**
     * JDBC 用户名
     */
    @ApiModelProperty(value = "JDBC 用户名")
    private String username;
    /**
     * 数据库状态(0无效，1有效)
     */
    @ApiModelProperty(value = "数据库状态(0无效，1有效)")
    private Integer status;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
}
