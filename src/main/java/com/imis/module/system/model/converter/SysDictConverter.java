package com.imis.module.system.model.converter;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.base.SortField;
import com.imis.module.system.model.po.SysDict;
import com.imis.module.system.model.po.SysDictItem;
import com.imis.module.system.model.ro.SysDictAddRO;
import com.imis.module.system.model.ro.SysDictItemAddRO;
import com.imis.module.system.model.ro.SysDictItemUpdateRO;
import com.imis.module.system.model.ro.SysDictUpdateRO;
import com.imis.module.system.model.vo.SysDictItemVO;
import com.imis.module.system.model.vo.SysDictVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 字典项 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
@Mapper
public interface SysDictConverter {

    /**
     * 字典 转换器实例
     */
    SysDictConverter INSTANCE = Mappers.getMapper(SysDictConverter.class);

    /**
     * 字典项 RO转PO
     *
     * @param add - 字典项添加对象
     * @return SysDict
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysDict getAddEntity(SysDictAddRO add);

    /**
     * 字典项 RO转VO
     *
     * @param sysDict - 字典项对象
     * @param update  - 字典项修改对象
     * @return SysDict
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget SysDict sysDict, SysDictUpdateRO update);

    /**
     * 字典项 PO转VO
     *
     * @param entity - 字典项
     * @return SysDictVO
     */
    @Mappings({})
    SysDictVO getReturnValue(SysDict entity);

    /**
     * 字典项 PO转VO
     *
     * @param entityList - 字典项
     * @return List<SysDictVO>
     */
    @Mappings({})
    List<SysDictVO> getDictReturnValueList(List<SysDict> entityList);

    /**
     * 字典项 PO转VO
     *
     * @param entityPage - 字典项
     * @return List<SysDictVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysDictVO> getDictReturnValueList(Page<SysDict> entityPage);

    //******************************************************************************************************************

    /**
     * 字典值 RO转PO
     *
     * @param add - 字典值添加对象
     * @return SysDict
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysDictItem getAddEntity(SysDictItemAddRO add);

    /**
     * 字典项 RO转VO
     *
     * @param sysDictItem - 字典值对象
     * @param update      - 字典值修改对象
     * @return SysDict
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget SysDictItem sysDictItem, SysDictItemUpdateRO update);

    /**
     * 字典值 PO转VO
     *
     * @param entity - 字典值
     * @return SysDictItemVO
     */
    @Mappings({})
    SysDictItemVO getReturnValue(SysDictItem entity);

    /**
     * 字典值 PO转VO
     *
     * @param entityList - 字典值 返回值
     * @return List<SysDictItemVO>
     */
    @Mappings({})
    List<SysDictItemVO> getDictItemReturnValueList(List<SysDictItem> entityList);

    /**
     * 字典值 PO转VO
     *
     * @param entityPage - 字典值
     * @return List<SysDictItemVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysDictItemVO> getDictItemReturnValueList(Page<SysDictItem> entityPage);

    /**
     * 排序字段
     * @param sortField - 排序字段
     * @return OrderItem - 排序字段
     */
    @Mappings({
            // 避免后端返回下划线命名的字段给前端
            @Mapping(target = "column", expression = "java(com.imis.base.util.ConvertUtils.camelToUnderline(sortField.getFieldsName()))")
    })
    OrderItem getOrderItem(SortField sortField);

    /**
     * 排序字段
     * @param sortFieldList - 排序字段
     * @return List<OrderItem> - 排序字段
     */
    @Mappings({})
    List<OrderItem> getOrderItemList(List<SortField> sortFieldList);

}
