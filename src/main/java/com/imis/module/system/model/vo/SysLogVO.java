package com.imis.module.system.model.vo;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.imis.base.annotation.Dict;
import com.imis.base.constant.DictCodeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * SysLogVO<br>
 * 系统日志查询
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月10日 11:22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "系统日志返回值对象", description = "系统日志返回值对象")
public class SysLogVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志ID
     */
    @ApiModelProperty(value = "主键")
    private String id;

    /**
     * 耗时
     */
    @ApiModelProperty(value = "耗时")
    private String costTime;

    /**
     * IP
     */
    @ApiModelProperty(value = "IP")
    private String ip;

    /**
     * 请求参数
     */
    @ApiModelProperty(value = "请求参数")
    private String requestParam;

    /**
     * 请求类型
     */
    @ApiModelProperty(value = "请求类型")
    private String requestType;

    /**
     * 请求路径
     */
    @ApiModelProperty(value = "请求路径")
    private String requestUrl;
    /**
     * 请求方法
     */
    @ApiModelProperty(value = "主键")
    private String method;

    /**
     * 操作人用户名称
     */
    @ApiModelProperty(value = "操作人用户名称")
    private String username;

    /**
     * 操作人用户账户
     */
    @ApiModelProperty(value = "操作人用户账户")
    private String userId;

    /**
     * 操作详细日志
     */
    @ApiModelProperty(value = "操作详细日志")
    private String logContent;

    /**
     * 日志类型（0成功 1失败）
     */
    @ApiModelProperty(value = "日志类型（0成功 1失败）")
    @Dict(dicCode = DictCodeConstant.LOG_TYPE)
    private Integer logType;

    /**
     * 操作类型（@RequiresPermissions）
     */
    @ApiModelProperty(value = "操作类型（@RequiresPermissions）")
    private String operateType;

    /**
     * 错误内容
     */
    @ApiModelProperty(value = "错误内容")
    private String errorContent;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime updateTime;

}
