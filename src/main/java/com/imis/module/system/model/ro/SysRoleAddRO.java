package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 *
 * 系统角色表-权限组 RO
 *
 * @author XinLau
 * @since 2020-03-12
*/
@Data
@ApiModel(value="角色添加对象", description="系统角色表-权限组-角色添加")
@EqualsAndHashCode(callSuper = false)
public class SysRoleAddRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String roleName;
    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String roleCode;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
}
