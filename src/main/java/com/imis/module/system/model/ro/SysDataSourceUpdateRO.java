package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 系统数据源管理 RO
 *
 * @author XinLau
 * @since 2020-12-30
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="DataSource更新对象", description="系统数据源管理")
public class SysDataSourceUpdateRO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 数据源ID
     */
    @ApiModelProperty(value = "数据源编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;
    /**
     * 数据库类型
     */
    @ApiModelProperty(value = "数据库类型", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String type;
    /**
     * 连接池名称
     */
    @ApiModelProperty(value = "连接池名称", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String poolName;
    /**
     * JDBC driver
     */
    @ApiModelProperty(value = "JDBC driver", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String driverClassName;
    /**
     * JDBC url 地址 IP
     */
    @ApiModelProperty(value = "JDBC url 地址 IP", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String ip;
    /**
     * JDBC url 地址 port
     */
    @ApiModelProperty(value = "JDBC url 地址 port", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Integer portNumber;
    /**
     * JDBC url 地址 数据库名称
     */
    @ApiModelProperty(value = "JDBC url 地址 数据库名称", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String databaseName;
    /**
     * JDBC 用户名
     */
    @ApiModelProperty(value = "JDBC 用户名", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String username;
    /**
     * JDBC 密码
     */
    @ApiModelProperty(value = "JDBC 密码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String password;
    /**
     * 自动运行的建表脚本
     */
    @ApiModelProperty(value = "自动运行的建表脚本")
    private String schemaUrl;
    /**
     * 自动运行的数据脚本
     */
    @ApiModelProperty(value = "自动运行的数据脚本")
    private String dataUrl;
    /**
     * 数据库状态(0无效，1有效)
     */
    // @ApiModelProperty(value = "数据库状态(0无效，1有效)")
    // private Integer status;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
}
