package com.imis.module.system.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 简化版下拉树对象 VO
 *
 * @author XinLau
 * @since 2020-04-22
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="简化版下拉树对象", description="下拉树")
public class DropDownBoxTreeVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 父级编号
     */
    @ApiModelProperty(value = "父级编号")
    private String parentId;
    /**
     * 是否叶子节点
     */
    @ApiModelProperty(value = "是否叶子节点")
    private Boolean isLeafNode;
    /**
     * ID
     */
    @ApiModelProperty(value = "主键")
    private String key;
    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String title;
    /**
     * 子级下拉树对象
     */
    @ApiModelProperty(value = "子级下拉树对象")
    private List<DropDownBoxTreeVO> children;
    /**
     * 特殊用途
     *
     * @return
     */
    public String getValue() {
        return this.key;
    }

}
