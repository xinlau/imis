package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统数据源管理 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-12-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_data_source")
public class SysDataSource extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库类型
     */
    @TableField("type")
    private String type;

    /**
     * 连接池名称
     */
    @TableField("pool_name")
    private String poolName;

    /**
     * JDBC driver
     */
    @TableField("driver_class_name")
    private String driverClassName;

    /**
     * JDBC url 地址 IP
     */
    @TableField("ip")
    private String ip;

    /**
     * JDBC url 地址 port
     */
    @TableField("port_number")
    private Integer portNumber;

    /**
     * JDBC url 地址 数据库名称
     */
    @TableField("database_name")
    private String databaseName;

    /**
     * JDBC 用户名
     */
    @TableField("username")
    private String username;

    /**
     * JDBC 密码
     */
    @TableField("password")
    private String password;

    /**
     * 自动运行的建表脚本
     */
    @TableField("schema_url")
    private String schemaUrl;

    /**
     * 自动运行的数据脚本
     */
    @TableField("data_url")
    private String dataUrl;

    /**
     * 数据库状态(0无效，1有效)
     */
    @TableField("status")
    private Integer status;

    /**
     * 描述
     */
    @TableField("description")
    private String description;

}
