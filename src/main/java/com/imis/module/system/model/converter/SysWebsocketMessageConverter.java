package com.imis.module.system.model.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.system.model.po.SysWebsocketMessage;
import com.imis.module.system.model.ro.SysWebsocketMessageAddRO;
import com.imis.module.system.model.vo.SysWebsocketMessageVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * WebSocket信息传递记录 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
@Mapper
public interface SysWebsocketMessageConverter {

    /**
     * WebSocket信息传递记录 转换器实例
     */
    SysWebsocketMessageConverter INSTANCE = Mappers.getMapper(SysWebsocketMessageConverter.class);

    /**
     * WebSocket信息传递记录 RO转PO
     *
     * @param add
     * @param textId
     * @return SysWebsocketMessage
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true),
            @Mapping(target = "textId", source = "textId")
    })
    SysWebsocketMessage getAddEntity(SysWebsocketMessageAddRO add, Long textId);

    /**
     * WebSocket信息传递记录 RO转PO
     *
     * @param addList
     * @param textId
     * @return List<SysWebsocketMessage>
     */
    default List<SysWebsocketMessage> getAddEntity(List<SysWebsocketMessageAddRO> addList, Long textId) {
        List<SysWebsocketMessage> list = new ArrayList<>();
        addList.forEach(sysWebsocketMessageAdd -> getAddEntity(sysWebsocketMessageAdd, textId));
        return list;
    }

    /**
     * WebSocket信息传递记录 PO转VO
     *
     * @param entity
     * @return SysWebsocketMessageVO
     */
    @Mappings({})
    SysWebsocketMessageVO getReturnValue(SysWebsocketMessage entity);

    /**
     * WebSocket信息传递记录 PO转VO
     *
     * @param entityList
     * @return List<SysWebsocketMessageVO>
     */
    @Mappings({})
    List<SysWebsocketMessageVO> getReturnValue(List<SysWebsocketMessage> entityList);

    /**
     * WebSocket信息传递记录 分页PO转分页VO
     *
     * @param entityPage
     * @return Page<SysWebsocketMessageVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysWebsocketMessageVO> getReturnValue(Page<SysWebsocketMessage> entityPage);

}
