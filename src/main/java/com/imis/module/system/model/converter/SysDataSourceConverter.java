package com.imis.module.system.model.converter;

import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.system.model.po.SysDataSource;
import com.imis.module.system.model.ro.SysDataSourceAddRO;
import com.imis.module.system.model.ro.SysDataSourceUpdateRO;
import com.imis.module.system.model.vo.SysDataSourceVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 系统数据源管理 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-12-30
 */
@Mapper
public interface SysDataSourceConverter {

    /**
     * 系统数据源管理 转换器实例
     */
    SysDataSourceConverter INSTANCE = Mappers.getMapper(SysDataSourceConverter.class);

    /**
     * 系统数据源管理 PO转VO
     *
     * @param entity - 系统数据源管理 PO
     * @return SysDataSourceVO - 系统数据源管理 VO
     */
    @Mappings({})
    SysDataSourceVO getReturnValue(SysDataSource entity);

    /**
     * 系统数据源管理 PO转VO
     *
     * @param entityList - 系统数据源管理 数组PO
     * @return List<SysDataSourceVO> - 系统数据源管理 数组VO
     */
    @Mappings({})
    List<SysDataSourceVO> getReturnValue(List<SysDataSource> entityList);

    /**
     * 系统数据源管理 分页PO转分页VO
     *
     * @param entityPage - 系统数据源管理 分页PO
     * @return Page<SysDataSourceVO> - 系统数据源管理 分页VO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
     })
    Page<SysDataSourceVO> getReturnValue(Page<SysDataSource> entityPage);

    /**
    * 系统数据源管理 RO转PO
    *
    * @param entity - 系统数据源管理 RO
    * @return  SysDataSource - 系统数据源管理 PO
    */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true),
            @Mapping(target = "status", expression = "java(com.imis.base.constant.CommonConstant.STATUS_ERR)"),
            @Mapping(target = "username", expression = "java(com.imis.base.util.PasswordUtil.encrypt(entity.getUsername()))"),
            @Mapping(target = "password", expression = "java(com.imis.base.util.PasswordUtil.encrypt(entity.getPassword()))")
    })
    SysDataSource getAddEntity(SysDataSourceAddRO entity);

    /**
     * 获取系统数据源
     * @param sysDataSource - 系统数据源管理 PO
     * @return DataSourceProperty - 系统数据源
     */
    @Mappings({
            @Mapping(target = "jndiName", ignore = true),
            @Mapping(target = "seata", ignore = true),
            @Mapping(target ="p6spy", ignore = true),
            @Mapping(target = "continueOnError", ignore = true),
            @Mapping(target = "separator", ignore = true),
            @Mapping(target = "druid", ignore = true),
            @Mapping(target = "hikari", ignore = true),
            @Mapping(target = "publicKey", ignore = true),
            @Mapping(target = "type", ignore = true),
            @Mapping(target = "schema", source = "sysDataSource.schemaUrl"),
            @Mapping(target = "data", source = "sysDataSource.dataUrl")
    })
    DataSourceProperty getDataSourceProperty(SysDataSourceVO sysDataSource);

    /**
     * 将更新对象与数据库对象合并成新的数据库更新对象
     *
     * @param sysDataSourceUpdate - 更新对象
     * @param sysDataSource         - 数据库对象
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true),
            @Mapping(target = "status", ignore = true),
            @Mapping(target = "username", expression = "java(com.imis.base.util.PasswordUtil.encrypt(sysDataSourceUpdate.getUsername()))"),
            @Mapping(target = "password", expression = "java(com.imis.base.util.PasswordUtil.encrypt(sysDataSourceUpdate.getPassword()))")
    })
    void getUpdateEntity(@MappingTarget SysDataSource sysDataSource, SysDataSourceUpdateRO sysDataSourceUpdate);

}
