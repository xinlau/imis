package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 富文本 RO
 *
 * @author XinLau
 * @since 2020-07-24
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SysRichText对象", description="富文本")
public class SysRichTextRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文本类型
     */
    @ApiModelProperty(value = "文本类型")
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Integer textType;
    /**
     * 主表id
     */
    @ApiModelProperty(value = "主表id")
    private Long primaryTableId;
    /**
     * 文本内容
     */
    @ApiModelProperty(value = "文本内容")
    private String content;
}
