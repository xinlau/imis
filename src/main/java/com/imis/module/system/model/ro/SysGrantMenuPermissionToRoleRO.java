package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * SysGrantMenuPermissionToRoleRO<br>
 * 角色授权功能菜单
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月21日 10:48
 */
@Data
@ApiModel(value="角色授权功能菜单", description="角色授权功能菜单")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysGrantMenuPermissionToRoleRO {

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long roleId;
    /**
     * 角色id
     */
    @ApiModelProperty(value = "功能菜单编号数组", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private List<Long> menuIdList;

}
