package com.imis.module.system.model.ro;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 *
 * 组织机构 RO
 *
 * @author XinLau
 * @since 2020-04-22
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="组织机构添加对象", description="组织机构")
public class SysOrganizationAddRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父级编号
     */
    @ApiModelProperty(value = "父级编号")
    private Long parentId;
    /**
     * 组织机构名称
     */
    @ApiModelProperty(value = "组织机构名称", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String organizationName;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 排序号
     */
    @ApiModelProperty(value = "排序号")
    private Double sortOrder;
    /**
     * 删除状态 0正常 1已删除
     */
    @ApiModelProperty(hidden = true)
    private Integer delFlag = CommonConstant.DEL_FLAG_NOT_DELETE;

}
