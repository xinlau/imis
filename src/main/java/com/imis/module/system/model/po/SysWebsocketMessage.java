package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * WebSocket信息传递记录 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_websocket_message")
public class SysWebsocketMessage extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = DataBaseConstant.P_KEY, type = IdType.ASSIGN_ID)
    @TableField(DataBaseConstant.P_KEY)
    private Long id;

    /**
     * 发送方
     */
    @TableField("sender")
    private String sender;

    /**
     * 接收方
     */
    @TableField("receiver")
    private String receiver;

    /**
     * 信息内容id
     */
    @TableField("text_id")
    private Long textId;

    /**
     * 信息状态（0：发送成功，1：等待接收，2：发送异常）
     */
    @TableField("message_state")
    private Integer messageState;

}
