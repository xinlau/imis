package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 字典值 RO
 *
 * @author XinLau
 * @since 2020-03-24
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="字典值更新对象", description="字典值")
public class SysDictItemUpdateRO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 字典值ID
     */
    @ApiModelProperty(value = "字典值编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;
    /**
     * 字典项编号
     */
    @ApiModelProperty(value = "字典项编号")
    private Long dictId;
    /**
     * 字典项文本
     */
    @ApiModelProperty(value = "字典值文本")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String itemText;
    /**
     * 字典值值
     */
    @ApiModelProperty(value = "字典值值")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String itemValue;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Double sortOrder;
    /**
     * 状态（1启用 0不启用）
     */
    @ApiModelProperty(value = "状态（1启用 0不启用）")
    private Integer status;
}
