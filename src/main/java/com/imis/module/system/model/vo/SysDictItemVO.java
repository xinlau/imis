package com.imis.module.system.model.vo;

import com.imis.base.annotation.Dict;
import com.imis.base.constant.DictCodeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 字典值表 VO
 *
 * @author XinLau
 * @since 2020-05-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "字典值表返回值对象", description = "字典值表返回值对象")
public class SysDictItemVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 字典值ID
     */
    @ApiModelProperty(value = "字典值编号")
    private String id;
    /**
     * 字典项编号
     */
    @ApiModelProperty(value = "字典项编号")
    private String dictId;
    /**
     * 字典项文本
     */
    @ApiModelProperty(value = "字典项文本")
    private String itemText;
    /**
     * 字典项值
     */
    @ApiModelProperty(value = "字典项值")
    private String itemValue;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Double sortOrder;
    /**
     * 状态（1启用 0不启用）
     */
    @Dict(dicCode = DictCodeConstant.STATUS)
    @ApiModelProperty(value = "状态（1启用 0不启用）")
    private Integer status;

}
