package com.imis.module.system.model.converter;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.system.model.po.SysRole;
import com.imis.module.system.model.po.SysUserRole;
import com.imis.module.system.model.ro.SysGrantRoleToUsersRO;
import com.imis.module.system.model.ro.SysGrantRolesToUserRO;
import com.imis.module.system.model.ro.SysRoleAddRO;
import com.imis.module.system.model.ro.SysRoleUpdateRO;
import com.imis.module.system.model.vo.SysRoleVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 系统角色表-权限组 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
@Mapper
public interface SysRoleConverter {

    /**
     * 系统角色表-权限组 实例
     */
    SysRoleConverter INSTANCE = Mappers.getMapper(SysRoleConverter.class);

    /**
     * 系统角色表-权限组 PO转VO
     *
     * @param entity
     * @return SysRoleVO
     */
    @Mappings({})
    SysRoleVO getReturnValue(SysRole entity);

    /**
     * 系统角色表-权限组 PO转VO
     *
     * @param entity
     * @return SysRoleVO
     */
    @Mappings({})
    List<SysRoleVO> getReturnValue(List<SysRole> entity);

    /**
     * 系统角色表-权限组 RO转PO
     *
     * @param entityPage
     * @return Page<SysRoleVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysRoleVO> getReturnValue(Page<SysRole> entityPage);

    /**
     * 系统角色表-权限组 RO转VO
     *
     * @param add
     * @return SysRole
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysRole getAddEntity(SysRoleAddRO add);

    /**
     * 系统角色表-权限组 RO转PO
     *
     * @param sysRole
     * @param update
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget SysRole sysRole, SysRoleUpdateRO update);

    /**
     * 初始化
     *
     * @param id
     * @param userId
     * @param roleId
     * @return SysUserRole
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysUserRole initSysUserRole(Long id, Long userId, Long roleId);

    /**
     * SysGrantRolesToUserRO 转 List<SysUserRole>
     *
     * @param ro - SysGrantRolesToUserRO
     * @return List<SysUserRole>
     */
    default List<SysUserRole> sysGrantUserToRolesRo2SysUserRoles(SysGrantRolesToUserRO ro) {
        List<SysUserRole> list = new ArrayList<>();
        ro.getRoleIdList().forEach((roleId) -> {
            SysUserRole sysUserRole = initSysUserRole(IdWorker.getId(), ro.getUserId(), roleId);
            list.add(sysUserRole);
        });
        return list;
    }

    /**
     * SysGrantRoleToUsersRO 转 List<SysUserRole>
     *
     * @param ro - SysGrantRoleToUsersRO
     * @return List<SysUserRole>
     */
    default List<SysUserRole> grantRoleToUsers(SysGrantRoleToUsersRO ro) {
        List<SysUserRole> list = new ArrayList<>();
        ro.getUserIdList().forEach((userId) -> {
            SysUserRole sysUserRole = initSysUserRole(IdWorker.getId(), userId, ro.getRoleId());
            list.add(sysUserRole);
        });
        return list;
    }

}
