package com.imis.module.system.model.converter;

import com.imis.module.system.model.po.SysFile;
import com.imis.module.system.model.vo.SysFileVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 文件存放 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
@Mapper
public interface SysFileConverter {

    /**
     * 文件存放 转换器实例
     */
    SysFileConverter INSTANCE = Mappers.getMapper(SysFileConverter.class);

    /**
     * 文件存放 PO转VO
     *
     * @param entity
     * @return SysFileVO
     */
    @Mappings({})
    SysFileVO getReturnValue(SysFile entity);

    /**
     * 文件存放 PO转VO
     *
     * @param entity
     * @return SysFileVO
     */
    @Mappings({})
    List<SysFileVO> getReturnValue(List<SysFile> entity);

}
