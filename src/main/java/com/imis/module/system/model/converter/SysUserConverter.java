package com.imis.module.system.model.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.api.model.ro.UserRegisterDTO;
import com.imis.module.api.model.ro.UserUpdateBasicInformationDTO;
import com.imis.module.system.model.po.SysUser;
import com.imis.module.system.model.ro.SysUserUpdateRO;
import com.imis.module.system.model.vo.SysUserChangeVO;
import com.imis.module.system.model.vo.SysUserVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * SysUserConverter<br>
 * 系统用户转换类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年04月15日 11:54
 */
@Mapper
public interface SysUserConverter {

    /**
     * 系统用户 实例
     */
    SysUserConverter INSTANCE = Mappers.getMapper(SysUserConverter.class);

    /**
     * PO 转 VO
     *
     * @param sysUser
     * @return SysUserVO
     */
    @Mappings({})
    SysUserVO getReturnValue(SysUser sysUser);

    /**
     * PO 转 VO
     *
     * @param sysUser
     * @return SysUserChangeVO
     */
    @Mappings({})
    SysUserChangeVO domain2Vo(SysUser sysUser);

    /**
     * PO 转 VO
     *
     * @param sysUserList
     * @return List<SysUserVO>
     */
    @Mappings({})
    List<SysUserVO> getReturnValue(List<SysUser> sysUserList);

    /**
     * RO 转 PO
     *
     * @param register - 用户注册对象
     * @return SysUser
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true),
            @Mapping(target = "salt", ignore = true)
    })
    SysUser getAddEntity(UserRegisterDTO register);

    /**
     * RO 转 PO
     *
     * @param sysUserUpdateRo
     * @return SysUser
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true),
            @Mapping(target = "username", ignore = true),
            @Mapping(target = "password", ignore = true),
            @Mapping(target = "salt", ignore = true),
            @Mapping(target = "status", ignore = true),
            @Mapping(target = "delFlag", ignore = true)
    })
    SysUser updateRo2Domain(SysUserUpdateRO sysUserUpdateRo);

    /**
     * 分页PO转分页VO
     *
     * @param sysUserPage
     * @return Page<SysUserVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysUserVO> getReturnValue(Page<SysUser> sysUserPage);

    /**
     * RO 转 PO
     *
     * @param sysUser
     * @param basicInformationUpdate
     * @return SysUser
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true),
            @Mapping(target = "status", ignore = true),
            @Mapping(target = "delFlag", ignore = true),
            @Mapping(target = "username", ignore = true),
            @Mapping(target = "password", ignore = true),
            @Mapping(target = "salt", ignore = true),
            @Mapping(target = "email", ignore = true),
            @Mapping(target = "phone", ignore = true),
            @Mapping(target = "realname", source = "basicInformationUpdate.realname"),
            @Mapping(target = "avatar", source = "basicInformationUpdate.avatar"),
            @Mapping(target = "birthday", source = "basicInformationUpdate.birthday"),
            @Mapping(target = "sex", source = "basicInformationUpdate.sex")
    })
    void getUpdateEntity(@MappingTarget SysUser sysUser, UserUpdateBasicInformationDTO basicInformationUpdate);

    /**
     * 将更新对象与数据库对象合并成新的数据库更新对象
     *
     * @param update - 更新对象
     * @param sysUser         - 数据库对象
     */
    @Mappings({
            @Mapping(target = "realname", source = "update.realname"),
            @Mapping(target = "avatar", source = "update.avatar"),
            @Mapping(target = "birthday", source = "update.birthday"),
            @Mapping(target = "sex", source = "update.sex"),
            @Mapping(target = "email", source = "update.email"),
            @Mapping(target = "phone", source = "update.phone"),
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "username", ignore = true),
            @Mapping(target = "password", ignore = true),
            @Mapping(target = "salt", ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true),
            @Mapping(target = "status", ignore = true),
            @Mapping(target = "delFlag", ignore = true)
    })
    void getUpdateEntity(@MappingTarget SysUser sysUser, SysUserUpdateRO update);

}
