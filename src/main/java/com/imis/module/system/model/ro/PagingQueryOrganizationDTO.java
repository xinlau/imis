package com.imis.module.system.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 组织机构 RO
 *
 * @author XinLau
 * @since 2020-04-22
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="组织机构查询对象", description="组织机构")
public class PagingQueryOrganizationDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 组织机构ID
     */
    @ApiModelProperty(value = "组织机构编号")
    private Long id;
    /**
     * 父级编号
     */
    @ApiModelProperty(value = "父级编号")
    private Long parentId;
    /**
     * 组织机构名称
     */
    @ApiModelProperty(value = "组织机构名称")
    private String organizationName;
    /**
     * 组织机构编号
     */
    @ApiModelProperty(value = "组织机构编号")
    private String organizationCode;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 排序号
     */
    @ApiModelProperty(value = "排序号")
    private Double sortOrder;

    /**
     * 删除状态(0-正常,1-已删除)
     */
    @ApiModelProperty(value = "删除状态(0-正常,1-已删除)")
    private Integer delFlag;

}
