package com.imis.module.system.model.ro;

import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * 功能菜单表 RO
 *
 * @author XinLau
 * @since 2020-03-17
*/
@Data
@ApiModel(value="功能菜单查询对象", description="功能菜单表")
@EqualsAndHashCode(callSuper = false)
public class QueryMenuDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父记录id
     */
    @ApiModelProperty(value = "父记录id")
    private Long parentId;
    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;
    /**
     * 路径
     */
    @ApiModelProperty(value = "路径")
    private String url;
    /**
     * 组件
     */
    @ApiModelProperty(value = "组件")
    private String component;
    /**
     * 菜单类型（0：一级菜单，1：子菜单，2按钮权限）
     */
    @ApiModelProperty(value = "菜单类型（0：一级菜单，1：子菜单，2按钮权限）")
    private Integer menuType;
    /**
     * 菜单权限编码
     */
    @ApiModelProperty(value = "菜单权限编码")
    private String perms;
    /**
     * 权限策略1显示2禁用
     */
    @ApiModelProperty(value = "权限策略1显示2禁用")
    private Integer permsType;
    /**
     * 删除状态 0正常 1已删除
     */
    @ApiModelProperty(value = "删除状态 0正常 1已删除")
    private Integer delFlag;
    /**
     * 按钮权限状态(0无效1有效)
     */
    @ApiModelProperty(value = "按钮权限状态(0无效1有效)")
    private Integer status;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间-起")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTimeStart;
    @ApiModelProperty(value = "创建时间-始")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTimeEnd;

}
