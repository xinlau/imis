package com.imis.module.system.model.converter;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.system.model.po.SysOrganization;
import com.imis.module.system.model.po.SysUserOrganization;
import com.imis.module.system.model.ro.SysGrantOrganizationToUserRO;
import com.imis.module.system.model.ro.SysGrantUserToOrganizationRO;
import com.imis.module.system.model.ro.SysOrganizationAddRO;
import com.imis.module.system.model.ro.SysOrganizationUpdateRO;
import com.imis.module.system.model.vo.DropDownBoxTreeVO;
import com.imis.module.system.model.vo.SysOrganizationTreeVO;
import com.imis.module.system.model.vo.SysOrganizationVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 组织机构 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-04-22
 */
@Mapper
public interface SysOrganizationConverter {

    /**
     * 转换器实例
     */
    SysOrganizationConverter INSTANCE = Mappers.getMapper(SysOrganizationConverter.class);

    /**
     * 组织机构添加RO转VO
     *
     * @param add             - 组织机构
     * @param organizationCod - 组织机构CODE
     * @return SysOrganization - 组织机构
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "organizationCode", source = "organizationCod", defaultValue = "0"),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysOrganization getAddEntity(SysOrganizationAddRO add, String organizationCod);

    /**
     * 更新RO转VO
     *
     * @param sysOrganization - 组织机构
     * @param update - 组织机构
     * @return
     */
    @Mappings({
            @Mapping(target = "organizationCode", ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget SysOrganization sysOrganization, SysOrganizationUpdateRO update);

    /**
     * PO转VO
     *
     * @param entity
     * @return SysOrganizationVO
     */
    @Mappings({})
    SysOrganizationVO getReturnValue(SysOrganization entity);

    /**
     * PO转VO
     *
     * @param entityList
     * @return List<SysOrganizationVO>
     */
    @Mappings({})
    List<SysOrganizationVO> getReturnValue(List<SysOrganization> entityList);

    /**
     * 分页PO转分页VO
     *
     * @param entityPage
     * @return Page<SysOrganizationVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysOrganizationVO> getReturnValue(Page<SysOrganization> entityPage);




    /**
     * PO转VO
     *
     * @param sysOrganization
     * @return
     */
    @Mappings({
            @Mapping(target = "childOrganization", ignore = true)
    })
    SysOrganizationTreeVO getTreeReturnValue(SysOrganization sysOrganization);

    /**
     * PO转VO
     *
     * @param sysOrganizationList
     * @return
     */
    @Mappings({})
    List<SysOrganizationTreeVO> getTreeReturnValue(List<SysOrganization> sysOrganizationList);

    /**
     * PO转TreeVO
     *
     * @param sysOrganizationPage
     * @return
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysOrganizationTreeVO> getTreeReturnValue(Page<SysOrganization> sysOrganizationPage);

    /**
     * 初始化
     *
     * @param id             - 编号
     * @param userId         - 用户编号
     * @param organizationId - 组织机构id
     * @param isResponsible  - 是否负责部门（0-不负责，1-负责）
     * @return SysUserOrganization
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysUserOrganization initSysUserOrganization(Long id, Long userId, Long organizationId, Integer isResponsible);

    /**
     * SysGrantOrganizationToUserRO 转 List<SysUserOrganization>
     *
     * @param ro - SysGrantOrganizationToUserRO
     * @return List<SysUserOrganization>
     */
    default List<SysUserOrganization> grantOrganizationToUser(SysGrantOrganizationToUserRO ro) {
        List<SysUserOrganization> list = new ArrayList<>();
        ro.getOrganizationIdList().forEach((organizationResponsible) -> {
            SysUserOrganization sysUserOrganization = initSysUserOrganization(IdWorker.getId(), ro.getUserId(), organizationResponsible.getOrganizationId(), organizationResponsible.getIsResponsible());
            list.add(sysUserOrganization);
        });
        return list;
    }

    /**
     * PO转下拉框VO
     *
     * @param sysOrganization
     * @return
     */
    @Mappings({
            @Mapping(target = "key", source = ConverterIgnoreConstant.ID),
            @Mapping(target = "title", source = "organizationName"),
            @Mapping(target = "parentId", source = "parentId"),
            @Mapping(target = "children", ignore = true),
            @Mapping(target = "isLeafNode", ignore = true)
    })
    DropDownBoxTreeVO getDropDownBoxTreeReturnValue(SysOrganization sysOrganization);

    /**
     * PO转下拉框VO
     *
     * @param sysOrganizationList
     * @return
     */
    List<DropDownBoxTreeVO> getDropDownBoxTreeReturnValue(List<SysOrganization> sysOrganizationList);

    /**
     * SysGrantUserToOrganizationRO 转 List<SysUserOrganization>
     *
     * @param ro - SysGrantOrganizationToUserRO
     * @return List<SysUserOrganization>
     */
    default List<SysUserOrganization> grantUserToOrganization(SysGrantUserToOrganizationRO ro) {
        List<SysUserOrganization> list = new ArrayList<>();
        ro.getUserIdList().forEach((userResponsibleOrganization) -> {
            SysUserOrganization sysUserOrganization = initSysUserOrganization(IdWorker.getId(), userResponsibleOrganization.getUserId(), ro.getOrganizationId(), userResponsibleOrganization.getIsResponsible());
            list.add(sysUserOrganization);
        });
        return list;
    }

}
