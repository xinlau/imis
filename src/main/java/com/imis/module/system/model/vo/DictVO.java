package com.imis.module.system.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 简化版字典项对象VO, 只有text和value
 *
 * @author XinLau
 * @since 2020-03-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "简化版字典对象", description = "简化版字典对象,只有text和value")
public class DictVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 空参构造函数
     */
    public DictVO() {
    }

    /**
     * 构造函数
     *
     * @param value
     * @param text
     */
    public DictVO(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 字典Value
     */
    @ApiModelProperty(value = "字典Value")
    private String value;

    /**
     * 字典文本
     */
    @ApiModelProperty(value = "字典文本")
    private String text;

    /**
     * 特殊用途： JgEditableTable
     *
     * @return
     */
    public String getTitle() {
        return this.text;
    }

}
