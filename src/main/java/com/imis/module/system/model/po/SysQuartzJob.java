package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 定时任务 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_quartz_job")
public class SysQuartzJob extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 任务名称
     */
    @TableField("job_name")
    private String jobName;

    /**
     * 任务分组
     */
    @TableField("job_group")
    private String jobGroup;

    /**
     * 定时任务执行类类名
     */
    @TableField("job_class_name")
    private String jobClassName;

    /**
     * cron表达式
     */
    @TableField("cron_expression")
    private String cronExpression;

    /**
     * 定时任务参数
     */
    @TableField("job_data_map")
    private String jobDataMap;

    /**
     * 任务状态
     */
    @TableField("job_state")
    private Integer jobState;

    /**
     * 描述
     */
    @TableField("description")
    private String description;

}
