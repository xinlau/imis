package com.imis.module.system.model.converter;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.system.model.po.SysMenu;
import com.imis.module.system.model.po.SysRoleMenu;
import com.imis.module.system.model.ro.SysGrantMenuPermissionToRoleRO;
import com.imis.module.system.model.ro.SysMenuAddRO;
import com.imis.module.system.model.ro.SysMenuUpdateRO;
import com.imis.module.system.model.vo.DropDownBoxTreeVO;
import com.imis.module.system.model.vo.SysMenuTreeVO;
import com.imis.module.system.model.vo.SysMenuVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 功能菜单表 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-03-17
 */
@Mapper
public interface SysMenuConverter {

    /**
     * 功能菜单表 实例
     */
    SysMenuConverter INSTANCE = Mappers.getMapper(SysMenuConverter.class);

    /**
     * 功能菜单表 AddRO转PO
     *
     * @param add - 添加对象
     * @return SysMenu
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysMenu getAddEntity(SysMenuAddRO add);

    /**
     * 功能菜单表 UpdateRO转PO
     *
     * @param sysMenu - 功能菜单
     * @param update  - 更新对象
     * @return SysMenu
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget SysMenu sysMenu, SysMenuUpdateRO update);

    /**
     * 功能菜单表 PO转VO
     *
     * @param entity
     * @return SysMenuVO
     */
    @Mappings({})
    SysMenuVO getReturnValue(SysMenu entity);

    /**
     * PO转VO
     *
     * @param entityList - List<SysMenu>
     * @return List<SysMenuTreeVO>
     */
    @Mappings({})
    List<SysMenuVO> getReturnValue(List<SysMenu> entityList);

    /**
     * 功能菜单表 PO转VO
     *
     * @param entity
     * @return SysMenuTreeVO
     */
    @Mappings({
            @Mapping(target = "childMenu", ignore = true)
    })
    SysMenuTreeVO getTreeReturnValue(SysMenu entity);

    /**
     * PO转VO
     *
     * @param entityList - List<SysMenu>
     * @return List<SysMenuTreeVO>
     */
    @Mappings({})
    List<SysMenuTreeVO> getTreeReturnValue(List<SysMenu> entityList);

    /**
     * 功能菜单表 分页PO转分页VO
     *
     * @param entityPage - Page<SysMenu>
     * @return Page<SysMenuTreeVO>
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<SysMenuTreeVO> getTreeReturnValue(Page<SysMenu> entityPage);

    /**
     * 初始化
     *
     * @param id
     * @param roleId
     * @param menuId
     * @return SysUserRole
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    SysRoleMenu initSysRoleMenu(Long id, Long roleId, Long menuId);

    /**
     * SysGrantMenuPermissionToRoleRO 转 List<SysRoleMenu>
     *
     * @param ro - SysGrantMenuPermissionToRoleRO
     * @return List<SysRoleMenu>
     */
    default List<SysRoleMenu> sysGrantMenuPermissionToRoleRo2SysUserRoles(SysGrantMenuPermissionToRoleRO ro) {
        List<SysRoleMenu> list = new ArrayList<>();
        ro.getMenuIdList().forEach((menuId) -> {
            SysRoleMenu sysRoleMenu = initSysRoleMenu(IdWorker.getId(), ro.getRoleId(), menuId);
            list.add(sysRoleMenu);
        });
        return list;
    }

    /**
     * 功能菜单表 PO转下拉框VO
     *
     * @param entity
     * @return DropDownBoxTreeVO
     */
    @Mappings({
            @Mapping(target = "key", source = ConverterIgnoreConstant.ID),
            @Mapping(target = "title", source = "name"),
            @Mapping(target = "parentId", source = "parentId"),
            @Mapping(target = "children", ignore = true),
            @Mapping(target = "isLeafNode", ignore = true)
    })
    DropDownBoxTreeVO getDropDownBoxTreeReturnValue(SysMenu entity);

    /**
     * 功能菜单表 PO转下拉框VO
     *
     * @param entityList -
     * @return List<DropDownBoxTreeVO
     */
    List<DropDownBoxTreeVO> getDropDownBoxTreeReturnValue(List<SysMenu> entityList);

}
