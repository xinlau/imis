package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 组织机构更新 RO
 *
 * @author XinLau
 * @since 2020-04-22
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="组织机构更新对象", description="组织机构")
public class SysOrganizationUpdateRO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty(value = "主键", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private String id;
    /**
     * 父级编号
     */
    @ApiModelProperty(value = "父级编号")
    private Long parentId;
    /**
     * 组织机构名称
     */
    @ApiModelProperty(value = "组织机构名称")
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String organizationName;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 排序号
     */
    @ApiModelProperty(value = "排序号")
    private Double sortOrder;

    /**
     * 删除状态(0-正常,1-已删除)
     */
    @ApiModelProperty(value = "删除状态(0-正常,1-已删除)")
    private Integer delFlag;

}
