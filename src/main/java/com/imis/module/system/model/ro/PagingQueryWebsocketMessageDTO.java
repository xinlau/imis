package com.imis.module.system.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * WebSocket信息传递记录 RO
 *
 * @author XinLau
 * @since 2020-07-24
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="WebSocket信息传递记录查询对象", description="WebSocket信息传递记录")
public class PagingQueryWebsocketMessageDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发送方
     */
    @ApiModelProperty(value = "发送方")
    private String sender;
    /**
     * 接收方
     */
    @ApiModelProperty(value = "接收方")
    private String receiver;
    /**
     * 信息状态（0：发送成功，1：等待接收，2：发送异常）
     */
    @ApiModelProperty(value = "信息状态（0：发送成功，1：等待接收，2：发送异常）")
    private Integer messageState;

}
