package com.imis.module.system.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * PagingQueryDictDTO<br>
 * 字典项查询
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
@Data
@ApiModel(value="字典项查询对象", description="字典项查询")
@EqualsAndHashCode(callSuper = false)
public class PagingQueryDictDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典项ID
     */
    @ApiModelProperty(value = "字典项编号")
    private Long id;

    /**
     * 字典名称
     */
    @ApiModelProperty(value = "字典名称")
    private String dictName;

    /**
     * 字典编码
     */
    @ApiModelProperty(value = "字典编码")
    private String dictCode;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;

    /**
     * 字典类型 （0为string,1为number）
     */
    @ApiModelProperty(value = "字典类型 （0为string, 1为number）")
    private Integer type;

    /**
     * 删除状态
     */
    @ApiModelProperty(value = "删除状态")
    private Integer delFlag;

}
