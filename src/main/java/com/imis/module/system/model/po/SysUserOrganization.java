package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户组织机构关联表 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-03-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_user_organization")
public class SysUserOrganization extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = DataBaseConstant.P_KEY, type = IdType.ASSIGN_ID)
    @TableField(DataBaseConstant.P_KEY)
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 组织机构id
     */
    @TableField("organization_id")
    private Long organizationId;

    /**
     * 是否负责部门（0-不负责，1-负责）
     */
    @TableField("is_responsible")
    private Integer isResponsible;

}
