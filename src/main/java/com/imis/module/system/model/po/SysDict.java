package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 字典项 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_dict")
public class SysDict extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典名称
     */
    @TableField("dict_name")
    private String dictName;

    /**
     * 字典编码
     */
    @TableField("dict_code")
    private String dictCode;

    /**
     * 描述
     */
    @TableField("description")
    private String description;

    /**
     * 字典类型0为string,1为number
     */
    @TableField("type")
    private Integer type;

    /**
     * 删除状态（0，正常，1已删除）
     * 强制要求逻辑删除的注解字段要放在最后
     * @TableLogic(value = "0")
     */
    @TableField("del_flag")
    private Integer delFlag;

}
