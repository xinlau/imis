package com.imis.module.system.model.vo;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * WebSocket信息传递记录 VO
 *
 * @author XinLau
 * @since 2020-07-24
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="WebSocket信息传递记录详情对象", description="WebSocket信息传递记录详情")
public class SysWebsocketMessageInfoVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String id;
    /**
     * 发送方
     */
    @ApiModelProperty(value = "发送方")
    private String sender;
    /**
     * 接收方
     */
    @ApiModelProperty(value = "接收方")
    private String receiver;
    /**
     * 信息内容id
     */
    @ApiModelProperty(value = "信息内容id")
    private String textId;
    /**
     * 文本内容
     */
    @ApiModelProperty(value = "文本内容")
    private String content;
    /**
     * 信息状态（0：发送成功，1：等待接收，2：发送异常）
     */
    @ApiModelProperty(value = "信息状态（0：发送成功，1：等待接收，2：发送异常）")
    private Integer messageState;
    /**
     * 信息状态
     */
    @ApiModelProperty(value = "信息状态")
    private String messageStateText;

    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTime;
    /**
     * 更新人
     */
    @ApiModelProperty(value="更新人")
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime updateTime;

}
