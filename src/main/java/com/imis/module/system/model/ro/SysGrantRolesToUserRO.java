package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * SysGrantRolesToUserRO<br>
 * 用户角色关联
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月19日 12:50
 */
@Data
@ApiModel(value="用户授权角色", description="用户授权角色")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysGrantRolesToUserRO {

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long userId;
    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色编号数组", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private List<Long> roleIdList;

}
