package com.imis.module.system.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户信息
 * </p>
 *
 * @author: XinLau
 * @create: 2020-01-10 16:11
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="自助修改密码-用户信息返回值",description="系统用户返回值 对象")
public class SysUserChangeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 真实姓名
     */
    @ApiModelProperty(value="真实姓名")
    private String realname;

    /**
     * 电子邮件
     */
    @ApiModelProperty(value="电子邮件")
    private String email;

    /**
     * 电话
     */
    @ApiModelProperty(value="电话")
    private String phone;

}
