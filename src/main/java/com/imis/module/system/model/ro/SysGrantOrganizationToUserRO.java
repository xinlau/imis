package com.imis.module.system.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * SysGrantOrganizationToUserRO<br>
 * 用户组织机构关联
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月19日 12:50
 */
@Data
@ApiModel(value="用户授权组织机构", description="用户授权组织机构")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysGrantOrganizationToUserRO {

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long userId;
    /**
     * 组织机构id
     */
    @ApiModelProperty(value = "组织机构编号数组", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private List<SysOrganizationResponsibleRO> organizationIdList;

}
