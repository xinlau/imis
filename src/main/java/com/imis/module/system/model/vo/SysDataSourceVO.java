package com.imis.module.system.model.vo;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.imis.base.annotation.Dict;
import com.imis.base.constant.DictCodeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * 系统数据源管理 VO
 *
 * @author XinLau
 * @since 2020-12-30
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="DataSource对象", description="系统数据源管理")
public class SysDataSourceVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 系统数据源ID
     */
    @ApiModelProperty(value = "系统数据源编号")
    private String id;
    /**
     * 数据库类型
     */
    @ApiModelProperty(value = "数据库类型")
    private String type;
    /**
     * 连接池名称
     */
    @ApiModelProperty(value = "连接池名称")
    private String poolName;
    /**
     * JDBC driver
     */
    @ApiModelProperty(value = "JDBC driver")
    private String driverClassName;
    /**
     * JDBC url 地址 IP
     */
    @ApiModelProperty(value = "JDBC url 地址 IP")
    private String ip;
    /**
     * JDBC url 地址 port
     */
    @ApiModelProperty(value = "JDBC url 地址 port")
    private Integer portNumber;
    /**
     * JDBC url 地址 数据库名称
     */
    @ApiModelProperty(value = "JDBC url 地址 数据库名称")
    private String databaseName;
    /**
     * JDBC 用户名
     */
    @ApiModelProperty(value = "JDBC 用户名")
    private String username;

    /**
     * JDBC 密码
     */
    @ApiModelProperty(value = "JDBC 密码")
    private String password;
    /**
     * 自动运行的建表脚本
     */
    @ApiModelProperty(value = "自动运行的建表脚本")
    private String schemaUrl;
    /**
     * 自动运行的数据脚本
     */
    @ApiModelProperty(value = "自动运行的数据脚本")
    private String dataUrl;
    /**
     * 数据库状态(0无效，1有效)
     */
    @Dict(dicCode = DictCodeConstant.STATUS)
    @ApiModelProperty(value = "数据库状态(0无效，1有效)")
    private Integer status;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;

    public String getUrl() {
        // TODO:This 完善不同数据库类型的数据库连接
        if (DbType.ORACLE.getDb().equalsIgnoreCase(this.type)){
            // return PasswordUtil.encrypt("jdbc:oracle:thin" + StringPool.COLON + StringPool.AT + StringPool.COLON + this.portNumber + StringPool.COLON + this.databaseName);
            return "jdbc:oracle:thin" + StringPool.COLON + StringPool.AT + StringPool.COLON + this.portNumber + StringPool.COLON + this.databaseName;
        }else {
            // return PasswordUtil.encrypt("jdbc:mysql://" + this.ip + StringPool.COLON + this.portNumber + StringPool.SLASH + this.databaseName + "?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false");
            return "jdbc:mysql://" + this.ip + StringPool.COLON + this.portNumber + StringPool.SLASH + this.databaseName + "?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false";
        }
    }

}
