package com.imis.module.system.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 功能菜单表 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-03-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_menu")
public class SysMenu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父记录id
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 菜单名称
     */
    @TableField("name")
    private String name;

    /**
     * 路径
     */
    @TableField("url")
    private String url;

    /**
     * 组件
     */
    @TableField("component")
    private String component;

    /**
     * 菜单类型（0：一级菜单，1：子菜单，2按钮权限）
     */
    @TableField("menu_type")
    private Integer menuType;

    /**
     * 菜单权限编码
     */
    @TableField("perms")
    private String perms;

    /**
     * 权限策略1显示2禁用
     */
    @TableField("perms_type")
    private Integer permsType;

    /**
     * 菜单排序
     */
    @TableField("sort_no")
    private Double sortNo;

    /**
     * 菜单图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 是否叶子节点:    1:是   0:不是
     */
    @TableField("leaf_type")
    private Integer leafType;

    /**
     * 菜单描述
     */
    @TableField("description")
    private String description;

    /**
     * 按钮权限状态(0无效1有效)
     */
    @TableField("status")
    private Integer status;

    /**
     * 删除状态（0，正常，1已删除）
     * 强制要求逻辑删除的注解字段要放在最后
     * @TableLogic(value = "0")
     */
    @TableField("del_flag")
    private Integer delFlag;

}
