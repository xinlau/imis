package com.imis.module.system.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 用户组织机构 VO
 *
 * @author XinLau
 * @since 2020-04-22
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="用户组织机构树对象", description="组织机构")
public class SysUserOrganizationTreeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 父级编号
     */
    @ApiModelProperty(value = "父级编号")
    private String parentId;
    /**
     * 组织机构名称
     */
    @ApiModelProperty(value = "组织机构名称")
    private String organizationName;
    /**
     * 组织机构编号
     */
    @ApiModelProperty(value = "组织机构编号")
    private String organizationCode;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 排序号
     */
    @ApiModelProperty(value = "排序号")
    private Integer sortOrder;
    /**
     * 是否负责部门
     */
    @ApiModelProperty(value = "是否负责部门（0-不负责，1-负责）")
    private Integer isResponsible;
    /**
     * 子级功能菜单
     */
    @ApiModelProperty(value = "子级组织机构树对象")
    private List<SysUserOrganizationTreeVO> childUserOrganization;

}
