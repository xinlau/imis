package com.imis.module.system.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * PagingQueryLogDTO<br>
 * 系统日志查询
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月10日 11:22
 */
@Data
@ApiModel(value="系统日志查询对象", description="系统日志查询")
@EqualsAndHashCode(callSuper = false)
public class PagingQueryLogDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志ID
     */
    @ApiModelProperty(value = "日志ID")
    private Long id;

    /**
     * 耗时
     */
    @ApiModelProperty(value = "耗时")
    private Long costTime;

    /**
     * IP
     */
    @ApiModelProperty(value = "IP地址")
    private String ip;

    /**
     * 请求参数
     */
    @ApiModelProperty(value = "请求参数")
    private String requestParam;

    /**
     * 请求类型
     */
    @ApiModelProperty(value = "请求类型")
    private String requestType;

    /**
     * 请求路径
     */
    @ApiModelProperty(value = "请求路径")
    private String requestUrl;
    /**
     * 请求方法
     */
    @ApiModelProperty(value = "请求方法")
    private String method;

    /**
     * 操作人用户名称
     */
    @ApiModelProperty(value = "操作人用户名称")
    private String username;

    /**
     * 操作人用户账户
     */
    @ApiModelProperty(value = "操作人用户账户")
    private String userId;

    /**
     * 操作详细日志
     */
    @ApiModelProperty(value = "操作详细日志")
    private String logContent;

    /**
     * 日志类型（0成功 1失败）
     */
    @ApiModelProperty(value = "日志类型（0成功 1失败）")
    private Integer logType;

    /**
     * 操作类型（@RequiresPermissions）
     */
    @ApiModelProperty(value = "操作类型（@RequiresPermissions）")
    private String operateType;

    /**
     * 错误内容
     */
    @ApiModelProperty(value = "错误内容")
    private String errorContent;

}
