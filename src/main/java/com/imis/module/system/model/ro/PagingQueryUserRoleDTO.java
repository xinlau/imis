package com.imis.module.system.model.ro;

import com.imis.base.constant.CommonConstant;
import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 分页查询用户授权角色 RO
 *
 * @author XinLau
 * @since 2020-03-12
*/
@Data
@ApiModel(value="授权角色查询对象", description="分页查询用户授权角色")
@EqualsAndHashCode(callSuper = false)
public class PagingQueryUserRoleDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户编号")
    private Long userId;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String roleName;
    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码")
    private String roleCode;
    /**
     * 角色描述
     */
    @ApiModelProperty(value = "角色描述")
    private String description;

    /**
     * 分页查询授权角色信息类型： 0-未授权 1-已授权 默认查询已授权
     */
    @ApiModelProperty(hidden = true)
    private Integer contain = CommonConstant.ROLE_GRANT;

}
