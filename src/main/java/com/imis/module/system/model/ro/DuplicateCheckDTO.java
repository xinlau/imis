package com.imis.module.system.model.ro;

import com.imis.base.constant.DuplicateCheckConstant;
import com.imis.base.util.ConvertUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 重复校验数据模型 RO
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
@Data
@ApiModel(value = "重复校验数据模型", description = "重复校验数据模型")
public class DuplicateCheckDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    public DuplicateCheckDTO() {
    }

    public DuplicateCheckDTO(String tableName, String fieldName, String fieldValue, Long dataId) {
        this.tableName = tableName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        if (ConvertUtils.isNotEmpty(dataId)){
            this.dataId = dataId;
        }
    }

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名", name = "tableName", example = DuplicateCheckConstant.TABLE_NAME_SYS_DICT)
    private String tableName;
    /**
     * 字段名
     */
    @ApiModelProperty(value = "字段名", name = "fieldName", example = DuplicateCheckConstant.FIELD_NAME_DICT_CODE)
    private String fieldName;
    /**
     * 字段值
     */
    @ApiModelProperty(value = "字段值", name = "fieldValue", example = "")
    private String fieldValue;
    /**
     * 数据ID
     */
    @ApiModelProperty(value = "数据ID", name = "dataId")
    private Long dataId;

}