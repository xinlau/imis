package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.api.model.ro.DictItemsByCodeQueryDTO;
import com.imis.module.api.model.ro.DictItemsQueryDTO;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysDict;
import com.imis.module.system.model.ro.PagingQueryDictDTO;
import com.imis.module.system.model.ro.QueryDictTreeDTO;
import com.imis.module.system.model.vo.DictVO;
import com.imis.module.system.model.vo.DropDownBoxTreeVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 字典项 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-03-23
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    /**
     * 通过查询指定code 获取字典值text
     *
     * @param code - 字段值
     * @param key  - 子表key
     * @return String
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 10:04
     */
    String queryDictTextByKey(@Param("code") final String code, @Param("key") final String key);

    /**
     * 通过查询指定表（table）的字段（text、code） 获取字典值text
     *
     * @param table - 表名
     * @param text  - 字段名
     * @param code  - 字段值
     * @param key   - 子表key
     * @return String
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/4/15 10:08
     */
    String queryTableDictTextByKey(@Param("table") final String table, @Param("text") final String text, @Param("code") final String code, @Param("key") final String key);

    /**
     * 分页查询字典项
     *
     * @param sysDictPage        - 字典项分页对象
     * @param pagingQueryDictDTO - 字典项查询对象
     * @return Page<SysDictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    Page<SysDict> pagingQuerySysDictListByParameter(@Param("pg") final Page<SysDict> sysDictPage, @Param("sysDictSelect") final PagingQueryDictDTO pagingQueryDictDTO);

    /**
     * 根据字典编码（code）获取下拉
     *
     * @param dictCode - 字典编码（code）
     * @return List<DictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    List<DictVO> queryDictItemsByDictCode(@Param("code") final String dictCode);

    /**
     * 验证表、表字段是否存在
     *
     * @param table      表名
     * @param fieldArray 字段数组
     * @return String
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    List<String> verifyTable(@Param("table") final String table, @Param("fieldArray") final String[] fieldArray);

    /**
     * 通过查询指定表（table）的 字段（text、code）进行条件SQL语句（filterSql）检索查询数据成为字典结果
     *
     * @param sysDictItemsQuery 指定表、字段的下拉查询对象
     * @return List<DictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    List<DictVO> queryTableDictItemsByParameter(@Param("parameter") final DictItemsQueryDTO sysDictItemsQuery);

    /**
     * 通过查询指定表（table）的 字段（text、code）进行指定内容（code）的检索查询数据成为字典结果
     *
     * @param sysDictItemsByCodeQuery 指定表、字段的指定内容下拉查询对象
     * @return List<DictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    List<DictVO> queryTableDictItemsByCode(@Param("parameter") final DictItemsByCodeQueryDTO sysDictItemsByCodeQuery);

    /**
     * 条件查询字典树
     *
     * @param queryDictTree - 字典树查询对象
     * @return List<DropDownBoxTreeVO> - 字典树
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/3 16:39
     */
    List<DropDownBoxTreeVO> queryDictTreeListByParameter(@Param("parameter") final QueryDictTreeDTO queryDictTree);

}