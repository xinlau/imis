package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysDictItem;
import com.imis.module.system.model.ro.PagingQueryDictItemDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 字典值 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-03-24
 */
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

    /**
     * 分页查询字典值
     *
     * @param sysDictItemPage        - 字典值分页对象
     * @param pagingQueryDictItemDTO - 字典值查询对象
     * @return Page<SysDictVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/5/12 9:55
     */
    Page<SysDictItem> pagingQuerySysDictItemListByParameter(@Param("pg") final Page<SysDictItem> sysDictItemPage, @Param("sysDictItemSelect") final PagingQueryDictItemDTO pagingQueryDictItemDTO);

}