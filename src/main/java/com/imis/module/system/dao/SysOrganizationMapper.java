package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.system.model.po.SysOrganization;
import com.imis.module.system.model.ro.PagingQueryOrganizationDTO;
import com.imis.module.system.model.vo.SysUserOrganizationVO;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysUserOrganization;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 组织机构 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-04-22
 */
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {

    /**
     * 分页查询组织机构
     *
     * @param sysOrganizationPage        - 分页参数
     * @param pagingQueryOrganizationDTO - 组织机构查询对象
     * @return Page<SysOrganization> - 分页组织机构
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    Page<SysOrganization> pagingQuerySysOrganizationParentListByParameter(@Param("pg") final Page<SysOrganization> sysOrganizationPage, @Param("sysOrganizationSelect") final PagingQueryOrganizationDTO pagingQueryOrganizationDTO);

    /**
     * 根据组织机构编号改变组织机构逻辑删除状态
     *
     * @param delFlag                    - 删除标志  1已删除 0未删除
     * @param organizationIdentification - 组织机构标识
     * @return int - 返回值
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    int changeOrganizationDelFlagByIdentification(@Param("delFlag") final Integer delFlag, @Param("identification") final Long organizationIdentification);

    /**
     * 根据组织机构编号修改组织机构信息
     *
     * @param sysOrganization - 组织机构对象
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    int updateByOrganizationIdentification(@Param("sysOrganization") final SysOrganization sysOrganization);

    /**
     * 根据用户ID查询用户拥有的组织机构
     *
     * @param userId - 用户ID
     * @return List<SysUserOrganizationVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<SysUserOrganizationVO> queryOrganizationByUserIdentification(@Param("userId") final Long userId);

    /**
     * 清除用户授权组织机构
     *
     * @param userId - 用户编号
     * @return int -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    int deleteGrantByUserId(@Param("userId") final Long userId);

    /**
     * 用户授权组织机构
     *
     * @param sysUserOrganizationList - 用户组织机构关联
     * @return int -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    int grantOrganizationToUser(@Param("grant") final List<SysUserOrganization> sysUserOrganizationList);

    /**
     * 根据组织机构标识查询用户编号
     *
     * @param organizationIdentification - 组织机构标识
     * @return List<Long> 用户编号
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<Long> queryUserIdentificationByOrganizationIdentification(@Param("identification")final Long organizationIdentification);

    /**
     * 根据组织机构名称获取下拉
     *
     * @param organizationName - 组织机构名称
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    List<SysOrganization> queryOrganizationTreeByOrganizationName(@Param("organizationName")final String organizationName);

}