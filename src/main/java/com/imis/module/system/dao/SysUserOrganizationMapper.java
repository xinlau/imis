package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.system.model.po.SysUser;
import com.imis.module.system.model.po.SysUserOrganization;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.ro.PagingQueryUserByOrganizationIdDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户-组织机构关联表 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
public interface SysUserOrganizationMapper extends BaseMapper<SysUserOrganization> {

    /**
     * 查询组织机构下的用户接口
     * @param sysUserPage - 用户查询对象
     * @param pagingQueryUserByOrganizationIdDTO - 根据组织机构查询用户对象
     * @return Page<SysUserVO> -
     * @author XinLau
     * @since 2020/7/18 16:23
     * @creed The only constant is change ! ! !
     */
    Page<SysUser> pagingQueryUserByOrganizationIdentification(@Param("pg") final Page<SysUser> sysUserPage, @Param("sysUserSelect") final PagingQueryUserByOrganizationIdDTO pagingQueryUserByOrganizationIdDTO);

    /**
     * 查询组织机构下没有的用户接口
     * @param sysUserPage - 用户查询对象
     * @param pagingQueryUserByOrganizationIdDTO - 根据组织机构查询用户对象
     * @return Page<SysUserVO> -
     * @author XinLau
     * @since 2020/7/18 16:23
     * @creed The only constant is change ! ! !
     */
    Page<SysUser> pagingQueryUnauthorizedUser(@Param("pg") final Page<SysUser> sysUserPage, @Param("sysUserSelect") final PagingQueryUserByOrganizationIdDTO pagingQueryUserByOrganizationIdDTO);

    /**
     * 组织机构分配用户
     * @param sysUserOrganizationList - 用户组织机构关联
     * @return int -
     * @author XinLau
     * @since 2020/7/20 9:45
     * @creed The only constant is change ! ! !
     */
    int authorizingUserToOrganization(@Param("authorizing") final List<SysUserOrganization> sysUserOrganizationList);

    /**
     * 组织机构取消分配用户
     * @param sysUserOrganizationList - 用户组织机构关联
     * @return int -
     * @author XinLau
     * @since 2020/7/20 9:45
     * @creed The only constant is change ! ! !
     */
    int unAuthorizingUserToOrganization(@Param("unAuthorizing") final List<SysUserOrganization> sysUserOrganizationList);

}