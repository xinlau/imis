package com.imis.module.system.dao;

import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysFile;
import com.imis.module.system.model.ro.DuplicateCheckDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 文件存放 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-03-27
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

    /**
     * 数据重复校验
     *
     * @param duplicateCheck - 重复校验数据模型
     * @return Long - 数量
     * @author XinLau
     * @since 2020/6/29 18:03
     * @creed The only constant is change ! ! !
     */
    Long doDuplicateCheck(@Param("parameter")final DuplicateCheckDTO duplicateCheck);

    /**
     * 批量保存文件
     *
     * @param sysFileList - 文件
     * @return int -
     * @author XinLau
     * @since 2020/6/29 18:03
     * @creed The only constant is change ! ! !
     */
    int saveBatchFile(@Param("parameter")final List<SysFile> sysFileList);

}