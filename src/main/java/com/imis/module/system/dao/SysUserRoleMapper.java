package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.system.model.po.SysUser;
import com.imis.module.system.model.po.SysUserRole;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.ro.PagingQueryUserByRoleIdDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    /**
     * 清除用户授权角色
     *
     * @param userId - 用户编号
     * @return int -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    int deleteGrantByUserId(@Param("userId") final Long userId);

    /**
     * 为用户授权角色
     *
     * @param sysUserRoleList - 用户授权角色
     * @return int -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/19 11:10
     */
    int grantRoleToUser(@Param("grant") final List<SysUserRole> sysUserRoleList);

    /**
     * 根据角色标识查看用户
     *
     * @param sysUserPage - 分页参数
     * @param pagingQueryUserByRoleIdDTO - 根据角色查询用户对象
     * @return Page<SysUser> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:26
     */
    Page<SysUser> pagingQuerySysUserListByRoleIdParameter(@Param("pg") final Page<SysUser> sysUserPage, @Param("sysUserSelect") final PagingQueryUserByRoleIdDTO pagingQueryUserByRoleIdDTO);

    /**
     * 根据角色标识查看未授权用户
     *
     * @param sysUserPage - 分页参数
     * @param pagingQueryUserByRoleIdDTO - 根据角色查询用户对象
     * @return Page<SysUser> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:26
     */
    Page<SysUser> pagingQueryUnauthorizedSysUserListByRoleIdParameter(@Param("pg") final Page<SysUser> sysUserPage, @Param("sysUserSelect") final PagingQueryUserByRoleIdDTO pagingQueryUserByRoleIdDTO);

}