package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysMenu;
import com.imis.module.system.model.ro.PagingQueryMenuDTO;
import com.imis.module.system.model.ro.QueryMenuDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 功能菜单表 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-03-17
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 分页查询功能菜单
     *
     * @param sysMenuPage        - 分页参数
     * @param pagingQueryMenuDTO - 功能菜单查询对象
     * @return Page<SysMenu> - 分页功能菜单
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    Page<SysMenu> pagingQuerySysMenuParentListByParameter(@Param("pg") final Page<SysMenu> sysMenuPage, @Param("sysMenuSelect") final PagingQueryMenuDTO pagingQueryMenuDTO);

    /**
     * 查询功能菜单树
     *
     * @param queryMenuDTO - 功能菜单查询对象
     * @return List<SysMenu>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/17 14:54
     */
    List<SysMenu> querySysMenuListByParameter(@Param("sysMenuSelect")final QueryMenuDTO queryMenuDTO);

    /**
     * 根据功能菜单编号查询角色编号
     *
     * @param menuIdentification - 功能菜单标识
     * @return Long[]
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<Long> selectSysRoleIdArrayByMenuId(@Param("menuId") final Long menuIdentification);

    /**
     * 根据功能菜单编号改变功能菜单逻辑删除状态
     *
     * @param delFlag        - 删除标志  1已删除 0未删除
     * @param identification - 功能菜单标识
     * @return int - 返回值
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    int changeMenuDelFlagByIdentification(@Param("delFlag") final Integer delFlag, @Param("identification") final Long identification);

    /**
     * 根据功能菜单编号修改功能菜单信息
     *
     * @param sysMenu - 功能菜单对象
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    int updateByMenuIdentification(@Param("sysMenu") final SysMenu sysMenu);

    /**
     * 查询指定角色的菜单集合
     *
     * @param roleIdListByUserId - 角色编号
     * @return List<SysMenu>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/23 11:04
     */
    List<SysMenu> querySysMenuTreeListByRoleIdList(@Param("roleIdList") final List<Long> roleIdListByUserId);

    /**
     * 查询指定角色的菜单集合
     *
     * @param roleIdentification - 角色编号
     * @return List<SysMenu>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/23 11:04
     */
    List<SysMenu> querySysMenuTreeListByRoleId(@Param("roleId") final Long roleIdentification);

    /**
     * 根据功能菜单名称获取功能菜单集合
     *
     * @param menuName - 功能菜单名称
     * @return List<SysMenu>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/23 11:04
     */
    List<SysMenu> queryMenuTreeByMenuName(@Param("menuName") final String menuName);

}