package com.imis.module.system.dao;

import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysRichText;

/**
 * <p>
 * 富文本 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
public interface SysRichTextMapper extends BaseMapper<SysRichText> {

}