package com.imis.module.system.dao;

import com.imis.module.system.model.po.SysRoleMenu;
import com.imis.module.base.BaseMapper;

/**
 * <p>
 * 角色-菜单权限关联表 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-06-18
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}