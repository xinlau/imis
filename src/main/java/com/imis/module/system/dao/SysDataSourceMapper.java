package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysDataSource;
import com.imis.module.system.model.ro.PagingQueryDataSourceDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统数据源管理 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-12-30
 */
public interface SysDataSourceMapper extends BaseMapper<SysDataSource> {

    /**
     * 分页查询数据源信息
     *
     * @param sysDataSourcePage     - 分页数据源信息
     * @param pagingQueryDataSource - 系统数据源查询对象
     * @return Page<SysDataSourceVO> - 分页数据源信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 15:47
     */
    Page<SysDataSource> pagingQueryDataSourceListByParameter(@Param("pg") final Page<SysDataSource> sysDataSourcePage, @Param("datasource") final PagingQueryDataSourceDTO pagingQueryDataSource);

}