package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysRole;
import com.imis.module.system.model.po.SysRoleMenu;
import com.imis.module.system.model.ro.PagingQueryRoleDTO;
import com.imis.module.system.model.ro.PagingQueryUserRoleDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统角色表-权限组 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 分页查询角色信息
     *
     * @param sysRolePage        - 分页参数
     * @param pagingQueryRoleDTO - 角色查询对象
     * @return Page<SysRole> - 角色信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Page<SysRole> pagingQuerySysRoleListByParameter(@Param("pg") final Page<SysRole> sysRolePage, @Param("sysRoleSelect") final PagingQueryRoleDTO pagingQueryRoleDTO);

    /**
     * 清除角色权限信息
     *
     * @param roleIdentification - 角色标识
     * @return boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:30
     */
    int cleanRolePermissionsById(@Param("roleId") final Long roleIdentification);

    /**
     * 根据角色编号查询用户编号
     *
     * @param roleIdentification - 角色标识
     * @return List<Long>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    List<Long> selectSysUserIdArrayByRoleId(@Param("roleId") final Long roleIdentification);

    /**
     * 根据用户编号查询角色编号
     *
     * @param userIdentification - 用户标识
     * @return List<Long> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/23 11:52
     */
    List<Long> selectSysRoleIdArrayByUserId(@Param("userId") final Long userIdentification);

    /**
     * 根据用户名查询角色编号
     *
     * @param userName - 用户名
     * @return List<Long> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/23 11:52
     */
    List<Long> selectSysRoleIdArrayByUserName(@Param("userName") final String userName);

    /**
     * 分页查询授权角色信息
     *
     * @param sysRolePage       - 分页参数
     * @param sysUserRoleSelect - 授权角色查询对象
     * @return Page<SysRole> - 角色信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/16 09:30
     */
    Page<SysRole> pagingQueryRoleListByParameter(@Param("pg") final Page<SysRole> sysRolePage, @Param("sysRoleSelect") final PagingQueryUserRoleDTO sysUserRoleSelect);

    /**
     * 为角色授权功能菜单权限
     *
     * @param sysRoleMenuList - 角色-菜单权限关联
     * @return int - 影响行数
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/21 13:37
     */
    int grantMenuPermissionsToRole(@Param("grant") final List<SysRoleMenu> sysRoleMenuList);

    /**
     * 根据用户编号查询角色信息
     *
     * @param userIdentification - 用户标识
     * @return List<SysRole> - 角色信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/29 11:52
     */
    List<SysRole> selectSysRoleListByUserId(@Param("userId") final Long userIdentification);

}