package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.api.model.ro.UserRegisterDTO;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysMenu;
import com.imis.module.system.model.po.SysUser;
import com.imis.module.system.model.ro.PagingQueryUserDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 重复注册验证
     *
     * @param register - 用户信息注册对象
     * @return List<SysUser> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    List<SysUser> duplicateRegistrationVerification(@Param("register") final UserRegisterDTO register);

    /**
     * 分页查询用户信息
     *
     * @param sysUserPage        - 分页查询对象
     * @param pagingQueryUserDTO - 用户信息查询对象
     * @return Page<SysUser> - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    Page<SysUser> pagingQuerySysUserListByParameter(@Param("pg") final Page<SysUser> sysUserPage, @Param("sysUserSelect") final PagingQueryUserDTO pagingQueryUserDTO);

    /**
     * 根据用户账号查询用户信息
     *
     * @param username - 用户账号
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/6 16:08
     */
    SysUser queryUserByName(@Param("username") final String username);

    /**
     * 根据用户标识符解、冻结用户
     *
     * @param status         - 状态(1：正常  2：冻结 ）
     * @param identification - 用户唯一标识
     * @return int - 返回值
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/5 17:25
     */
    int changeUserStatusByIdentification(@Param("status") final Integer status, @Param("identification") final Long identification);

    /**
     * 通过用户编号获取用户角色集合
     *
     * @param userId - 用户编号
     * @return Set<String> - 用户角色集合
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    List<String> queryRoleIdListByUserId(@Param("userId") final Long userId);

    /**
     * 通过角色编号获取功能菜单集合
     *
     * @param roleIdList - 角色编号
     * @return List<SysMenu> - 功能菜单集合
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    List<SysMenu> queryPermissionListByRoleIdArray(@Param("roleList") final List<Long> roleIdList);

    /**
     * 根据手机号查询用户信息
     *
     * @param phone - 手机号
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/21 16:00
     */
    SysUser queryUserByPhone(@Param("phone") final String phone);

    /**
     * 根据邮箱查询用户信息
     *
     * @param email - 邮箱
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/21 16:00
     */
    SysUser queryUserByEmail(@Param("email") final String email);

    /**
     * 修改密码
     *
     * @param username   - 用户名
     * @param encrypt    - 新密码
     * @param stringSalt - 密码盐
     * @return int - 影响行数
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 17:25
     */
    int changeUserPassword(@Param("username") final String username, @Param("encrypt") final String encrypt, @Param("stringSalt") final String stringSalt);

}
