package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysWebsocketMessage;
import com.imis.module.system.model.ro.PagingQueryWebsocketMessageDTO;
import com.imis.module.system.model.vo.SysWebsocketMessageInfoVO;
import com.imis.module.websocket.dto.PagingQueryMessageDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * WebSocket信息传递记录 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-07-24
 */
public interface SysWebsocketMessageMapper extends BaseMapper<SysWebsocketMessage> {

    /**
     * 分页查询WebSocket信息接口
     * @param sysWebsocketMessagePage - WebSocket信息分页对象
     * @param pagingQueryWebsocketMessageDTO - WebSocket信息传递记录查询对象
     * @return Page<SysWebsocketMessage> -
     * @author XinLau
     * @since 2020/7/24 15:30
     * @creed The only constant is change ! ! !
     */
    Page<SysWebsocketMessage> pagingQuerySysWebsocketListByParameter(@Param("pg")final Page<SysWebsocketMessage> sysWebsocketMessagePage, @Param("sysWebsocketSelect")final PagingQueryWebsocketMessageDTO pagingQueryWebsocketMessageDTO);

    /**
     * WebSocket信息查看接口
     *
     * @param websocketIdentification - WebSocket信息传递记录标识
     * @return SysWebsocketMessageInfoVO -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:48
     */
    SysWebsocketMessageInfoVO queryWebsocketById(@Param(DataBaseConstant.P_KEY)final Long websocketIdentification);

    /**
     * 分页查询WebSocket历史信息接口
     *
     * @param sysWebsocketMessagePage - 分页查询WebSocket信息
     * @param pagingQueryMessageDTO - 分页WebSocket信息对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/24 14:39
     */
    Page<SysWebsocketMessageInfoVO> pagingQueryHistoricalMassages(@Param("pg")final Page<SysWebsocketMessageInfoVO> sysWebsocketMessagePage, @Param("sysWebsocketSelect")final PagingQueryMessageDTO pagingQueryMessageDTO);

    /**
     * 批量保存WebSocket信息
     *
     * @param sysWebsocketMessageList - WebSocket信息
     * @return int -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/25 11:10
     */
    int saveBatchSysWebsocketMessage(@Param("sysWebsocket")final List<SysWebsocketMessage> sysWebsocketMessageList);

}