package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.system.model.po.SysQuartzJob;
import com.imis.module.system.model.ro.PagingQueryQuartzJobDTO;
import com.imis.module.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 定时任务 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
public interface SysQuartzJobMapper extends BaseMapper<SysQuartzJob> {

    /**
     * 分页查询定时任务
     *
     * @param sysQuartzJobPage - 定时任务分页对象
     * @param pagingQueryQuartzJobDTO - 定时任务查询对象
     * @return Page<SysQuartzJob> - 分页定时任务
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/6/16 08:54
     */
    Page<SysQuartzJob> pagingQuerySysQuartzJobListByParameter(@Param("pg")final Page<SysQuartzJob> sysQuartzJobPage, @Param("sysQuartzJobSelect")final PagingQueryQuartzJobDTO pagingQueryQuartzJobDTO);

}