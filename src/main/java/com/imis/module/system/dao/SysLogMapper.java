package com.imis.module.system.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.base.BaseMapper;
import com.imis.module.system.model.po.SysLog;
import com.imis.module.system.model.ro.PagingQueryLogDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统日志表 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

    /**
     * 分页查询系统日志
     *
     * @param sysLogPage        - 分页参数
     * @param pagingQueryLogDTO - 查询参数
     * @return Page<SysLog> - 系统日志分页
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 10:51
     */
    Page<SysLog> pagingQuerySysLogListByParameter(@Param("pg") final Page<SysLog> sysLogPage, @Param("sysLogSelect") final PagingQueryLogDTO pagingQueryLogDTO);

}
