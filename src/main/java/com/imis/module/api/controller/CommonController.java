package com.imis.module.api.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.api.bus.CommonBus;
import com.imis.module.api.model.ro.SendEmailDTO;
import com.imis.module.api.model.ro.SendSmsDTO;
import com.imis.module.api.model.vo.VerificationCodeVO;
import com.imis.module.base.BaseController;
import com.imis.module.system.model.vo.SysFileVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * CommonController<br>
 * 常用接口相关 前端控制器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月26日 11:11
 */
@Slf4j
@Controller
@RequestMapping(path = "/sys/common")
@Api(tags = {"常用接口"})
@ApiSort(2)
public class CommonController extends BaseController<CommonBus> {

    @ResponseBody
    @GetMapping(path = "/captcha.png")
    @ApiOperation(value = "获取验证码图片接口", notes = "获取验证码")
    @ApiImplicitParam(name = "type", value = "char - 字符验证码；operation - 运算验证码；slide - 滑块验证码", dataType = "String", dataTypeClass = String.class, required = false)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<VerificationCodeVO> getVerificationCode(@RequestParam(name = "type", required = false, defaultValue = "operation") String type) {
        return service.getVerificationCode(type);
    }

    @ResponseBody
    @GetMapping(path = "/sendSms")
    @ApiOperation(value = "获取短信验证码接口", notes = "获取短信验证码")
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse sendSms(@Valid SendSmsDTO sendSms) {
        return service.sendSms(sendSms);
    }

    @ResponseBody
    @GetMapping(path = "/sendEmail")
    @ApiOperation(value = "获取邮箱验证码接口", notes = "获取邮箱验证码")
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse sendEmail(@Valid SendEmailDTO sendEmail) {
        return service.sendEmail(sendEmail);
    }

    @ResponseBody
    @GetMapping(path = "/json/{fileName}")
    @ApiOperation(value = "获取JSON文件内容", notes = "获取JSON文件内容")
    @ApiImplicitParam(name = "fileName", value = "JSON文件名称", dataType = "String", dataTypeClass = String.class, required = true)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public CommonResponse<String> getJsonData(@PathVariable(name = "fileName", required = true) String fileName) {
        return service.getJsonData(fileName);
    }

    @ResponseBody
    @PostMapping(path ="/upload" )
    @ApiOperation(value = "文件上传接口", notes = "使用Spring文件上传")
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<List<SysFileVO>> doFileUpload(MultipartHttpServletRequest multipartHttpServletRequest) {
        return service.doFileUpload(multipartHttpServletRequest);
    }

    @GetMapping(path = "/download")
    @ApiOperation(value = "文件下载接口", notes = "跨服务器文件下载")
    @ApiImplicitParam(name = "fileId", value = "文件编号", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public BaseResponse doFileDownload(@RequestParam(name = "fileId", required = true) Long fileId) {
        return service.doFileDownload(fileId);
    }

    @GetMapping(path = "/preview")
    @ApiOperation(value = "文件在线预览接口", notes = "文件在线预览接口")
    @ApiImplicitParam(name = "fileId", value = "文件编号", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public void doOnlinePreview(@RequestParam(name = "fileId", required = true) Long fileId) throws IOException {
        service.doOnlinePreview(fileId);
    }

}
