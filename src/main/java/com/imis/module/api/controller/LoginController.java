package com.imis.module.api.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.base.shiro.UserInfo;
import com.imis.module.api.bus.LoginBus;
import com.imis.module.api.model.ro.LoginDTO;
import com.imis.module.api.model.ro.PhoneLoginDTO;
import com.imis.module.api.model.ro.UserChangePasswordDTO;
import com.imis.module.api.model.ro.UserRegisterDTO;
import com.imis.module.base.BaseController;
import com.imis.module.system.model.vo.SysMenuTreeVO;
import com.imis.module.system.model.vo.SysUserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * LoginController<br>
 * 用户登录模块
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年04月15日 11:54
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys")
@Api(tags = {"登录模块"})
@ApiSort(1)
public class LoginController extends BaseController<LoginBus> {

    @PostMapping(path = "/register")
    @ApiOperation(value = "用户注册接口", notes = "注册")
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<SysUserVO>  register(@RequestBody @Valid UserRegisterDTO register) {
        return service.register(register);
    }

    @PostMapping(path = "/login")
    @ApiOperation(value = "用户登录接口", notes = "登录")
    @ApiOperationSupport(order = 2, author = "XinLau")
    public CommonResponse<UserInfo> login(@RequestBody @Valid LoginDTO login) {
        return service.login(login);
    }

    @PostMapping(path = "/phoneLogin")
    @ApiOperation(value = "手机登录接口", notes = "登录")
    @ApiOperationSupport(order = 3, author = "XinLau")
    public CommonResponse<UserInfo> loginByPhone(@RequestBody @Valid PhoneLoginDTO phoneLogin) {
        return service.loginByPhone(phoneLogin);
    }

    @PutMapping(path = "/passwordChange")
    @ApiOperation(value = "修改密码接口", notes = "用户自助修改密码")
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse passwordChange(@RequestBody @Valid UserChangePasswordDTO changePassword) {
        return service.passwordChange(changePassword);
    }

    @GetMapping(path = "/querySysMenuTreeListByCurrentUser")
    @ApiOperation(value = "获取功能菜单", notes = "菜单树")
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<List<SysMenuTreeVO>> querySysMenuTreeListByCurrentUser() {
        return service.querySysMenuTreeListByCurrentUser();
    }

}
