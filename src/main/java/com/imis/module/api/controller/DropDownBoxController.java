package com.imis.module.api.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.api.bus.DropDownBoxBus;
import com.imis.module.api.model.ro.DictItemsByCodeQueryDTO;
import com.imis.module.api.model.ro.DictItemsQueryDTO;
import com.imis.module.api.model.vo.ClassParameter;
import com.imis.module.base.BaseController;
import com.imis.module.system.model.ro.QueryDictTreeDTO;
import com.imis.module.system.model.vo.DictVO;
import com.imis.module.system.model.vo.DropDownBoxTreeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * DropDownBoxController<br>
 * 下拉框接口相关 前端控制器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年05月29日 10:39
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/selectBox")
@Api(tags = {"下拉接口"})
@ApiSort(3)
public class DropDownBoxController extends BaseController<DropDownBoxBus> {

    @GetMapping(path = "/queryDictItemsByDictCode")
    @ApiOperation(value = "根据字典编码（code）获取下拉", notes = "根据字典编码（code）获取下拉")
    @ApiImplicitParam(name = "dictCode", value = "字典编码（code）", dataType = "String", dataTypeClass = String.class, required = true)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<List<DictVO>> queryDictItemsByDictCode(@RequestParam(name = "dictCode", required = true) String dictCode) {
        return service.queryDictItemsByDictCode(dictCode);
    }

    @GetMapping(path = "/queryTableDictItemsByParameter")
    @ApiOperation(value = "查询指定表、字段的下拉", notes = "通过查询指定表（table）的 字段（text、code）进行条件SQL语句（filterSql）检索查询数据成为字典结果")
    @ApiOperationSupport(order = 2, author = "XinLau")
    public CommonResponse<List<DictVO>> queryTableDictItemsByParameter(@RequestBody @Valid DictItemsQueryDTO sysDictItemsQuery) {
        return service.queryTableDictItemsByParameter(sysDictItemsQuery);
    }

    @GetMapping(path = "/queryTableDictItemsByCode")
    @ApiOperation(value = "查询指定表、字段的指定内容（code）的下拉", notes = "通过查询指定表（table）的 字段（text、code）进行指定内容（code）的检索查询数据成为字典结果")
    @ApiOperationSupport(order = 3, author = "XinLau")
    public CommonResponse<List<DictVO>> queryTableDictItemsByCode(@RequestBody @Valid DictItemsByCodeQueryDTO sysDictItemsByCodeQuery) {
        return service.queryTableDictItemsByCode(sysDictItemsByCodeQuery);
    }

    @GetMapping(path = "/queryOrganizationTreeByName")
    @ApiOperation(value = "根据组织机构名称获取下拉", notes = "根据组织机构名称获取下拉")
    @ApiImplicitParam(name = "organizationName", value = "组织机构名称", dataType = "String", dataTypeClass = String.class, required = false)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public CommonResponse<List<DropDownBoxTreeVO>> queryOrganizationTreeByOrganizationName(@RequestParam(name = "organizationName", required = false) String organizationName) {
        return service.queryOrganizationTreeByOrganizationName(organizationName);
    }

    @GetMapping(path = "/queryMenuTreeByMenuName")
    @ApiOperation(value = "根据功能菜单名称获取下拉", notes = "根据功能菜单名称获取下拉")
    @ApiImplicitParam(name = "menuName", value = "功能菜单名称", dataType = "String", dataTypeClass = String.class, required = false)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<List<DropDownBoxTreeVO>> queryMenuTreeByMenuName(@RequestParam(name = "menuName", required = false) String menuName) {
        return service.queryMenuTreeByMenuName(menuName);
    }

    @PostMapping(path = "/queryDictTreeListByParameter")
    @ApiOperation(value = "条件查询字典树下拉", notes = "条件查询字典树下拉")
    @ApiOperationSupport(order = 6, author = "XinLau")
    public CommonResponse<List<DropDownBoxTreeVO>> queryDictTreeListByParameter(@RequestBody QueryDictTreeDTO queryDictTree) {
        return service.queryDictTreeListByParameter(queryDictTree);
    }

    @GetMapping(path = "/queryQuartzJobClassName")
    @ApiOperation(value = "获取定时任务的Class类名", notes = "获取定时任务的Class类名")
    @ApiOperationSupport(order = 7, author = "XinLau")
    public CommonResponse<List<String>> queryQuartzJobClassName() {
        return service.queryQuartzJobClassName();
    }

    @GetMapping(path = "/queryQuartzJobParameterName")
    @ApiOperation(value = "获取定时任务Class类的参数信息", notes = "获取定时任务Class类的参数信息")
    @ApiImplicitParam(name = "className", value = "定时任务Class类名", dataType = "String", dataTypeClass = String.class, required = true)
    @ApiOperationSupport(order = 8, author = "XinLau")
    public CommonResponse<List<ClassParameter>> queryQuartzJobParameterName(@RequestParam(name = "className", required = true) String className) {
        return service.queryQuartzJobParameterName(className);
    }

    @GetMapping(path = "/queryPermissionsValueConstantField")
    @ApiOperation(value = "获取请求权限常量类内的常量", notes = "获取请求权限常量类内的常量")
    @ApiImplicitParam(name = "modeName", value = "模块名关键字", dataType = "String", dataTypeClass = String.class, required = false)
    @ApiOperationSupport(order = 9, author = "XinLau")
    public CommonResponse<List<String>> queryPermissionsValueConstantField(@RequestParam(name = "modeName", required = false) String modeName) {
        return service.queryPermissionsValueConstantField(modeName);
    }

    @GetMapping(path = "/queryDictCodeConstantField")
    @ApiOperation(value = "获取DictCode常量", notes = "获取DictCode常量")
    @ApiOperationSupport(order = 10, author = "XinLau")
    public CommonResponse<List<String>> queryDictCodeConstantField() {
        return service.queryDictCodeConstantField();
    }

    @GetMapping(path = "/queryDataBaseType")
    @ApiOperation(value = "获取数据库类型常量", notes = "获取数据库类型常量")
    @ApiOperationSupport(order = 11, author = "XinLau")
    public CommonResponse<List<DictVO>> queryDataBaseType() {
        return service.queryDataBaseType();
    }

    @GetMapping(path = "/queryMultipleDataSourcesMethodName")
    @ApiOperation(value = "获取多数据源方法名常量", notes = "获取多数据源方法名常量")
    @ApiOperationSupport(order = 12, author = "XinLau")
    public CommonResponse<List<String>> queryMultipleDataSourcesMethodName() {
        return service.queryMultipleDataSourcesMethodName();
    }

    // 查询所有用户  作为字典信息 username -->value,realname -->text

}
