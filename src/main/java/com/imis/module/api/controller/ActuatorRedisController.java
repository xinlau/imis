package com.imis.module.api.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.api.bus.ActuatorRedisBus;
import com.imis.module.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * DuplicateCheckController<br>
 * 数据重复校验 前端控制器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年06月30日 09:02
 */
@Slf4j
@RestController
@RequestMapping(path = "/actuator/redis")
@Api(tags = {"Redis性能监控"})
@ApiSort(5)
public class ActuatorRedisController extends BaseController<ActuatorRedisBus> {

    @GetMapping(path = "/info")
    @ApiOperation(value = "获取Redis服务信息", notes = "获取Redis服务信息")
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<?> getRedisServeRedisInfo(){
        return service.getRedisServeInfo();
    }

    @GetMapping(path = "/keysSize")
    @ApiOperation(value = "获取RedisKey实时数量信息", notes = "获取RedisKey实时数量信息")
    @ApiOperationSupport(order = 2, author = "XinLau")
    public CommonResponse<?> getRedisServeKeysSize(){
        return service.getRedisServeKeysSize();
    }

    @GetMapping(path = "/memoryInfo")
    @ApiOperation(value = "获取Redis内存实时占用情况信息", notes = "获取Redis内存实时占用情况信息")
    @ApiOperationSupport(order = 3, author = "XinLau")
    public CommonResponse<?> getRedisServeMemoryInfo(){
        return service.getRedisServeMemoryInfo();
    }

    @GetMapping(path = "/serverMemoryInfo")
    @ApiOperation(value = "获取服务器磁盘占用情况信息", notes = "获取服务器磁盘占用情况信息")
    @ApiOperationSupport(order = 4, author = "XinLau")
    public CommonResponse<?> getServerMemoryInfo(){
        return service.getServerMemoryInfo();
    }

}
