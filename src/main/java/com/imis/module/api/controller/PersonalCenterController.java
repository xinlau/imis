package com.imis.module.api.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.api.bus.PersonalCenterBus;
import com.imis.module.api.model.ro.UserUpdateBasicInformationDTO;
import com.imis.module.api.model.ro.UserUpdateEmailAddressDTO;
import com.imis.module.api.model.ro.UserUpdatePhoneNumberDTO;
import com.imis.module.base.BaseController;
import com.imis.module.system.model.vo.SysUserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * PersonalCenterController<br>
 * 个人中心 前端控制器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月23日 11:14
 */
@Slf4j
@RestController
@RequestMapping(path = "/personal/center")
@Api(tags = {"个人中心"})
@ApiSort(6)
public class PersonalCenterController extends BaseController<PersonalCenterBus> {

    @GetMapping(path = "/queryBasicInformation")
    @ApiOperation(value = "查看基本信息", notes = "查看基本信息")
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<SysUserVO> queryBasicInformation() {
        return service.queryBasicInformation();
    }

    @PutMapping(path = "/updateBasicInformation")
    @ApiOperation(value = "更新基本信息", notes = "更新基本信息")
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse updateBasicInformation(@RequestBody @Valid UserUpdateBasicInformationDTO updateBasicInformation) {
        return service.updateBasicInformation(updateBasicInformation);
    }

    @PutMapping(path = "/updatePhoneNumber")
    @ApiOperation(value = "更新密保手机", notes = "更新密保手机")
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse updatePhoneNumber(@RequestBody @Valid UserUpdatePhoneNumberDTO updatePhoneNumber) {
        return service.updatePhoneNumber(updatePhoneNumber);
    }

    @PutMapping(path = "/updateEmailAddress")
    @ApiOperation(value = "更新电子邮箱", notes = "更新电子邮箱")
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse updateEmailAddress(@RequestBody @Valid UserUpdateEmailAddressDTO updateEmailAddress) {
        return service.updateEmailAddress(updateEmailAddress);
    }

}
