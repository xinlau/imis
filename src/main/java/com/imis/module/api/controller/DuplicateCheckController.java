package com.imis.module.api.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.module.api.bus.DuplicateCheckBus;
import com.imis.module.base.BaseController;
import com.imis.module.system.model.ro.DuplicateCheckDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * DuplicateCheckController<br>
 * 数据重复校验 前端控制器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年06月30日 09:02
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/duplicateCheck")
@Api(tags = {"数据重复校验"})
@ApiSort(4)
public class DuplicateCheckController extends BaseController<DuplicateCheckBus> {

    @PostMapping(path = "/doDuplicateCheck")
    @ApiOperation(value = "数据重复校验接口", notes = "数据重复校验")
    @ApiOperationSupport(order = 1, author = "XinLau")
    public BaseResponse doDuplicateCheck(@RequestBody DuplicateCheckDTO duplicateCheck) {
        return service.doDuplicateCheck(duplicateCheck);
    }

    @GetMapping(path = "/checkUserName")
    @ApiOperation(value = "登录账号重复校验接口", notes = "登录账号重复校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "登录账号", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "主键", dataType = "Long", dataTypeClass = Long.class, required = false)
    })
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse checkUserName(@RequestParam(name = "userName", required = true) String userName, @RequestParam(name = DataBaseConstant.P_KEY, required = false) Long id) {
        return service.checkUserName(userName, id);
    }

    @GetMapping(path = "/checkUserPhone")
    @ApiOperation(value = "手机号重复校验接口", notes = "手机号重复校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "主键", dataType = "Long", dataTypeClass = Long.class, required = false)
    })
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse checkUserPhone(@RequestParam(name = "phone", required = true) String phone, @RequestParam(name = DataBaseConstant.P_KEY, required = false) Long id) {
        return service.checkUserPhone(phone, id);
    }

    @GetMapping(path = "/checkUserEmail")
    @ApiOperation(value = "邮箱重复校验接口", notes = "邮箱重复校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "邮箱", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "主键", dataType = "Long", dataTypeClass = Long.class, required = false)
    })
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse checkUserEmail(@RequestParam(name = "email", required = true) String email, @RequestParam(name = DataBaseConstant.P_KEY, required = false) Long id) {
        return service.checkUserEmail(email, id);
    }

    @GetMapping(path = "/checkRoleName")
    @ApiOperation(value = "角色名称重复校验接口", notes = "角色名称重复校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleName", value = "角色名称", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "主键", dataType = "Long", dataTypeClass = Long.class, required = false)
    })
    @ApiOperationSupport(order = 5, author = "XinLau")
    public BaseResponse checkRoleName(@RequestParam(name = "roleName", required = true) String roleName, @RequestParam(name = DataBaseConstant.P_KEY, required = false) Long id) {
        return service.checkRoleName(roleName, id);
    }

    @GetMapping(path = "/checkRoleCode")
    @ApiOperation(value = "角色编码重复校验接口", notes = "角色编码重复校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleCode", value = "角色编码", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "主键", dataType = "Long", dataTypeClass = Long.class, required = false)
    })
    @ApiOperationSupport(order = 6, author = "XinLau")
    public BaseResponse checkRoleCode(@RequestParam(name = "roleCode", required = true) String roleCode, @RequestParam(name = DataBaseConstant.P_KEY, required = false) Long id) {
        return service.checkRoleCode(roleCode, id);
    }

    @GetMapping(path = "/checkMenuName")
    @ApiOperation(value = "功能菜单名称重复校验接口", notes = "功能菜单名称重复校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "menuName", value = "功能菜单名称", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "主键", dataType = "Long", dataTypeClass = Long.class, required = false)
    })
    @ApiOperationSupport(order = 7, author = "XinLau")
    public BaseResponse checkMenuName(@RequestParam(name = "menuName", required = true) String menuName, @RequestParam(name = DataBaseConstant.P_KEY, required = false) Long id) {
        return service.checkMenuName(menuName, id);
    }

    @GetMapping(path = "/checkOrganizationName")
    @ApiOperation(value = "组织机构名称重复校验接口", notes = "组织机构名称重复校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "organizationName", value = "组织机构名称", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "主键", dataType = "Long", dataTypeClass = Long.class, required = false)
    })
    @ApiOperationSupport(order = 8, author = "XinLau")
    public BaseResponse checkOrganizationName(@RequestParam(name = "organizationName", required = true) String organizationName, @RequestParam(name = DataBaseConstant.P_KEY, required = false) Long id) {
        return service.checkOrganizationName(organizationName, id);
    }

    @GetMapping(path = "/checkQuartzJobName")
    @ApiOperation(value = "定时任务名称重复校验接口", notes = "定时任务名称重复校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "jobName", value = "定时任务名称", dataType = "String", dataTypeClass = String.class, required = true),
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "主键", dataType = "Long", dataTypeClass = Long.class, required = false)
    })
    @ApiOperationSupport(order = 9, author = "XinLau")
    public BaseResponse checkQuartzJobName(@RequestParam(name = "jobName", required = true) String jobName, @RequestParam(name = DataBaseConstant.P_KEY, required = false) Long id) {
        return service.checkQuartzJobName(jobName, id);
    }

}
