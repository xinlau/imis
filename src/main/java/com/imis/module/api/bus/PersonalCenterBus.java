package com.imis.module.api.bus;

import com.imis.base.annotation.VerificationCaptcha;
import com.imis.base.constant.enums.ArgumentResponseEnum;
import com.imis.base.constant.enums.VerificationCodeTypeEnum;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.base.util.CurrentUserUtils;
import com.imis.module.api.model.ro.UserUpdateBasicInformationDTO;
import com.imis.module.api.model.ro.UserUpdateEmailAddressDTO;
import com.imis.module.api.model.ro.UserUpdatePhoneNumberDTO;
import com.imis.module.base.BaseBus;
import com.imis.module.system.model.converter.SysUserConverter;
import com.imis.module.system.model.po.SysUser;
import com.imis.module.system.model.vo.SysUserVO;
import com.imis.module.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * PersonalCenterBus<br>
 * 个人中心 业务处理类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月23日 11:17
 */
@Slf4j
@Service
public class PersonalCenterBus extends BaseBus {

    /**
     * 系统用户表 服务类
     */
    private ISysUserService sysUserService;

    @Autowired
    public void setSysUserService(ISysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    /**
     * 获取当前用户的基本信息
     *
     * @return SysUser - 用户信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/23 13:36
     */
    private SysUser queryUserInfoByToken() {
        return this.sysUserService.queryUserByName(CurrentUserUtils.getUserNameByToken());
    }

    /**
     * 查看基本信息
     *
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/23 16:08
     */
    public CommonResponse<SysUserVO> queryBasicInformation() {
        return new CommonResponse(SysUserConverter.INSTANCE.getReturnValue(queryUserInfoByToken()));
    }

    /**
     * 更新用户基本信息
     *
     * @param updateBasicInformation - 用户基本信息更新表单对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/23 11:34
     */
    public BaseResponse updateBasicInformation(final UserUpdateBasicInformationDTO updateBasicInformation) {
        SysUser sysUser = this.queryUserInfoByToken();
        ArgumentResponseEnum.USER_UPDATE_ERR_NON.assertNotNull(sysUser);
        SysUserConverter.INSTANCE.getUpdateEntity(sysUser, updateBasicInformation);
        boolean updateById = this.sysUserService.updateById(sysUser);
        ArgumentResponseEnum.USER_REGISTER_ERR.assertIsTrue(updateById);
        return new BaseResponse();
    }

    /**
     * 更新密保手机
     *
     * @param updatePhoneNumber - 更新密保手机对象
     * @return Result -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/23 13:52
     */
    @VerificationCaptcha(type = VerificationCodeTypeEnum.SMS, captcha = "#updatePhoneNumber.captcha", verificationCodeIdentification = "#updatePhoneNumber.phone")
    public BaseResponse updatePhoneNumber(UserUpdatePhoneNumberDTO updatePhoneNumber) {
        SysUser sysUser = this.queryUserInfoByToken();
        ArgumentResponseEnum.USER_PHONE_UPDATE_ERR_NON.assertNotNull(sysUser);
        sysUser.setPhone(updatePhoneNumber.getPhone());
        boolean updateById = this.sysUserService.updateById(sysUser);
        ArgumentResponseEnum.USER_PHONE_UPDATE_ERR.assertIsTrue(updateById);
        return new BaseResponse();
    }

    /**
     * 更新电子邮箱地址
     *
     * @param updateEmailAddress - 更新邮箱地址对象
     * @return null -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/23 14:03
     */
    @VerificationCaptcha(type = VerificationCodeTypeEnum.EMAIL, captcha = "#updateEmailAddress.captcha", verificationCodeIdentification = "#updateEmailAddress.email")
    public BaseResponse updateEmailAddress(UserUpdateEmailAddressDTO updateEmailAddress) {
        SysUser sysUser = this.queryUserInfoByToken();
        ArgumentResponseEnum.USER_EMAIL_UPDATE_ERR_NON.assertNotNull(sysUser);
        sysUser.setEmail(updateEmailAddress.getEmail());
        boolean updateById = this.sysUserService.updateById(sysUser);
        ArgumentResponseEnum.USER_EMAIL_UPDATE_ERR.assertIsTrue(updateById);
        return new BaseResponse();
    }

}
