package com.imis.module.api.bus;

import com.imis.base.constant.DataBaseConstant;
import com.imis.base.globle.response.CommonResponse;
import com.imis.base.util.ConvertUtils;
import com.imis.module.api.model.vo.RedisInfoVO;
import com.imis.module.base.BaseBus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.util.*;

/**
 * <p>
 * ActuatorRedisBus<br>
 * Redis性能监控 业务处理类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月21日 11:08
 */
@Slf4j
@Service
public class ActuatorRedisBus extends BaseBus {

    /**
     * Redis连接的线程安全工厂
     */
    @Resource
    private RedisConnectionFactory redisConnectionFactory;

    /**
     * 获取Redis服务信息
     *
     * @return CommonResponse<List<RedisInfoVO>> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/21 11:31
     */
    public CommonResponse<List<RedisInfoVO>> getRedisServeInfo() {
        List<RedisInfoVO> redisInfoArrayList = new ArrayList<>();
        // 读取Redis服务信息
        Properties info = this.redisConnectionFactory.getConnection().info();
        if (ConvertUtils.isNotEmpty(info)) {
            for (Map.Entry<Object, Object> entry : info.entrySet()) {
                RedisInfoVO redisInfo = new RedisInfoVO();
                redisInfo.setKey(ConvertUtils.getString(entry.getKey()));
                redisInfo.setValue(ConvertUtils.getString(entry.getValue()));
                redisInfoArrayList.add(redisInfo);
            }
        }
        return new CommonResponse<>(redisInfoArrayList);
    }

    /**
     * 获取RedisKey实时数量信息
     *
     * @return CommonResponse<Map<String, Object>> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/21 11:31
     */
    public CommonResponse<Map<String, Object>> getRedisServeKeysSize() {
        Long dbSize = this.redisConnectionFactory.getConnection().dbSize();
        Map<String, Object> map = new HashMap<>(2);
        map.put(DataBaseConstant.CREATE_TIME_FIELD, System.currentTimeMillis());
        map.put("dbSize", dbSize);
        return new CommonResponse<>(map);
    }

    /**
     * 获取Redis内存实时占用情况信息
     *
     * @return CommonResponse<Map<String, Object>> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/21 11:31
     */
    public CommonResponse<Map<String, Object>> getRedisServeMemoryInfo() {
        Map<String, Object> map = new HashMap<>(2);
        Properties info = this.redisConnectionFactory.getConnection().info();
        if (ConvertUtils.isNotEmpty(info)) {
            for (Map.Entry<Object, Object> entry : info.entrySet()) {
                String key = ConvertUtils.getString(entry.getKey());
                if ("used_memory".equals(key)) {
                    map.put("used_memory", entry.getValue());
                    map.put(DataBaseConstant.CREATE_TIME_FIELD, System.currentTimeMillis());
                }
            }
        }
        return new CommonResponse<>(map);
    }

    /**
     * 获取服务器磁盘占用情况信息
     *
     * @return CommonResponse<List<Map<String, Object>>> -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/21 11:56
     */
    public CommonResponse<List<Map<String, Object>>> getServerMemoryInfo() {
        // 当前文件系统类
        FileSystemView fileSystemView = FileSystemView.getFileSystemView();
        // 列出所有可用的磁盘文件系统根目录。
        File[] listRoots = File.listRoots();
        log.debug("查询磁盘信息 : " + listRoots.length + "个");
        List<Map<String, Object>> list = new ArrayList<>();
        for (File listRoot : listRoots) {
            if (listRoot.getTotalSpace() == 0) {
                continue;
            }
            Map<String, Object> map = new HashMap<>(4);
            map.put("name", fileSystemView.getSystemDisplayName(listRoot));
            map.put("max", listRoot.getTotalSpace());
            map.put("rest", listRoot.getFreeSpace());
            map.put("restPPT", (listRoot.getTotalSpace() - listRoot.getFreeSpace()) * 100 / listRoot.getTotalSpace());
            list.add(map);
        }
        return new CommonResponse<>(list);
    }

}
