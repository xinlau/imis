package com.imis.module.api.bus;

import com.imis.base.constant.DuplicateCheckConstant;
import com.imis.base.constant.enums.CommonResponseEnum;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.ErrorResponse;
import com.imis.module.base.BaseBus;
import com.imis.module.system.model.ro.DuplicateCheckDTO;
import com.imis.module.system.service.ISysFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * DuplicateCheckBus<br>
 * 数据重复校验 业务处理类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年06月30日 09:07
 */
@Slf4j
@Service
public class DuplicateCheckBus extends BaseBus {

    /**
     * 文件存放 服务类
     */
    private ISysFileService iSysFileService;

    @Autowired
    public void setSysFileService(ISysFileService iSysFileService) {
        this.iSysFileService = iSysFileService;
    }

    /**
     * 数据重复校验
     *
     * @param duplicateCheck - 重复校验数据模型
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse doDuplicateCheck(final DuplicateCheckDTO duplicateCheck) {
        try {
            boolean aBoolean = this.iSysFileService.doDuplicateCheck(duplicateCheck);
            CommonResponseEnum.ERROR_500.assertIsTrue(aBoolean);
        } catch (Exception e) {
            return new ErrorResponse(CommonResponseEnum.ERROR_500);
        }
        return new BaseResponse();
    }

    /**
     * 登录账号重复校验
     *
     * @param userName - 登录账号
     * @param id       - 主键
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse checkUserName(final String userName, final Long id) {
        DuplicateCheckDTO duplicateCheck = new DuplicateCheckDTO(
                DuplicateCheckConstant.TABLE_NAME_SYS_USER,
                DuplicateCheckConstant.FIELD_NAME_USER_NAME,
                userName,
                id
        );
        return doDuplicateCheck(duplicateCheck);
    }

    /**
     * 手机号重复校验
     *
     * @param phone - 手机号
     * @param id    - 主键
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse checkUserPhone(final String phone, final Long id) {
        DuplicateCheckDTO duplicateCheck = new DuplicateCheckDTO(
                DuplicateCheckConstant.TABLE_NAME_SYS_USER,
                DuplicateCheckConstant.FIELD_NAME_USER_PHONE,
                phone,
                id
        );
        return doDuplicateCheck(duplicateCheck);
    }

    /**
     * 邮箱重复校验
     *
     * @param email - 邮箱
     * @param id    - 主键
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse checkUserEmail(final String email, final Long id) {
        DuplicateCheckDTO duplicateCheck = new DuplicateCheckDTO(
                DuplicateCheckConstant.TABLE_NAME_SYS_USER,
                DuplicateCheckConstant.FIELD_NAME_USER_EMAIL,
                email,
                id
        );
        return doDuplicateCheck(duplicateCheck);
    }

    /**
     * 角色名称重复校验
     *
     * @param roleName - 角色名称
     * @param id       - 主键
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse checkRoleName(final String roleName, final Long id) {
        DuplicateCheckDTO duplicateCheck = new DuplicateCheckDTO(
                DuplicateCheckConstant.TABLE_NAME_SYS_ROLE,
                DuplicateCheckConstant.FIELD_NAME_ROLE_NAME,
                roleName,
                id
        );
        return doDuplicateCheck(duplicateCheck);
    }

    /**
     * 角色编码重复校验
     *
     * @param roleCode - 角色编码
     * @param id       - 主键
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse checkRoleCode(final String roleCode, final Long id) {
        DuplicateCheckDTO duplicateCheck = new DuplicateCheckDTO(
                DuplicateCheckConstant.TABLE_NAME_SYS_ROLE,
                DuplicateCheckConstant.FIELD_NAME_ROLE_CODE,
                roleCode,
                id
        );
        return doDuplicateCheck(duplicateCheck);
    }

    /**
     * 功能菜单名称重复校验
     *
     * @param menuName - 功能菜单名称
     * @param id       - 主键
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse checkMenuName(final String menuName, final Long id) {
        DuplicateCheckDTO duplicateCheck = new DuplicateCheckDTO(
                DuplicateCheckConstant.TABLE_NAME_SYS_MENU,
                DuplicateCheckConstant.FIELD_NAME_MENU_NAME,
                menuName,
                id
        );
        return doDuplicateCheck(duplicateCheck);
    }

    /**
     * 组织机构名称重复校验
     *
     * @param organizationName - 组织机构名称
     * @param id               - 主键
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse checkOrganizationName(final String organizationName, final Long id) {
        DuplicateCheckDTO duplicateCheck = new DuplicateCheckDTO(
                DuplicateCheckConstant.TABLE_NAME_SYS_ORGANIZATION,
                DuplicateCheckConstant.FIELD_NAME_ORGANIZATION_NAME,
                organizationName,
                id
        );
        return doDuplicateCheck(duplicateCheck);
    }

    /**
     * 定时任务名称重复校验
     *
     * @param quartzJobName - 定时任务名称
     * @param id            - 主键
     * @return BaseResponse -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 12:01
     */
    public BaseResponse checkQuartzJobName(final String quartzJobName, final Long id) {
        DuplicateCheckDTO duplicateCheck = new DuplicateCheckDTO(
                DuplicateCheckConstant.TABLE_NAME_SYS_QUARTZ_JOB,
                DuplicateCheckConstant.FIELD_NAME_QUARTZ_JOB_NAME,
                quartzJobName,
                id
        );
        return doDuplicateCheck(duplicateCheck);
    }

}
