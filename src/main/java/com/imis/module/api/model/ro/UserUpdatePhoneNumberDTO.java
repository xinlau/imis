package com.imis.module.api.model.ro;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * <p>
 * UserUpdatePhoneNumberDTO<br>
 * 更新密保手机
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月08日 16:58
 */
@Data
@ApiModel(value="更新密保手机对象", description="更新密保手机对象")
@EqualsAndHashCode(callSuper = false)
public class UserUpdatePhoneNumberDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "手机号", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    @Pattern(regexp = CommonConstant.PHONE_REGULAR_EXPRESSION, message = ValidationMessageConstant.FORMAT_ERR)
    private String phone;

    @ApiModelProperty(value = "验证码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String captcha;

}
