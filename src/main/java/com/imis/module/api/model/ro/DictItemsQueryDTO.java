package com.imis.module.api.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * DictItemsQueryDTO<br>
 * 指定表、字段的下拉查询DTO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年05月30日 10:24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="指定表、字段的下拉查询对象", description="指定表、字段的下拉查询对象")
public class DictItemsQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String table;
    /**
     * 名-字段
     */
    @ApiModelProperty(value = "名-字段", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String textField;
    /**
     * 编号-字段
     */
    @ApiModelProperty(value = "编号-字段", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String codeField;
    /**
     * 条件SQL语句
     */
    @ApiModelProperty(value = "条件SQL语句")
    private String filterSql;

}
