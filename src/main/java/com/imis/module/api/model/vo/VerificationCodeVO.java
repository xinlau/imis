package com.imis.module.api.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.imis.base.constant.CommonConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * VerificationCodeVO<br>
 * 验证码返回值 对象
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月30日 10:59
 */
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="验证码返回值",description="验证码返回值 对象")
public class VerificationCodeVO implements Serializable {

    /**
     * 验证码类型
     */
    @ApiModelProperty(value = "验证码类型")
    private String type;

    /**
     * 验证码标识
     */
    @ApiModelProperty(value = "验证码标识")
    private String verificationCodeIdentification;

    /**
     * 字符验证码、运算验证码、滑动验证码原图
     */
    @ApiModelProperty(name = "字符验证码、运算验证码、滑动验证码原图")
    private String verificationCode;
    public void setVerificationCode(String verificationCode) {
        this.verificationCode = String.format(CommonConstant.KAPTCHA_PREFIX + "%s", verificationCode);
    }

    /**
     * 滑动验证码-遮罩图
     */
    @ApiModelProperty(name = "滑动验证码-遮罩图")
    private String shadeImage;
    public void setShadeImage(String shadeImage) {
        this.shadeImage = String.format(CommonConstant.KAPTCHA_PREFIX + "%s", shadeImage);
    }

    /**
     * 滑动验证码-裁剪图
     */
    @ApiModelProperty(name = "滑动验证码-裁剪图")
    private String cutoutImage;
    public void setCutoutImage(String cutoutImage) {
        this.cutoutImage = String.format(CommonConstant.KAPTCHA_PREFIX + "%s", cutoutImage);
    }

    /**
     * 滑动验证码，X轴
     */
    @ApiModelProperty(name = "滑动验证码，X轴")
    private Integer x;

    /**
     * 滑动验证码，Y轴
     */
    @ApiModelProperty(name = "滑动验证码，Y轴")
    private Integer y;

}
