package com.imis.module.api.model.ro;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * 用户注册表单对象 RO
 *
 * @author XinLau
 * @since 2020-03-05
*/
@Data
@ApiModel(value="用户注册对象", description="用户注册")
@EqualsAndHashCode(callSuper = false)
public class UserRegisterDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 登录账号
     */
    @ApiModelProperty(value = "登录账号", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String username;

    /**
     * 真实姓名
     */
    @ApiModelProperty(value = "真实姓名", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String realname;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String password;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String avatar;

    /**
     * 生日
     */
    @ApiModelProperty(value = "出生日期")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    private LocalDate birthday;

    /**
     * 性别(0：默认未知 1：男 2：女)
     */
    @ApiModelProperty(value = "性别(0：默认未知 1：男 2：女)")
    private int sex;

    /**
     * 电子邮件
     */
    @ApiModelProperty(value = "电子邮箱")
    @Email(message = ValidationMessageConstant.FORMAT_ERR)
    private String email;

    /**
     * 电话
     */
    @ApiModelProperty(value = "电话", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    @Pattern(regexp = CommonConstant.PHONE_REGULAR_EXPRESSION, message = ValidationMessageConstant.FORMAT_ERR)
    private String phone;

    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码")
    private String captcha;
    @ApiModelProperty(value = "验证码标识")
    private String verificationCodeIdentification;

    /**
     * 状态(1：正常  2：冻结 ）
     */
    @ApiModelProperty(hidden = true)
    private int status = CommonConstant.USER_UNFREEZE;

    /**
     * 删除状态（0，正常，1已删除）
     */
    @ApiModelProperty(hidden = true)
    private int delFlag = CommonConstant.DEL_FLAG_NOT_DELETE;

}
