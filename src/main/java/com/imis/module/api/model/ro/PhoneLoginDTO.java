package com.imis.module.api.model.ro;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 *
 * 用户登录表单对象 RO
 *
 * @author XinLau
 * @since 2020-03-17
 */
@Data
@ApiModel(value="手机号登录对象", description="登录对象")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PhoneLoginDTO {

	@ApiModelProperty(value = "手机号", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    @Pattern(regexp = CommonConstant.PHONE_REGULAR_EXPRESSION, message = ValidationMessageConstant.FORMAT_ERR)
    private String phone;

	@ApiModelProperty(value = "验证码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String captcha;

}