package com.imis.module.api.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 *
 * 用户登录表单对象 RO
 *
 * @author XinLau
 * @since 2020-03-17
 */
@Data
@ApiModel(value="登录对象", description="登录对象")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LoginDTO {

	@ApiModelProperty(value = "账号", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String username;

	@ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String password;

	@ApiModelProperty(value = "验证码")
    private String captcha;

    @ApiModelProperty(value = "验证码标识")
    private String verificationCodeIdentification;
}