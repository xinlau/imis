package com.imis.module.api.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * ClassParameter<br>
 * 类参数对象
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月18日 10:07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "类参数对象", description = "类参数对象,只有name和type")
public class ClassParameter {

    /**
     * 类参数名称
     */
    @ApiModelProperty(value = "类参数名称")
    private String name;

    /**
     * 类参数类型
     */
    @ApiModelProperty(value = "类参数类型")
    private String typeName;

    public ClassParameter(){

    }

    public ClassParameter(String name, String typeName) {
        this.name = name;
        this.typeName = typeName;
    }
}
