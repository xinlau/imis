package com.imis.module.api.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * SendEmailDTO<br>
 * 获取邮箱验证码
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月08日 16:58
 */
@Data
@ApiModel(value="邮箱验证码发送对象", description="邮箱验证码发送对象")
@EqualsAndHashCode(callSuper = false)
public class SendEmailDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "邮箱地址", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    @Email(message = ValidationMessageConstant.FORMAT_ERR)
    private String email;

    @ApiModelProperty(value = "验证码", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String captcha;

    @ApiModelProperty(value = "验证码标识", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String verificationCodeIdentification;

}
