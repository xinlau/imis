package com.imis.module.api.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * DictItemsByCodeQueryDTO<br>
 * 指定表、字段的指定内容下拉查询DTO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年05月30日 10:24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="指定表、字段的指定内容下拉查询对象", description="指定表、字段的指定内容下拉查询对象")
public class DictItemsByCodeQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String table;
    /**
     * 名-字段
     */
    @ApiModelProperty(value = "名-字段", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String textField;
    /**
     * 编号-字段
     */
    @ApiModelProperty(value = "编号-字段", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String codeField;
    /**
     * 指定内容条件
     */
    @ApiModelProperty(value = "指定内容条件")
    private String[] codeArray;

}
