package com.imis.module.online.table.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * PagingQueryTableDTO<br>
 * 数据库表查询对象
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月09日 14:02
 */
@Data
@ApiModel(value = "数据库表查询对象", description = "数据库表查询")
@EqualsAndHashCode(callSuper = false)
public class PagingQueryTableDTO extends BasePageDTO implements Serializable {

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名", required = true)
    private String tableName;
    /**
     * 表类型（ 0单表、1主表、2附表）
     */
    @ApiModelProperty(value = "表类型（ 0单表、1主表、2附表）")
    private Integer tableType;
    /**
     * 同步数据库状态（N未同步；Y已同步）
     */
    @ApiModelProperty(value = "同步数据库状态（N未同步；Y已同步）")
    private String isSynchronize;
    /**
     * 表说明
     */
    @ApiModelProperty(value = "表说明")
    private String tableDescription;

}
