package com.imis.module.online.table.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 在线开发-数据库表字段信息 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("onl_table_fields")
public class TableFields extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 数据库属性 **/
    /**
     * 表ID
     */
    @TableField("table_head_id")
    private Long tableHeadId;
    /**
     * 字段名字
     */
    @TableField("field_name")
    private String fieldName;
    /**
     * 字段备注
     */
    @TableField("field_txt")
    private String fieldTxt;
    /**
     * 原字段名
     */
    @TableField("field_name_old")
    private String fieldNameOld;
    /**
     * 是否主键（0否 1是）
     */
    @TableField("is_key")
    private Integer isKey;
    /**
     * 是否允许为空（0否 1是）
     */
    @TableField("is_null")
    private Integer isNull;
    /**
     * 数据库字段类型
     */
    @TableField("field_type")
    private String fieldType;
    /**
     * 数据库字段长度
     */
    @TableField("field_length")
    private Integer fieldLength;
    /**
     * 小数点
     */
    @TableField("field_point_length")
    private Integer fieldPointLength;
    /**
     * 表字段默认值
     */
    @TableField("field_default_value")
    private String fieldDefaultValue;

    /** 页面属性 **/
    /**
     * 表单控件类型
     */
    @TableField("field_show_type")
    private String fieldShowType;
    /**
     * 表单控件长度
     */
    @TableField("form_control_length")
    private Integer formControlLength;
    /**
     * 控件默认值，不同的表达式展示不同的结果。
     * 1. 纯字符串直接赋给默认值；
     * 2. #{普通变量}；
     * 3. {{ 动态JS表达式 }}；
     * 4. ${填值规则编码}；
     * 填值规则表达式只允许存在一个，且不能和其他规则混用。
     */
    @TableField("control_default_value")
    private String controlDefaultValue;
    /**
     * 是否查询条件（0否 1是）
     */
    @TableField("is_query")
    private Integer isQuery;
    /**
     * 表单是否显示（0否 1是）
     */
    @TableField("is_show_form")
    private Integer isShowForm;
    /**
     * 列表是否显示（0否 1是）
     */
    @TableField("is_show_list")
    private Integer isShowList;
    /**
     * 是否支持排序（0否 1是）
     */
    @TableField("sort_flag")
    private Integer sortFlag;
    /**
     * 是否是只读（0否 1是）
     */
    @TableField("is_read_only")
    private Integer isReadOnly;
    /**
     * 扩展参数JSON
     */
    @TableField("field_extend_json")
    private String fieldExtendJson;
    /**
     * 自定义值转换器
     */
    @TableField("converter")
    private String converter;

    /**
     * 查询模式
     */
    @TableField("query_mode")
    private String queryMode;

    /**
     * 字典code
     */
    @TableField("dict_field")
    private String dictField;

    /**
     * 字典表
     */
    @TableField("dict_table")
    private String dictTable;

    /**
     * 字典Text
     */
    @TableField("dict_text")
    private String dictText;

    /**
     * 跳转URL
     */
    @TableField("field_href")
    private String fieldHref;

    /**
     * 表单字段校验规则
     */
    @TableField("field_valid_type")
    private String fieldValidType;

    /**
     * 字段是否必填（0否 1是）
     */
    @TableField("field_must_input")
    private Integer fieldMustInput;

    /**
     * 外键主表名
     */
    @TableField("main_table")
    private String mainTable;

    /**
     * 外键主键字段
     */
    @TableField("main_field")
    private String mainField;

    /**
     * 排序
     */
    @TableField("sort_order")
    private Integer sortOrder;

    /**
     * 查询默认值
     */
    @TableField("query_def_value")
    private String queryDefValue;

    /**
     * 查询配置字典text
     */
    @TableField("query_dict_text")
    private String queryDictText;

    /**
     * 查询配置字典code
     */
    @TableField("query_dict_field")
    private String queryDictField;

    /**
     * 查询配置字典table
     */
    @TableField("query_dict_table")
    private String queryDictTable;

    /**
     * 查询显示控件
     */
    @TableField("query_show_type")
    private String queryShowType;

    /**
     * 是否启用查询配置（0否 1是）
     */
    @TableField("query_config_flag")
    private Integer queryConfigFlag;

    /**
     * 查询字段校验类型
     */
    @TableField("query_valid_type")
    private String queryValidType;

    /**
     * 查询字段是否必填（0否 1是）
     */
    @TableField("query_must_input")
    private Integer queryMustInput;

}
