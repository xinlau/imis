package com.imis.module.online.table.model.converter;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.online.table.model.po.TableIndex;
import com.imis.module.online.table.model.ro.TableIndexAddDTO;
import com.imis.module.online.table.model.ro.TableIndexUpdateDTO;
import com.imis.module.online.table.model.vo.TableIndexVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 在线开发-数据库表索引 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Mapper
public interface TableIndexConverter {

    /**
     * 在线开发-数据库表索引 转换器实例
     */
    TableIndexConverter INSTANCE = Mappers.getMapper(TableIndexConverter.class);

    /**
     * 在线开发-数据库表索引 PO转VO
     *
     * @param entity - 在线开发-数据库表索引 PO
     * @return TableIndexVO - 在线开发-数据库表索引 VO
     */
    @Mappings({})
    TableIndexVO getReturnValue(TableIndex entity);

    /**
     * 在线开发-数据库表索引 PO转VO
     *
     * @param entityList - 在线开发-数据库表索引 数组PO
     * @return List<TableIndexVO> - 在线开发-数据库表索引 数组VO
     */
    @Mappings({})
    List<TableIndexVO> getReturnValue(List<TableIndex> entityList);

    /**
     * 在线开发-数据库表索引 RO转PO
     *
     * @param add         - 在线开发-数据库表索引 RO
     * @param tableHeadId - 在线开发-数据库表编号
     * @return TableIndex - 在线开发-数据库表索引 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "tableHeadId", source = "tableHeadId"),
            @Mapping(target = "isSynchronize", constant = CommonConstant.TO_UPPER_CASE_N),
            @Mapping(target = "delFlag", constant = "0"),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    TableIndex getAddEntity(TableIndexAddDTO add, Long tableHeadId);

    /**
     * 在线开发-数据库表索引 RO转PO
     *
     * @param entityList  - 在线开发-数据库表字段信息 RO
     * @param tableHeadId - 在线开发-数据库表编号
     * @return List<TableIndex> - 在线开发-数据库表字段信息 PO
     */
    default List<TableIndex> addListRo2ListPo(List<TableIndexAddDTO> entityList, Long tableHeadId) {
        List<TableIndex> tableFieldsList = new ArrayList<>(entityList.size());
        entityList.forEach(entity -> {
            TableIndex tableIndex = getAddEntity(entity, tableHeadId);
            tableFieldsList.add(tableIndex);
        });
        return tableFieldsList;
    }

    /**
     * 在线开发-数据库表索引 RO转PO
     *
     * @param update      - 在线开发-数据库表索引 RO
     * @param tableHeadId - 在线开发-数据库表编号
     * @return TableIndex - 在线开发-数据库表索引 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "tableHeadId", source = "tableHeadId"),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    TableIndex getUpdateEntity(TableIndexUpdateDTO update, final Long tableHeadId);

    /**
     * 在线开发-数据库表索引 RO转PO
     *
     * @param tableIndex       - 在线开发-数据库表索引 PO
     * @param tableIndexUpdate - 在线开发-数据库表索引 RO
     * @return TableIndex - 在线开发-数据库表索引 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "tableHeadId", ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget TableIndex tableIndex, TableIndexUpdateDTO tableIndexUpdate);

    /**
     * 在线开发-数据库表索引 PO转PO
     *
     * @param sourceTableIndexList - 在线开发-数据库表索引 PO
     * @param tableHeadId          - 数据库表编号
     * @return List<TableIndex> - 在线开发-数据库表索引 PO
     */
    default List<TableIndex> getDataBaseTableIndexListBackup(List<TableIndex> sourceTableIndexList, Long tableHeadId) {
        sourceTableIndexList.forEach(tableIndex -> {
            tableIndex.setId(null);
            tableIndex.setCreateBy(null);
            tableIndex.setCreateTime(null);
            tableIndex.setUpdateBy(null);
            tableIndex.setCreateTime(null);
            tableIndex.setTableHeadId(tableHeadId);
        });
        return sourceTableIndexList;
    }
}
