package com.imis.module.online.table.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.imis.base.annotation.Dict;
import com.imis.base.constant.DictCodeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * 在线开发-数据库表表信息 VO
 *
 * @author XinLau
 * @since 2020-10-03
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="TableHead对象", description="在线开发-数据库表表信息")
public class TableHeadVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 表名
     */
    @ApiModelProperty(value = "表名")
    private String tableName;
    /**
     * 表类型（ 0单表、1主表、2附表）
     */
    @ApiModelProperty(value = "表类型（ 0单表、1主表、2附表）")
    private Integer tableType;
    /**
     * 表类型中文
     */
    @ApiModelProperty(value = "表类型")
    private String tableTypeText;
    /**
     * 表版本
     */
    @ApiModelProperty(value = "表版本")
    private Integer tableVersion;
    /**
     * 表说明
     */
    @ApiModelProperty(value = "表说明")
    private String tableDescription;
    /**
     * 主键类型
     */
    @ApiModelProperty(value = "主键类型")
    private String idType;
    /**
     * 主键生成序列
     */
    @ApiModelProperty(value = "主键生成序列")
    private String idSequence;
    /**
     * 是否带 checkbox（0否 1是）
     */
    @ApiModelProperty(value = "是否带 checkbox（0否 1是）")
    @Dict(dicCode = DictCodeConstant.STATUS_OR_NOT)
    private Integer isCheckbox;
    /**
     * 同步数据库状态（N未同步；Y已同步）
     */
    @ApiModelProperty(value = "同步数据库状态（N未同步；Y已同步）")
    @Dict(dicCode = DictCodeConstant.SYNCHRONIZE_STATE)
    private String isSynchronize;
    /**
     * 是否分页（0否 1是）
     */
    @ApiModelProperty(value = "是否分页（0否 1是）")
    @Dict(dicCode = DictCodeConstant.STATUS_OR_NOT)
    private Integer isPage;
    /**
     * 是否是树（0否 1是）
     */
    @ApiModelProperty(value = "是否是树（0否 1是）")
    @Dict(dicCode = DictCodeConstant.STATUS_OR_NOT)
    private Integer isTree;
    /**
     * 查询模式
     */
    @ApiModelProperty(value = "查询模式")
    private String queryMode;
    /**
     * 映射关系（0一对多  1一对一）
     */
    @ApiModelProperty(value = "映射关系（0一对多  1一对一）")
    private Integer relationType;
    /**
     * 附表清单
     */
    @ApiModelProperty(value = "附表清单")
    private String subTableCheckList;
    /**
     * 附表排序序号
     */
    @ApiModelProperty(value = "附表排序序号")
    private Integer tabSortOrder;
    /**
     * 树表主键字段
     */
    @ApiModelProperty(value = "树表主键字段")
    private String treeIdFieldName;
    /**
     * 树开表单列字段
     */
    @ApiModelProperty(value = "树开表单列字段")
    private String treeFieldName;
    /**
     * 树形表单父id
     */
    @ApiModelProperty(value = "树形表单父id")
    private String treeParentIdFieldName;
    /**
     * 表单分类
     */
    @ApiModelProperty(value = "表单分类")
    private String formCategory;
    /**
     * PC表单模板
     */
    @ApiModelProperty(value = "PC表单模板")
    private String formTemplate;
    /**
     * 表单模板样式(移动端)
     */
    @ApiModelProperty(value = "表单模板样式(移动端)")
    private String formTemplateMobile;
    /**
     * 是否有横向滚动条（0否 1是）
     */
    @ApiModelProperty(value = "是否有横向滚动条（0否 1是）")
    @Dict(dicCode = DictCodeConstant.STATUS_OR_NOT)
    private Integer isScroll;
    /**
     * 复制版本号
     */
    @ApiModelProperty(value = "复制版本号")
    private Integer copyVersion;
    /**
     * 复制表类型（1为复制表 0为原始表）
     */
    @ApiModelProperty(value = "复制表类型（1为复制表 0为原始表）")
    private Integer copyType;
    /**
     * 原始表ID
     */
    @ApiModelProperty(value = "原始表ID")
    private String physicId;
    /**
     * 主题模板
     */
    @ApiModelProperty(value = "主题模板")
    private String themeTemplate;

}
