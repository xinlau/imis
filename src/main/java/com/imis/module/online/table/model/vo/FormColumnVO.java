package com.imis.module.online.table.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 在线开发-分页查询字段对象信息 VO
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "分页查询字段对象", description = "在线开发-分页查询字段对象信息")
public class FormColumnVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字段名字
     */
    @ApiModelProperty(value = "字段名字")
    private String fieldName;
    /**
     * 字段备注
     */
    @ApiModelProperty(value = "字段备注")
    private String fieldTxt;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sortOrder;
}
