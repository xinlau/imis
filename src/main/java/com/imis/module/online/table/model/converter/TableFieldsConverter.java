package com.imis.module.online.table.model.converter;

import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.online.table.model.po.TableFields;
import com.imis.module.online.table.model.ro.TableFieldsAddDTO;
import com.imis.module.online.table.model.ro.TableFieldsUpdateDTO;
import com.imis.module.online.table.model.vo.DataQueryConditionsFieldVO;
import com.imis.module.online.table.model.vo.TableFieldsVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 在线开发-数据库表字段信息 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Mapper
public interface TableFieldsConverter {

    /**
     * 在线开发-数据库表字段信息 转换器实例
     */
    TableFieldsConverter INSTANCE = Mappers.getMapper(TableFieldsConverter.class);

    /**
     * 在线开发-数据库表字段信息 PO转VO
     *
     * @param entity - 在线开发-数据库表字段信息 PO
     * @return TableFieldsVO - 在线开发-数据库表字段信息 VO
     */
    @Mappings({})
    TableFieldsVO getReturnValue(TableFields entity);

    /**
     * 在线开发-数据库表字段信息 PO转VO
     *
     * @param entityList - 在线开发-数据库表字段信息 数组PO
     * @return List<TableFieldsVO> - 在线开发-数据库表字段信息 数组VO
     */
    @Mappings({})
    List<TableFieldsVO> getReturnValue(List<TableFields> entityList);

    /**
     * 在线开发-数据库表字段信息 RO转PO
     *
     * @param add    - 在线开发-数据库表字段信息 RO
     * @param tableHeadId - 在线开发-数据库表编号
     * @return TableFields - 在线开发-数据库表字段信息 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "tableHeadId", source = "tableHeadId"),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    TableFields getAddEntity(TableFieldsAddDTO add, Long tableHeadId);

    /**
     * 在线开发-数据库表字段信息 RO转PO
     *
     * @param entityList  - 在线开发-数据库表字段信息 RO
     * @param tableHeadId - 在线开发-数据库表编号
     * @return List<TableFields> - 在线开发-数据库表字段信息 PO
     */
    default List<TableFields> getAddEntity(List<TableFieldsAddDTO> entityList, Long tableHeadId) {
        List<TableFields> tableFieldsList = new ArrayList<>(entityList.size());
        entityList.forEach(add -> {
            TableFields tableFields = getAddEntity(add, tableHeadId);
            tableFieldsList.add(tableFields);
        });
        return tableFieldsList;
    }

    /**
     * 在线开发-数据库表字段信息 RO转PO
     *
     * @param update    - 在线开发-数据库表字段信息 RO
     * @param tableHeadId - 在线开发-数据库表编号
     * @return TableFields - 在线开发-数据库表字段信息 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "tableHeadId", source = "tableHeadId"),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    TableFields getUpdateEntity(TableFieldsUpdateDTO update, final Long tableHeadId);

    /**
     * 在线开发-数据库表字段信息 RO转PO
     *
     * @param update - 在线开发-数据库表字段信息 RO
     * @return TableFields - 在线开发-数据库表字段信息 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    TableFields getUpdateEntity(TableFieldsUpdateDTO update);

    /**
     * 在线开发-数据库表字段信息 RO转PO
     *
     * @param entityList - 在线开发-数据库表字段信息 RO
     * @return List<TableFields> - 在线开发-数据库表字段信息 PO
     */
    @Mappings({})
    List<TableFields> getUpdateEntity(List<TableFieldsUpdateDTO> entityList);

    /**
     * 在线开发-数据库表字段信息 PO转PO
     *
     * @param sourceTableFieldsList - 在线开发-数据库表字段信息 PO
     * @param tableHeadId     - 数据库表编号
     * @return List<TableFields> - 在线开发-数据库表字段信息 PO
     */
    default List<TableFields> getDataBaseTableFieldsListBackup(List<TableFields> sourceTableFieldsList, Long tableHeadId){
        sourceTableFieldsList.forEach(tableFields -> {
            tableFields.setId(null);
            tableFields.setCreateBy(null);
            tableFields.setCreateTime(null);
            tableFields.setUpdateBy(null);
            tableFields.setCreateTime(null);
            tableFields.setTableHeadId(tableHeadId);
        });
        return sourceTableFieldsList;
    }

    /**
     * 在线开发-获取数据查询条件字段
     * @param tableFields - 在线开发-数据库表字段信息 PO
     * @return DataQueryConditionsFieldVO - 数据查询条件字段
     */
    @Mappings({})
    DataQueryConditionsFieldVO getDataQueryConditionsField(TableFields tableFields);

}
