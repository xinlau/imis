package com.imis.module.online.table.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * DataBaseTableSynchronizeDTO<br>
 * 数据库表同步 RO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月04日 16:39
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "数据库表同步对象", description = "在线开发-数据库表同步对象")
public class DataBaseTableSynchronizeDTO implements Serializable {
    /**
     * 数据库表ID
     */
    @ApiModelProperty(value = "数据库表ID", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long tableHeadId;
    /**
     * 同步方式
     */
    @ApiModelProperty(value = "同步方式", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String synchronizationMode;

}
