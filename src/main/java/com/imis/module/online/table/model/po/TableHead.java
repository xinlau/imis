package com.imis.module.online.table.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 在线开发-数据库表表信息 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("onl_table_head")
public class TableHead extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表名
     */
    @TableField("table_name")
    private String tableName;

    /**
     * 表类型（ 0单表、1主表、2附表）
     */
    @TableField("table_type")
    private Integer tableType;

    /**
     * 表版本
     */
    @TableField("table_version")
    private Integer tableVersion;

    /**
     * 表说明
     */
    @TableField("table_description")
    private String tableDescription;

    /**
     * 主键类型
     */
    @TableField("id_type")
    private String idType;

    /**
     * 主键生成序列
     */
    @TableField("id_sequence")
    private String idSequence;

    /**
     * 是否带 checkbox（0否 1是）
     */
    @TableField("is_checkbox")
    private Integer isCheckbox;

    /**
     * 同步数据库状态（N未同步；Y已同步）
     */
    @TableField("is_synchronize")
    private String isSynchronize;

    /**
     * 是否分页（0否 1是）
     */
    @TableField("is_page")
    private Integer isPage;

    /**
     * 是否是树（0否 1是）
     */
    @TableField("is_tree")
    private Integer isTree;

    /**
     * 查询模式
     */
    @TableField("query_mode")
    private String queryMode;

    /**
     * 映射关系（0一对多  1一对一）
     */
    @TableField("relation_type")
    private Integer relationType;

    /**
     * 附表清单（表名）
     */
    @TableField("sub_table_check_list")
    private String subTableCheckList;

    /**
     * 附表排序序号
     */
    @TableField("tab_sort_order")
    private Integer tabSortOrder;

    /**
     * 树表主键字段
     */
    @TableField("tree_id_field_name")
    private String treeIdFieldName;

    /**
     * 树开表单列字段
     */
    @TableField("tree_field_name")
    private String treeFieldName;

    /**
     * 树形表单父id
     */
    @TableField("tree_parent_id_field_name")
    private String treeParentIdFieldName;

    /**
     * 表单分类
     */
    @TableField("form_category")
    private String formCategory;

    /**
     * PC表单模板
     */
    @TableField("form_template")
    private String formTemplate;

    /**
     * 表单模板样式(移动端)
     */
    @TableField("form_template_mobile")
    private String formTemplateMobile;

    /**
     * 是否有横向滚动条（0否 1是）
     */
    @TableField("is_scroll")
    private Integer isScroll;

    /**
     * 复制版本号
     */
    @TableField("copy_version")
    private Integer copyVersion;

    /**
     * 复制表类型（1为复制表 0为原始表）
     */
    @TableField("copy_type")
    private Integer copyType;

    /**
     * 原始表ID
     */
    @TableField("physic_id")
    private Long physicId;

    /**
     * 主题模板
     */
    @TableField("theme_template")
    private String themeTemplate;

}
