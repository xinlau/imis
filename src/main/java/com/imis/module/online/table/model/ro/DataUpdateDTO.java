package com.imis.module.online.table.model.ro;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 在线开发-数据库表数据-更新 RO
 *
 * @author XinLau
 * @since 2020-10-03
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="数据库表数据更新对象", description="在线开发-数据库表数据-更新")
public class DataUpdateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库表编号
     */
    @ApiModelProperty(value = "数据库表编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;

    /**
     * 数据库表表数据
     */
    @ApiModelProperty(value = "数据库表表数据", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private JSONObject tableDataJson;

}
