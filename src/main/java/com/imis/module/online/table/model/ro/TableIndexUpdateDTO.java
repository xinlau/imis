package com.imis.module.online.table.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * 在线开发-数据库表索引-更新 RO
 *
 * @author XinLau
 * @since 2020-10-03
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TableIndex更新对象", description="在线开发-数据库表索引-更新")
public class TableIndexUpdateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库表索引编号
     */
    @ApiModelProperty(value = "数据库表索引编号")
    private Long id;
    /**
     * 数据库编号
     */
    @ApiModelProperty(value = "数据库编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long tableHeadId;
    /**
     * 索引名称
     */
    @ApiModelProperty(value = "索引名称", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String indexName;
    /**
     * 索引栏位
     */
    @ApiModelProperty(value = "索引栏位", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String indexField;
    /**
     * 索引类型
     */
    @ApiModelProperty(value = "索引类型", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String indexType;
    /**
     * 是否同步数据库（N未同步；Y已同步）
     */
    @ApiModelProperty(value = "是否同步数据库（N未同步；Y已同步）")
    private String isSynchronize;
    /**
     * 是否删除（0未删除 1删除）
     */
    @ApiModelProperty(value = "是否删除（0未删除 1删除）")
    private Integer delFlag;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
}
