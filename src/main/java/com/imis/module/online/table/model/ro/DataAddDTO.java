package com.imis.module.online.table.model.ro;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * DataAddDTO<br>
 * 在线开发-数据库表数据-新增 RO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月05日 15:42
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "数据库表数据新增对象", description = "在线开发-数据库表数据-新增")
public class DataAddDTO {

    /**
     * 数据库表编号
     */
    @ApiModelProperty(value = "数据库表编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;

    /**
     * 数据库表表数据
     */
    @ApiModelProperty(value = "数据库表表数据", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private JSONObject tableDataJson;

}
