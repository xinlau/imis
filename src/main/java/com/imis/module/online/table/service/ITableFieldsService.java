package com.imis.module.online.table.service;

import com.imis.module.base.BaseService;
import com.imis.module.online.table.model.po.TableFields;

import java.util.List;

/**
 * <p>
 * 在线开发-数据库表字段信息 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
public interface ITableFieldsService extends BaseService<TableFields> {

    /**
     * 查询权限筛选后的数据库表字段信息
     *
     * @param tableIdentification - 数据库表表主键
     * @return List<TableFieldsVO> - 数据库表字段信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    List<TableFields> queryTableFieldPermissionFilteringListByTableHeadId(final Long tableIdentification);

    /**
     * 查询数据库表字段信息
     *
     * @param tableName - 数据库表表名
     * @return List<TableFields> - 数据库表字段信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    List<TableFields> queryTableFieldListByTableName(final String tableName);

}