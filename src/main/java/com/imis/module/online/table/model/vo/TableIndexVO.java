package com.imis.module.online.table.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.imis.base.annotation.Dict;
import com.imis.base.constant.DictCodeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * 在线开发-数据库表索引 VO
 *
 * @author XinLau
 * @since 2020-10-03
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="TableIndex对象", description="在线开发-数据库表索引")
public class TableIndexVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    @ApiModelProperty(value = "数据库表索引信息主键")
    private String id;
    /**
     * 数据库表表编号
     */
    @ApiModelProperty(value = "数据库表表编号")
    private String tableHeadId;
    /**
     * 索引名称
     */
    @ApiModelProperty(value = "索引名称")
    private String indexName;
    /**
     * 索引栏位
     */
    @ApiModelProperty(value = "索引栏位")
    private String indexField;
    /**
     * 索引类型
     */
    @ApiModelProperty(value = "索引类型")
    private String indexType;
    /**
     * 是否同步数据库（N未同步；Y已同步）
     */
    @ApiModelProperty(value = "是否同步数据库（N未同步；Y已同步）")
    @Dict(dicCode = DictCodeConstant.SYNCHRONIZE_STATE)
    private String isSynchronize;
    /**
     * 是否删除（0未删除 1删除）
     */
    @ApiModelProperty(value = "是否删除（0未删除 1删除）")
    private Integer delFlag;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;

}
