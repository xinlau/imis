package com.imis.module.online.table.model.ro;

import com.imis.module.online.table.model.po.TableHead;
import com.imis.module.online.table.model.po.TableIndex;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * TableIndexDTO<br>
 * 数据库表索引
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月06日 15:56
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TableIndexDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库表表信息-更新
     */
    private TableHead tableHead;

    /**
     * 数据库表索引信息-添加
     */
    List<TableIndex> tableIndexAddList;

    /**
     * 数据库表索引信息-更新
     */
    List<TableIndex> tableIndexUpdateList;

    public TableIndexDTO(TableHead tableHead, List<TableIndex> tableIndexAddList, List<TableIndex> tableIndexUpdateList) {
        this.tableHead = tableHead;
        this.tableIndexAddList = tableIndexAddList;
        this.tableIndexUpdateList = tableIndexUpdateList;
    }

}
