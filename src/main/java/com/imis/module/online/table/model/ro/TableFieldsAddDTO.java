package com.imis.module.online.table.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 在线开发-数据库表字段信息-添加 RO
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "TableFields添加对象", description = "在线开发-数据库表字段信息")
public class TableFieldsAddDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字段名字
     */
    @ApiModelProperty(value = "字段名字", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String fieldName;
    /**
     * 字段备注
     */
    @ApiModelProperty(value = "字段备注")
    private String fieldTxt;
    /**
     * 原字段名
     */
    @ApiModelProperty(value = "原字段名")
    private String fieldNameOld;
    /**
     * 是否主键（0否 1是）
     */
    @ApiModelProperty(value = "是否主键（0否 1是）")
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Integer isKey;
    /**
     * 是否允许为空（0否 1是）
     */
    @ApiModelProperty(value = "是否允许为空（0否 1是）")
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Integer isNull;
    /**
     * 数据库字段类型
     */
    @ApiModelProperty(value = "数据库字段类型", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String fieldType;
    /**
     * 数据库字段长度
     */
    @ApiModelProperty(value = "数据库字段长度")
    private Integer fieldLength;
    /**
     * 小数点
     */
    @ApiModelProperty(value = "小数点")
    private Integer fieldPointLength;
    /**
     * 表字段默认值
     */
    @ApiModelProperty(value = "表字段默认值")
    private String fieldDefaultValue;
    /**
     * 字典code
     */
    @ApiModelProperty(value = "字典code")
    private String dictField;
    /**
     * 字典表
     */
    @ApiModelProperty(value = "字典表")
    private String dictTable;
    /**
     * 字典Text
     */
    @ApiModelProperty(value = "字典Text")
    private String dictText;
    /**
     * 表单控件类型
     */
    @ApiModelProperty(value = "表单控件类型")
    private String fieldShowType;
    /**
     * 跳转URL
     */
    @ApiModelProperty(value = "跳转URL")
    private String fieldHref;
    /**
     * 表单控件长度
     */
    @ApiModelProperty(value = "表单控件长度")
    private Integer formControlLength;
    /**
     * 表单字段校验规则
     */
    @ApiModelProperty(value = "表单字段校验规则")
    private String fieldValidType;
    /**
     * 字段是否必填（0否 1是）
     */
    @ApiModelProperty(value = "字段是否必填（0否 1是）")
    private Integer fieldMustInput;
    /**
     * 扩展参数JSON
     */
    @ApiModelProperty(value = "扩展参数JSON")
    private String fieldExtendJson;
    /**
     * 控件默认值，不同的表达式展示不同的结果。
     * 1. 纯字符串直接赋给默认值；
     * 2. #{普通变量}；
     * 3. {{ 动态JS表达式 }}；
     * 4. ${填值规则编码}；
     * 填值规则表达式只允许存在一个，且不能和其他规则混用。
     */
    @ApiModelProperty(value = "控件默认值，不同的表达式展示不同的结果。 1. 纯字符串直接赋给默认值； 2. #{普通变量}； 3. {{ 动态JS表达式 }}； 4. ${填值规则编码}； 填值规则表达式只允许存在一个，且不能和其他规则混用。")
    private String controlDefaultValue;
    /**
     * 是否查询条件（0否 1是）
     */
    @ApiModelProperty(value = "是否查询条件（0否 1是）")
    private Integer isQuery;
    /**
     * 表单是否显示（0否 1是）
     */
    @ApiModelProperty(value = "表单是否显示（0否 1是）")
    private Integer isShowForm;
    /**
     * 列表是否显示（0否 1是）
     */
    @ApiModelProperty(value = "列表是否显示（0否 1是）")
    private Integer isShowList;
    /**
     * 是否是只读（1是 0否）
     */
    @ApiModelProperty(value = "是否是只读（1是 0否）")
    private Integer isReadOnly;
    /**
     * 查询模式
     */
    @ApiModelProperty(value = "查询模式")
    private String queryMode;
    /**
     * 外键主表名
     */
    @ApiModelProperty(value = "外键主表名")
    private String mainTable;
    /**
     * 外键主键字段
     */
    @ApiModelProperty(value = "外键主键字段")
    private String mainField;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Integer sortOrder;
    /**
     * 自定义值转换器
     */
    @ApiModelProperty(value = "自定义值转换器")
    private String converter;
    /**
     * 查询默认值
     */
    @ApiModelProperty(value = "查询默认值")
    private String queryDefValue;
    /**
     * 查询配置字典text
     */
    @ApiModelProperty(value = "查询配置字典text")
    private String queryDictText;
    /**
     * 查询配置字典code
     */
    @ApiModelProperty(value = "查询配置字典code")
    private String queryDictField;
    /**
     * 查询配置字典table
     */
    @ApiModelProperty(value = "查询配置字典table")
    private String queryDictTable;
    /**
     * 查询显示控件
     */
    @ApiModelProperty(value = "查询显示控件")
    private String queryShowType;
    /**
     * 是否启用查询配置（1是 0否）
     */
    @ApiModelProperty(value = "是否启用查询配置（1是 0否）")
    private Integer queryConfigFlag;
    /**
     * 查询字段校验类型
     */
    @ApiModelProperty(value = "查询字段校验类型")
    private String queryValidType;
    /**
     * 查询字段是否必填（1是 0否）
     */
    @ApiModelProperty(value = "查询字段是否必填（1是 0否）")
    private Integer queryMustInput;
    /**
     * 是否支持排序（1是 0否）
     */
    @ApiModelProperty(value = "是否支持排序（1是 0否）")
    private Integer sortFlag;
}
