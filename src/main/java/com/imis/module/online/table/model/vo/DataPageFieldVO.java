package com.imis.module.online.table.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * DataPageFieldVO<br>
 * 数据页列表字段
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2021年01月11日 11:06
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "分页查询列表字段对象", description = "在线开发-分页查询列表字段对象信息")
public class DataPageFieldVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    @ApiModelProperty(value = "数据库表字段信息主键")
    private String id;
    /**
     * 字段名字
     */
    @ApiModelProperty(value = "字段名字")
    private String fieldName;
    /**
     * 字段备注
     */
    @ApiModelProperty(value = "字段备注")
    private String fieldTxt;
    /**
     * 表字段默认值
     */
    @ApiModelProperty(value = "表字段默认值")
    private String fieldDefaultValue;
    /**
     * 表单控件类型
     */
    @ApiModelProperty(value = "表单控件类型")
    private String fieldShowType;
    /**
     * 表单控件长度
     */
    @ApiModelProperty(value = "表单控件长度")
    private Integer formControlLength;
    /**
     * 扩展参数JSON
     */
    @ApiModelProperty(value = "扩展参数JSON")
    private String fieldExtendJson;
    /**
     * 控件默认值，不同的表达式展示不同的结果。
     * 1. 纯字符串直接赋给默认值；
     * 2. #{普通变量}；
     * 3. {{ 动态JS表达式 }}；
     * 4. ${填值规则编码}；
     * 填值规则表达式只允许存在一个，且不能和其他规则混用。
     */
    @ApiModelProperty(value = "控件默认值，不同的表达式展示不同的结果。 1. 纯字符串直接赋给默认值； 2. #{普通变量}； 3. {{ 动态JS表达式 }}； 4. ${填值规则编码}； 填值规则表达式只允许存在一个，且不能和其他规则混用。")
    private String controlDefaultValue;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sortOrder;
    /**
     * 自定义值转换器
     */
    @ApiModelProperty(value = "自定义值转换器")
    private String converter;
    /**
     * 查询显示控件
     */
    @ApiModelProperty(value = "查询显示控件")
    private String queryShowType;

}
