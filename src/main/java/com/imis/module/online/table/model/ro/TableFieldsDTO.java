package com.imis.module.online.table.model.ro;

import com.imis.module.online.table.model.po.TableFields;
import com.imis.module.online.table.model.po.TableHead;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 在线开发-数据库表、表字段信息-添加、修改 RO
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TableFieldsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库表表信息-更新
     */
    private TableHead tableHead;

    /**
     * 数据库表字段信息-添加
     */
    private List<TableFields> tableFieldsList;

    /**
     * 数据库表字段信息-更新
     */
    private List<TableFieldsUpdateDTO> tableFieldsUpdateList;

    public TableFieldsDTO(TableHead tableHead, List<TableFields> tableFieldsList, List<TableFieldsUpdateDTO> tableFieldsUpdateList) {
        this.tableHead = tableHead;
        this.tableFieldsList = tableFieldsList;
        this.tableFieldsUpdateList = tableFieldsUpdateList;
    }

}
