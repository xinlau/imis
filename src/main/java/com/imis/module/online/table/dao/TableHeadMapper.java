package com.imis.module.online.table.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.BaseMapper;
import com.imis.module.online.table.model.po.TableHead;
import com.imis.module.online.table.model.ro.PagingQueryTableDTO;
import com.imis.module.online.table.model.vo.TableBasicVO;
import com.imis.module.online.table.model.vo.TableHeadVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 在线开发-数据库表表信息 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
public interface TableHeadMapper extends BaseMapper<TableHead> {

    /**
     * 查询数据库表
     *
     * @param tableBasicPage      - 数据库表分页对象
     * @param pagingQueryTableDTO - 数据库表查询对象
     * @return Page<TableBasicVO> - 数据库表表基础信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    Page<TableBasicVO> pagingQueryTableListByParameter(@Param("pg") final Page<TableBasicVO> tableBasicPage, @Param("tableBasicSelect") final PagingQueryTableDTO pagingQueryTableDTO);

    /**
     * 查询数据库表
     *
     * @param tableIdentification - 数据库表表主键
     * @return TableHeadVO - 数据库表表详细信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    TableHeadVO queryTableListById(@Param(DataBaseConstant.P_KEY) final Long tableIdentification);

}