package com.imis.module.online.table.model.vo;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.imis.base.annotation.Dict;
import com.imis.base.constant.DictCodeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * 在线开发-数据库表表基础信息 VO
 *
 * @author XinLau
 * @since 2020-10-03
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="TableBasic对象", description="在线开发-数据库表表基础信息")
public class TableBasicVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 表名
     */
    @ApiModelProperty(value = "表名")
    private String tableName;
    /**
     * 表类型（ 0单表、1主表、2附表）
     */
    @ApiModelProperty(value = "表类型（ 0单表、1主表、2附表）")
    @Dict(dicCode = DictCodeConstant.TABLE_TYPE)
    private Integer tableType;
    /**
     * 表类型中文
     */
    @ApiModelProperty(value = "表类型")
    private String tableTypeText;
    /**
     * 表版本
     */
    @ApiModelProperty(value = "表版本")
    private Integer tableVersion;
    /**
     * 表说明
     */
    @ApiModelProperty(value = "表说明")
    private String tableDescription;
    /**
     * 主键类型
     */
    @ApiModelProperty(value = "主键类型")
    private String idType;
    /**
     * 同步数据库状态（N未同步；Y已同步）
     */
    @ApiModelProperty(value = "同步数据库状态（N未同步；Y已同步）")
    @Dict(dicCode = DictCodeConstant.SYNCHRONIZE_STATE)
    private String isSynchronize;
    /**
     * 映射关系（0一对多  1一对一）
     */
    @ApiModelProperty(value = "映射关系（0一对多  1一对一）")
    private Integer relationType;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime updateTime;

}
