package com.imis.module.online.table.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.CurrentUserUtils;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.online.table.dao.TableFieldsMapper;
import com.imis.module.online.table.model.po.TableFields;
import com.imis.module.online.table.service.ITableFieldsService;
import com.imis.module.system.service.ISysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 在线开发-数据库表字段信息 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class TableFieldsServiceImpl extends BaseServiceImpl<TableFieldsMapper, TableFields> implements ITableFieldsService {

    /**
     * 系统角色表-权限组 服务类
     */
    private ISysRoleService serviceBySysRoleService;

    @Autowired
    public void setServiceBySysRoleService(ISysRoleService serviceBySysRoleService) {
        this.serviceBySysRoleService = serviceBySysRoleService;
    }

    /**
     * 查询权限筛选后的数据库表字段信息
     *
     * @param tableIdentification - 数据库表表主键
     * @return List<TableFieldsVO> - 数据库表字段信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    @Override
    public List<TableFields> queryTableFieldPermissionFilteringListByTableHeadId(final Long tableIdentification) {
        // 权限过滤后的 数据库表字段信息
        List<TableFields> tableFieldPermissionFilteringList = new ArrayList<>();
        // 1、查询数据库表字段信息
        List<TableFields> tableFieldsList = this.mapper.selectList(lambdaQuery().eq(TableFields::getTableHeadId, tableIdentification));
        if (!tableFieldsList.isEmpty()){
            // 用户拥有的角色编号
            List<Long> roleIdList = this.serviceBySysRoleService.selectSysRoleIdArrayByUserName(CurrentUserUtils.getUserNameByToken());
            // 可见的 数据库表字段 ID 集合
            List<Long> visibleTableFieldsId = this.mapper.queryTableFieldIdListByRoleIdList(tableFieldsList.get(0).getTableHeadId(), roleIdList);
            if (visibleTableFieldsId.isEmpty()){
                return tableFieldsList;
            }
            // 2、权限过滤后的 数据库表字段信息
            tableFieldPermissionFilteringList = tableFieldsList.stream()
                    // 数据库表字段信息 列表 排除掉 不可见的 数据库表字段 ID 只保留可见字段
                    .filter(tableFields -> visibleTableFieldsId.contains(tableFields.getId()))
                    // 排序
                    .sorted(Comparator.comparing(TableFields::getSortOrder))
                    // 转换成为 List
                    .collect(Collectors.toList());
        }
        return tableFieldPermissionFilteringList;
    }

    /**
     * 查询数据库表字段信息
     *
     * @param tableName - 数据库表表名
     * @return List<TableFields> - 数据库表字段信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    @Override
    public List<TableFields> queryTableFieldListByTableName(final String tableName) {
        return this.mapper.queryTableFieldListByTableName(tableName);
    }

}