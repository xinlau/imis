package com.imis.module.online.table.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.online.table.dao.TableHeadMapper;
import com.imis.module.online.table.model.po.TableHead;
import com.imis.module.online.table.model.ro.PagingQueryTableDTO;
import com.imis.module.online.table.model.vo.TableBasicVO;
import com.imis.module.online.table.model.vo.TableHeadVO;
import com.imis.module.online.table.service.ITableHeadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-数据库表表信息 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class TableHeadServiceImpl extends BaseServiceImpl<TableHeadMapper, TableHead> implements ITableHeadService {

    /**
     * 查询数据库表
     *
     * @param pagingQueryTableDTO - 数据库表查询对象
     * @return Page<TableBasicVO> - 数据库表表基础信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    @Override
    public Page<TableBasicVO> pagingQueryTableListByParameter(final PagingQueryTableDTO pagingQueryTableDTO) {
//        1.页码、页长
        Page<TableBasicVO> tableBasicPage = new Page<>(pagingQueryTableDTO.getPageNumber(), pagingQueryTableDTO.getPageSize());
//        2.排序字段
        tableBasicPage.addOrder(OrderItem.desc(DataBaseConstant.CREATE_TIME_FIELD), OrderItem.desc(DataBaseConstant.UPDATE_TIME_FIELD));
//        3.条件分页查询
        tableBasicPage = mapper.pagingQueryTableListByParameter(tableBasicPage, pagingQueryTableDTO);
//        4.返回值
        return tableBasicPage;
    }

    /**
     * 查询数据库表
     *
     * @param tableIdentification - 数据库表表主键
     * @return TableHeadVO - 数据库表表详细信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    @Override
    public TableHeadVO queryTableListById(final Long tableIdentification) {
        return mapper.queryTableListById(tableIdentification);
    }

}