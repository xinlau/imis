package com.imis.module.online.table.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * PagingQueryDataDTO<br>
 * 数据库表数据分页查询对象
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月09日 14:02
 */
@Data
@ApiModel(value = "数据库表数据分页查询对象", description = "数据库表数据查询")
@EqualsAndHashCode(callSuper = false)
public class PagingQueryDataDTO extends BasePageDTO implements Serializable {

    /**
     * 数据库表编号
     */
    @ApiModelProperty(value = "数据库表编号", required = true)
    @NotNull(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;

    /**
     * 查询条件对象
     */
    @ApiModelProperty(value = "查询条件对象")
    private Map<String, Object> queryConditions;

    /**
     * 是否分页
     */
    @ApiModelProperty(value = "是否分页")
    private Boolean whetherToPage;

}
