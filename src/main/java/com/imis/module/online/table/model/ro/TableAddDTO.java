package com.imis.module.online.table.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * TableAddDTO<br>
 * 在线开发-数据库表-新增 RO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月05日 15:42
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "数据库表新增对象", description = "在线开发-数据库表-新增")
public class TableAddDTO {

    @ApiModelProperty(value = "数据库表表信息")
    private TableHeadAddDTO tableHead;

    @ApiModelProperty(value = "数据库表字段信息")
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private List<TableFieldsAddDTO> tableFieldsList;

    @ApiModelProperty(value = "数据库表索引信息")
    private List<TableIndexAddDTO> tableIndexList;

}
