package com.imis.module.online.table.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.imis.module.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色-数据库表表字段关联表 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-12-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role_table_field")
public class RoleTableField extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    @TableField("role_id")
    private Long roleId;

    /**
     * 数据库表id
     */
    @TableField("table_id")
    private Long tableId;

    /**
     * 数据库表字段id
     */
    @TableField("table_filed_id")
    private Long tableFiledId;

}
