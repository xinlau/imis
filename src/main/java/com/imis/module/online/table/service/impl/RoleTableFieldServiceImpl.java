package com.imis.module.online.table.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.online.table.model.po.RoleTableField;
import com.imis.module.online.table.dao.RoleTableFieldMapper;
import com.imis.module.online.table.service.IRoleTableFieldService;
import com.imis.module.base.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 角色-数据库表表字段关联表 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-12-28
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class RoleTableFieldServiceImpl extends BaseServiceImpl<RoleTableFieldMapper, RoleTableField> implements IRoleTableFieldService {

}