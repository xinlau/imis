package com.imis.module.online.table.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * TableUpdateDTO<br>
 * 在线开发-数据库表-更新 RO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月05日 15:42
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "数据库表更新对象", description = "在线开发-数据库表-更新")
public class TableUpdateDTO {

    @ApiModelProperty(value = "数据库表表信息", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private TableHeadUpdateDTO tableHead;

    @ApiModelProperty(value = "数据库表字段信息")
    private List<TableFieldsUpdateDTO> tableFieldsList;
    @ApiModelProperty(value = "被删除的数据库表字段")
    private List<Long> deleteFieldIds;

    @ApiModelProperty(value = "数据库表索引信息")
    private List<TableIndexUpdateDTO> tableIndexList;
    @ApiModelProperty(value = "被删除的数据库表索引")
    private List<Long> deleteIndexIds;

}
