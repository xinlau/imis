package com.imis.module.online.table.model.ro;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 角色-数据库表表字段关联表 RO
 *
 * @author XinLau
 * @since 2020-12-28
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="RoleTableField对象", description="角色-数据库表表字段关联表")
public class RoleTableFieldDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private Long roleId;
    /**
     * 数据库表id
     */
    @ApiModelProperty(value = "数据库表id")
    private Long tableId;
    /**
     * 数据库表字段id
     */
    @ApiModelProperty(value = "数据库表字段id")
    private Long tableFiledId;
}
