package com.imis.module.online.table.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 *
 * 在线开发-数据库表索引-添加 RO
 *
 * @author XinLau
 * @since 2020-10-03
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TableIndex添加对象", description="在线开发-数据库表索引")
public class TableIndexAddDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 索引名称
     */
    @ApiModelProperty(value = "索引名称", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String indexName;
    /**
     * 索引栏位
     */
    @ApiModelProperty(value = "索引栏位", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String indexField;
    /**
     * 索引类型
     */
    @ApiModelProperty(value = "索引类型", required = true)
    @NotBlank(message = ValidationMessageConstant.EMPTY_ERR)
    private String indexType;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
}
