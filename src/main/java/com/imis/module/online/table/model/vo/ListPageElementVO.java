package com.imis.module.online.table.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.imis.module.online.form.model.vo.FormButtonVO;
import com.imis.module.system.model.vo.DictVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * ListPageElementVO<br>
 * 列表页面元素
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2021年01月11日 11:31
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "分页查询列表页面元素对象", description = "在线开发-分页查询列表页面元素对象信息")
public class ListPageElementVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表ID
     */
    @ApiModelProperty(value = "数据库表表编号")
    private String tableHeadId;

    /**
     * 表单风格
     */
    @ApiModelProperty(value = "表单风格")
    private String formTemplate;

    /**
     * 表说明
     */
    @ApiModelProperty(value = "表说明")
    private String tableDescription;

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名")
    private String tableName;

    /**
     * 表类型（ 0单表、1主表、2附表）
     */
    @ApiModelProperty(value = "表类型（ 0单表、1主表、2附表）")
    private Integer tableType;

    /**
     * 是否分页（0否 1是）
     */
    @ApiModelProperty(value = "是否分页（0否 1是）")
    private Integer isPage;

    /**
     * 是否带 checkbox（0否 1是）
     */
    @ApiModelProperty(value = "是否带 checkbox（0否 1是）")
    private Integer isCheckbox;

    /**
     * 是否有横向滚动条（0否 1是）
     */
    @ApiModelProperty(value = "是否有横向滚动条（0否 1是）")
    private Integer isScroll;

    /**
     * 数据查询条件字段列表
     */
    @ApiModelProperty(value = "数据查询条件字段列表")
    private List<DataQueryConditionsFieldVO> dataQueryConditionsFieldList;

    /**
     * 下拉字典数据
     */
    @ApiModelProperty(value = "下拉字典数据")
    private Map<String, List<DictVO>> dictOptions;

    /**
     * 自定义按钮
     */
    @ApiModelProperty(value = "自定义按钮")
    private List<FormButtonVO> formButtonList;

    /**
     * 数据页列表字段
     */
    @ApiModelProperty(value = "数据页列表字段")
    private List<DataPageFieldVO> dataPageFieldList;

    /**
     * JS增强字符串
     */
    @ApiModelProperty(value = "JS增强字符串")
    private String javaScriptEnhance;

    /**
     * 树表主键字段
     */
    @ApiModelProperty(value = "树表主键字段")
    private String treeIdFieldName;

    /**
     * 树开表单列字段
     */
    @ApiModelProperty(value = "树开表单列字段")
    private String treeFieldName;

    /**
     * 树形表单父id
     */
    @ApiModelProperty(value = "树形表单父id")
    private String treeParentIdFieldName;

}
