package com.imis.module.online.table.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * TableVO<br>
 * 数据库表表详细信息 VO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月09日 17:17
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="Table对象", description="在线开发-数据库表表详细信息")
public class TableVO {

    @ApiModelProperty(value = "数据库表表信息")
    private TableHeadVO tableHead;

    @ApiModelProperty(value = "数据库表字段信息")
    private List<TableFieldsVO> tableFieldsList;

    @ApiModelProperty(value = "数据库表索引信息")
    private List<TableIndexVO> tableIndexList;

}
