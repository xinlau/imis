package com.imis.module.online.table.model.converter;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.online.table.model.po.TableHead;
import com.imis.module.online.table.model.ro.TableHeadAddDTO;
import com.imis.module.online.table.model.ro.TableHeadUpdateDTO;
import com.imis.module.online.table.model.vo.ListPageElementVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * <p>
 * 在线开发-数据库表表信息 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Mapper
public interface TableHeadConverter {

    /**
     * 在线开发-数据库表表信息 转换器实例
     */
    TableHeadConverter INSTANCE = Mappers.getMapper(TableHeadConverter.class);

    /**
     * 在线开发-数据库表表信息 RO转PO
     *
     * @param add - 在线开发-数据库表表信息 RO
     * @return TableHead - 在线开发-数据库表表信息 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "tableVersion", constant = "1"),
            @Mapping(target = "isSynchronize", constant = CommonConstant.TO_UPPER_CASE_N),
            @Mapping(target = "copyVersion", ignore = true),
            @Mapping(target = "copyType", ignore = true),
            @Mapping(target = "physicId", ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    TableHead getAddEntity(TableHeadAddDTO add);

    /**
     * 在线开发-数据库表表信息 RO转PO
     *
     * @param update - 在线开发-数据库表表信息 RO
     * @return TableHead - 在线开发-数据库表表信息 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "tableVersion", ignore = true),
            @Mapping(target = "isSynchronize", ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    TableHead getUpdateEntity(TableHeadUpdateDTO update);

    /**
     * 在线开发-数据库表表信息 PO转PO
     *
     * @param tableHeadBackupVersion     - 当前最新 Copy 最新版本号
     * @param sourceTableHead - 在线开发-源数据库表表信息 PO
     * @return TableHead - 在线开发-数据库表表信息 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true),
            @Mapping(target = "tableVersion", ignore = true),
            @Mapping(target = "relationType", ignore = true),
            @Mapping(target = "subTableCheckList", ignore = true),
            @Mapping(target = "tabSortOrder", ignore = true),
            @Mapping(target = "tableName", expression = "java(sourceTableHead.getTableName() + com.imis.base.constant.CommonConstant.DOLLAR + tableHeadBackupVersion)"),
            @Mapping(target = "physicId", source = "sourceTableHead.id"),
            @Mapping(target = "copyVersion", source = "tableHeadBackupVersion"),
            @Mapping(target = "copyType", constant = CommonConstant.ONE),
            @Mapping(target = "tableType", constant = CommonConstant.ONE),
            @Mapping(target = "isSynchronize", constant = CommonConstant.TO_UPPER_CASE_N)
    })
    TableHead getDataBaseTableHeadBackup(TableHead sourceTableHead, Integer tableHeadBackupVersion);

    /**
     * 在线开发-数据库表表信息 RO转PO
     *
     * @param tableHead - 在线开发-数据库表表信息 PO
     * @param update  - 在线开发-数据库表表信息 RO
     * @return TableHead - 在线开发-数据库表表信息 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = "tableVersion", ignore = true),
            @Mapping(target = "isSynchronize", ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget TableHead tableHead, TableHeadUpdateDTO update);

    /**
     * 在线开发-数据库表表信息 RO转PO
     *
     * @param tableHead - 在线开发-数据库表表信息 PO
     * @return ListPageElementVO - 在线开发-分页查询列表字段对象
     */
    @Mappings({
            @Mapping(target = "dataQueryConditionsFieldList", ignore = true),
            @Mapping(target = "formButtonList", ignore = true),
            @Mapping(target = "dataPageFieldList", ignore = true),
            @Mapping(target = "dictOptions", ignore = true),
            @Mapping(target = "javaScriptEnhance", ignore = true),
            @Mapping(target = "treeIdFieldName", expression = "java(com.imis.base.constant.CommonConstant.INTEGER_YES.equals(tableHead.getIsTree()) ? tableHead.getTreeIdFieldName() : null )"),
            @Mapping(target = "treeFieldName", expression = "java(com.imis.base.constant.CommonConstant.INTEGER_YES.equals(tableHead.getIsTree()) ? tableHead.getTreeFieldName() : null )"),
            @Mapping(target = "treeParentIdFieldName", expression = "java(com.imis.base.constant.CommonConstant.INTEGER_YES.equals(tableHead.getIsTree()) ? tableHead.getTreeParentIdFieldName() : null )"),
            @Mapping(target = "tableHeadId", source = "tableHead.id")
    })
    ListPageElementVO getListPageElement(TableHead tableHead);

}
