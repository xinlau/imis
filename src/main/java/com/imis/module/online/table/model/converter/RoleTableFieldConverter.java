package com.imis.module.online.table.model.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.online.table.model.po.RoleTableField;
import com.imis.module.online.table.model.vo.RoleTableFieldVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 角色-数据库表表字段关联表 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-12-28
 */
@Mapper
public interface RoleTableFieldConverter {

    /**
     * 角色-数据库表表字段关联表 转换器实例
     */
    RoleTableFieldConverter INSTANCE = Mappers.getMapper(RoleTableFieldConverter.class);

    /**
     * 角色-数据库表表字段关联表 PO转VO
     *
     * @param entity - 角色-数据库表表字段关联表 PO
     * @return RoleTableFieldVO - 角色-数据库表表字段关联表 VO
     */
    @Mappings({})
    RoleTableFieldVO getReturnValue(RoleTableField entity);

    /**
     * 角色-数据库表表字段关联表 PO转VO
     *
     * @param entityList - 角色-数据库表表字段关联表 数组PO
     * @return List<RoleTableFieldVO> - 角色-数据库表表字段关联表 数组VO
     */
    @Mappings({})
    List<RoleTableFieldVO> getReturnValue(List<RoleTableField> entityList);

    /**
     * 角色-数据库表表字段关联表 分页PO转分页VO
     *
     * @param entityPage - 角色-数据库表表字段关联表 分页PO
     * @return Page<RoleTableFieldVO> - 角色-数据库表表字段关联表 分页VO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
     })
    Page<RoleTableFieldVO> getReturnValue(Page<RoleTableField> entityPage);

}
