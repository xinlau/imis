package com.imis.module.online.table.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.online.table.bus.DataBaseTableBus;
import com.imis.module.online.table.model.ro.DataBaseTableSynchronizeDTO;
import com.imis.module.online.table.model.ro.PagingQueryTableDTO;
import com.imis.module.online.table.model.ro.TableAddDTO;
import com.imis.module.online.table.model.ro.TableUpdateDTO;
import com.imis.module.online.table.model.vo.TableBasicVO;
import com.imis.module.online.table.model.vo.TableVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 在线开发-数据库表 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Slf4j
@RestController
@RequestMapping(path = "/onl/table")
@Api(tags = {"在线开发-数据库表"})
@ApiSort(1)
public class DataBaseTableController extends BaseController<DataBaseTableBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询数据库表接口", notes = "分页查询数据库表基础信息接口")
    @RequiresPermissions(PermissionsValueConstant.DATABASE_TABLE_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<TableBasicVO>> pagingQueryTableListByParameter(PagingQueryTableDTO pagingQueryTableDTO) {
        return service.pagingQueryTableListByParameter(pagingQueryTableDTO);
    }

    @GetMapping(path = "/queryById")
    @ApiOperation(value = "查询数据库表信息接口", notes = "查询数据库表详细信息接口")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_PAGE, PermissionsValueConstant.DATABASE_TABLE_QUERY_BY_ID}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public CommonResponse<TableVO> queryTableInfoListById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryTableInfoListById(id);
    }

    @PostMapping(path = "/addDataBaseTable")
    @RequiresPermissions(PermissionsValueConstant.DATABASE_TABLE_ADD)
    @ApiOperation(value = "添加数据库表", notes = "添加数据库表")
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse addDataBaseTable(@RequestBody @Valid TableAddDTO addDataBaseTable) {
        return service.addDataBaseTable(addDataBaseTable);
    }

    @PutMapping(path = "/copyDataBaseTable")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_PAGE, PermissionsValueConstant.DATABASE_TABLE_COPY}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperation(value = "复制数据库表", notes = "复制数据库表")
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse copyDataBaseTable(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.copyDataBaseTable(id);
    }

    @PutMapping(path = "/updateDataBaseTable")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_PAGE, PermissionsValueConstant.DATABASE_TABLE_UPDATE}, logical = Logical.AND)
    @ApiOperation(value = "修改数据库表", notes = "修改数据库表")
    @ApiOperationSupport(order = 5, author = "XinLau")
    public BaseResponse updateDataBaseTable(@RequestBody @Valid TableUpdateDTO updateDataBaseTable) {
        return service.updateDataBaseTable(updateDataBaseTable);
    }

    @PostMapping(path = "/doDataBaseTableSynchronize")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_PAGE, PermissionsValueConstant.DATABASE_TABLE_SYNCHRONIZE}, logical = Logical.AND)
    @ApiOperation(value = "数据库表同步", notes = "数据库表同步")
    @ApiOperationSupport(order = 6, author = "XinLau")
    public BaseResponse doDataBaseTableSynchronize(@RequestBody @Valid DataBaseTableSynchronizeDTO dataBaseTableSynchronize) {
        return service.doDataBaseTableSynchronize(dataBaseTableSynchronize);
    }

    @GetMapping(path = "/removeDataBaseTable")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_PAGE, PermissionsValueConstant.DATABASE_TABLE_REMOVE}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperation(value = "移除数据库表", notes = "删除数据库表，保留信息")
    @ApiOperationSupport(order = 7, author = "XinLau")
    public BaseResponse removeDataBaseTable(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.removeDataBaseTable(id);
    }

    @GetMapping(path = "/deleteDataBaseTable")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_PAGE, PermissionsValueConstant.DATABASE_TABLE_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperation(value = "删除数据库表", notes = "删除数据库表所有信息")
    @ApiOperationSupport(order = 8, author = "XinLau")
    public BaseResponse deleteDataBaseTable(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.deleteDataBaseTable(id);
    }

}