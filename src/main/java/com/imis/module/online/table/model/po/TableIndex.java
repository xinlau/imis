package com.imis.module.online.table.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 在线开发-数据库表索引 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("onl_table_index")
public class TableIndex extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主表id
     */
    @TableField("table_head_id")
    private Long tableHeadId;

    /**
     * 索引名称
     */
    @TableField("index_name")
    private String indexName;

    /**
     * 索引栏位
     */
    @TableField("index_field")
    private String indexField;

    /**
     * 索引类型
     */
    @TableField("index_type")
    private String indexType;

    /**
     * 是否同步数据库（N未同步；Y已同步）
     */
    @TableField("is_synchronize")
    private String isSynchronize;

    /**
     * 是否删除（0未删除 1删除）
     */
    @TableField("del_flag")
    private Integer delFlag;

    /**
     * 备注
     */
    @TableField("description")
    private String description;

}
