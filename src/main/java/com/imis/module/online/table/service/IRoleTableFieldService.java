package com.imis.module.online.table.service;

import com.imis.module.online.table.model.po.RoleTableField;
import com.imis.module.base.BaseService;

/**
 * <p>
 * 角色-数据库表表字段关联表 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-12-28
 */
public interface IRoleTableFieldService extends BaseService<RoleTableField> {

}