package com.imis.module.online.table.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.online.table.bus.FormApiBus;
import com.imis.module.online.table.model.ro.DataAddDTO;
import com.imis.module.online.table.model.ro.DataUpdateDTO;
import com.imis.module.online.table.model.ro.PagingQueryDataDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 在线开发-数据库API 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Slf4j
@RestController
@RequestMapping(path = "/onl/api")
@Api(tags = {"在线开发-数据库API"})
@ApiSort(2)
public class FormApiController extends BaseController<FormApiBus> {

    @GetMapping(path = "/queryListPageElement")
    @ApiOperation(value = "获取列表页面元素接口", notes = "获取列表页面构成元素")
    @RequiresPermissions(PermissionsValueConstant.DATABASE_TABLE_DATA_QUERY_LIST_PAGE_ELEMENT)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<?> queryListPageElement(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryListPageElement(id);
    }

    @GetMapping(path = "/queryJavaScript")
    @ApiOperation(value = "查询数据库表自定义JS接口", notes = "数据库表自定义JS")
    @RequiresPermissions(PermissionsValueConstant.JAVA_SCRIPT_QUERY_BY_JAVA_SCRIPT_TYPE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表编号", dataType = "Long", dataTypeClass = Long.class, required = true),
            @ApiImplicitParam(name = "javaScriptType", value = "JS类型", dataType = "String", dataTypeClass = String.class, required = false)
    })
    @ApiOperationSupport(order = 2, author = "XinLau")
    public CommonResponse<?> queryJavaScript(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id, @RequestParam(name = "javaScriptType", required = false) String javaScriptType) {
        return service.queryJavaScript(id, javaScriptType);
    }

    @GetMapping(path = "/queryJava")
    @ApiOperation(value = "查询数据库表自定义Java接口", notes = "数据库表自定义Java")
    @RequiresPermissions(PermissionsValueConstant.JAVA_QUERY_BY_BUTTON_CODE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表编号", dataType = "Long", dataTypeClass = Long.class, required = true),
            @ApiImplicitParam(name = "buttonCode", value = "自定义按钮", dataType = "String", dataTypeClass = String.class, required = false)
    })
    @ApiOperationSupport(order = 3, author = "XinLau")
    public CommonResponse<?> queryJava(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id, @RequestParam(name = "buttonCode", required = false) String buttonCode) {
        return service.queryJava(id, buttonCode);
    }

    @GetMapping(path = "/queryStructuredQueryLanguage")
    @ApiOperation(value = "查询数据库表自定义SQL接口", notes = "数据库表自定义SQL")
    @RequiresPermissions(PermissionsValueConstant.STRUCTURED_QUERY_LANGUAGE_QUERY_BY_BUTTON_CODE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表编号", dataType = "Long", dataTypeClass = Long.class, required = true),
            @ApiImplicitParam(name = "buttonCode", value = "自定义按钮", dataType = "String", dataTypeClass = String.class, required = false)
    })
    @ApiOperationSupport(order = 4, author = "XinLau")
    public CommonResponse<?> queryStructuredQueryLanguage(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id, @RequestParam(name = "buttonCode", required = false) String buttonCode) {
        return service.queryStructuredQueryLanguage(id, buttonCode);
    }

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询数据库表数据接口", notes = "分页查询数据库表数据")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_DATA_QUERY_LIST_PAGE_ELEMENT, PermissionsValueConstant.DATABASE_TABLE_DATA_PAGE}, logical = Logical.AND)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public CommonResponse<?> pagingQueryDataListByParameter(PagingQueryDataDTO pagingQueryData) {
        return service.pagingQueryDataListByParameter(pagingQueryData);
    }

    @GetMapping(path = "/queryDataAddField")
    @ApiOperation(value = "查询数据库表数据添加字段接口", notes = "数据库表数据添加字段")
    @RequiresPermissions(PermissionsValueConstant.DATABASE_TABLE_DATA_ADD_FIELD)
    @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 6, author = "XinLau")
    public CommonResponse<?> queryDataAddField(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id) {
        return service.queryDataAddField(id);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "添加数据库表数据接口", notes = "单条添加")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_DATA_ADD_FIELD, PermissionsValueConstant.DATABASE_TABLE_DATA_ADD}, logical = Logical.AND)
    @ApiOperationSupport(order = 7, author = "XinLau")
    public BaseResponse addData(@RequestBody @Valid DataAddDTO dataAdd) {
        return service.addData(dataAdd);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "删除数据库表数据接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_DATA_PAGE, PermissionsValueConstant.DATABASE_TABLE_DATA_DELETE}, logical = Logical.AND)
    @ApiImplicitParams({
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表编号", dataType = "Long", dataTypeClass = Long.class, required = true),
            @ApiImplicitParam(name = "dataId", value = "数据库表数据编号", dataType = "Long", dataTypeClass = Long.class, required = true)
    })
    @ApiOperationSupport(order = 8, author = "XinLau")
    public BaseResponse deleteData(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id, @RequestParam(name = "dataId", required = true) Long dataId) {
        return service.deleteData(id, dataId);
    }

    @GetMapping(path = "/queryDataById")
    @ApiOperation(value = "查询数据库表数据信息接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.DATABASE_TABLE_DATA_QUERY_BY_ID)
    @ApiImplicitParams({
            @ApiImplicitParam(name = DataBaseConstant.P_KEY, value = "数据库表编号", dataType = "Long", dataTypeClass = Long.class, required = true),
            @ApiImplicitParam(name = "dataId", value = "数据库表数据编号", dataType = "String", dataTypeClass = String.class, required = true)
    })
    @ApiOperationSupport(order = 9, author = "XinLau")
    public CommonResponse<?> queryDataById(@RequestParam(name = DataBaseConstant.P_KEY, required = true) Long id, @RequestParam(name = "dataId", required = true) String dataId) {
        return service.queryDataById(id, dataId);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "修改数据库表数据接口", notes = "单条修改")
    @RequiresPermissions(value = {PermissionsValueConstant.DATABASE_TABLE_DATA_QUERY_BY_ID, PermissionsValueConstant.DATABASE_TABLE_DATA_UPDATE}, logical = Logical.AND)
    @ApiOperationSupport(order = 10, author = "XinLau")
    public BaseResponse updateData(@RequestBody @Valid DataUpdateDTO dataUpdate) {
        return service.updateData(dataUpdate);
    }

}
