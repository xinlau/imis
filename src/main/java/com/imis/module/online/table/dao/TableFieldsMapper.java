package com.imis.module.online.table.dao;

import com.imis.module.base.BaseMapper;
import com.imis.module.online.table.model.po.TableFields;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 在线开发-数据库表字段信息 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
public interface TableFieldsMapper extends BaseMapper<TableFields> {

    /**
     * 查询数据库表字段信息
     *
     * @param tableName - 数据库表表名
     * @return List<TableFields> - 数据库表字段信息
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    List<TableFields> queryTableFieldListByTableName(@Param("tableName") final String tableName);

    /**
     * 根据角色编号查询角色可见数据库表字段编号集合
     *
     * @param tableHeadId - 数据库表编号
     * @param roleIdList  - 用户编号
     * @return List<Long> - 角色可见数据库表字段编号集合
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/28 11:07
     */
    List<Long> queryTableFieldIdListByRoleIdList(@Param("tableHeadId") final Long tableHeadId, @Param("roleIdList") List<Long> roleIdList);

}