package com.imis.module.online.table.dao;

import com.imis.module.online.table.model.po.RoleTableField;
import com.imis.module.base.BaseMapper;

/**
 * <p>
 * 角色-数据库表表字段关联表 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-12-28
 */
public interface RoleTableFieldMapper extends BaseMapper<RoleTableField> {

}