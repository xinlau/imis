package com.imis.module.online.table.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.ConvertUtils;
import com.imis.base.util.form.FormUtil;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.online.table.dao.TableIndexMapper;
import com.imis.module.online.table.model.po.TableIndex;
import com.imis.module.online.table.service.ITableIndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 在线开发-数据库表索引 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class TableIndexServiceImpl extends BaseServiceImpl<TableIndexMapper, TableIndex> implements ITableIndexService {

    /**
     * 执行数据库模式定义语言（DDL）<br>
     * 关键字：CREATE, ALTER, DROP, RENAME, TRUNCATE
     *
     * @param structureQueryLanguage - DataDefinitionLanguage 数据库模式定义语言（DDL）
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 10:57
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Boolean executeDataDefinitionLanguage(final String structureQueryLanguage) {
        try {
            baseMapper.executeDataDefinitionLanguage(structureQueryLanguage);
            return Boolean.TRUE;
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
            return Boolean.FALSE;
        }
    }

    /**
     * 执行 COUNT SQL语句
     *
     * @param structureQueryLanguage - 统计索引数量SQL
     * @return Integer -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Integer executeCountStructuredQueryLanguage(final String structureQueryLanguage) {
        return baseMapper.countTheNumberOfIndexes(structureQueryLanguage);
    }

    /**
     * 判断索引是否存在
     *
     * @param structureQueryLanguage - 统计索引数量SQL
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Boolean isExistIndex(final String structureQueryLanguage) {
        if (ConvertUtils.isEmpty(structureQueryLanguage)) {
            return Boolean.TRUE;
        }
        Integer count = this.executeCountStructuredQueryLanguage(structureQueryLanguage);
        if (count != null && count > 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * 执行 INSERT SQL语句
     *
     * @param params - 统计索引数量SQL
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Boolean executeInsertStructuredQueryLanguage(final Map<String, Object> params) {
        try {
            baseMapper.executeInsertStructuredQueryLanguage(params);
            return Boolean.TRUE;
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
            return Boolean.FALSE;
        }
    }

    /**
     * 执行 DELETE SQL语句
     *
     * @param structureQueryLanguage - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Boolean executeDeleteStructuredQueryLanguage(final String structureQueryLanguage) {
        try {
            baseMapper.executeDeleteStructuredQueryLanguage(structureQueryLanguage);
            return Boolean.TRUE;
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
            return Boolean.FALSE;
        }
    }

    /**
     * 执行 SELECT SQL语句
     *
     * @param structureQueryLanguage - SQL语句
     * @return Map<String, Object> - 数据库表数据
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Map<String, Object> executeQueryStructuredQueryLanguage(final String structureQueryLanguage) {
        return baseMapper.executeQueryStructuredQueryLanguage(structureQueryLanguage);
    }

    /**
     * 执行 UPDATE SQL语句
     *
     * @param structureQueryLanguage - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Boolean executeUpdateStructuredQueryLanguage(final String structureQueryLanguage) {
        try {
            Map<String, Object> params = new HashMap<>(1);
            params.put("execute_structured_query_language_string", structureQueryLanguage);
            baseMapper.executeUpdateStructuredQueryLanguage(params);
            return Boolean.TRUE;
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
            return Boolean.FALSE;
        }
    }

    /**
     * 执行 UPDATE SQL语句
     *
     * @param params - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Boolean executeUpdateStructuredQueryLanguage(final Map<String, Object> params) {
        try {
            baseMapper.executeUpdateStructuredQueryLanguage(params);
            return Boolean.TRUE;
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
            return Boolean.FALSE;
        }
    }

    /**
     * 查询指定数据库表的指定数据所有字段内容
     *
     * @param tableName - 数据库表表名
     * @param dataId    - 数据库表数据编号
     * @return Map<String, Object> - 指定数据库表的指定数据所有字段内容
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Map<String, Object> queryTableDataByTableNameAndDataId(final String tableName, final Long dataId) {
        return baseMapper.queryTableDataByTableNameAndDataId(tableName, dataId);
    }

    /**
     * 查询子节点数量
     *
     * @param tableName             - 数据库表表名
     * @param treeParentIdFieldName - 树形表单父 ID 字段
     * @param parentId              - 数据 父 ID
     * @return Integer - 子节点数量
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Integer queryNumberOfChildNodes(final String tableName, final String treeParentIdFieldName, final String parentId) {
        return baseMapper.queryNumberOfChildNodes(tableName, treeParentIdFieldName, parentId);
    }

    /**
     * 修改树状态 无子节点的has_child设置为0
     *
     * @param tableName - 数据库表表名
     * @param hasChild  - 树表主键字段
     * @param parentId  - 数据 父 ID
     * @return Integer - 子节点数量
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Boolean updateTreeNodeNoChild(final String tableName, final String hasChild, final String parentId) {
        Map<String, Object> params = FormUtil.getTreeNodeNoChildNodeStructuredQueryLanguage(tableName, hasChild, parentId);
        try {
            baseMapper.executeUpdateStructuredQueryLanguage(params);
            return Boolean.TRUE;
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
            return Boolean.FALSE;
        }
    }

    /**
     * 执行 SELECT SQL语句
     *
     * @param structureQueryLanguage - SQL语句
     * @return List<Map<String, Object>> - 数据库表数据
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public List<Map<String, Object>> executeQueryTableDataListByStructuredQueryLanguage(final String structureQueryLanguage) {
        return baseMapper.executeQueryTableDataListByStructuredQueryLanguage(structureQueryLanguage);
    }

    /**
     * 执行分页 SELECT SQL语句
     *
     * @param pageQuery - 分页对象
     * @param structureQueryLanguage - SQL语句
     * @return List<Map<String, Object>> - 数据库表数据
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/5 11:11
     */
    @Override
    @DS(DataBaseConstant.DATA_SOURCE_BUSINESS)
    public Page<Map<String, Object>> executePagingQueryTableDataListByStructuredQueryLanguage(final Page<Map<String, Object>> pageQuery, final String structureQueryLanguage) {
        return baseMapper.executePagingQueryTableDataListByStructuredQueryLanguage(pageQuery, structureQueryLanguage);
    }

}