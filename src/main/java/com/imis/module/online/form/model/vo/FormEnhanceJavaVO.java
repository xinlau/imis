package com.imis.module.online.form.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.imis.base.annotation.Dict;
import com.imis.base.constant.DictCodeConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * 在线开发-Java增强 VO
 *
 * @author XinLau
 * @since 2020-10-27
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="FormEnhanceJava对象", description="在线开发-Java增强")
public class FormEnhanceJavaVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Java增强 编号
     */
    @ApiModelProperty(value = "Java增强 编号")
    private String id;
    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID")
    private String tableHeadId;
    /**
     * 按钮编码
     */
    @ApiModelProperty(value = "按钮编码")
    private String buttonCode;
    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String javaType;
    /**
     * 数值
     */
    @ApiModelProperty(value = "数值")
    private String javaValue;
    /**
     * 生效状态(0无效，1有效)
     */
    @Dict(dicCode = DictCodeConstant.STATUS)
    @ApiModelProperty(value = "生效状态(0无效，1有效)")
    private Integer activeStatus;
    /**
     * 事件状态(end:结束，start:开始)
     */
    @ApiModelProperty(value = "事件状态(end:结束，start:开始)")
    private String event;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
}
