package com.imis.module.online.form.bus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.enums.ArgumentResponseEnum;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseBus;
import com.imis.module.online.form.model.converter.FormEnhanceStructuredQueryLanguageConverter;
import com.imis.module.online.form.model.po.FormEnhanceStructuredQueryLanguage;
import com.imis.module.online.form.model.ro.FormEnhanceStructuredQueryLanguageAddDTO;
import com.imis.module.online.form.model.ro.FormEnhanceStructuredQueryLanguageUpdateDTO;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceStructuredQueryLanguageDTO;
import com.imis.module.online.form.model.vo.FormEnhanceStructuredQueryLanguageVO;
import com.imis.module.online.form.service.IFormEnhanceStructuredQueryLanguageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-SQL增强 业务处理类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-28
 */
@Slf4j
@Service
public class FormEnhanceStructuredQueryLanguageBus extends BaseBus {

    /**
     * 在线开发-SQL增强 服务类
     */
    private IFormEnhanceStructuredQueryLanguageService serviceByFormEnhanceStructuredQueryLanguageService;

    @Autowired
    public void setFormEnhanceStructuredQueryLanguageService(IFormEnhanceStructuredQueryLanguageService serviceByFormEnhanceStructuredQueryLanguageService) {
        this.serviceByFormEnhanceStructuredQueryLanguageService = serviceByFormEnhanceStructuredQueryLanguageService;
    }

    /**
     * 在线开发-StructuredQueryLanguage 增强-添加校验
     *
     * @param formEnhanceStructuredQueryLanguageAdd - FormEnhanceStructuredQueryLanguage 添加对象
     * @return FormEnhanceStructuredQueryLanguage - 在线开发-StructuredQueryLanguage 增强
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 15:47
     */
    private FormEnhanceStructuredQueryLanguage formEnhanceStructuredQueryLanguageAddVerification(final FormEnhanceStructuredQueryLanguageAddDTO formEnhanceStructuredQueryLanguageAdd) {
        FormEnhanceStructuredQueryLanguage formEnhanceStructuredQueryLanguage = FormEnhanceStructuredQueryLanguageConverter.INSTANCE.getAddEntity(formEnhanceStructuredQueryLanguageAdd);
        return formEnhanceStructuredQueryLanguage;
    }

    /**
     * 在线开发-StructuredQueryLanguage 增强-更新校验
     *
     * @param formEnhanceStructuredQueryLanguageUpdate - FormEnhanceStructuredQueryLanguage 更新对象
     * @return FormEnhanceStructuredQueryLanguage - 在线开发-StructuredQueryLanguage 增强
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    private FormEnhanceStructuredQueryLanguage formEnhanceStructuredQueryLanguageUpdateVerification(final FormEnhanceStructuredQueryLanguageUpdateDTO formEnhanceStructuredQueryLanguageUpdate) {
        FormEnhanceStructuredQueryLanguage formEnhanceStructuredQueryLanguage = serviceByFormEnhanceStructuredQueryLanguageService.getById(formEnhanceStructuredQueryLanguageUpdate.getId());
        // 校验在线开发-StructuredQueryLanguage 增强是否存在
        ArgumentResponseEnum.FORM_STRUCTURED_QUERY_LANGUAGE_UPDATE_ERR_NON.assertIsNull(formEnhanceStructuredQueryLanguage, formEnhanceStructuredQueryLanguageUpdate.getId());
        FormEnhanceStructuredQueryLanguageConverter.INSTANCE.getUpdateEntity(formEnhanceStructuredQueryLanguage, formEnhanceStructuredQueryLanguageUpdate);
        return formEnhanceStructuredQueryLanguage;
    }

    /**
     * 在线开发-StructuredQueryLanguage 增强-分页查询
     *
     * @param pagingQueryFormEnhanceStructuredQueryLanguage - FormEnhanceStructuredQueryLanguage分页查询对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public CommonResponse<Page<FormEnhanceStructuredQueryLanguageVO>> pagingQueryListByParameter(final PagingQueryFormEnhanceStructuredQueryLanguageDTO pagingQueryFormEnhanceStructuredQueryLanguage) {
        // 查询数据库内的StructuredQueryLanguage 增强
        Page<FormEnhanceStructuredQueryLanguageVO> formEnhanceStructuredQueryLanguagePage = serviceByFormEnhanceStructuredQueryLanguageService.pagingQueryListByParameter(pagingQueryFormEnhanceStructuredQueryLanguage);
        return new CommonResponse(formEnhanceStructuredQueryLanguagePage);
    }

    /**
     * 在线开发-StructuredQueryLanguage 增强-添加
     *
     * @param formEnhanceStructuredQueryLanguageAdd - FormEnhanceStructuredQueryLanguage添加对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse add(final FormEnhanceStructuredQueryLanguageAddDTO formEnhanceStructuredQueryLanguageAdd) {
        // 创建新StructuredQueryLanguage 增强 校验StructuredQueryLanguage 增强重复
        FormEnhanceStructuredQueryLanguage formEnhanceStructuredQueryLanguage = formEnhanceStructuredQueryLanguageAddVerification(formEnhanceStructuredQueryLanguageAdd);
        boolean save = serviceByFormEnhanceStructuredQueryLanguageService.save(formEnhanceStructuredQueryLanguage);
        ArgumentResponseEnum.FORM_STRUCTURED_QUERY_LANGUAGE_ADD_ERR.assertIsTrue(save);
        return new BaseResponse();
    }

    /**
     * 在线开发-StructuredQueryLanguage 增强-查询
     *
     * @param id - FormEnhanceStructuredQueryLanguage标识
     * @return CommonResponse<FormEnhanceStructuredQueryLanguageVO>
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public CommonResponse<FormEnhanceStructuredQueryLanguageVO> queryById(final Long id) {
        FormEnhanceStructuredQueryLanguage formEnhanceStructuredQueryLanguage = serviceByFormEnhanceStructuredQueryLanguageService.getById(id);
        // 校验StructuredQueryLanguage 增强是否存在
        ArgumentResponseEnum.FORM_STRUCTURED_QUERY_LANGUAGE_QUERY_ERR_FORM_STRUCTURED_QUERY_LANGUAGE_NON.assertNotNull(formEnhanceStructuredQueryLanguage, id);
        return new CommonResponse(FormEnhanceStructuredQueryLanguageConverter.INSTANCE.getReturnValue(formEnhanceStructuredQueryLanguage));
    }

    /**
     * 在线开发-StructuredQueryLanguage 增强-更新
     *
     * @param formEnhanceStructuredQueryLanguageUpdate - FormEnhanceStructuredQueryLanguage更新对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse updateById(final FormEnhanceStructuredQueryLanguageUpdateDTO formEnhanceStructuredQueryLanguageUpdate) {
        // 更新StructuredQueryLanguage 增强 校验StructuredQueryLanguage 增强更新对象
        FormEnhanceStructuredQueryLanguage formEnhanceStructuredQueryLanguage = formEnhanceStructuredQueryLanguageUpdateVerification(formEnhanceStructuredQueryLanguageUpdate);
        boolean save = serviceByFormEnhanceStructuredQueryLanguageService.updateById(formEnhanceStructuredQueryLanguage);
        ArgumentResponseEnum.FORM_STRUCTURED_QUERY_LANGUAGE_UPDATE_ERR.assertIsTrue(save, formEnhanceStructuredQueryLanguageUpdate.getId());
        return new BaseResponse();
    }

    /**
     * 在线开发-StructuredQueryLanguage 增强-删除
     *
     * @param id - FormEnhanceStructuredQueryLanguage标识
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse deleteById(final Long id) {
        FormEnhanceStructuredQueryLanguage formEnhanceStructuredQueryLanguage = serviceByFormEnhanceStructuredQueryLanguageService.getById(id);
        // 校验StructuredQueryLanguage 增强是否存在
        ArgumentResponseEnum.FORM_STRUCTURED_QUERY_LANGUAGE_DELETE_ERR_FORM_STRUCTURED_QUERY_LANGUAGE_NULL.assertNotNull(formEnhanceStructuredQueryLanguage, id);
        boolean remove = serviceByFormEnhanceStructuredQueryLanguageService.removeById(id);
        ArgumentResponseEnum.FORM_STRUCTURED_QUERY_LANGUAGE_DELETE_ERR.assertIsTrue(remove, id);
        return new BaseResponse();
    }

}