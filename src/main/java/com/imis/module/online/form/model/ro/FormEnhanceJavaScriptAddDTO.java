package com.imis.module.online.form.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 *
 * 在线开发-JavaScript 增强-新增 RO
 *
 * @author XinLau
 * @since 2020-10-19
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="FormEnhanceJavaScript新增对象", description="在线开发-JavaScript 增强-新增")
public class FormEnhanceJavaScriptAddDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private Long tableHeadId;
    /**
     * JS增强内容
     */
    @ApiModelProperty(value = "JS增强内容", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private String javaScriptContent;
    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String javaScriptType;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;

}
