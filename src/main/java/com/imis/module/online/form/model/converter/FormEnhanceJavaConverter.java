package com.imis.module.online.form.model.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.online.form.model.po.FormEnhanceJava;
import com.imis.module.online.form.model.ro.FormEnhanceJavaAddDTO;
import com.imis.module.online.form.model.ro.FormEnhanceJavaUpdateDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 在线开发-Java增强 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-27
 */
@Mapper
public interface FormEnhanceJavaConverter {

    /**
     * 在线开发-Java增强 转换器实例
     */
    FormEnhanceJavaConverter INSTANCE = Mappers.getMapper(FormEnhanceJavaConverter.class);

    /**
     * 在线开发-Java增强 PO转VO
     *
     * @param entity - 在线开发-Java增强 PO
     * @return FormEnhanceJavaVO - 在线开发-Java增强 VO
     */
    @Mappings({})
    FormEnhanceJavaVO getReturnValue(FormEnhanceJava entity);

    /**
     * 在线开发-Java增强 PO转VO
     *
     * @param entityList - 在线开发-Java增强 数组PO
     * @return List<FormEnhanceJavaVO> - 在线开发-Java增强 数组VO
     */
    @Mappings({})
    List<FormEnhanceJavaVO> getReturnValue(List<FormEnhanceJava> entityList);

    /**
     * 在线开发-Java增强 分页PO转分页VO
     *
     * @param entityPage - 在线开发-Java增强 分页PO
     * @return Page<FormEnhanceJavaVO> - 在线开发-Java增强 分页VO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<FormEnhanceJavaVO> getReturnValue(Page<FormEnhanceJava> entityPage);

    /**
     * 在线开发-Java 增强-新增 RO转PO
     *
     * @param add - 在线开发-Java 增强-新增 RO
     * @return FormEnhanceJava - 在线开发-Java 增强 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    FormEnhanceJava getAddEntity(FormEnhanceJavaAddDTO add);

    /**
     * 在线开发-Java增强 RO转PO
     *
     * @param entity - 在线开发-Java增强
     * @param update - 在线开发-Java增强 RO
     * @return FormEnhanceJava - 在线开发-Java增强 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget FormEnhanceJava entity, FormEnhanceJavaUpdateDTO update);

}
