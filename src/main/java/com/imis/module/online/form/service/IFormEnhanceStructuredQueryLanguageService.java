package com.imis.module.online.form.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.online.form.model.po.FormEnhanceStructuredQueryLanguage;
import com.imis.module.base.BaseService;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceStructuredQueryLanguageDTO;
import com.imis.module.online.form.model.vo.FormEnhanceStructuredQueryLanguageVO;

/**
 * <p>
 * 在线开发-SQL增强 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-28
 */
public interface IFormEnhanceStructuredQueryLanguageService extends BaseService<FormEnhanceStructuredQueryLanguage> {

    /**
     * 在线开发-SQL 增强-分页查询
     *
     * @param pagingQueryFormEnhanceStructuredQueryLanguage - FormEnhanceStructuredQueryLanguage 分页查询对象
     * @return Page<FormEnhanceStructuredQueryLanguageVO> - FormEnhanceStructuredQueryLanguage 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    Page<FormEnhanceStructuredQueryLanguageVO> pagingQueryListByParameter(final PagingQueryFormEnhanceStructuredQueryLanguageDTO pagingQueryFormEnhanceStructuredQueryLanguage);

}