package com.imis.module.online.form.model.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.online.form.model.po.FormEnhanceJavaScript;
import com.imis.module.online.form.model.ro.FormEnhanceJavaScriptAddDTO;
import com.imis.module.online.form.model.ro.FormEnhanceJavaScriptUpdateDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaScriptVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 在线开发-JavaScript 增强 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Mapper
public interface FormEnhanceJavaScriptConverter {

    /**
     * 在线开发-JavaScript 增强 转换器实例
     */
    FormEnhanceJavaScriptConverter INSTANCE = Mappers.getMapper(FormEnhanceJavaScriptConverter.class);

    /**
     * 在线开发-JavaScript 增强 PO转VO
     *
     * @param entity - 在线开发-JavaScript 增强 PO
     * @return FormEnhanceJavaScriptVO - 在线开发-JavaScript 增强 VO
     */
    @Mappings({})
    FormEnhanceJavaScriptVO getReturnValue(FormEnhanceJavaScript entity);

    /**
     * 在线开发-JavaScript 增强 PO转VO
     *
     * @param entityList - 在线开发-JavaScript 增强 数组PO
     * @return List<FormEnhanceJavaScriptVO> - 在线开发-JavaScript 增强 数组VO
     */
    @Mappings({})
    List<FormEnhanceJavaScriptVO> getReturnValue(List<FormEnhanceJavaScript> entityList);

    /**
     * 在线开发-JavaScript 增强 分页PO转分页VO
     *
     * @param entityPage - 在线开发-JavaScript 增强 分页PO
     * @return Page<FormEnhanceJavaScriptVO> - 在线开发-JavaScript 增强 分页VO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<FormEnhanceJavaScriptVO> getReturnValue(Page<FormEnhanceJavaScript> entityPage);

    /**
     * 在线开发-JavaScript 增强-新增 RO转PO
     *
     * @param add - 在线开发-JavaScript 增强-新增 RO
     * @return FormEnhanceJavaScript - 在线开发-JavaScript 增强 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    FormEnhanceJavaScript getAddEntity(FormEnhanceJavaScriptAddDTO add);

    /**
     * 在线开发-JavaScript 增强 RO转PO
     *
     * @param entity - 在线开发-JavaScript 增强
     * @param update - 在线开发-JavaScript 增强 RO
     * @return FormEnhanceJavaScript - 在线开发-JavaScript 增强 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget FormEnhanceJavaScript entity, FormEnhanceJavaScriptUpdateDTO update);

}
