package com.imis.module.online.form.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.online.form.bus.FormEnhanceJavaScriptBus;
import com.imis.module.online.form.model.ro.FormEnhanceJavaScriptAddDTO;
import com.imis.module.online.form.model.ro.FormEnhanceJavaScriptUpdateDTO;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaScriptDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaScriptVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 在线开发-JavaScript 增强 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Slf4j
@RestController
@RequestMapping(path = "/onl/form/js")
@Api(tags = {"在线开发-JavaScript 增强"})
@ApiSort(2)
public class FormEnhanceJavaScriptController extends BaseController<FormEnhanceJavaScriptBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询JavaScript 增强信息接口", notes = "分页查询JavaScript 增强信息接口")
    @RequiresPermissions(PermissionsValueConstant.JAVA_SCRIPT_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<FormEnhanceJavaScriptVO>> pagingQueryListByParameter(PagingQueryFormEnhanceJavaScriptDTO pagingQueryFormEnhanceJavaScript){
        return service.pagingQueryListByParameter(pagingQueryFormEnhanceJavaScript);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "JavaScript 增强添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.JAVA_SCRIPT_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addJavaScript(@RequestBody @Valid FormEnhanceJavaScriptAddDTO formEnhanceJavaScriptAdd) {
        return service.add(formEnhanceJavaScriptAdd);
    }

    @GetMapping(path = "/queryById")
    @ApiOperation(value = "JavaScript 增强查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.JAVA_SCRIPT_QUERY_BY_ID)
    @ApiImplicitParam(name = "id", value = "JavaScript 增强标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public CommonResponse<FormEnhanceJavaScriptVO> queryById(@RequestParam(name = "id", required = true) Long id) {
        return service.queryById(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "JavaScript 增强信息更新接口", notes = "单条更新")
    @RequiresPermissions(PermissionsValueConstant.JAVA_SCRIPT_UPDATE)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse updateById(@RequestBody @Valid FormEnhanceJavaScriptUpdateDTO formEnhanceJavaScriptUpdate) {
        return service.updateById(formEnhanceJavaScriptUpdate);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "JavaScript 增强删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.JAVA_SCRIPT_PAGE, PermissionsValueConstant.JAVA_SCRIPT_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = "id", value = "JavaScript 增强标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public BaseResponse deleteById(@RequestParam(name = "id", required = true) Long id) {
        return service.deleteById(id);
    }

}