package com.imis.module.online.form.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.online.form.model.po.FormEnhanceJava;
import com.imis.module.base.BaseService;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaVO;

/**
 * <p>
 * 在线开发-Java增强 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-27
 */
public interface IFormEnhanceJavaService extends BaseService<FormEnhanceJava> {

    /**
     * 在线开发-Java 增强-分页查询
     *
     * @param pagingQueryFormEnhanceJava - EnhanceJava 分页查询对象
     * @return Page<FormEnhanceJavaScriptVO> - EnhanceJava 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    Page<FormEnhanceJavaVO> pagingQueryListByParameter(final PagingQueryFormEnhanceJavaDTO pagingQueryFormEnhanceJava);

}