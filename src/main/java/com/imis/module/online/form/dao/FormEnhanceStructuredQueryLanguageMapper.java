package com.imis.module.online.form.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.online.form.model.po.FormEnhanceStructuredQueryLanguage;
import com.imis.module.base.BaseMapper;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceStructuredQueryLanguageDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 在线开发-SQL增强 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-10-28
 */
public interface FormEnhanceStructuredQueryLanguageMapper extends BaseMapper<FormEnhanceStructuredQueryLanguage> {

    /**
     * 在线开发-SQL 增强-分页查询
     *
     * @param formEnhanceStructuredQueryLanguagePage        - FormEnhanceStructuredQueryLanguage 分页对象
     * @param pagingQueryFormEnhanceStructuredQueryLanguage - FormEnhanceStructuredQueryLanguage 分页查询对象
     * @return Page<FormEnhanceStructuredQueryLanguage> - FormEnhanceStructuredQueryLanguage 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    Page<FormEnhanceStructuredQueryLanguage> pagingQueryListByParameter(@Param("pg") final Page<FormEnhanceStructuredQueryLanguage> formEnhanceStructuredQueryLanguagePage, @Param("sql") final PagingQueryFormEnhanceStructuredQueryLanguageDTO pagingQueryFormEnhanceStructuredQueryLanguage);

}