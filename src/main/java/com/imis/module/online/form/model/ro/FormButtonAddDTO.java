package com.imis.module.online.form.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * 在线开发-表单按钮新增 RO
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FormButton新增对象", description = "在线开发-表单按钮新增")
public class FormButtonAddDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID")
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private Long tableHeadId;
    /**
     * 按钮编码
     */
    @ApiModelProperty(value = "按钮编码", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private String buttonCode;
    /**
     * 按钮图标
     */
    @ApiModelProperty(value = "按钮图标")
    private String buttonIcon;
    /**
     * 按钮名称
     */
    @ApiModelProperty(value = "按钮名称", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private String buttonName;
    /**
     * 按钮状态(0无效，1有效)
     */
    @ApiModelProperty(value = "按钮状态(0无效，1有效)")
    private Integer buttonStatus;
    /**
     * 按钮类型
     */
    @ApiModelProperty(value = "按钮类型", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private String buttonType;
    /**
     * 按钮样式
     */
    @ApiModelProperty(value = "按钮样式")
    private String buttonStyle;
    /**
     * 按钮位置（1侧面 2底部）
     */
    @ApiModelProperty(value = "按钮位置（1侧面 2底部）")
    private Integer buttonPosition;
    /**
     * 表达式
     */
    @ApiModelProperty(value = "表达式")
    private String expression;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Double sortOrder;

}
