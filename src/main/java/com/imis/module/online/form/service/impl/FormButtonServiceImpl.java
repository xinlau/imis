package com.imis.module.online.form.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.online.form.dao.FormButtonMapper;
import com.imis.module.online.form.model.converter.FormButtonConverter;
import com.imis.module.online.form.model.po.FormButton;
import com.imis.module.online.form.model.ro.PagingQueryFormButtonDTO;
import com.imis.module.online.form.model.vo.FormButtonVO;
import com.imis.module.online.form.service.IFormButtonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-表单按钮 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Slf4j
@Service
@DS(DataBaseConstant.DATA_SOURCE_MASTER)
public class FormButtonServiceImpl extends BaseServiceImpl<FormButtonMapper, FormButton> implements IFormButtonService {

    /**
     * 在线开发-表单按钮-分页查询
     *
     * @param pagingQueryFormButton - FormButton分页查询对象
     * @return Page<FormButtonVO> - FormButton对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    @Override
    public Page<FormButtonVO> pagingQueryListByParameter(final PagingQueryFormButtonDTO pagingQueryFormButton) {
        // 1.页码、页长
        Page<FormButton> formButtonPage = new Page<>(pagingQueryFormButton.getPageNumber(), pagingQueryFormButton.getPageSize());
        // 2.排序字段
        formButtonPage.addOrder(OrderItem.asc("sort_order"), OrderItem.desc(DataBaseConstant.CREATE_TIME_FIELD), OrderItem.desc(DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        formButtonPage = mapper.pagingQueryListByParameter(formButtonPage, pagingQueryFormButton);
        // 4.PO分页对象转VO实体分页对象
        return FormButtonConverter.INSTANCE.getReturnValue(formButtonPage);
    }

}