package com.imis.module.online.form.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 在线开发-树形下拉框 VO
 *
 * @author XinLau
 * @since 2020-10-28
 */
@Data
public class TreeSelectColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    private String fieldName;

    private String tableName;

    private String codeField;

    private String textField;

    private String pidField;

    private String pidValue;

    private String hsaChildField;

}
