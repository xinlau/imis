package com.imis.module.online.form.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.online.form.model.po.FormButton;
import com.imis.module.base.BaseMapper;
import com.imis.module.online.form.model.ro.PagingQueryFormButtonDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 在线开发-表单按钮 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
public interface FormButtonMapper extends BaseMapper<FormButton> {

    /**
     * 在线开发-表单按钮-分页查询
     *
     * @param formButtonPage        - FormButton分页对象
     * @param pagingQueryFormButton - FormButton分页查询对象
     * @return Page<FormButtonVO> - FormButton对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    Page<FormButton> pagingQueryListByParameter(@Param("pg") final Page<FormButton> formButtonPage, @Param("button") PagingQueryFormButtonDTO pagingQueryFormButton);

}