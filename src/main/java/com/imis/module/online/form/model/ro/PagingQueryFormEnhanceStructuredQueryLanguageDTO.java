package com.imis.module.online.form.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 在线开发-SQL增强-分页查询 RO
 *
 * @author XinLau
 * @since 2020-10-28
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="FormEnhanceStructuredQueryLanguage分页查询对象", description="在线开发-SQL增强-分页查询")
public class PagingQueryFormEnhanceStructuredQueryLanguageDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID")
    private Long tableHeadId;
    /**
     * 按钮编码
     */
    @ApiModelProperty(value = "按钮编码")
    private String buttonCode;
    /**
     * SQL内容
     */
    @ApiModelProperty(value = "SQL内容")
    private String structuredQueryLanguageContent;
    /**
     * SQL名称
     */
    @ApiModelProperty(value = "SQL名称")
    private String structuredQueryLanguageName;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;

}
