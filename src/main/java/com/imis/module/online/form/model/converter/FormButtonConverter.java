package com.imis.module.online.form.model.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import com.imis.module.online.form.model.ro.FormButtonAddDTO;
import com.imis.module.online.form.model.ro.FormButtonUpdateDTO;
import com.imis.module.online.form.model.vo.FormButtonVO;
import com.imis.module.online.form.model.po.FormButton;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * 在线开发-表单按钮 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Mapper
public interface FormButtonConverter {

    /**
     * 在线开发-表单按钮 转换器实例
     */
    FormButtonConverter INSTANCE = Mappers.getMapper(FormButtonConverter.class);

    /**
     * 在线开发-表单按钮 PO转VO
     *
     * @param entity - 在线开发-表单按钮 PO
     * @return FormButtonVO - 在线开发-表单按钮 VO
     */
    @Mappings({})
    FormButtonVO getReturnValue(FormButton entity);

    /**
     * 在线开发-表单按钮 PO转VO
     *
     * @param entityList - 在线开发-表单按钮 数组PO
     * @return List<FormButtonVO> - 在线开发-表单按钮 数组VO
     */
    @Mappings({})
    List<FormButtonVO> getReturnValue(List<FormButton> entityList);

    /**
     * 在线开发-表单按钮 分页PO转分页VO
     *
     * @param entityPage - 在线开发-表单按钮 分页PO
     * @return Page<FormButtonVO> - 在线开发-表单按钮 分页VO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
    })
    Page<FormButtonVO> getReturnValue(Page<FormButton> entityPage);

    /**
     * 在线开发-表单按钮-新增 RO转PO
     *
     * @param add - 在线开发-表单按钮-新增 RO
     * @return FormButton - 在线开发-表单按钮 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    FormButton getAddEntity(FormButtonAddDTO add);

    /**
     * 在线开发-表单按钮-更新 RO转PO
     *
     * @param update     - 在线开发-表单按钮-更新 RO
     * @param formButton - 在线开发-表单按钮-更新 PO
     * @return FormButton - 在线开发-表单按钮 PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ID, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    void getUpdateEntity(@MappingTarget FormButton formButton, FormButtonUpdateDTO update);

}
