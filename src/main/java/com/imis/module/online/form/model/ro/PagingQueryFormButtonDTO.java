package com.imis.module.online.form.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 在线开发-表单按钮-分页查询 RO
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "FormButton分页查询对象", description = "在线开发-表单按钮-分页查询")
public class PagingQueryFormButtonDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID")
    private Long tableHeadId;
    /**
     * 按钮编码
     */
    @ApiModelProperty(value = "按钮编码")
    private String buttonCode;
    /**
     * 按钮图标
     */
    @ApiModelProperty(value = "按钮图标")
    private String buttonIcon;
    /**
     * 按钮名称
     */
    @ApiModelProperty(value = "按钮名称")
    private String buttonName;
    /**
     * 按钮状态
     */
    @ApiModelProperty(value = "按钮状态")
    private Integer buttonStatus;
    /**
     * 按钮类型
     */
    @ApiModelProperty(value = "按钮类型")
    private String buttonType;
    /**
     * 按钮样式
     */
    @ApiModelProperty(value = "按钮样式")
    private String buttonStyle;
    /**
     * 按钮位置（1侧面 2底部）
     */
    @ApiModelProperty(value = "按钮位置（1侧面 2底部）")
    private Integer buttonPosition;
    /**
     * 表达式
     */
    @ApiModelProperty(value = "表达式")
    private String expression;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Double sortOrder;

}
