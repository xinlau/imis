package com.imis.module.online.form.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.online.form.bus.FormEnhanceJavaBus;
import com.imis.module.online.form.model.ro.FormEnhanceJavaAddDTO;
import com.imis.module.online.form.model.ro.FormEnhanceJavaUpdateDTO;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 在线开发-Java增强 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-27
 */
@Slf4j
@RestController
@RequestMapping(path = "/onl/form/java")
@Api(tags = {"在线开发-Java增强"})
@ApiSort(3)
public class FormEnhanceJavaController extends BaseController<FormEnhanceJavaBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询Java 增强信息接口", notes = "分页查询Java 增强信息接口")
    @RequiresPermissions(PermissionsValueConstant.JAVA_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<FormEnhanceJavaVO>> pagingQueryListByParameter(PagingQueryFormEnhanceJavaDTO pagingQueryFormEnhanceJava) {
        return service.pagingQueryListByParameter(pagingQueryFormEnhanceJava);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "Java 增强添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.JAVA_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addJava(@RequestBody @Valid FormEnhanceJavaAddDTO formEnhanceJavaAdd) {
        return service.add(formEnhanceJavaAdd);
    }

    @GetMapping(path = "/queryById")
    @ApiOperation(value = "Java 增强查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.JAVA_QUERY_BY_ID)
    @ApiImplicitParam(name = "id", value = "Java 增强标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public CommonResponse<FormEnhanceJavaVO> queryById(@RequestParam(name = "id", required = true) Long id) {
        return service.queryById(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Java 增强信息更新接口", notes = "单条更新")
    @RequiresPermissions(PermissionsValueConstant.JAVA_UPDATE)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse updateById(@RequestBody @Valid FormEnhanceJavaUpdateDTO formEnhanceJavaUpdate) {
        return service.updateById(formEnhanceJavaUpdate);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "Java 增强删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.JAVA_PAGE, PermissionsValueConstant.JAVA_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = "id", value = "Java 增强标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public BaseResponse deleteById(@RequestParam(name = "id", required = true) Long id) {
        return service.deleteById(id);
    }

}