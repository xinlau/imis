package com.imis.module.online.form.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 *
 * 在线开发-SQL增强 RO
 *
 * @author XinLau
 * @since 2020-10-28
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="FormEnhanceStructuredQueryLanguage对象", description="在线开发-SQL增强")
public class FormEnhanceStructuredQueryLanguageUpdateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * FormEnhanceStructuredQueryLanguage增强 编号
     */
    @ApiModelProperty(value = "FormEnhanceStructuredQueryLanguage增强 编号", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;
    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private Long tableHeadId;
    /**
     * 按钮编码
     */
    @ApiModelProperty(value = "按钮编码", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private String buttonCode;
    /**
     * SQL内容
     */
    @ApiModelProperty(value = "SQL内容", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private String structuredQueryLanguageContent;
    /**
     * SQL名称
     */
    @ApiModelProperty(value = "SQL名称")
    private String structuredQueryLanguageName;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
}
