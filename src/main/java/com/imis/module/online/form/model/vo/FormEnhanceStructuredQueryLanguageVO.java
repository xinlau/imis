package com.imis.module.online.form.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * 在线开发-SQL增强 VO
 *
 * @author XinLau
 * @since 2020-10-28
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="FormEnhanceStructuredQueryLanguage对象", description="在线开发-SQL增强")
public class FormEnhanceStructuredQueryLanguageVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * SQL增强 编号
     */
    @ApiModelProperty(value = "SQL增强 编号")
    private String id;
    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID")
    private String tableHeadId;
    /**
     * 按钮编码
     */
    @ApiModelProperty(value = "按钮编码")
    private String buttonCode;
    /**
     * SQL内容
     */
    @ApiModelProperty(value = "SQL内容")
    private String structuredQueryLanguageContent;
    /**
     * SQL名称
     */
    @ApiModelProperty(value = "SQL名称")
    private String structuredQueryLanguageName;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
}
