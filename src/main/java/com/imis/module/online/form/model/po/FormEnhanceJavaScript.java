package com.imis.module.online.form.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.imis.module.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 在线开发-JavaScript 增强 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("onl_form_enhance_java_script")
public class FormEnhanceJavaScript extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @TableField("table_head_id")
    private Long tableHeadId;

    /**
     * JS增强内容
     */
    @TableField("java_script_content")
    private String javaScriptContent;

    /**
     * 类型
     */
    @TableField("java_script_type")
    private String javaScriptType;

    /**
     * 备注
     */
    @TableField("description")
    private String description;

}
