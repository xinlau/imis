package com.imis.module.online.form.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.imis.module.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 在线开发-SQL增强 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-10-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("onl_form_enhance_structured_query_language")
public class FormEnhanceStructuredQueryLanguage extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @TableField("table_head_id")
    private Long tableHeadId;

    /**
     * 按钮编码
     */
    @TableField("button_code")
    private String buttonCode;

    /**
     * SQL内容
     */
    @TableField("structured_query_language_content")
    private String structuredQueryLanguageContent;

    /**
     * SQL名称
     */
    @TableField("structured_query_language_name")
    private String structuredQueryLanguageName;

    /**
     * 备注
     */
    @TableField("description")
    private String description;

}
