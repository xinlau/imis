package com.imis.module.online.form.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.online.form.dao.FormEnhanceStructuredQueryLanguageMapper;
import com.imis.module.online.form.model.converter.FormEnhanceStructuredQueryLanguageConverter;
import com.imis.module.online.form.model.po.FormEnhanceStructuredQueryLanguage;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceStructuredQueryLanguageDTO;
import com.imis.module.online.form.model.vo.FormEnhanceStructuredQueryLanguageVO;
import com.imis.module.online.form.service.IFormEnhanceStructuredQueryLanguageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-SQL增强 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-28
 */
@Slf4j
@Service
public class FormEnhanceStructuredQueryLanguageServiceImpl extends BaseServiceImpl<FormEnhanceStructuredQueryLanguageMapper, FormEnhanceStructuredQueryLanguage> implements IFormEnhanceStructuredQueryLanguageService {

    /**
     * 在线开发-SQL 增强-分页查询
     *
     * @param pagingQueryFormEnhanceStructuredQueryLanguage - FormEnhanceStructuredQueryLanguage 分页查询对象
     * @return Page<FormEnhanceStructuredQueryLanguageVO> - FormEnhanceStructuredQueryLanguage 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    @Override
    public Page<FormEnhanceStructuredQueryLanguageVO> pagingQueryListByParameter(final PagingQueryFormEnhanceStructuredQueryLanguageDTO pagingQueryFormEnhanceStructuredQueryLanguage) {
        // 1.页码、页长
        Page<FormEnhanceStructuredQueryLanguage> formEnhanceStructuredQueryLanguagePage = new Page<>(pagingQueryFormEnhanceStructuredQueryLanguage.getPageNumber(), pagingQueryFormEnhanceStructuredQueryLanguage.getPageSize());
        // 2.排序字段
        formEnhanceStructuredQueryLanguagePage.addOrder(OrderItem.desc(DataBaseConstant.CREATE_TIME_FIELD), OrderItem.desc(DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        formEnhanceStructuredQueryLanguagePage = mapper.pagingQueryListByParameter(formEnhanceStructuredQueryLanguagePage, pagingQueryFormEnhanceStructuredQueryLanguage);
        // 4.PO分页对象转VO实体分页对象
        return FormEnhanceStructuredQueryLanguageConverter.INSTANCE.getReturnValue(formEnhanceStructuredQueryLanguagePage);
    }

}