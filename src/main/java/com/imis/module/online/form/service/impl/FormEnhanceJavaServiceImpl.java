package com.imis.module.online.form.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.online.form.dao.FormEnhanceJavaMapper;
import com.imis.module.online.form.model.converter.FormEnhanceJavaConverter;
import com.imis.module.online.form.model.po.FormEnhanceJava;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaVO;
import com.imis.module.online.form.service.IFormEnhanceJavaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-Java增强 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-27
 */
@Slf4j
@Service
public class FormEnhanceJavaServiceImpl extends BaseServiceImpl<FormEnhanceJavaMapper, FormEnhanceJava> implements IFormEnhanceJavaService {

    /**
     * 在线开发-Java 增强-分页查询
     *
     * @param pagingQueryFormEnhanceJava - EnhanceJava 分页查询对象
     * @return Page<FormEnhanceJavaVO> - EnhanceJava 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    @Override
    public Page<FormEnhanceJavaVO> pagingQueryListByParameter(PagingQueryFormEnhanceJavaDTO pagingQueryFormEnhanceJava) {
        // 1.页码、页长
        Page<FormEnhanceJava> formEnhanceJavaPage = new Page<>(pagingQueryFormEnhanceJava.getPageNumber(), pagingQueryFormEnhanceJava.getPageSize());
        // 2.排序字段
        formEnhanceJavaPage.addOrder(OrderItem.desc(DataBaseConstant.CREATE_TIME_FIELD), OrderItem.desc(DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        formEnhanceJavaPage = mapper.pagingQueryListByParameter(formEnhanceJavaPage, pagingQueryFormEnhanceJava);
        // 4.PO分页对象转VO实体分页对象
        return FormEnhanceJavaConverter.INSTANCE.getReturnValue(formEnhanceJavaPage);
    }

}