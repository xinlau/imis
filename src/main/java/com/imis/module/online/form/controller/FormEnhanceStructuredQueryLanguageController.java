package com.imis.module.online.form.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.online.form.model.ro.FormEnhanceStructuredQueryLanguageAddDTO;
import com.imis.module.online.form.model.ro.FormEnhanceStructuredQueryLanguageUpdateDTO;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceStructuredQueryLanguageDTO;
import com.imis.module.online.form.model.vo.FormEnhanceStructuredQueryLanguageVO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.imis.module.base.BaseController;
import com.imis.module.online.form.bus.FormEnhanceStructuredQueryLanguageBus;

import javax.validation.Valid;

/**
 * <p>
 * 在线开发-SQL增强 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-28
 */
@Slf4j
@RestController
@RequestMapping(path = "/onl/form/sql")
@Api(tags = {"在线开发-SQL增强"})
@ApiSort(4)
public class FormEnhanceStructuredQueryLanguageController extends BaseController<FormEnhanceStructuredQueryLanguageBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询StructuredQueryLanguage 增强信息接口", notes = "分页查询StructuredQueryLanguage 增强信息接口")
    @RequiresPermissions(PermissionsValueConstant.STRUCTURED_QUERY_LANGUAGE_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<FormEnhanceStructuredQueryLanguageVO>> pagingQueryListByParameter(PagingQueryFormEnhanceStructuredQueryLanguageDTO pagingQueryFormEnhanceStructuredQueryLanguage){
        return service.pagingQueryListByParameter(pagingQueryFormEnhanceStructuredQueryLanguage);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "StructuredQueryLanguage 增强添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.STRUCTURED_QUERY_LANGUAGE_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addStructuredQueryLanguage(@RequestBody @Valid FormEnhanceStructuredQueryLanguageAddDTO formEnhanceStructuredQueryLanguageAdd) {
        return service.add(formEnhanceStructuredQueryLanguageAdd);
    }

    @GetMapping(path = "/queryById")
    @ApiOperation(value = "StructuredQueryLanguage 增强查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.STRUCTURED_QUERY_LANGUAGE_QUERY_BY_ID)
    @ApiImplicitParam(name = "id", value = "StructuredQueryLanguage 增强标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public CommonResponse<FormEnhanceStructuredQueryLanguageVO> queryById(@RequestParam(name = "id", required = true) Long id) {
        return service.queryById(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "StructuredQueryLanguage 增强信息更新接口", notes = "单条更新")
    @RequiresPermissions(PermissionsValueConstant.STRUCTURED_QUERY_LANGUAGE_UPDATE)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse updateById(@RequestBody @Valid FormEnhanceStructuredQueryLanguageUpdateDTO formEnhanceStructuredQueryLanguageUpdate) {
        return service.updateById(formEnhanceStructuredQueryLanguageUpdate);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "StructuredQueryLanguage 增强删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.STRUCTURED_QUERY_LANGUAGE_PAGE, PermissionsValueConstant.STRUCTURED_QUERY_LANGUAGE_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = "id", value = "StructuredQueryLanguage 增强标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public BaseResponse deleteById(@RequestParam(name = "id", required = true) Long id) {
        return service.deleteById(id);
    }

}