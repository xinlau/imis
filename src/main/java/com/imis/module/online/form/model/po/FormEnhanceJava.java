package com.imis.module.online.form.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 在线开发-Java增强 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-10-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("onl_form_enhance_java")
public class FormEnhanceJava extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @TableField("table_head_id")
    private Long tableHeadId;

    /**
     * 按钮编码
     */
    @TableField("button_code")
    private String buttonCode;

    /**
     * 类型
     */
    @TableField("java_type")
    private String javaType;

    /**
     * 数值
     */
    @TableField("java_value")
    private String javaValue;

    /**
     * 生效状态
     */
    @TableField("active_status")
    private Integer activeStatus;

    /**
     * 事件状态(end:结束，start:开始)
     */
    @TableField("event")
    private String event;

    /**
     * 备注
     */
    @TableField("description")
    private String description;

}
