package com.imis.module.online.form.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.constant.permissions.PermissionsValueConstant;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.online.form.bus.FormButtonBus;
import com.imis.module.online.form.model.ro.FormButtonAddDTO;
import com.imis.module.online.form.model.ro.FormButtonUpdateDTO;
import com.imis.module.online.form.model.ro.PagingQueryFormButtonDTO;
import com.imis.module.online.form.model.vo.FormButtonVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 在线开发-表单按钮 前端控制器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Slf4j
@RestController
@RequestMapping(path = "/onl/form/button")
@Api(tags = {"在线开发-表单按钮"})
@ApiSort(1)
public class FormButtonController extends BaseController<FormButtonBus> {

    @GetMapping(path = "/page")
    @ApiOperation(value = "分页查询表单按钮信息接口", notes = "分页查询表单按钮信息接口")
    @RequiresPermissions(PermissionsValueConstant.FORM_BUTTON_PAGE)
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Page<FormButtonVO>> pagingQueryListByParameter(PagingQueryFormButtonDTO pagingQueryFormButton){
        return service.pagingQueryListByParameter(pagingQueryFormButton);
    }

    @PostMapping(path = "/add")
    @ApiOperation(value = "表单按钮添加接口", notes = "单条添加")
    @RequiresPermissions(PermissionsValueConstant.FORM_BUTTON_ADD)
    @ApiOperationSupport(order = 2, author = "XinLau")
    public BaseResponse addButton(@RequestBody @Valid FormButtonAddDTO formButtonAdd) {
        return service.add(formButtonAdd);
    }

    @GetMapping(path = "/queryById")
    @ApiOperation(value = "表单按钮查看接口", notes = "单条查看")
    @RequiresPermissions(PermissionsValueConstant.FORM_BUTTON_QUERY_BY_ID)
    @ApiImplicitParam(name = "id", value = "表单按钮标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 3, author = "XinLau")
    public CommonResponse<FormButtonVO> queryById(@RequestParam(name = "id", required = true) Long id) {
        return service.queryById(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "表单按钮信息更新接口", notes = "单条更新")
    @RequiresPermissions(PermissionsValueConstant.FORM_BUTTON_UPDATE)
    @ApiOperationSupport(order = 4, author = "XinLau")
    public BaseResponse updateById(@RequestBody @Valid FormButtonUpdateDTO formButtonUpdate) {
        return service.updateById(formButtonUpdate);
    }

    @GetMapping(path = "/delete")
    @ApiOperation(value = "表单按钮删除接口", notes = "单条删除")
    @RequiresPermissions(value = {PermissionsValueConstant.FORM_BUTTON_PAGE, PermissionsValueConstant.FORM_BUTTON_DELETE}, logical = Logical.AND)
    @ApiImplicitParam(name = "id", value = "表单按钮标识", dataType = "Long", dataTypeClass = Long.class, required = true)
    @ApiOperationSupport(order = 5, author = "XinLau")
    public BaseResponse deleteById(@RequestParam(name = "id", required = true) Long id) {
        return service.deleteById(id);
    }

}