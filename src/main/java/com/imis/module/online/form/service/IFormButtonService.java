package com.imis.module.online.form.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.online.form.model.po.FormButton;
import com.imis.module.base.BaseService;
import com.imis.module.online.form.model.ro.PagingQueryFormButtonDTO;
import com.imis.module.online.form.model.vo.FormButtonVO;

/**
 * <p>
 * 在线开发-表单按钮 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
public interface IFormButtonService extends BaseService<FormButton> {

    /**
     * 在线开发-表单按钮-分页查询
     *
     * @param pagingQueryFormButton - FormButton分页查询对象
     * @return Page<FormButtonVO> - FormButton对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    Page<FormButtonVO> pagingQueryListByParameter(final PagingQueryFormButtonDTO pagingQueryFormButton);

}