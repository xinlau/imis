package com.imis.module.online.form.bus;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.enums.ArgumentResponseEnum;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.base.util.ConvertUtils;
import com.imis.module.base.BaseBus;
import com.imis.module.online.form.model.converter.FormButtonConverter;
import com.imis.module.online.form.model.po.FormButton;
import com.imis.module.online.form.model.ro.FormButtonAddDTO;
import com.imis.module.online.form.model.ro.FormButtonUpdateDTO;
import com.imis.module.online.form.model.ro.PagingQueryFormButtonDTO;
import com.imis.module.online.form.model.vo.FormButtonVO;
import com.imis.module.online.form.service.IFormButtonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-表单按钮 业务处理类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Slf4j
@Service
public class FormButtonBus extends BaseBus {

    /**
     * 在线开发-表单按钮 服务类
     */
    private IFormButtonService serviceByFormButtonService;

    @Autowired
    public void setFormButtonService(IFormButtonService serviceByFormButtonService) {
        this.serviceByFormButtonService = serviceByFormButtonService;
    }

    /**
     * 在线开发-表单按钮-添加校验
     *
     * @param formButtonAdd - FormButton 添加对象
     * @return FormButton - 在线开发-表单按钮
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 15:47
     */
    private FormButton formButtonAddVerification(final FormButtonAddDTO formButtonAdd) {
        FormButton formButton = serviceByFormButtonService.getOne(
                Wrappers.<FormButton>lambdaQuery()
                        .eq(FormButton::getTableHeadId, formButtonAdd.getTableHeadId())
                        .eq(FormButton::getButtonName, formButtonAdd.getButtonName())
                        .or().eq(FormButton::getButtonCode, formButtonAdd.getButtonCode())
                , Boolean.FALSE);
        if (ConvertUtils.isNotEmpty(formButton)) {
            ArgumentResponseEnum.FORM_BUTTON_ADD_ERR_DEACTIVATE.assertIsTrue(CommonConstant.STATUS_ERR.equals(formButton.getButtonStatus()), formButtonAdd.getButtonName());
            // 校验在线开发-表单按钮重复
            ArgumentResponseEnum.FORM_BUTTON_ADD_ERR_REPEAT.assertIsNull(formButton, formButtonAdd.getButtonName(), formButtonAdd.getButtonCode());
        }
        formButton = FormButtonConverter.INSTANCE.getAddEntity(formButtonAdd);
        return formButton;
    }

    /**
     * 在线开发-表单按钮-更新校验
     *
     * @param formButtonUpdate - FormButton 更新对象
     * @return FormButton - 在线开发-表单按钮
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    private FormButton formButtonUpdateVerification(final FormButtonUpdateDTO formButtonUpdate) {
        FormButton formButton = serviceByFormButtonService.getById(formButtonUpdate.getId());
        // 校验在线开发-表单按钮是否存在
        ArgumentResponseEnum.FORM_BUTTON_UPDATE_ERR_NON.assertNotNull(formButton, formButtonUpdate.getId());
        if (!formButtonUpdate.getButtonName().equalsIgnoreCase(formButton.getButtonName()) || !formButtonUpdate.getButtonCode().equalsIgnoreCase(formButton.getButtonCode())) {
            formButton = serviceByFormButtonService.getOne(Wrappers.<FormButton>lambdaQuery()
                            .eq(FormButton::getButtonName, formButtonUpdate.getButtonName())
                            .or().eq(FormButton::getButtonCode, formButtonUpdate.getButtonCode())
                    , Boolean.FALSE);
            // 校验在线开发-表单按钮名称重复
            ArgumentResponseEnum.FORM_BUTTON_UPDATE_ERR_REPEAT.assertIsNull(formButton, formButtonUpdate.getButtonName(), formButtonUpdate.getButtonCode());
        }
        FormButtonConverter.INSTANCE.getUpdateEntity(formButton, formButtonUpdate);
        return formButton;
    }

    /**
     * 在线开发-表单按钮-分页查询
     *
     * @param pagingQueryFormButton - FormButton分页查询对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public CommonResponse<Page<FormButtonVO>> pagingQueryListByParameter(final PagingQueryFormButtonDTO pagingQueryFormButton) {
        // 查询数据库内的表单按钮
        Page<FormButtonVO> formButtonPage = serviceByFormButtonService.pagingQueryListByParameter(pagingQueryFormButton);
        return new CommonResponse(formButtonPage);
    }

    /**
     * 在线开发-表单按钮-添加
     *
     * @param formButtonAdd - FormButton添加对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse add(final FormButtonAddDTO formButtonAdd) {
        // 创建新表单按钮 校验表单按钮重复
        FormButton formButton = formButtonAddVerification(formButtonAdd);
        boolean save = serviceByFormButtonService.save(formButton);
        ArgumentResponseEnum.FORM_BUTTON_ADD_ERR.assertIsTrue(save, formButtonAdd.getButtonName());
        return new BaseResponse();
    }

    /**
     * 在线开发-表单按钮-查询
     *
     * @param id - FormButton标识
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public CommonResponse<FormButtonVO> queryById(final Long id) {
        FormButton formButton = serviceByFormButtonService.getById(id);
        // 校验表单按钮是否存在
        ArgumentResponseEnum.FORM_BUTTON_QUERY_ERR_FORM_BUTTON_NON.assertNotNull(formButton, id);
        return new CommonResponse(FormButtonConverter.INSTANCE.getReturnValue(formButton));
    }

    /**
     * 在线开发-表单按钮-更新
     *
     * @param formButtonUpdate - FormButton更新对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse updateById(final FormButtonUpdateDTO formButtonUpdate) {
        // 更新表单按钮 校验表单按钮更新对象
        FormButton formButton = formButtonUpdateVerification(formButtonUpdate);
        boolean save = serviceByFormButtonService.updateById(formButton);
        ArgumentResponseEnum.FORM_BUTTON_UPDATE_ERR.assertIsTrue(save, formButtonUpdate.getId());
        return new BaseResponse();
    }

    /**
     * 在线开发-表单按钮-删除
     *
     * @param id - FormButton标识
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse deleteById(final Long id) {
        FormButton formButton = serviceByFormButtonService.getById(id);
        // 校验表单按钮是否存在
        ArgumentResponseEnum.FORM_BUTTON_DELETE_ERR_FORM_BUTTON_NULL.assertNotNull(formButton, id);
        ArgumentResponseEnum.FORM_BUTTON_DELETE_ERR_USING.assertIsTrue(CommonConstant.STATUS_OK.equals(formButton.getButtonStatus()), formButton.getButtonName());
        boolean remove = serviceByFormButtonService.removeById(id);
        ArgumentResponseEnum.DATASOURCE_DELETE_ERR.assertIsTrue(remove, id);
        return new BaseResponse();
    }

}