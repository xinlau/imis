package com.imis.module.online.form.bus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.enums.ArgumentResponseEnum;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseBus;
import com.imis.module.online.form.model.converter.FormEnhanceJavaScriptConverter;
import com.imis.module.online.form.model.po.FormEnhanceJavaScript;
import com.imis.module.online.form.model.ro.FormEnhanceJavaScriptAddDTO;
import com.imis.module.online.form.model.ro.FormEnhanceJavaScriptUpdateDTO;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaScriptDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaScriptVO;
import com.imis.module.online.form.service.IFormEnhanceJavaScriptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-JavaScript 增强 业务处理类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Slf4j
@Service
public class FormEnhanceJavaScriptBus extends BaseBus {

    /**
     * 在线开发-JavaScript 增强 服务类
     */
    private IFormEnhanceJavaScriptService serviceByFormEnhanceJavaScriptService;

    @Autowired
    public void setFormEnhanceJavaScriptService(IFormEnhanceJavaScriptService serviceByFormEnhanceJavaScriptService) {
        this.serviceByFormEnhanceJavaScriptService = serviceByFormEnhanceJavaScriptService;
    }

    /**
     * 在线开发-JavaScript 增强-添加校验
     *
     * @param formEnhanceJavaScriptAdd - FormEnhanceJavaScript 添加对象
     * @return FormEnhanceJavaScript - 在线开发-JavaScript 增强
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 15:47
     */
    private FormEnhanceJavaScript formEnhanceJavaScriptAddVerification(final FormEnhanceJavaScriptAddDTO formEnhanceJavaScriptAdd) {
        FormEnhanceJavaScript formEnhanceJavaScript = FormEnhanceJavaScriptConverter.INSTANCE.getAddEntity(formEnhanceJavaScriptAdd);
        return formEnhanceJavaScript;
    }

    /**
     * 在线开发-JavaScript 增强-更新校验
     *
     * @param formEnhanceJavaScriptUpdate - FormEnhanceJavaScript 更新对象
     * @return FormEnhanceJavaScript - 在线开发-JavaScript 增强
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    private FormEnhanceJavaScript formEnhanceJavaScriptUpdateVerification(final FormEnhanceJavaScriptUpdateDTO formEnhanceJavaScriptUpdate) {
        FormEnhanceJavaScript formEnhanceJavaScript = serviceByFormEnhanceJavaScriptService.getById(formEnhanceJavaScriptUpdate.getId());
        // 校验在线开发-JavaScript 增强是否存在
        ArgumentResponseEnum.FORM_JAVA_SCRIPT_UPDATE_ERR_NON.assertIsNull(formEnhanceJavaScript, formEnhanceJavaScriptUpdate.getId());
        FormEnhanceJavaScriptConverter.INSTANCE.getUpdateEntity(formEnhanceJavaScript, formEnhanceJavaScriptUpdate);
        return formEnhanceJavaScript;
    }

    /**
     * 在线开发-JavaScript 增强-分页查询
     *
     * @param pagingQueryFormEnhanceJavaScript - FormEnhanceJavaScript分页查询对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public CommonResponse<Page<FormEnhanceJavaScriptVO>> pagingQueryListByParameter(final PagingQueryFormEnhanceJavaScriptDTO pagingQueryFormEnhanceJavaScript) {
        // 查询数据库内的JavaScript 增强
        Page<FormEnhanceJavaScriptVO> formEnhanceJavaScriptPage = serviceByFormEnhanceJavaScriptService.pagingQueryListByParameter(pagingQueryFormEnhanceJavaScript);
        return new CommonResponse(formEnhanceJavaScriptPage);
    }

    /**
     * 在线开发-JavaScript 增强-添加
     *
     * @param formEnhanceJavaScriptAdd - FormEnhanceJavaScript添加对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse add(final FormEnhanceJavaScriptAddDTO formEnhanceJavaScriptAdd) {
        // 创建新JavaScript 增强 校验JavaScript 增强重复
        FormEnhanceJavaScript formEnhanceJavaScript = formEnhanceJavaScriptAddVerification(formEnhanceJavaScriptAdd);
        boolean save = serviceByFormEnhanceJavaScriptService.save(formEnhanceJavaScript);
        ArgumentResponseEnum.FORM_JAVA_SCRIPT_ADD_ERR.assertIsTrue(save);
        return new BaseResponse();
    }

    /**
     * 在线开发-JavaScript 增强-查询
     *
     * @param id - FormEnhanceJavaScript标识
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public CommonResponse<FormEnhanceJavaScriptVO> queryById(final Long id) {
        FormEnhanceJavaScript formEnhanceJavaScript = serviceByFormEnhanceJavaScriptService.getById(id);
        // 校验JavaScript 增强是否存在
        ArgumentResponseEnum.FORM_JAVA_SCRIPT_QUERY_ERR_FORM_JAVA_SCRIPT_NON.assertNotNull(formEnhanceJavaScript, id);
        return new CommonResponse(FormEnhanceJavaScriptConverter.INSTANCE.getReturnValue(formEnhanceJavaScript));
    }

    /**
     * 在线开发-JavaScript 增强-更新
     *
     * @param formEnhanceJavaScriptUpdate - FormEnhanceJavaScript更新对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse updateById(final FormEnhanceJavaScriptUpdateDTO formEnhanceJavaScriptUpdate) {
        // 更新JavaScript 增强 校验JavaScript 增强更新对象
        FormEnhanceJavaScript formEnhanceJavaScript = formEnhanceJavaScriptUpdateVerification(formEnhanceJavaScriptUpdate);
        boolean save = serviceByFormEnhanceJavaScriptService.updateById(formEnhanceJavaScript);
        ArgumentResponseEnum.FORM_JAVA_SCRIPT_UPDATE_ERR.assertIsTrue(save, formEnhanceJavaScriptUpdate.getId());
        return new BaseResponse();
    }

    /**
     * 在线开发-JavaScript 增强-删除
     *
     * @param id - FormEnhanceJavaScript标识
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse deleteById(final Long id) {
        FormEnhanceJavaScript formEnhanceJavaScript = serviceByFormEnhanceJavaScriptService.getById(id);
        // 校验JavaScript 增强是否存在
        ArgumentResponseEnum.FORM_JAVA_SCRIPT_DELETE_ERR_FORM_JAVA_SCRIPT_NULL.assertNotNull(formEnhanceJavaScript, id);
        boolean remove = serviceByFormEnhanceJavaScriptService.removeById(id);
        ArgumentResponseEnum.FORM_JAVA_SCRIPT_DELETE_ERR.assertIsTrue(remove, id);
        return new BaseResponse();
    }

}