package com.imis.module.online.form.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 在线开发-Java增强-分页查询 RO
 *
 * @author XinLau
 * @since 2020-10-27
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="FormEnhanceJava分页查询对象", description="在线开发-Java增强-分页查询")
public class PagingQueryFormEnhanceJavaDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID")
    private Long tableHeadId;
    /**
     * 按钮编码
     */
    @ApiModelProperty(value = "按钮编码")
    private String buttonCode;
    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String javaType;
    /**
     * 数值
     */
    @ApiModelProperty(value = "数值")
    private String javaValue;
    /**
     * 生效状态(0无效，1有效)
     */
    @ApiModelProperty(value = "生效状态(0无效，1有效)")
    private Integer activeStatus;
    /**
     * 事件状态(end:结束，start:开始)
     */
    @ApiModelProperty(value = "事件状态(end:结束，start:开始)")
    private String event;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
}
