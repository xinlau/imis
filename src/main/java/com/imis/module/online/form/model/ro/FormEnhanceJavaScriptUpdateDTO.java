package com.imis.module.online.form.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 *
 * 在线开发-JavaScript 增强-更新 RO
 *
 * @author XinLau
 * @since 2020-10-19
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="FormEnhanceJavaScript更新对象", description="在线开发-JavaScript 增强-更新")
public class FormEnhanceJavaScriptUpdateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * JavaScript增强 编号
     */
    @ApiModelProperty(value = "JavaScript增强 编号", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private Long id;

    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private Long tableHeadId;
    /**
     * JS增强内容
     */
    @ApiModelProperty(value = "JS增强内容", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private String javaScriptContent;
    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String javaScriptType;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;

}
