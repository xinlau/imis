package com.imis.module.online.form.model.ro;

import com.imis.base.constant.ValidationMessageConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 *
 * 在线开发-Java增强-新增 RO
 *
 * @author XinLau
 * @since 2020-10-27
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="FormEnhanceJava新增对象", description="在线开发-Java增强")
public class FormEnhanceJavaAddDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private Long tableHeadId;
    /**
     * 按钮编码
     */
    @ApiModelProperty(value = "按钮编码", required = true)
    @NotEmpty(message = ValidationMessageConstant.EMPTY_ERR)
    private String buttonCode;
    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String javaType;
    /**
     * 数值
     */
    @ApiModelProperty(value = "数值")
    private String javaValue;
    /**
     * 生效状态(0无效，1有效)
     */
    @ApiModelProperty(value = "生效状态(0无效，1有效)")
    private Integer activeStatus;
    /**
     * 事件状态(end:结束，start:开始)
     */
    @ApiModelProperty(value = "事件状态(end:结束，start:开始)")
    private String event;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
}
