package com.imis.module.online.form.enhance;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.globle.exception.JavaEnhanceException;

/**
 * <p>
 * JavaEnhanceInterface<br>
 * JAVA 增强接口定义
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年11月20日 14:45:15
 */
public interface JavaEnhanceInterface {

    /**
     * 入的时候此返回值有效，返回 0则丢弃此数据，1则新增数据(默认1) ，2则修改此数据 注意：如果返回2 则需要往json中写入主键ID值
     *
     * @param tableName - 数据库表表名
     * @param json - 数据库表数据
     * @return Integer -
     * @throws JavaEnhanceException - Java 增强执行异常
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    Integer execute(String tableName, JSONObject json) throws JavaEnhanceException;


}
