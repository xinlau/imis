package com.imis.module.online.form.enhance;

import com.imis.base.globle.exception.JavaEnhanceException;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * JavaEnhanceInterface<br>
 * JAVA 增强接口定义（批量数据处理）
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年11月20日 14:45:15
 */
public interface JavaEnhanceListInterface {

    /**
     * 查询或导出逻辑增强处理（批量数据处理）
     *
     * @param tableName - 数据库表表名
     * @param dataList - 数据库表数据
     * @return Integer -
     * @throws JavaEnhanceException - Java 增强执行异常
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    Integer execute(String tableName, List<Map<String, Object>> dataList) throws JavaEnhanceException;


}
