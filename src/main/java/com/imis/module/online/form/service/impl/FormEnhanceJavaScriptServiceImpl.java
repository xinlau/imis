package com.imis.module.online.form.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.base.impl.BaseServiceImpl;
import com.imis.module.online.form.dao.FormEnhanceJavaScriptMapper;
import com.imis.module.online.form.model.converter.FormEnhanceJavaScriptConverter;
import com.imis.module.online.form.model.po.FormEnhanceJavaScript;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaScriptDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaScriptVO;
import com.imis.module.online.form.service.IFormEnhanceJavaScriptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-JavaScript 增强 服务实现类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Slf4j
@Service
public class FormEnhanceJavaScriptServiceImpl extends BaseServiceImpl<FormEnhanceJavaScriptMapper, FormEnhanceJavaScript> implements IFormEnhanceJavaScriptService {

    /**
     * 在线开发-JavaScript 增强-分页查询
     *
     * @param pagingQueryFormEnhanceJavaScript - EnhanceJavaScript 分页查询对象
     * @return Page<FormEnhanceJavaScriptVO> - EnhanceJavaScript 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    @Override
    public Page<FormEnhanceJavaScriptVO> pagingQueryListByParameter(final PagingQueryFormEnhanceJavaScriptDTO pagingQueryFormEnhanceJavaScript) {
        // 1.页码、页长
        Page<FormEnhanceJavaScript> formEnhanceJavaScriptPage = new Page<>(pagingQueryFormEnhanceJavaScript.getPageNumber(), pagingQueryFormEnhanceJavaScript.getPageSize());
        // 2.排序字段
        formEnhanceJavaScriptPage.addOrder(OrderItem.desc(DataBaseConstant.CREATE_TIME_FIELD), OrderItem.desc(DataBaseConstant.UPDATE_TIME_FIELD));
        // 3.条件分页查询
        formEnhanceJavaScriptPage = mapper.pagingQueryListByParameter(formEnhanceJavaScriptPage, pagingQueryFormEnhanceJavaScript);
        // 4.PO分页对象转VO实体分页对象
        return FormEnhanceJavaScriptConverter.INSTANCE.getReturnValue(formEnhanceJavaScriptPage);
    }

}