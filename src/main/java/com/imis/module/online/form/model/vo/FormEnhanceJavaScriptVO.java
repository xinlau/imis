package com.imis.module.online.form.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * 在线开发-JavaScript 增强 VO
 *
 * @author XinLau
 * @since 2020-10-19
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="FormEnhanceJavaScript对象", description="在线开发-JavaScript 增强")
public class FormEnhanceJavaScriptVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * JavaScript增强 编号
     */
    @ApiModelProperty(value = "JavaScript增强 编号")
    private String id;
    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID")
    private String tableHeadId;
    /**
     * JS增强内容
     */
    @ApiModelProperty(value = "JS增强内容")
    private String javaScriptContent;
    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String javaScriptType;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
}
