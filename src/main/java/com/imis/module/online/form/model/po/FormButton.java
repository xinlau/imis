package com.imis.module.online.form.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.imis.module.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 在线开发-表单按钮 PO
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("onl_form_button")
public class FormButton extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @TableField("table_head_id")
    private Long tableHeadId;

    /**
     * 按钮编码
     */
    @TableField("button_code")
    private String buttonCode;

    /**
     * 按钮图标
     */
    @TableField("button_icon")
    private String buttonIcon;

    /**
     * 按钮名称
     */
    @TableField("button_name")
    private String buttonName;

    /**
     * 按钮状态(0无效，1有效)
     */
    @TableField("button_status")
    private Integer buttonStatus;

    /**
     * 按钮类型
     */
    @TableField("button_type")
    private String buttonType;

    /**
     * 按钮样式
     */
    @TableField("button_style")
    private String buttonStyle;

    /**
     * 按钮位置（1侧面 2底部）
     */
    @TableField("button_position")
    private Integer buttonPosition;

    /**
     * 表达式
     */
    @TableField("expression")
    private String expression;

    /**
     * 排序
     */
    @TableField("sort_order")
    private Double sortOrder;

}
