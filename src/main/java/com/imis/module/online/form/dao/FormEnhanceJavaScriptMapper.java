package com.imis.module.online.form.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.online.form.model.po.FormEnhanceJavaScript;
import com.imis.module.base.BaseMapper;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaScriptDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 在线开发-JavaScript 增强 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
public interface FormEnhanceJavaScriptMapper extends BaseMapper<FormEnhanceJavaScript> {

    /**
     * 在线开发-JavaScript 增强-分页查询
     *
     * @param formEnhanceJavaScriptPage - EnhanceJavaScript 分页对象
     * @param pagingQueryFormEnhanceJavaScript - EnhanceJavaScript 分页查询对象
     * @return Page<FormEnhanceJavaScript> - EnhanceJavaScript 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    Page<FormEnhanceJavaScript> pagingQueryListByParameter(@Param("pg") final Page<FormEnhanceJavaScript> formEnhanceJavaScriptPage, @Param("js") final PagingQueryFormEnhanceJavaScriptDTO pagingQueryFormEnhanceJavaScript);

}