package com.imis.module.online.form.model.ro;

import com.imis.module.base.BasePageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 *
 * 在线开发-JavaScript 增强-分页查询 RO
 *
 * @author XinLau
 * @since 2020-10-19
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="FormEnhanceJavaScript分页查询对象", description="在线开发-JavaScript 增强-分页查询")
public class PagingQueryFormEnhanceJavaScriptDTO extends BasePageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单ID
     */
    @ApiModelProperty(value = "表单ID")
    private Long tableHeadId;
    /**
     * JS增强内容
     */
    @ApiModelProperty(value = "JS增强内容")
    private String javaScriptContent;
    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String javaScriptType;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;
    
}
