package com.imis.module.online.form.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.online.form.model.po.FormEnhanceJavaScript;
import com.imis.module.base.BaseService;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaScriptDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaScriptVO;

/**
 * <p>
 * 在线开发-JavaScript 增强 服务类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-19
 */
public interface IFormEnhanceJavaScriptService extends BaseService<FormEnhanceJavaScript> {

    /**
     * 在线开发-JavaScript 增强-分页查询
     *
     * @param pagingQueryFormEnhanceJavaScript - EnhanceJavaScript 分页查询对象
     * @return Page<FormEnhanceJavaScriptVO> - EnhanceJavaScript 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    Page<FormEnhanceJavaScriptVO> pagingQueryListByParameter(final PagingQueryFormEnhanceJavaScriptDTO pagingQueryFormEnhanceJavaScript);

}