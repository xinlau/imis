package com.imis.module.online.form.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.module.online.form.model.po.FormEnhanceJava;
import com.imis.module.base.BaseMapper;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 在线开发-Java增强 Mapper 接口
 * </p>
 *
 * @author XinLau
 * @since 2020-10-27
 */
public interface FormEnhanceJavaMapper extends BaseMapper<FormEnhanceJava> {

    /**
     * 在线开发-Java 增强-分页查询
     *
     * @param formEnhanceJavaPage        - EnhanceJava 分页对象
     * @param pagingQueryFormEnhanceJava - EnhanceJava 分页查询对象
     * @return Page<FormEnhanceJavaVO> - EnhanceJava 对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    Page<FormEnhanceJava> pagingQueryListByParameter(@Param("pg") final Page<FormEnhanceJava> formEnhanceJavaPage, @Param("java") final PagingQueryFormEnhanceJavaDTO pagingQueryFormEnhanceJava);

}