package com.imis.module.online.form.bus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.enums.ArgumentResponseEnum;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.base.util.form.FormUtil;
import com.imis.module.base.BaseBus;
import com.imis.module.online.form.model.converter.FormEnhanceJavaConverter;
import com.imis.module.online.form.model.po.FormEnhanceJava;
import com.imis.module.online.form.model.ro.FormEnhanceJavaAddDTO;
import com.imis.module.online.form.model.ro.FormEnhanceJavaUpdateDTO;
import com.imis.module.online.form.model.ro.PagingQueryFormEnhanceJavaDTO;
import com.imis.module.online.form.model.vo.FormEnhanceJavaVO;
import com.imis.module.online.form.service.IFormEnhanceJavaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线开发-Java增强 业务处理类
 * </p>
 *
 * @author XinLau
 * @since 2020-10-27
 */
@Slf4j
@Service
public class FormEnhanceJavaBus extends BaseBus {

    /**
     * 在线开发-Java增强 服务类
     */
    private IFormEnhanceJavaService serviceByFormEnhanceJavaService;

    @Autowired
    public void setFormEnhanceJavaService(IFormEnhanceJavaService serviceByFormEnhanceJavaService) {
        this.serviceByFormEnhanceJavaService = serviceByFormEnhanceJavaService;
    }

    /**
     * 在线开发-Java 增强-添加校验
     *
     * @param formEnhanceJavaAdd - FormEnhanceJava 添加对象
     * @return FormEnhanceJava - 在线开发-Java 增强
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/12 15:47
     */
    private FormEnhanceJava formEnhanceJavaAddVerification(final FormEnhanceJavaAddDTO formEnhanceJavaAdd) {
        FormEnhanceJava formEnhanceJava = FormEnhanceJavaConverter.INSTANCE.getAddEntity(formEnhanceJavaAdd);
        // 判断 Class 或是 Spring 的 Bean 是否存在
        Boolean check = FormUtil.checkClassOrSpringBeanIsExist(formEnhanceJava);
        ArgumentResponseEnum.FORM_JAVA_ADD_ERR_CLASS_BEAN_NON.assertIsTrue(check);
        return formEnhanceJava;
    }

    /**
     * 在线开发-Java 增强-更新校验
     *
     * @param formEnhanceJavaUpdate - FormEnhanceJava 更新对象
     * @return FormEnhanceJava - 在线开发-Java 增强
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    private FormEnhanceJava formEnhanceJavaUpdateVerification(final FormEnhanceJavaUpdateDTO formEnhanceJavaUpdate) {
        FormEnhanceJava formEnhanceJava = serviceByFormEnhanceJavaService.getById(formEnhanceJavaUpdate.getId());
        // 校验在线开发-Java 增强是否存在
        ArgumentResponseEnum.FORM_JAVA_UPDATE_ERR_NON.assertNotNull(formEnhanceJava, formEnhanceJavaUpdate.getId());
        FormEnhanceJavaConverter.INSTANCE.getUpdateEntity(formEnhanceJava, formEnhanceJavaUpdate);
        // 判断 Class 或是 Spring 的 Bean 是否存在
        Boolean check = FormUtil.checkClassOrSpringBeanIsExist(formEnhanceJava);
        ArgumentResponseEnum.FORM_JAVA_UPDATE_ERR_CLASS_BEAN_NON.assertIsTrue(check);
        return formEnhanceJava;
    }

    /**
     * 在线开发-Java 增强-分页查询
     *
     * @param pagingQueryFormEnhanceJava - FormEnhanceJava分页查询对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public CommonResponse<Page<FormEnhanceJavaVO>> pagingQueryListByParameter(final PagingQueryFormEnhanceJavaDTO pagingQueryFormEnhanceJava) {
        // 查询数据库内的Java 增强
        Page<FormEnhanceJavaVO> formEnhanceJavaPage = serviceByFormEnhanceJavaService.pagingQueryListByParameter(pagingQueryFormEnhanceJava);
        return new CommonResponse(formEnhanceJavaPage);
    }

    /**
     * 在线开发-Java 增强-添加
     *
     * @param formEnhanceJavaAdd - FormEnhanceJava添加对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse add(final FormEnhanceJavaAddDTO formEnhanceJavaAdd) {
        // 创建新Java 增强 校验Java 增强重复
        FormEnhanceJava formEnhanceJava = formEnhanceJavaAddVerification(formEnhanceJavaAdd);
        boolean save = serviceByFormEnhanceJavaService.save(formEnhanceJava);
        ArgumentResponseEnum.FORM_JAVA_ADD_ERR.assertIsTrue(save);
        return new BaseResponse();
    }

    /**
     * 在线开发-Java 增强-查询
     *
     * @param id - FormEnhanceJava标识
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public CommonResponse<FormEnhanceJavaVO> queryById(final Long id) {
        FormEnhanceJava formEnhanceJava = serviceByFormEnhanceJavaService.getById(id);
        // 校验Java 增强是否存在
        ArgumentResponseEnum.FORM_JAVA_QUERY_ERR_FORM_JAVA_NON.assertNotNull(formEnhanceJava, id);
        return new CommonResponse(FormEnhanceJavaConverter.INSTANCE.getReturnValue(formEnhanceJava));
    }

    /**
     * 在线开发-Java 增强-更新
     *
     * @param formEnhanceJavaUpdate - FormEnhanceJava更新对象
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse updateById(final FormEnhanceJavaUpdateDTO formEnhanceJavaUpdate) {
        // 更新Java 增强 校验Java 增强更新对象
        FormEnhanceJava formEnhanceJava = formEnhanceJavaUpdateVerification(formEnhanceJavaUpdate);
        boolean save = serviceByFormEnhanceJavaService.updateById(formEnhanceJava);
        ArgumentResponseEnum.FORM_JAVA_UPDATE_ERR.assertIsTrue(save, formEnhanceJavaUpdate.getId());
        return new BaseResponse();
    }

    /**
     * 在线开发-Java 增强-删除
     *
     * @param id - FormEnhanceJava标识
     * @return Result
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/12/31 15:47
     */
    public BaseResponse deleteById(final Long id) {
        FormEnhanceJava formEnhanceJava = serviceByFormEnhanceJavaService.getById(id);
        // 校验Java 增强是否存在
        ArgumentResponseEnum.FORM_JAVA_DELETE_ERR_FORM_JAVA_NULL.assertNotNull(formEnhanceJava, id);
        boolean remove = serviceByFormEnhanceJavaService.removeById(id);
        ArgumentResponseEnum.FORM_JAVA_DELETE_ERR.assertIsTrue(remove, id);
        return new BaseResponse();
    }

}