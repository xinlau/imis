package com.imis.module.websocket.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.imis.base.globle.response.BaseResponse;
import com.imis.base.globle.response.CommonResponse;
import com.imis.module.base.BaseController;
import com.imis.module.system.model.vo.SysWebsocketMessageInfoVO;
import com.imis.module.websocket.bus.InstantMessagingBus;
import com.imis.module.websocket.dto.PagingQueryMessageDTO;
import com.imis.module.websocket.dto.WebSocketMessageDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * InstantMessagingController<br>
 * 即时通讯 前端控制器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月24日 17:43
 */
@Slf4j
@RestController
@RequestMapping(path = "/sys/messaging")
@Api(tags = {"即时通讯"})
@ApiSort(1)
public class InstantMessagingController extends BaseController<InstantMessagingBus> {

    @GetMapping(path = "/queryOnlineNumber")
    @ApiOperation(value = "查询在线人数接口", notes = "查询在线人数接口")
    @ApiOperationSupport(order = 1, author = "XinLau")
    public CommonResponse<Integer>queryOnlineNumber() {
        return service.queryOnlineNumber();
    }

    @PostMapping(path = "/pagingQueryHistoricalMassages")
    @ApiOperation(value = "查看历史消息接口", notes = "查看指定好友的历史消息接口")
    @ApiOperationSupport(order = 2, author = "XinLau")
    public CommonResponse<Page<SysWebsocketMessageInfoVO>> pagingQueryHistoricalMassages(@RequestBody PagingQueryMessageDTO pagingQueryMessageDTO) {
        return service.pagingQueryHistoricalMassages(pagingQueryMessageDTO);
    }

    @PostMapping(path = "/sendMassage")
    @ApiOperation(value = "发送消息接口", notes = "发送消息接口")
    @ApiOperationSupport(order = 3, author = "XinLau")
    public BaseResponse sendMassage(@RequestBody WebSocketMessageDTO webSocketMessage) {
        return service.sendMassage(webSocketMessage);
    }

}
