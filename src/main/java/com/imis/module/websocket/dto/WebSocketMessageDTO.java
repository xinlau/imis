package com.imis.module.websocket.dto;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * WebSocketMessageDTO<br>
 * WebSocket信息 DTO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月24日 10:37
 */
@Data
@ApiModel(value="WebSocket信息对象", description="WebSocket信息")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WebSocketMessageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发送方标识
     */
    @ApiModelProperty(value = "发送方标识", hidden = true)
    private String senderIdentification;

    /**
     * 接收方标识
     */
    @ApiModelProperty(value = "接收方标识", required = true)
    private String[] receiverIdentificationArray;

    /**
     * 信息内容
     */
    @ApiModelProperty(value = "信息内容", required = true)
    private JSONObject message;

}
