package com.imis.module.websocket.dto;

import cn.hutool.core.date.DatePattern;
import com.imis.base.util.ConvertUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * PagingQueryMessageDTO<br>
 * 分页查看历史消息 RO
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月11日 15:06
 */
@Data
@ApiModel(value="分页WebSocket信息对象", description="分页WebSocket信息")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PagingQueryMessageDTO implements Serializable {

    /**
     * 接收方标识
     */
    @ApiModelProperty(value = "接收方标识", required = true)
    private String receiverIdentification;

    /**
     * 发送方标识
     */
    @ApiModelProperty(value = "发送方标识", hidden = true)
    private String senderIdentification;

    /**
     * 页码
     */
    @ApiModelProperty(value = "页码", required = true)
    private Long pageNumber;
    public Long getPageNumber() {
        return this.pageNumber = ConvertUtils.isEmpty(pageNumber) ? 1L : pageNumber;
    }

    /**
     * 页长
     */
    @ApiModelProperty(value = "页长", required = true)
    private Long pageSize;
    public Long getPageSize() {
        return this.pageSize = ConvertUtils.isEmpty(pageSize) ? 10L : pageSize;
    }

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间-起")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTimeStart;
    @ApiModelProperty(value = "创建时间-始")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private LocalDateTime createTimeEnd;

}
