package com.imis.base.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * RequestParameterWrapper<br>
 * 请求参数修改
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月16日 15:44
 */
public class RequestParameterWrapper extends HttpServletRequestWrapper {

    /**
     * 参数缓存对象
     */
    private Map<String, String[]> parameters = new HashMap<>();

    /**
     * Constructs a request object wrapping the given request.
     * 构造一个包装给定请求的请求对象
     *
     * @param request - The request to wrap（包装请求）
     * @throws IllegalArgumentException - if the request is null
     */
    public RequestParameterWrapper(HttpServletRequest request) {
        super(request);
        // 将现有 parameter 传递给 parameters
        this.parameters.putAll(request.getParameterMap());
    }

    /**
     * 添加参数
     *
     * @param extraParams - 参数
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/16 15:49
     */
    void addParameters(Map<String, Object> extraParams) {
        for (Map.Entry<String, Object> entry : extraParams.entrySet()) {
            addParameter(entry.getKey(), entry.getValue());
        }
    }

    /**
     * 重写 getParameter()，参数从当前类中的 parameters Map 获取
     *
     * @param parameterName - 参数名
     * @return String - 参数
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/16 15:49
     */
    @Override
    public String getParameter(String parameterName) {
        String[] values = parameters.get(parameterName);
        if (values == null || values.length == 0) {
            return null;
        }
        return values[0];
    }

    /**
     * 重写 getParameterValues()，参数从当前类中的 parameters Map 获取
     *
     * @param parameterName - 参数名
     * @return String[] - 参数
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/16 15:49
     */
    @Override
    public String[] getParameterValues(String parameterName) {
        return parameters.get(parameterName);
    }

    /**
     * 添加参数
     *
     * @param parameterName  - 参数名
     * @param parameterValue - 参数值
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/16 15:56
     */
    private void addParameter(String parameterName, Object parameterValue) {
        if (parameterValue != null) {
            if (parameterValue instanceof String[]) {
                parameters.put(parameterName, (String[]) parameterValue);
            } else if (parameterValue instanceof String) {
                parameters.put(parameterName, new String[]{(String) parameterValue});
            } else {
                parameters.put(parameterName, new String[]{String.valueOf(parameterValue)});
            }
        }
    }

}
