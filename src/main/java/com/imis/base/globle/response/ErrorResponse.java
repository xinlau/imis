package com.imis.base.globle.response;

import com.imis.base.constant.IResponseEnum;

/**
 * <p>
 * ErrorResponse<br>
 * 错误返回结果
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月31日 16:50
 */
public class ErrorResponse extends BaseResponse {

    public ErrorResponse() {
    }

    /**
     * @param code    -CODE
     * @param message - MESSAGE
     */
    public ErrorResponse(Integer code, String message) {
        super(code, message);
    }

    /**
     * @param responseEnum - IResponseEnum
     */
    public ErrorResponse(IResponseEnum responseEnum) {
        super(responseEnum.getCode(), responseEnum.getMessage());
    }

}