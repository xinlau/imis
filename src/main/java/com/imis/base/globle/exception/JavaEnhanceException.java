package com.imis.base.globle.exception;

import com.imis.base.constant.IResponseEnum;

/**
 * <p>
 * JavaEnhanceException<br>
 * Java 增强执行异常
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年11月20日 14:46
 */
public class JavaEnhanceException extends BaseException {
    private static final long serialVersionUID = 1L;

    /**
     * @param responseEnum - 异常返回码枚举接口
     * @param args         - 参数
     * @param message      - MESSAGE
     */
    public JavaEnhanceException(IResponseEnum responseEnum, Object[] args, String message) {
        super(responseEnum, args, message);
    }

    /**
     * @param responseEnum - 异常返回码枚举接口
     * @param args         - 参数
     * @param message      - MESSAGE
     * @param throwable    - Throwable
     */
    public JavaEnhanceException(IResponseEnum responseEnum, Object[] args, String message, Throwable throwable) {
        super(responseEnum, args, message, throwable);
    }

}