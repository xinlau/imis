package com.imis.base.constant;

import org.springframework.http.HttpHeaders;

/**
 * <p>
 * HttpHeadersConstants<br>
 * HTTP请求或响应标头的数据结构常量
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public class HttpHeadersConstants extends HttpHeaders {

    /**
     * 自定义Token<br>
     * eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTYxNjUyNzIsInVzZXJuYW1lIjoiYWRtaW5fcmljaGFuZyJ9.R8ZaEbHFP95Arjo9WZZGAtFZngfAWlIQ_URJjq-24Vg
     */
    public static final String X_ACCESS_TOKEN = "X-Access-Token";
    /**
     * 用户编号
     */
    public static final String X_ACCESS_USER_ID = "X-Access-User_Id";
    /**
     * 角色编号
     */
    public static final String X_ACCESS_ROLE_ID = "X-Access-Role_Id";
    /**
     * 组织机构编号
     */
    public static final String X_ACCESS_ORGANIZATION_ID = "X-Access-Organization_Id";
    /**
     * 数据权限控制
     */
    public static final String MENU_DATA_AUTHOR_RULES = "MENU_DATA_AUTHOR_RULES";
    /**
     * 数据权限控制 SQL
     */
    public static final String MENU_DATA_AUTHOR_RULE_SQL = "MENU_DATA_AUTHOR_RULE_SQL";

}
