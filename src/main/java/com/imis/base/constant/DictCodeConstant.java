package com.imis.base.constant;

/**
 * <p>
 * DictCodeConstant<br>
 * DictCode常量
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月24日 16:02
 */
public interface DictCodeConstant {

    /**
     * 状态（1启用 0不启用）
     */
    String STATUS = "status";
    /**
     * 系统日志类型（0成功 1失败）
     */
    String LOG_TYPE = "logType";
    /**
     * 菜单类型（0：一级菜单，1：子菜单，2按钮权限）
     */
    String MENU_TYPE = "menuType";
    /**
     * 任务状态
     */
    String JOB_STATE = "jobState";
    /**
     * 性别(0：默认未知 1：男 2：女)
     */
    String SEX = "sex";
    /**
     * 信息状态（0：发送成功，1：等待接收，2：发送异常）
     */
    String MESSAGE_STATE = "messageState";
    /**
     * 同步数据库状态（N未同步；Y已同步）
     */
    String SYNCHRONIZE_STATE = "isSynchronize";
    /**
     * 是否（0否 1是）
     */
    String STATUS_OR_NOT = "statusOrNot";
    /**
     * 表类型（ 0单表、1主表、2附表）
     */
    String TABLE_TYPE = "tableType";
    /**
     * 流程状态
     */
    String PROCESS_STATE = "processState";

}
