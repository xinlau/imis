package com.imis.base.constant;

import com.baomidou.mybatisplus.core.toolkit.Constants;

/**
 * <p>
 * CommonConstant<br>
 * 公共常量接口
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public interface CommonConstant extends Constants {

    /**
     * 项目文件地址
     */
    String FILE_PATH = System.getProperty("user.dir");

    char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * 验证码标识长度
     */
    int VERIFICATION_CODE_IDENTIFICATION_LENGTH = 18;

    String MD2 = "MD2";

    String OK = "OK";

    String UNKNOWN = "unknown";

    String TO_UPPER_CASE_N = "N";
    String TO_UPPER_CASE_Y = "Y";

    /**
     * CLASS
     */
    String CLASS = "class";

    /**
     * SPRING
     */
    String SPRING = "spring";

    /**
     * Kaptcha 验证码相关
     */
    String KAPTCHA_SESSION_KEY = "KAPTCHA_SESSION_KEY";
    String KAPTCHA_SESSION_DATE = "KAPTCHA_SESSION_DATE";
    /**
     * Kaptcha 验证码有效期
     */
    long KAPTCHA_SESSION_EFFECTIVE_TIME = 2 * 60 * 1000;
    /**
     * Kaptcha 验证码 Base64前缀
     */
    String KAPTCHA_PREFIX = "data:image/png;base64,";
    /**
     * 短信验证码有效期
     */
    long SME_KAPTCHA_SESSION_EFFECTIVE_TIME = 5 * 60 * 1000;
    /**
     * 邮件验证码有效期
     */
    long EMAIL_KAPTCHA_SESSION_EFFECTIVE_TIME = 5 * 60 * 1000;

    /**
     * 用户是否已被冻结  1(解冻)正常 2冻结
     */
    Integer USER_UNFREEZE = 1;
    Integer USER_FREEZE = 2;

    /**
     * 删除标志  1已删除 0未删除
     */
    Integer DEL_FLAG_DELETE = 1;
    Integer DEL_FLAG_NOT_DELETE = 0;

    /**
     * 系统日志类型： 0成功 1失败 2未知
     */
    Integer LOG_TYPE_OK = 0;
    Integer LOG_TYPE_ERROR = 1;
    Integer LOG_TYPE_UNKNOWN = 2;

    /**
     * 分页查询授权角色信息类型： 0-未授权 1-已授权
     */
    Integer ROLE_GRANT = 1;
    Integer ROLE_NOT_GRANT = 0;

    /**
     * 菜单类型： 0：一级菜单  1：子菜单  2：按钮权限
     */
    Integer MENU_TYPE_MENU = 0;
    Integer MENU_TYPE_SEC_MENU = 1;
    Integer MENU_TYPE_BUTTON = 2;

    /**
     * 状态(0无效  1有效)
     */
    Integer STATUS_ERR = 0;
    Integer STATUS_OK = 1;

    /**
     * 是否（0否 1是）
     */
    Integer INTEGER_YES = 1;
    Integer INTEGER_NO = 0;

    /**
     * 事件状态(end:结束，start:开始)
     */
    String EVENT_START = "start";
    String EVENT_END = "end";

    /**
     * 表类型（ 0单表、1主表、2附表）
     */
    Integer TABLE_TYPE_0 = 0;
    Integer TABLE_TYPE_1 = 1;
    Integer TABLE_TYPE_2 = 2;

    /**
     * online 导入数据标识 走新增逻辑
     */
    Integer OL_IMPORT_INSERT = 1;

    /**
     * online 导入数据标识 走编辑逻辑
     */
    Integer OL_IMPORT_UPDATE = 2;

    /**
     * online 导入数据标识 丢弃
     */
    Integer OL_IMPORT_ABANDON = 0;

    /**
     * 信息状态（0：发送成功，1：等待接收，2：发送异常）
     */
    Integer MESSAGE_STATE_SENT_SUCCESSFULLY = 0;
    Integer MESSAGE_STATE_WAITING_TO_RECEIVE = 1;
    Integer MESSAGE_STATE_SEND_EXCEPTION = 2;

    /**
     * JS增强变更主方法名
     * 单表： onlineChange
     * 子表： {表名}_onlineChange
     */
    String ONLINE_JAVA_SCRIPT_CHANGE_FUNCTION_NAME = "onlineChange";

    /**
     * 按钮样式
     */
    String ONLINE_BUTTON_STYLE_LINK = "link";
    String ONLINE_BUTTON_STYLE_BUTTON = "button";
    String ONLINE_BUTTON_STYLE_FORM = "form";

    /**
     * JAVA增强的编码 导入
     */
    String ENHANCE_IMPORT = "import";
    /**
     * JAVA增强的编码 导出
     */
    String ENHANCE_EXPORT = "export";
    /**
     * JAVA增强的编码 查询
     */
    String ENHANCE_QUERY = "query";

    /**
     * JS增强 form
     */
    String ENHANCE_FORM = "form";
    /**
     * JS增强 list
     */
    String ENHANCE_LIST = "list";

    /**
     * 字典翻译文本后缀
     */
    String DICT_TEXT_SUFFIX = "_dictText";

    /**
     * 定时任务类全名称前缀
     */
    String QUARTZ_CLASS_NAME = "com.imis.module.quartz";

    /**
     * 文件上传类型
     */
    String UPLOAD_TYPE_LOCAL = "local";

    /**
     * SQL注入校验
     */
    String SQL_REGULAR_EXPRESSION_1 = "select|update|delete|insert|truncate|char|chr|into|substr|ascii|declare|exec|count|master|drop|execute|waitfor";
    /**
     * SQL注入校验
     */
    String SQL_REGULAR_EXPRESSION = "\\b(exec|insert|select|drop|grant|alter|delete|update|count|chr|mid|master|truncate|char|declare)\\b|(\\*|;|\\+|'|%)";
    /**
     * SQL注入校验
     */
    String SQL_REGULAR_EXPRESSION_2 = "'|and |exec |insert |select |delete |update |drop |count |chr |mid |master |truncate |char |declare |;|or |+|,";

    /**
     * 电话号码正则表达式
     */
    String PHONE_REGULAR_EXPRESSION = "^[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}$";

    /**
     * 移动端正则表达式
     */
    String MOBILE_TYPE_REGULAR_EXPRESSION = "(phone|pad|pod|iphone|ipod|ios|ipad|android|mobile|blackberry|iemobile|mqqbrowser|juc|fennec|wosbrowser|browserng|webos|symbian|windows phone)";

    /**
     * 数据库表模板名
     */
    String TABLE_TEMPLATE = "/templates/table/tableTemplate.ftl";

}
