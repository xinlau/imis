package com.imis.base.constant;

/**
 * <p>
 * ConverterIgnoreConstant<br>
 * Converter 转换器忽略转换的对象字段
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年08月10日 14:21
 */
public interface ConverterIgnoreConstant {

    String ID = "id";
    /**
     * 创建者登录名称
     */
    String CREATE_BY = DataBaseConstant.CREATE_BY;
    /**
     * 创建日期时间
     */
    String CREATE_TIME = DataBaseConstant.CREATE_TIME;
    /**
     * 更新用户登录名称
     */
    String UPDATE_BY = DataBaseConstant.UPDATE_BY;
    /**
     * 更新日期时间
     */
    String UPDATE_TIME = DataBaseConstant.UPDATE_TIME;
    /**
     * 分页对象-升序-多个
     */
    String ASCS = "ascs";
    /**
     * 分页对象-升序
     */
    String ASC = "asc";
    /**
     * 分页对象-降序-多个
     */
    String DESCS = "descs";
    /**
     * 分页对象-降序
     */
    String DESC = "desc";
    /**
     * 分页对象-优化计数SQL
     */
    String OPTIMIZE_COUNT_SQL = "optimizeCountSql";

}
