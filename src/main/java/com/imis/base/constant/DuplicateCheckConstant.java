package com.imis.base.constant;

/**
 * <p>
 * DuplicateCheckConstant<br>
 * 重复校验数据 常量类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年06月30日 09:20
 */
public interface DuplicateCheckConstant {

    /**
     * 字典项 表名
     */
    String TABLE_NAME_SYS_DICT = "sys_dict";
    /**
     * 字典项-字典项名称 列名
     */
    String FIELD_NAME_DICT_NAME = "dict_name";
    /**
     * 字典项-字典项编号 列名
     */
    String FIELD_NAME_DICT_CODE = "dict_code";

    /**
     * 系统用户 表名
     */
    String TABLE_NAME_SYS_USER = "sys_user";
    /**
     * 系统用户-登录账号 列名
     */
    String FIELD_NAME_USER_NAME = "username";
    /**
     * 系统用户-电话 列名
     */
    String FIELD_NAME_USER_PHONE = "phone";
    /**
     * 系统用户-邮箱 列名
     */
    String FIELD_NAME_USER_EMAIL = "email";

    /**
     * 系统角色 表名
     */
    String TABLE_NAME_SYS_ROLE = "sys_role";
    /**
     * 系统角色-角色名称 列名
     */
    String FIELD_NAME_ROLE_NAME = "role_name";
    /**
     * 系统角色-角色编码 列名
     */
    String FIELD_NAME_ROLE_CODE = "role_code";

    /**
     * 功能菜单 表名
     */
    String TABLE_NAME_SYS_MENU = "sys_menu";
    /**
     * 功能菜单-功能菜单名称 列名
     */
    String FIELD_NAME_MENU_NAME = "name";

    /**
     * 组织机构 表名
     */
    String TABLE_NAME_SYS_ORGANIZATION = "sys_organization";
    /**
     * 组织机构-组织机构名称 列名
     */
    String FIELD_NAME_ORGANIZATION_NAME = "organization_name";

    /**
     * 定时任务 表名
     */
    String TABLE_NAME_SYS_QUARTZ_JOB = "sys_quartz_job";
    /**
     * 定时任务-定时任务名称 列名
     */
    String FIELD_NAME_QUARTZ_JOB_NAME = "job_name";

}
