package com.imis.base.constant.permissions;

/**
 * <p>
 * PermissionsValueConstant<br>
 * 请求权限常量类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月10日 11:31
 */
public interface PermissionsValueConstant {

    /**
     * 分页查询系统日志接口
     */
    String LOG_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.LOG + FunctionalNameConstant.PAGE;
    /**
     * 系统日志查看接口
     */
    String LOG_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.LOG + FunctionalNameConstant.QUERY_BY_ID;

    /**
     * 分页查询字典项接口
     */
    String DICT_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT + FunctionalNameConstant.PAGE;
    /**
     * 字典项添加接口
     */
    String DICT_ADD = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT + FunctionalNameConstant.ADD;
    /**
     * 字典项删除接口
     */
    String DICT_DELETE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT + FunctionalNameConstant.DELETE;
    /**
     * 字典项查看接口
     */
    String DICT_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 字典项更新接口
     */
    String DICT_UPDATE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT + FunctionalNameConstant.UPDATE;

    /**
     * 字典项查询字典值接口
     */
    String DICT_ITEM_QUERY_BY_DICT = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT_ITEM + "queryDictItemByDict";
    /**
     * 分页查询字典值接口
     */
    String DICT_ITEM_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT_ITEM + FunctionalNameConstant.PAGE;
    /**
     * 字典值添加接口
     */
    String DICT_ITEM_ADD = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT_ITEM + FunctionalNameConstant.ADD;
    /**
     * 字典值删除接口
     */
    String DICT_ITEM_DELETE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT_ITEM + FunctionalNameConstant.DELETE;
    /**
     * 字典值查看接口
     */
    String DICT_ITEM_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT_ITEM + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 字典值更新接口
     */
    String DICT_ITEM_UPDATE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DICT_ITEM + FunctionalNameConstant.UPDATE;

    /**
     * 分页查询系统用户接口
     */
    String USER_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + FunctionalNameConstant.PAGE;
    /**
     * 系统用户添加接口
     */
    String USER_ADD = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + FunctionalNameConstant.ADD;
    /**
     * 系统用户冻结接口
     */
    String USER_FREEZE = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + "freeze";
    /**
     * 系统用户解冻接口
     */
    String USER_UN_FREEZE = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + "unFreeze";
    /**
     * 系统用户删除接口
     */
    String USER_DELETE = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + FunctionalNameConstant.DELETE;
    /**
     * 系统用户查看接口
     */
    String USER_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 系统用户更新接口
     */
    String USER_UPDATE = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + FunctionalNameConstant.UPDATE;
    /**
     * 用户授权角色
     */
    String USER_GRANT_ROLE_TO_USER = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + "grantRoleToUser";
    /**
     * 取消用户角色授权
     */
    String USER_TERMINATE_ALL_ROLE_TO_USER = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + "terminateAllRoleToUser";
    /**
     * 为用户修改密码
     */
    String USER_CHANGE_USER_PASSWORD = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + "changeUserPassword";
    /**
     * 查询组织机构下的用户接口
     */
    String USER_PAGE_USER_BY_ORGANIZATION_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + "pagingQueryUserByOrganizationIdentification";
    /**
     * 查询组织机构下没有的用户接口
     */
    String USER_PAGE_UNAUTHORIZED_USER_BY_ORGANIZATION_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.USER + "pagingQueryUnauthorizedUserByOrganizationIdentification";

    /**
     * 分页查询系统角色接口
     */
    String ROLE_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + FunctionalNameConstant.PAGE;
    /**
     * 系统角色添加接口
     */
    String ROLE_ADD = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + FunctionalNameConstant.ADD;
    /**
     * 系统角色删除接口
     */
    String ROLE_DELETE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + FunctionalNameConstant.DELETE;
    /**
     * 系统角色查看接口
     */
    String ROLE_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 根据角色分页查询用户对象接口
     */
    String USER_PAGE_BY_ROLE_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + "pagingQueryUserByRoleId";
    /**
     * 根据角色分页查询未授权用户对象接口
     */
    String UNAUTHORIZED_USER_PAGE_BY_ROLE_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + "pagingQueryUnauthorizedUser";
    /**
     * 系统角色更新接口
     */
    String ROLE_UPDATE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + FunctionalNameConstant.UPDATE;
    /**
     * 根据用户ID分页查找已经授权的角色
     */
    String GRANT_ROLE_PAGE_BY_USER = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + "pagingQueryGrantRoleListByParameter";
    /**
     * 根据条件分页查询还未授权的角色
     */
    String UN_GRANT_ROLE_PAGE_BY_USER = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + "pagingQueryNotGrantRoleListByParameter";
    /**
     * 为角色授权功能菜单权限
     */
    String GRANT_MENU_TO_ROLE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + "grantMenuPermissionsToRole";
    /**
     * 撤销角色所有功能菜单权限
     */
    String TERMINATE_ALL_MENU_TO_ROLE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + "terminateAllMenuPermissionsToRole";
    /**
     * 将角色授权给用户
     */
    String AUTHORIZING_ROLE_TO_USER = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + "authorizingRoleToUsers";
    /**
     * 取消对用户的角色授权
     */
    String UN_AUTHORIZING_ROLE_TO_USER = ModuleNamConstant.SYS + FunctionalCategoryConstant.ROLE + "unAuthorizingRoleToUsers";

    /**
     * 分页查询功能菜单接口
     */
    String MENU_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.MENU + FunctionalNameConstant.PAGE;
    /**
     * 查询功能菜单树接口
     */
    String MENU_TREE = ModuleNamConstant.SYS + FunctionalCategoryConstant.MENU + FunctionalNameConstant.TREE;
    /**
     * 功能菜单添加接口
     */
    String MENU_ADD = ModuleNamConstant.SYS + FunctionalCategoryConstant.MENU + FunctionalNameConstant.ADD;
    /**
     * 功能菜单删除接口
     */
    String MENU_DELETE = ModuleNamConstant.SYS + FunctionalCategoryConstant.MENU + FunctionalNameConstant.DELETE;
    /**
     * 功能菜单查看接口
     */
    String MENU_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.MENU + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 功能菜单更新接口
     */
    String MENU_UPDATE = ModuleNamConstant.SYS + FunctionalCategoryConstant.MENU + FunctionalNameConstant.UPDATE;
    /**
     * 根据角色查看功能菜单
     */
    String MENU_QUERY_BY_ROLE_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.MENU + "queryMenuByRoleId";

    /**
     * 分页查询组织机构接口
     */
    String ORGANIZATION_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + FunctionalNameConstant.PAGE;
    /**
     * 组织机构添加接口
     */
    String ORGANIZATION_ADD = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + FunctionalNameConstant.ADD;
    /**
     * 组织机构删除接口
     */
    String ORGANIZATION_DELETE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + FunctionalNameConstant.DELETE;
    /**
     * 组织机构查看接口
     */
    String ORGANIZATION_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 查询组织机构树接口
     */
    String ORGANIZATION_TREE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + FunctionalNameConstant.TREE;
    /**
     * 组织机构更新接口
     */
    String ORGANIZATION_UPDATE = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + FunctionalNameConstant.UPDATE;
    /**
     * 根据父组织机构标识查询组织机构树接口
     */
    String ORGANIZATION_QUERY_BY_PARENT_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + "getTreeByParentId";
    /**
     * 根据用户标识查询用户拥有的组织机构接口
     */
    String ORGANIZATION_QUERY_BY_USER_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + "getOrganizationByUserIdentification";
    /**
     * 用户授权组织机构
     */
    String ORGANIZATION_GRANT_TO_USER = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + "grantOrganizationToUser";
    /**
     * 取消用户组织机构授权
     */
    String ORGANIZATION_TERMINATE_GRANT_TO_USER = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + "terminateAllOrganizationToUser";
    /**
     * 组织机构分配用户
     */
    String ORGANIZATION_AUTHORIZING_USER_TO_ORGANIZATION = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + "authorizingUserToOrganization";
    /**
     * 组织机构取消分配用户
     */
    String ORGANIZATION_UN_AUTHORIZING_USER_TO_ORGANIZATION = ModuleNamConstant.SYS + FunctionalCategoryConstant.ORGANIZATION + "unAuthorizingUserToOrganization";

    /**
     * 分页查询定时任务接口
     */
    String QUARTZ_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + FunctionalNameConstant.PAGE;
    /**
     * 定时任务添加接口
     */
    String QUARTZ_ADD = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + FunctionalNameConstant.ADD;
    /**
     * 定时任务删除接口
     */
    String QUARTZ_DELETE = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + FunctionalNameConstant.DELETE;
    /**
     * 定时任务查看接口
     */
    String QUARTZ_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 定时任务更新接口
     */
    String QUARTZ_UPDATE = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + FunctionalNameConstant.UPDATE;
    /**
     * 定时任务构建接口
     */
    String QUARTZ_SCHEDULER = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + "scheduler";
    /**
     * 定时任务执行接口
     */
    String QUARTZ_TRIGGER = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + "trigger";
    /**
     * 定时任务暂停接口
     */
    String QUARTZ_PAUSE = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + "pause";
    /**
     * 定时任务恢复接口
     */
    String QUARTZ_RESUME = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + "resume";
    /**
     * 定时任务移除接口
     */
    String QUARTZ_REMOVE = ModuleNamConstant.SYS + FunctionalCategoryConstant.QUARTZ + "remove";

    /**
     * 分页查询WebSocket信息传递记录接口
     */
    String WEBSOCKET_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.WEBSOCKET + FunctionalNameConstant.PAGE;
    /**
     * WebSocket信息传递记录查看接口
     */
    String WEBSOCKET_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.WEBSOCKET + FunctionalNameConstant.QUERY_BY_ID;

    /**
     * 分页查询数据源接口
     */
    String DATASOURCE_PAGE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DATASOURCE + FunctionalNameConstant.PAGE;
    /**
     * 数据源添加接口
     */
    String DATASOURCE_ADD = ModuleNamConstant.SYS + FunctionalCategoryConstant.DATASOURCE + FunctionalNameConstant.ADD;
    /**
     * 数据源冻结接口
     */
    String DATASOURCE_FREEZE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DATASOURCE + "freeze";
    /**
     * 数据源解冻接口
     */
    String DATASOURCE_UN_FREEZE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DATASOURCE + "unFreeze";
    /**
     * 数据源删除接口
     */
    String DATASOURCE_DELETE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DATASOURCE + FunctionalNameConstant.DELETE;
    /**
     * 数据源查看接口
     */
    String DATASOURCE_QUERY_BY_ID = ModuleNamConstant.SYS + FunctionalCategoryConstant.DATASOURCE + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 数据源更新接口
     */
    String DATASOURCE_UPDATE = ModuleNamConstant.SYS + FunctionalCategoryConstant.DATASOURCE + FunctionalNameConstant.UPDATE;
    /**
     * 数据源加密串解密
     */
    String DATASOURCE_DECRYPTION = ModuleNamConstant.SYS + FunctionalCategoryConstant.DATASOURCE + "decryption";

    /**
     * 在线开发-数据库表-查询
     */
    String DATABASE_TABLE_PAGE = ModuleNamConstant.ONL + FunctionalCategoryConstant.TABLE + FunctionalNameConstant.PAGE;
    /**
     * 在线开发-数据库表-详细查询
     */
    String DATABASE_TABLE_QUERY_BY_ID = ModuleNamConstant.ONL + FunctionalCategoryConstant.TABLE + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 在线开发-数据库表-新增
     */
    String DATABASE_TABLE_ADD = ModuleNamConstant.ONL + FunctionalCategoryConstant.TABLE + FunctionalNameConstant.ADD;
    /**
     * 在线开发-数据库表-更新
     */
    String DATABASE_TABLE_UPDATE = ModuleNamConstant.ONL + FunctionalCategoryConstant.TABLE + FunctionalNameConstant.UPDATE;
    /**
     * 在线开发-数据库表-复制
     */
    String DATABASE_TABLE_COPY = ModuleNamConstant.ONL + FunctionalCategoryConstant.TABLE + "copy";
    /**
     * 在线开发-数据库表-同步
     */
    String DATABASE_TABLE_SYNCHRONIZE = ModuleNamConstant.ONL + FunctionalCategoryConstant.TABLE + "synchronize";
    /**
     * 在线开发-数据库表-删除（删除数据库表所有信息）
     */
    String DATABASE_TABLE_DELETE = ModuleNamConstant.ONL + FunctionalCategoryConstant.TABLE + FunctionalNameConstant.DELETE;
    /**
     * 在线开发-数据库表-移除（删除数据库表，保留信息）
     */
    String DATABASE_TABLE_REMOVE = ModuleNamConstant.ONL + FunctionalCategoryConstant.TABLE + "remove";

    /**
     * 在线开发-数据库表数据-查询
     */
    String DATABASE_TABLE_DATA_QUERY_LIST_PAGE_ELEMENT = ModuleNamConstant.ONL + FunctionalCategoryConstant.DATA + "queryListPageElement";
    String DATABASE_TABLE_DATA_PAGE = ModuleNamConstant.ONL + FunctionalCategoryConstant.DATA + FunctionalNameConstant.PAGE;
    /**
     * 在线开发-数据库表数据-详细查询
     */
    String DATABASE_TABLE_DATA_QUERY_BY_ID = ModuleNamConstant.ONL + FunctionalCategoryConstant.DATA + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 在线开发-数据库表数据-新增
     */
    String DATABASE_TABLE_DATA_ADD_FIELD = ModuleNamConstant.ONL + FunctionalCategoryConstant.DATA + "queryDataAddField";
    String DATABASE_TABLE_DATA_ADD = ModuleNamConstant.ONL + FunctionalCategoryConstant.DATA + FunctionalNameConstant.ADD;
    /**
     * 在线开发-数据库表数据-更新
     */
    String DATABASE_TABLE_DATA_UPDATE_FIELD = ModuleNamConstant.ONL + FunctionalCategoryConstant.DATA + "queryDataUpdateField";
    String DATABASE_TABLE_DATA_UPDATE = ModuleNamConstant.ONL + FunctionalCategoryConstant.DATA + FunctionalNameConstant.UPDATE;
    /**
     * 在线开发-数据库表数据-删除
     */
    String DATABASE_TABLE_DATA_DELETE = ModuleNamConstant.ONL + FunctionalCategoryConstant.DATA + FunctionalNameConstant.DELETE;

    /**
     * 在线开发-表单按钮 - 分页查询
     */
    String FORM_BUTTON_PAGE = ModuleNamConstant.ONL + FunctionalCategoryConstant.BUTTON + FunctionalNameConstant.PAGE;
    /**
     * 在线开发-表单按钮 - 添加接口
     */
    String FORM_BUTTON_ADD = ModuleNamConstant.ONL + FunctionalCategoryConstant.BUTTON + FunctionalNameConstant.ADD;
    /**
     * 在线开发-表单按钮 - 删除接口
     */
    String FORM_BUTTON_DELETE = ModuleNamConstant.ONL + FunctionalCategoryConstant.BUTTON + FunctionalNameConstant.DELETE;
    /**
     * 在线开发-表单按钮 - 查看接口
     */
    String FORM_BUTTON_QUERY_BY_ID = ModuleNamConstant.ONL + FunctionalCategoryConstant.BUTTON + FunctionalNameConstant.QUERY_BY_ID;
    /**
     * 在线开发-表单按钮 - 更新接口
     */
    String FORM_BUTTON_UPDATE = ModuleNamConstant.ONL + FunctionalCategoryConstant.BUTTON + FunctionalNameConstant.UPDATE;

    /**
     * 在线开发-JavaScript 增强 - 分页查询
     */
    String JAVA_SCRIPT_PAGE = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA_SCRIPT + FunctionalNameConstant.PAGE;
    /**
     * 在线开发-JavaScript 增强 - 添加接口
     */
    String JAVA_SCRIPT_ADD = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA_SCRIPT + FunctionalNameConstant.ADD;
    /**
     * 在线开发-JavaScript 增强 - 删除接口
     */
    String JAVA_SCRIPT_DELETE = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA_SCRIPT + FunctionalNameConstant.DELETE;
    /**
     * 在线开发-JavaScript 增强 - 查看接口
     */
    String JAVA_SCRIPT_QUERY_BY_ID = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA_SCRIPT + FunctionalNameConstant.QUERY_BY_ID;
    String JAVA_SCRIPT_QUERY_BY_JAVA_SCRIPT_TYPE = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA_SCRIPT + "queryByJavaScriptType";
    /**
     * 在线开发-JavaScript 增强 - 更新接口
     */
    String JAVA_SCRIPT_UPDATE = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA_SCRIPT + FunctionalNameConstant.UPDATE;

    /**
     * 在线开发-Java 增强 - 分页查询
     */
    String JAVA_PAGE = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA + FunctionalNameConstant.PAGE;
    /**
     * 在线开发-Java 增强 - 添加接口
     */
    String JAVA_ADD = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA + FunctionalNameConstant.ADD;
    /**
     * 在线开发-Java 增强 - 删除接口
     */
    String JAVA_DELETE = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA + FunctionalNameConstant.DELETE;
    /**
     * 在线开发-Java 增强 - 查看接口
     */
    String JAVA_QUERY_BY_ID = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA + FunctionalNameConstant.QUERY_BY_ID;
    String JAVA_QUERY_BY_BUTTON_CODE = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA + "queryByButtonCode";
    /**
     * 在线开发-Java 增强 - 更新接口
     */
    String JAVA_UPDATE = ModuleNamConstant.ONL + FunctionalCategoryConstant.JAVA + FunctionalNameConstant.UPDATE;

    /**
     * 在线开发-StructuredQueryLanguage 增强 - 分页查询
     */
    String STRUCTURED_QUERY_LANGUAGE_PAGE = ModuleNamConstant.ONL + FunctionalCategoryConstant.STRUCTURED_QUERY_LANGUAGE + FunctionalNameConstant.PAGE;
    /**
     * 在线开发-StructuredQueryLanguage 增强 - 添加接口
     */
    String STRUCTURED_QUERY_LANGUAGE_ADD = ModuleNamConstant.ONL + FunctionalCategoryConstant.STRUCTURED_QUERY_LANGUAGE + FunctionalNameConstant.ADD;
    /**
     * 在线开发-StructuredQueryLanguage 增强 - 删除接口
     */
    String STRUCTURED_QUERY_LANGUAGE_DELETE = ModuleNamConstant.ONL + FunctionalCategoryConstant.STRUCTURED_QUERY_LANGUAGE + FunctionalNameConstant.DELETE;
    /**
     * 在线开发-StructuredQueryLanguage 增强 - 查看接口
     */
    String STRUCTURED_QUERY_LANGUAGE_QUERY_BY_ID = ModuleNamConstant.ONL + FunctionalCategoryConstant.STRUCTURED_QUERY_LANGUAGE + FunctionalNameConstant.QUERY_BY_ID;
    String STRUCTURED_QUERY_LANGUAGE_QUERY_BY_BUTTON_CODE = ModuleNamConstant.ONL + FunctionalCategoryConstant.STRUCTURED_QUERY_LANGUAGE + "queryByButtonCode";
    /**
     * 在线开发-StructuredQueryLanguage 增强 - 更新接口
     */
    String STRUCTURED_QUERY_LANGUAGE_UPDATE = ModuleNamConstant.ONL + FunctionalCategoryConstant.STRUCTURED_QUERY_LANGUAGE + FunctionalNameConstant.UPDATE;

}
