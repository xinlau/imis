package com.imis.base.constant.permissions;

import com.imis.base.constant.CommonConstant;

/**
 * <p>
 * ModuleNamConstant<br>
 * 模块名称
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月10日 11:31
 */
public interface ModuleNamConstant {

    /**
     * 系统基础模块 - sys
     */
    String SYS = "sys" + CommonConstant.COLON;
    /**
     * 在线开发模块 - onl
     */
    String ONL = "onl" + CommonConstant.COLON;

}
