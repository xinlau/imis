package com.imis.base.constant.permissions;

import com.imis.base.constant.CommonConstant;

/**
 * <p>
 * FunctionalCategoryConstant<br>
 * 功能类别
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月10日 11:31
 */
public interface FunctionalCategoryConstant {

    /**
     * 系统日志 - log
     */
    String LOG = "log" + CommonConstant.COLON;
    /**
     * 字典项 - dict
     */
    String DICT = "dict" + CommonConstant.COLON;
    /**
     * 字典值 - dictItem
     */
    String DICT_ITEM = "dictItem" + CommonConstant.COLON;
    /**
     * 系统用户 - user
     */
    String USER = "user" + CommonConstant.COLON;
    /**
     * 系统角色 - role
     */
    String ROLE = "role" + CommonConstant.COLON;
    /**
     * 功能菜单 - menu
     */
    String MENU = "menu" + CommonConstant.COLON;
    /**
     * 组织机构 - organization
     */
    String ORGANIZATION = "organization" + CommonConstant.COLON;
    /**
     * 定时任务 - quartz
     */
    String QUARTZ = "quartz" + CommonConstant.COLON;
    /**
     * WebSocket信息传递记录 - websocket
     */
    String WEBSOCKET = "websocket" + CommonConstant.COLON;
    /**
     * 数据源 - SysDataSource
     */
    String DATASOURCE = "datasource" + CommonConstant.COLON;
    /**
     * 在线开发-数据库表
     */
    String TABLE = "table" + CommonConstant.COLON;
    /**
     * 在线开发-数据库表数据
     */
    String DATA = "data" + CommonConstant.COLON;
    /**
     * 在线开发-表单按钮
     */
    String BUTTON = "button" + CommonConstant.COLON;
    /**
     * 在线开发-JavaScript 增强
     */
    String JAVA_SCRIPT = "js" + CommonConstant.COLON;
    /**
     * 在线开发-Java 增强
     */
    String JAVA = "java" + CommonConstant.COLON;
    /**
     * 在线开发-StructuredQueryLanguage 增强
     */
    String STRUCTURED_QUERY_LANGUAGE = "sql" + CommonConstant.COLON;

}
