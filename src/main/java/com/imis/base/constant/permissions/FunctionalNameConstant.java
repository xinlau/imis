package com.imis.base.constant.permissions;

/**
 * <p>
 * FunctionalNameConstant<br>
 * 功能名称
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月10日 11:31
 */
public interface FunctionalNameConstant {

    /**
     * 分页 - page
     */
    String PAGE = "page";
    /**
     * 添加 - add
     */
    String ADD = "add";
    /**
     * 删除 - delete
     */
    String DELETE = "delete";
    /**
     * 修改 - update
     */
    String UPDATE = "update";
    /**
     * 主键查询 - queryById
     */
    String QUERY_BY_ID = "queryById";
    /**
     * 树形 - tree
     */
    String TREE = "tree";

}
