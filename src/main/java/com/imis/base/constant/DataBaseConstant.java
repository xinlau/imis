package com.imis.base.constant;

import com.baomidou.mybatisplus.core.toolkit.StringPool;

/**
 * <p>
 * DataBaseConstant<br>
 * 数据库上下文常量
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public interface DataBaseConstant extends StringPool {
    /**
     * 数据源名称 - master
     */
    String DATA_SOURCE_MASTER = "master";
    /**
     * 数据源名称 - record
     */
    String DATA_SOURCE_RECORD = "record";
    /**
     * 数据源名称 - business
     */
    String DATA_SOURCE_BUSINESS = "business";

    /**
     * 数据库类型
     */
    String DB_TYPE_MYSQL = "MYSQL";
    String MYSQL = "mysql";
    String DB_TYPE_ORACLE = "ORACLE";
    String ORACLE = "oracle";
    String DB_TYPE_POSTGRE_SQL = "POSTGRESQL";
    String POSTGRE_SQL = "postgresql";
    String DB_TYPE_SQL_SERVER = "SQLSERVER";
    String SQLSERVER = "sqlserver";
    String SQL_SERVER = "sql server";

    //*********系统建表标准字段****************************************

    /**
     * 主键字段
     */
    String P_KEY_FIELD = "ID";
    String P_KEY = "id";
    /**
     * 创建人字段
     */
    String CREATE_BY_FIELD = "CREATE_BY";
    String CREATE_BY = "createBy";
    /**
     * 创建时间字段
     */
    String CREATE_TIME_FIELD = "CREATE_TIME";
    String CREATE_TIME = "createTime";
    /**
     * 修改人字段
     */
    String UPDATE_BY_FIELD = "UPDATE_BY";
    String UPDATE_BY = "updateBy";
    /**
     * 修改时间字段
     */
    String UPDATE_TIME_FIELD = "UPDATE_TIME";
    String UPDATE_TIME = "updateTime";
    /**
     * 组织机构编号字段
     */
    String ORGANIZATION_CODE_FIELD = "ORGANIZATION_CODE";
    String ORGANIZATION_CODE = "organizationCode";
    /**
     * 字典编码字段
     */
    String DICT_CODE_FIELD = "DICT_CODE";
    String DICT_CODE = "dictCode";
    /**
     * 流程状态字段
     */
    String PROCESS_STATE_FIELD = "PROCESS_STATE";
    String PROCESS_STATE = "processState";

    /**
     * 高级查询前端传来的参数名
     */
    String SUPER_QUERY_PARAMS = "superQueryParams";
    /**
     * 高级查询前端传来的拼接方式参数名
     */
    String SUPER_QUERY_MATCH_TYPE = "superQueryMatchType";
    /**
     * 逻辑删除标志 delFlag
     */
    String DEL_FLAG = "delFlag";

    /**
     * 字段常量
     */
    String CHILD_TABLE_NAME = "childTableName";
    String CHILD_TABLE_FIELD = "childTableField";
    String MAIN_TABLE_NAME = "mainTableName";
    String MAIN_TABLE_FIELD = "mainTableField";

    //*********系统建表字段类型****************************************

    String BIG_DECIMAL = "BigDecimal";
    String BIG_DECIMAL_ = "bigdecimal";
    String DECIMAL = "decimal";

    String BLOB = "Blob";
    String BLOB_ = "BLOB";

    String LONG = "long";
    String BIGINT_ = "bigint";

    String FLOAT_ = "float";

    String DATE = "Date";
    String DATE_ = "date";

    String DATE_TIME = "Datetime";
    String DATE_TIME_ = "datetime";
    String TIMESTAMP = "Timestamp";
    String TIMESTAMP_ = "TIMESTAMP";

    String BYTEA = "bytea";
    String BYTEA_ = "BYTEA";

    String NUMERIC = "numeric";
    String NUMERIC_ = "NUMERIC";

    String DOUBLE = "double";

    String INT = "int";
    String INTEGER = "Integer";

    String NUMBER = "number";
    String NUMBER_ = "NUMBER";

    String STRING = "string";

    String TEXT = "Text";
    String TEXT_ = "text";
    String CLOB = "clob";
    String CLOB_ = "CLOB";

    String CHARACTER_VARYING = "character varying";

    String NVARCHAR2 = "nvarchar2";

    String NVARCHAR = "nvarchar";

    String INT4 = "int4";

    String NTEXT = "ntext";

    String IMAGE = "image";

    String VARBINARY = "varbinary";

    String TEXTAREA = "textarea";

    String VARCHAR = "varchar";
    String VARCHAR2 = "varchar2";

    /**
     * 强制同步
     */
    String FORCE_SYNCHRONIZATION = "force";
    /**
     * 正常同步
     */
    String NORMAL_SYNCHRONIZATION = "normal";

    String SQL_SELECT = "SELECT ";
    String SQL_INSERT_INTO = "INSERT INTO ";
    String SQL_UPDATE = "UPDATE ";
    String SQL_CREATE = "CREATE ";
    String SQL_DELETE = "DELETE ";
    String SQL_COUNT = " COUNT(*) ";
    String SQL_VALUES = " VALUES ";
    String SQL_SET = " SET ";
    String SQL_FROM = " FROM ";
    String SQL_WHERE = " WHERE ";
    String SQL_WHERE_TRUE = " WHERE 1=1  ";
    String SQL_AND = " AND ";
    String SQL_IN = " IN ";
    String SQL_NULL = " NULL ";
    String SQL_NOT_NULL = " NOT NULL ";
    String SQL_LIKE = " LIKE ";
    String SQL_ON = SPACE + ON + SPACE;
    String SQL_ORDER = " ORDER BY ";
    String SQL_ASC = "ASC";
    String SQL_DESC = "DESC";
    String SQL_INDEX = SPACE + "INDEX" + SPACE;
    String SQL_ADD = SPACE + "ADD" + SPACE;
    String SQL_RENAME = SPACE + "RENAME" + SPACE;
    String SQL_CHANGE = SPACE + "CHANGE" + SPACE;
    String SQL_MODIFY = SPACE + "MODIFY" + SPACE;
    String SQL_ALTER = SPACE + "ALTER" + SPACE;
    String SQL_DROP = SPACE + "DROP" + SPACE;
    String SQL_COLUMN = SPACE + "COLUMN" + SPACE;
    String SQL_TABLE = SPACE + "TABLE" + SPACE;
    String SQL_IF = SPACE + "IF" + SPACE;
    String SQL_EXISTS = SPACE + "EXISTS" + SPACE;
    String SQL_COMMENT = SPACE + "COMMENT" + SPACE;
    String SQL_DEFAULT = SPACE + "DEFAULT" + SPACE;
    String SQL_IS = SPACE + "IS" + SPACE;
    String SQL_TO = SPACE + "TO" + SPACE;
    String SQL_TYPE = SPACE + "TYPE" + SPACE;
    String SQL_SET_DEFAULT = SQL_SET + SQL_DEFAULT;
    String SQL_DROP_DEFAULT = SQL_DROP + SQL_DEFAULT;

    /**
     * 不等于
     */
    String SQL_NOT_EQUAL_TO = DataBaseConstant.EXCLAMATION_MARK + DataBaseConstant.EQUALS;
    /**
     * 小于等于
     */
    String LESS_THAN_OR_EQUAL_TO = DataBaseConstant.LEFT_CHEV + DataBaseConstant.EQUALS;
    /**
     * 大于等于
     */
    String GREATER_THAN_OR_EQUAL_TO = DataBaseConstant.RIGHT_CHEV + DataBaseConstant.EQUALS;

    /**
     * 查询模式 普通查询
     */
    String QUERY_MODE_SINGLE = "single";

    /**
     * 字段的后缀
     */
    String SUFFIX_START = "Start";
    String SUFFIX_END = "End";

}
