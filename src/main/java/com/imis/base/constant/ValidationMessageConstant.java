package com.imis.base.constant;

/**
 * <p>
 * ValidationMessageConstant<br>
 * 验证消息常量
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月16日 15:26
 */
public interface ValidationMessageConstant {

    String EMPTY_ERR = "字段不能为空";

    String FORMAT_ERR = "字段格式不正确";

}
