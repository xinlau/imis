package com.imis.base.constant;

/**
 * <p>
 * ControlTypeConstant<br>
 * 控件类型常量
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月22日 16:41
 */
public interface ControlTypeConstant {

    /**
     * 控件类型 - 复选框
     */
    String CHECK_BOX = "checkbox";
    /**
     * 控件类型 - 文本输入框
     */
    String INPUT = "input";
    /**
     * 控件类型 - 隐藏
     */
    String HIDDEN = "hidden";
    /**
     * 控件类型 - 数字输入框
     */
    String INPUT_NUMBER = "inputNumber";
    /**
     * 控件类型 - 日期选择框
     */
    String DATE = "date";
    /**
     * 控件类型 - 日期选择框
     */
    String DATETIME = "datetime";
    /**
     * 控件类型 - 选择框
     */
    String SELECT = "select";
    /**
     * 控件类型 - 开关
     */
    String SWITCH = "switch";
    /**
     * 控件类型 - POPUP
     */
    String POPUP = "popup";
    /**
     * 控件类型 - image
     */
    String IMAGE = "image";
    /**
     * 控件类型 - 自定义树 tree
     */
    String SELECT_TREE = "select_tree";
    /**
     * 控件类型 - 分类字典树控件
     */
    String DICT_TREE = "dict_tree";
    /**
     * 控件类型 - 级联下拉控件
     */
    String LINK_DOWN = "link_down";
    /**
     * 控件类型 - 表单搜索控件
     */
    String SELECT_SEARCH = "select_search";

}
