package com.imis.base.constant.enums;

/**
 * <p>
 * 重复操作锁定类型
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public enum RepeatOperationLockEnum {

    /**
     * 参数
     */
    PARAMETER,
    /**
     * 基于方法与参数
     */
    METHOD,
    /**
     * 基于IP与参数
     */
    IP,
    /**
     * 基于Token与参数
     */
    TOKEN

}
