package com.imis.base.constant.enums;

import com.imis.base.util.ConvertUtils;
import lombok.Getter;

/**
 * <p>
 * QueryRuleEnum<br>
 * Query 规则 常量
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
public enum QueryRuleEnum {

    /**
     * 大于
     */
    GT(">", "gt", "大于"),
    /**
     * 大于等于
     */
    GE(">=", "ge", "大于等于"),
    /**
     * 小于
     */
    LT("<", "lt", "小于"),
    /**
     * 小于等于
     */
    LE("<=", "le", "小于等于"),
    /**
     * 等于
     */
    EQ("=", "eq", "等于"),
    /**
     * 不等于
     */
    NE("!=", "ne", "不等于"),
    /**
     * 包含
     */
    IN("IN", "in", "包含"),
    /**
     * 全模糊
     */
    LIKE("LIKE", "like", "全模糊"),
    /**
     * 左模糊
     */
    LEFT_LIKE("LEFT_LIKE", "left_like", "左模糊"),
    /**
     * 右模糊
     */
    RIGHT_LIKE("RIGHT_LIKE", "right_like", "右模糊"),
    /**
     * 自定义SQL片段
     */
    SQL_FRAGMENT("USER_SQL_FRAGMENT", "ext", "自定义SQL片段");

    @Getter
    private String value;

    @Getter
    private String condition;

    @Getter
    private String message;

    QueryRuleEnum(String value, String condition, String message) {
        this.value = value;
        this.condition = condition;
        this.message = message;
    }

    public static QueryRuleEnum getByValue(String value) {
        if (ConvertUtils.isEmpty(value)) {
            return null;
        }
        for (QueryRuleEnum val : values()) {
            if (val.getValue().equals(value) || val.getCondition().equals(value)) {
                return val;
            }
        }
        return null;
    }
}
