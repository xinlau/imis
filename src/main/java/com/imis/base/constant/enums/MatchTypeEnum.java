package com.imis.base.constant.enums;

import com.imis.base.util.ConvertUtils;
import lombok.Getter;

/**
 * <p>
 * MatchTypeEnum<br>
 * 查询链接规则枚举类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public enum MatchTypeEnum {

    /**
     * AND
     */
    AND("AND"),
    /**
     * OR
     */
    OR("OR");

    @Getter
    private final String value;

    MatchTypeEnum(String value) {
        this.value = value;
    }

    public static MatchTypeEnum getByValue(Object value) {
        if (ConvertUtils.isEmpty(value)) {
            return null;
        }
        return getByValue(value.toString());
    }

    public static MatchTypeEnum getByValue(String value) {
        if (ConvertUtils.isEmpty(value)) {
            return null;
        }
        for (MatchTypeEnum val : values()) {
            if (val.getValue().toLowerCase().equals(value.toLowerCase())) {
                return val;
            }
        }
        return null;
    }
}
