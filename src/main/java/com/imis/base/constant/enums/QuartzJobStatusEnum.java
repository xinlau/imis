package com.imis.base.constant.enums;

import lombok.Getter;

/**
 * <p>
 * QuartzJobStatusEnum<br>
 * 定时任务 状态枚举类
 * NONE, NORMAL, PAUSED, COMPLETE, ERROR, BLOCKED
 * </p>
 *
 * @author XinLau
 * @since 2020-06-15
 */
public enum QuartzJobStatusEnum {

    /**
     * 不存在
     */
    NONE(1, "NONE"),
    /**
     * 正常
     */
    NORMAL(2, "NORMAL"),
    /**
     * 暂停
     */
    PAUSED(3, "PAUSED"),
    /**
     * 运行
     */
    RUNNING(4, "RUNNING"),
    /**
     * 完成
     */
    COMPLETE(5, "COMPLETE"),
    /**
     * 出错
     */
    ERROR(6, "ERROR"),
    /**
     * 阻塞
     */
    BLOCKED(7, "BLOCKED");

    /**
     * 参数键
     */
    @Getter
    private final Integer key;

    /**
     * 定时任务响应码
     */
    @Getter
    private final String status;

    /**
     * 定时任务 状态枚举类
     *
     * @param key    - 参数键
     * @param status - 定时任务响应码
     */
    QuartzJobStatusEnum(Integer key, String status) {
        this.key = key;
        this.status = status;
    }

}
