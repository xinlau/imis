package com.imis.base.constant.enums;

import lombok.Getter;

/**
 * <p>
 * FormValidPatternEnum<br>
 * 表单校验枚举
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public enum FormValidPatternEnum {

    /**
     * 唯一
     */
    ONLY("only", "only", ""),
    /**
     * 6-16位的数字
     */
    NUM6_16("n6-16", "^\\d{6,18}$", "请输入6-16位的数字"),
    /**
     * 6-16位任意字符
     */
    STRING6_16("*6-16", "^.{6,16}$", "请输入6-16位任意字符"),
    /**
     * 6-18位字母
     */
    LETTER6_18("s6-18", "^[a-z|A-Z]{6,18}$", "请输入6-18位字母"),
    /**
     * 网址
     */
    URL("url", "^((ht|f)tps?):\\/\\/[\\w\\-]+(\\.[\\w\\-]+)+([\\w\\-.,@?^=%&:\\/~+#]*[\\w\\-@?^=%&\\/~+#])?$", "请输入正规的网址"),
    /**
     * 手机号码
     */
    MOBILE("m", "^1[3456789]\\d{9}$", "请输入正规的手机号码"),
    /**
     * 邮政编码
     */
    POSTAL("p", "^[1-9]\\d{5}$", "请输入正规的邮政编码"),
    /**
     * 字母
     */
    LETTER("s", "[A-Z|a-z]+$", "请输入字母"),
    /**
     * 数字，数字包含正整数、负整数、正小数、负小数、零
     */
    NUMBER("n", "^-?\\d+(\\.?\\d+|\\d?)$", "请输入数字"),
    /**
     * 整数 （整数在前端判断，这里不用写正则）
     */
    INTEGER("z", "z", "请输入整数"),
    /**
     * 非空
     */
    NOTNULL("*", "^.+$", "该字段不能为空"),
    /**
     * 邮箱地址
     */
    EMAIL("e", "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$", "请输入正确格式的邮箱地址"),
    /**
     * 金额
     */
    MONEY("money", "^(([1-9][0-9]*)|([0]\\.\\d{0,2}|[1-9][0-9]*\\.\\d{0,5}))$", "请输入正确的金额");


    @Getter
    private final String type;

    @Getter
    private final String pattern;

    @Getter
    private final String message;

    FormValidPatternEnum(String type, String pattern, String message) {
        this.pattern = pattern;
        this.message = message;
        this.type = type;
    }

    public static FormValidPatternEnum getPatternInfoByType(String type) {
        for (FormValidPatternEnum e : FormValidPatternEnum.values()) {
            if (e.type.equals(type)) {
                return e;
            }
        }
        return null;
    }

}
