package com.imis.base.constant.enums;

/**
 * <p>
 * ButtonCodeEnum<br>
 * 按钮代码枚举类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public enum ButtonCodeEnum {

    /**
     * delete
     */
    DELETE("delete"),
    /**
     * add
     */
    ADD("add"),
    /**
     * edit
     */
    EDIT("edit"),

    ;

    /**
     * 数据源名称
     */
    private final String value;

    /**
     *
     * @param value - 数据源名称
     */
    ButtonCodeEnum(String value){
        this.value = value;
    }

    /**
     * 获取数据源名称
     * @return String
     */
    public String getValue() {
        return value;
    }
}
