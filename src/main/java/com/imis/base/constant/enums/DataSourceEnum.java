package com.imis.base.constant.enums;

/**
 * <p>
 * DataSourceEnum<br>
 * 数据源枚举类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public enum DataSourceEnum {

    /**
     * master
     */
    DB_MASTER("master"),
    /**
     * record
     */
    DB_RECORD("record");

    /**
     * 数据源名称
     */
    private final String value;

    /**
     *
     * @param value - 数据源名称
     */
    DataSourceEnum(String value){
        this.value = value;
    }

    /**
     * 获取数据源名称
     * @return String
     */
    public String getValue() {
        return value;
    }
}
