package com.imis.base.constant.enums;

/**
 * <p>
 * BrowserTypeEnum<br>
 * 浏览器类型枚举类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public enum BrowserTypeEnum {
    /**
     * 浏览器类型
     */
    IE11, IE10, IE9, IE8, IE7, IE6, Firefox, Safari, Chrome, Opera, Camino, Gecko
}
