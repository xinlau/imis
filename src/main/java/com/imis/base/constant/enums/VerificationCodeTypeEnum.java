package com.imis.base.constant.enums;

/**
 * <p>
 * 验证码类型
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public enum VerificationCodeTypeEnum {

    /**
     * 字符验证码
     */
    CHAR,
    /**
     * 运算验证码
     */
    OPERATION,
    /**
     * 滑动验证码
     */
    SLIDE,
    /**
     * 邮箱验证码
     */
    EMAIL,
    /**
     * 短信验证码
     */
    SMS
}
