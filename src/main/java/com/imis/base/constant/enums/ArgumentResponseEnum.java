package com.imis.base.constant.enums;


import com.imis.base.globle.CommonExceptionAssert;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>
 * ArgumentResponseEnum<br>
 * 参数校验异常返回结果<br>
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2021年01月20日 13:24
 */
@Getter
@AllArgsConstructor
public enum ArgumentResponseEnum implements CommonExceptionAssert {

    /**
     * 200 - 逻辑错误类型   00 - 模块   01 - 功能   00 - 错误码
     */
    COMMON_EXCEPTION(200000000, "参数校验异常"),
    NULL_POINTER_EXCEPTION(200000001, "空指针异常"),

    /**
     * 日志分页查询 2000001
     */
    LOG_PAGE_QUERY_OK(200000100, "创建系统日志分页信息成功"),
    LOG_PAGE_QUERY_ERR(200000101, "创建系统日志分页信息错误，错误信息：%s"),

    /**
     * 用户注册     2000101
     */
    USER_REGISTER_OK(200010100, "用户注册成功 %s"),
    USER_REGISTER_ERR(200010101, "用户注册失败"),
    USER_REGISTER_ERR_REPEAT(200010102, "用户注册失败，%s 已存在，请确认信息后重新注册"),
    USER_REGISTER_ERR_FREEZE(200010103, "用户注册失败，%s 已冻结，请联系管理员"),
    USER_REGISTER_ERR_DELETE(200010104, "用户注册失败，%s 已注销，请联系管理员"),
    /**
     * 用户登录     2000102
     */
    USER_LOGIN_OK(200010200, "用户登录成功，用户名：%s"),
    USER_LOGIN_ERR(200010201, "用户名或密码错误，用户名：%s"),
    USER_LOGIN_ERR_NON(200010202, "该用户不存在"),
    USER_LOGIN_ERR_FREEZE(200010203, "该用户已冻结"),
    USER_LOGIN_ERR_DELETE(200010204, "该用户已注销"),
    USER_LOGIN_ERR_PHONE_NON(200010206, "该用户不存在"),
    /**
     * 用户退出登录   2000103
     * 用户信息分页查询 2000104
     */
    USER_PAGE_QUERY_OK(200010400, "创建用户信息分页信息成功"),
    USER_PAGE_QUERY_ERR(200010401, "创建用户信息分页信息错误，错误信息：%s"),
    /**
     * 用户添加     2000105
     */
    USER_ADD_OK(200010500, "用户添加成功，用户名：%s"),
    USER_ADD_ERR(200010501, "用户添加失败，用户名：%s，请确认信息后重新添加"),
    USER_ADD_ERR_REPEAT(200010502, "用户添加失败，%s 已存在，请确认信息后重新添加"),
    USER_ADD_ERR_FREEZE(200010503, "用户添加失败，%s 已冻结，请确认信息后重新添加"),
    USER_ADD_ERR_DELETE(200010504, "用户添加失败，%s 已注销，请确认信息后重新添加"),
    /**
     * 用户冻结     2000106
     */
    USER_FREEZE_OK(200010600, "用户冻结成功，用户标识：%s，操作人编号：%s"),
    USER_FREEZE_ERR(200010601, "用户冻结失败，用户标识：%s ，错误信息：%s"),
    USER_FREEZE_ERR_USER_NULL(200010602, "用户冻结失败，用户不存在，用户标识：%s"),
    USER_FREEZE_ERR_REDIS_NULL(200010603, "用户冻结失败，获取用户缓存失败，用户标识：%s"),
    /**
     * 用户解冻     2000107
     */
    USER_UNFREEZE_OK(200010700, "用户解冻成功，用户标识：%s"),
    USER_UNFREEZE_ERR(200010701, "用户解结失败，用户标识：%s"),
    /**
     * 用户信息更新   2000108
     */
    USER_QUERY_ERR(200010800, "用户信息查看失败，用户标识：%s"),
    USER_UPDATE_OK(200010800, "用户信息更新成功，用户标识：%s"),
    USER_UPDATE_ERR(200010801, "用户信息更新失败，用户标识：%s"),
    USER_UPDATE_ERR_NON(200010802, "用户信息更新失败，用户信息不存在"),
    USER_PHONE_UPDATE_ERR(200010803, "密保手机更新失败"),
    USER_PHONE_UPDATE_ERR_NON(200010804, "密保手机更新失败，用户信息不存在"),
    USER_EMAIL_UPDATE_ERR(200010805, "电子邮箱更新失败"),
    USER_EMAIL_UPDATE_ERR_NON(200010806, "电子邮箱更新失败，用户信息不存在"),

    /**
     * 角色添加     2000201
     */
    ROLE_ADD_OK(200020100, "角色添加成功, 角色名：%s"),
    ROLE_ADD_ERR(200020101, "角色添加失败 角色名：%s"),
    ROLE_ADD_ERR_NAME_REPEAT(200020102, "角色添加失败，角色名 %s 存在"),
    ROLE_ADD_ERR_CODE_REPEAT(200020103, "角色添加失败，角色编码 %s 存在"),
    /**
     * 角色删除     2000202
     */
    ROLE_DELETE_OK(200020200, "角色删除成功, 角色名：%s"),
    ROLE_DELETE_ERR(200020201, "角色删除失败，角色名：%s"),
    ROLE_DELETE_ERR_NON(200020202, "角色删除失败，角色不存在"),
    ROLE_DELETE_ERR_ASSIGNED(200020203, "角色删除失败，%s 角色已分配给 %s"),
    ROLE_DELETE_ERR_PERMISSIONS(200020204, "清除角色分配的权限失败，角色名：%s"),
    /**
     * 角色更新     2000203
     */
    ROLE_QUERY_ERR(200020301, "角色查看失败"),
    ROLE_UPDATE_OK(200020300, "更新角色成功，角色名：%s"),
    ROLE_UPDATE_ERR(200020301, "更新角色失败，角色名：%s"),
    ROLE_UPDATE_ERR_NON(200020302, "更新角色失败，角色 %s 不存在"),
    ROLE_UPDATE_ERR_NAME_REPEAT(200020303, "更新角色失败，角色名 %s 存在"),
    ROLE_UPDATE_ERR_CODE_REPEAT(200020304, "更新角色失败，角色编码 %s 存在"),
    /**
     * 角色信息分页查询 2000204
     */
    ROLE_PAGE_QUERY_OK(200020400, "创建角色信息分页信息成功"),
    ROLE_PAGE_QUERY_ERR(200020401, "创建角色信息分页信息错误，错误信息：%s"),
    GRANT_ROLE_PAGE_QUERY_ERR(200020402, "创建已授权角色信息分页信息错误，错误信息：%s"),
    NOT_GRANT_ROLE_PAGE_QUERY_ERR(200020403, "创建未授权角色信息分页信息错误，错误信息：%s"),

    /**
     * 功能菜单添加     2000301
     */
    MENU_ADD_OK(200030100, "功能菜单添加成功, 功能菜单名：%s"),
    MENU_ADD_ERR(200030101, "功能菜单添加失败 功能菜单名：%s"),
    MENU_ADD_ERR_REPEAT(200030102, "功能菜单添加失败，功能菜单 %s 存在"),
    MENU_ADD_ERR_DELETE(200030103, "功能菜单添加失败，功能菜单 %s 已删除"),
    MENU_ADD_ERR_PARENT_NON(200030104, "功能菜单添加失败，父级功能菜单不存在"),
    MENU_ADD_ERR_PARENT_DELETE(200030105, "功能菜单添加失败，编号 %s 的父级功能菜单已删除"),
    /**
     * 功能菜单删除     2000302
     */
    MENU_DELETE_OK(200030200, "功能菜单删除成功, 功能菜单名：%s"),
    MENU_DELETE_ERR(200030201, "功能菜单删除失败，功能菜单名：%s"),
    MENU_DELETE_ERR_NON(200030202, "功能菜单删除失败，功能菜单不存在"),
    MENU_DELETE_ERR_ASSIGNED(200030203, "功能菜单删除失败，%s 功能菜单已分配给 %s"),
    MENU_DELETE_ERR_HAVE_CHILD(200030204, "功能菜单删除失败，%s 功能菜单是父级功能菜单"),
    /**
     * 功能菜单更新     2000303
     */
    MENU_UPDATE_OK(200030300, "更新功能菜单成功，功能菜单名：%s"),
    MENU_UPDATE_ERR(200030301, "更新功能菜单失败，功能菜单名：%s"),
    MENU_UPDATE_ERR_NON(200030302, "更新功能菜单失败，功能菜单 %s 不存在"),
    MENU_UPDATE_ERR_REPEAT(200030304, "更新功能菜单失败，功能菜单 %s 存在"),
    /**
     * 功能菜单信息分页查询 2000304
     */
    MENU_PAGE_QUERY_OK(200030400, "创建功能菜单信息分页信息成功"),
    MENU_PAGE_QUERY_ERR(200030401, "创建功能菜单信息分页信息错误，错误信息：%s"),
    /**
     * 查询功能菜单树 2000305
     */
    MENU_QUERY_ERR(200030500, "查询功能菜单信息失败"),
    MENU_TREE_QUERY_OK(200030501, "查询功能菜单树信息成功"),
    MENU_TREE_QUERY_ERR(200030502, "查询功能菜单树信息错误，错误信息：%s"),
    MENU_TREE_QUERY_ERR_NON_USER(200030503, "查询功能菜单树信息错误，非法的用户"),
    MENU_TREE_QUERY_ERR_NON_ROLE(200030504, "查询功能菜单树信息错误，%s 未分配角色"),
    MENU_TREE_QUERY_ERR_ROLE_NON(200030505, "查询功能菜单树信息错误，非法的角色"),

    /**
     * 用户授权角色 2000401   authorizingRoleToUsers
     */
    GRANT_ROLES_TO_USER_OK(200040100, "为用户 %s 授权角色成功"),
    GRANT_ROLES_TO_USER_ERR(200040101, "为用户 %s 授权角色失败"),
    GRANT_ROLES_TO_USER_ERR_NON_USER(200040102, "授权角色失败，用户编号 %s 不存在"),
    GRANT_ROLES_TO_USER_ERR_NON_ROLE(200040103, "授权角色失败，角色编号 %s 不存在"),
    AUTHORIZING_ROLE_TO_USERS_OK(200040104, "为角色 %s 授权用户成功"),
    AUTHORIZING_ROLE_TO_USERS_ERR(200040105, "为角色 %s 授权用户失败"),
    AUTHORIZING_ROLE_TO_USERS_ERR_NON_USER(200040106, "授权角色失败，用户编号 %s 不存在"),
    AUTHORIZING_ROLE_TO_USERS_ERR_NON_ROLE(200040107, "授权角色失败，角色编号 %s 不存在"),
    UN_AUTHORIZING_ROLE_TO_USERS_OK(200040108, "取消角色 %s 授权用户成功"),
    UN_AUTHORIZING_ROLE_TO_USERS_ERR(200040109, "取消角色 %s 授权用户失败"),
    UN_AUTHORIZING_ROLE_TO_USERS_ERR_NON_USER(200040110, "取消授权角色失败，用户编号 %s 不存在"),
    UN_AUTHORIZING_ROLE_TO_USERS_ERR_NON_ROLE(200040111, "取消授权角色失败，角色编号 %s 不存在"),
    /**
     * 撤销用户角色授权 2000402
     */
    TERMINATE_ALL_ROLE_TO_USER_OK(200040200, "为用户 %s 撤销角色授权成功"),
    TERMINATE_ALL_ROLE_TO_USER_ERR(200040201, "为用户 %s 撤销角色授权失败"),
    TERMINATE_ALL_ROLE_TO_USER_ERR_NON_USER(200040202, "用户编号 %s 不存在"),
    /**
     * 角色授权功能菜单 2000403
     */
    GRANT_MENU_TO_ROLE_OK(200040300, "为角色 %s 授权功能菜单成功"),
    GRANT_MENU_TO_ROLE_ERR(200040301, "为角色 %s 授权功能菜单失败"),
    GRANT_MENU_TO_ROLE_ERR_NON_ROLE(200040302, "角色编号 %s 不存在"),
    GRANT_MENU_TO_ROLE_ERR_NON_MENU(200040303, "功能菜单编号 %s 不存在"),
    /**
     * 撤销角色功能菜单授权 2000404
     */
    TERMINATE_ALL_MENU_TO_ROLE_OK(200040400, "为角色 %s 撤销功能菜单授权成功"),
    TERMINATE_ALL_MENU_TO_ROLE_ERR(200040401, "为角色 %s 撤销功能菜单授权失败"),
    TERMINATE_ALL_MENU_TO_ROLE_ERR_NON_ROLE(200040402, "角色编号 %s 不存在"),
    /**
     * 修改用户密码 2000405
     */
    CHANGE_USER_PASSWORD_OK(200040500, "用户 %s 密码修改成功"),
    CHANGE_USER_PASSWORD_ERR(200040501, "用户 %s 密码修改失败"),
    CHANGE_USER_PASSWORD_ERR_NON_USER(200040502, "密码修改失败，用户 %s 不存在"),
    CHANGE_USER_PASSWORD_OK_REPEAT_PASSWORD(200040503, "密码修改成功，新密码与旧密码一致"),

    /**
     * 文件上传 2000501
     */
    FILE_ADD_OK(200050100, "文件上传成功"),
    FILE_ADD_ERR(200050101, "%s 上传失败"),
    /**
     * 文件下载 2000502
     */
    FILE_DOWNLOAD_OK(200050200, "文件下载成功"),
    FILE_DOWNLOAD_ERR(200050201, "文件下载失败，文件不存在"),
    FILE_DOWNLOAD_ERR_IO(200050202, "文件下载失败 %s"),
    FILE_ONLINE_PREVIEW_ERR(200050203, "文件预览失败"),
    /**
     * 验证码相关 2000503
     */
    VERIFICATION_CODE_ERR(200050301, "验证码获取失败"),
    VERIFICATION_CODE_ERR_FAIL(200050302, "验证码验证失败"),
    VERIFICATION_CODE_ERR_INVALID(200050303, "验证码无效"),
    /**
     * 短信相关 2000504
     */
    SEND_SMS_OK(200050400, "短信发送成功，请注意查收"),
    SEND_SMS_ERR(200050401, "短信发送失败，请联系管理员"),
    SEND_SMS_ERR_REPEAT(200050402, "短信发送失败，手机号 % 的验证码5分钟内仍然有效！"),
    SEND_SMS_ERR_API(200050403, "请求失败，请求返回错误信息： %s （具体详情请参考：https://help.aliyun.com/document_detail/101346.html?spm=a2c4g.11186623.2.14.282556e0F44gYZ）"),
    /**
     * 邮件相关 2000505
     */
    SEND_EMAIL_OK(200050500, "邮件发送成功，请注意查收"),
    SEND_EMAIL_ERR(200050501, "邮件发送失败，请联系管理员"),

    /**
     * 组织机构添加     2000601
     */
    ORGANIZATION_ADD_OK(200060100, "组织机构添加成功, 组织机构名：%s"),
    ORGANIZATION_ADD_ERR(200060101, "组织机构添加失败 组织机构名：%s"),
    ORGANIZATION_ADD_ERR_REPEAT(200060102, "组织机构添加失败，组织机构 %s 存在"),
    ORGANIZATION_ADD_ERR_DELETE(200060103, "组织机构添加失败，组织机构 %s 已删除"),
    ORGANIZATION_ADD_ERR_PARENT_NON(200060104, "组织机构添加失败，父级组织机构不存在"),
    ORGANIZATION_ADD_ERR_PARENT_DELETE(200060105, "组织机构添加失败，编号 %s 的父级组织机构已删除"),
    ORGANIZATION_ADD_ERR_CODE_NON(200060106, "组织机构添加失败，组织机构编号生成失败"),
    /**
     * 组织机构删除     2000602
     */
    ORGANIZATION_DELETE_OK(200060200, "组织机构删除成功, 组织机构名：%s"),
    ORGANIZATION_DELETE_ERR(200060201, "组织机构删除失败，组织机构名：%s"),
    ORGANIZATION_DELETE_ERR_NON(200060202, "组织机构删除失败，组织机构不存在"),
    ORGANIZATION_DELETE_ERR_ASSIGNED(200060203, "组织机构删除失败，%s 组织机构已分配给 %s"),
    ORGANIZATION_DELETE_ERR_HAVE_CHILD(200060204, "组织机构删除失败，%s 组织机构是父级组织机构"),
    /**
     * 组织机构更新     2000603
     */
    ORGANIZATION_UPDATE_OK(200060300, "更新组织机构成功，组织机构名：%s"),
    ORGANIZATION_UPDATE_ERR(200060301, "更新组织机构失败，组织机构名：%s"),
    ORGANIZATION_UPDATE_ERR_NON(200060302, "更新组织机构失败，组织机构 %s 不存在"),
    ORGANIZATION_UPDATE_ERR_REPEAT(200060304, "更新组织机构失败，组织机构 %s 存在"),
    /**
     * 组织机构信息分页查询 2000604
     */
    ORGANIZATION_PAGE_QUERY_OK(200060400, "创建组织机构信息分页信息成功"),
    ORGANIZATION_PAGE_QUERY_ERR(200060401, "创建组织机构信息分页信息错误，错误信息：%s"),
    /**
     * 查询组织机构树 2000605
     */
    ORGANIZATION_TREE_QUERY_OK(200060500, "查询组织机构树信息成功"),
    ORGANIZATION_TREE_QUERY_ERR(200060501, "查询组织机构树信息错误，错误信息：%s"),
    ORGANIZATION_TREE_QUERY_ERR_NON_PARENT(200060502, "查询组织机构树信息错误，编号 %s 的父级组织机构不存在"),
    ORGANIZATION_TREE_QUERY_ERR_NON_USER(200060503, "查询组织机构树信息错误， %s 非法的用户"),
    ORGANIZATION_TREE_QUERY_ERR_NON_ROLE(200060504, "查询组织机构树信息错误，%s 未分配角色"),
    /**
     * 查询组织机构树 2000606
     */
    ORGANIZATION_QUERY_OK(200060600, "查询组织机构树信息成功"),
    ORGANIZATION_QUERY_ERR(200060601, "查询组织机构树信息错误"),

    /**
     * 用户授权组织机构 2000701
     */
    GRANT_ORGANIZATION_TO_USER_OK(200070100, "为用户 %s 授权组织机构成功"),
    GRANT_ORGANIZATION_TO_USER_ERR(200070101, "为用户 %s 授权组织机构失败"),
    GRANT_ORGANIZATION_TO_USER_ERR_NON_USER(200070102, "授权组织机构失败，用户编号 %s 不存在"),
    GRANT_ORGANIZATION_TO_USER_ERR_NON_ORGANIZATION(200070103, "授权组织机构失败，组织机构编号 %s 不存在"),
    /**
     * 撤销用户组织机构授权 2000702
     */
    TERMINATE_ALL_ORGANIZATION_TO_USER_OK(200070200, "为用户 %s 撤销组织机构授权成功"),
    TERMINATE_ALL_ORGANIZATION_TO_USER_ERR(200070201, "为用户 %s 撤销组织机构授权失败"),
    TERMINATE_ALL_ORGANIZATION_TO_USER_ERR_NON_USER(200070202, "用户编号 %s 不存在"),
    /**
     * 组织机构分配用户 2000703
     */
    AUTHORIZING_USER_TO_ORGANIZATION_OK(200070300, "为组织机构 %s 分配用户成功"),
    AUTHORIZING_USER_TO_ORGANIZATION_ERR(200070301, "为组织机构 %s 分配用户失败"),
    AUTHORIZING_USER_TO_ORGANIZATION_NON_USER(200070302, "组织机构分配用户失败失败，用户编号 %s 不存在"),
    AUTHORIZING_USER_TO_ORGANIZATION_NON_ORGANIZATION(200070303, "组织机构分配用户失败失败，组织机构编号 %s 不存在"),
    /**
     * 组织机构取消分配用户 2000704
     */
    UN_AUTHORIZING_USER_TO_ORGANIZATION_OK(200070400, "为组织机构 %s 取消分配用户成功"),
    UN_AUTHORIZING_USER_TO_ORGANIZATION_ERR(200070401, "为组织机构 %s 取消分配用户失败"),
    UN_AUTHORIZING_USER_TO_ORGANIZATION_NON_ORGANIZATION(200070402, "组织机构编号 %s 不存在"),

    /**
     * 字典项添加     2000801
     */
    DICT_ADD_OK(200080100, "字典项添加成功, 字典项名：%s"),
    DICT_ADD_ERR(200080101, "字典项添加失败 字典项名：%s"),
    DICT_ADD_ERR_NAME_REPEAT(200080102, "字典项添加失败，字典项名称 %s 存在"),
    DICT_ADD_ERR_NAME_DELETE(200080103, "字典项添加失败，字典项名称 %s 已删除"),
    DICT_ADD_ERR_CODE_REPEAT(200080102, "字典项添加失败，字典项编码 %s 存在"),
    DICT_ADD_ERR_CODE_DELETE(200080103, "字典项添加失败，字典项编码 %s 已删除"),
    /**
     * 字典项删除     2000802
     */
    DICT_DELETE_OK(200080200, "字典项删除成功, 字典项名：%s"),
    DICT_DELETE_ERR(200080201, "字典项删除失败，字典项名：%s"),
    DICT_DELETE_ERR_NON(200080202, "字典项删除失败，字典项不存在"),
    DICT_DELETE_ERR_USE(200080203, "字典项删除失败，字典项名：%s 还在使用"),
    /**
     * 字典项更新     2000803
     */
    DICT_UPDATE_OK(200080300, "更新字典项成功，字典项名：%s"),
    DICT_UPDATE_ERR(200080301, "更新字典项失败，字典项名：%s"),
    DICT_UPDATE_ERR_NON(200080302, "更新字典项失败，字典项 %s 不存在"),
    DICT_UPDATE_ERR_REPEAT(200080304, "更新字典项失败，字典项 %s 存在"),
    DICT_UPDATE_ERR_NAME_REPEAT(200080305, "更新字典项失败，字典项名称 %s 存在"),
    DICT_UPDATE_ERR_CODE_REPEAT(200080306, "更新字典项失败，字典项编码 %s 存在"),
    /**
     * 字典项信息分页查询 2000804
     */
    DICT_PAGE_QUERY_OK(200080400, "创建字典项信息分页信息成功"),
    DICT_PAGE_QUERY_ERR(200080401, "创建字典项信息分页信息错误，错误信息：%s"),
    DICT_ITEM_PAGE_QUERY_OK(200080402, "创建字典值信息分页信息成功"),
    DICT_ITEM_PAGE_QUERY_ERR(200080403, "创建字典值信息分页信息错误，错误信息：%s"),
    /**
     * 字典项信息查询 2000805
     */
    DICT_QUERY_OK(200080500, "字典项信息查询成功"),
    DICT_QUERY_ERR(200080501, "查询字典项信息失败"),
    DICT_QUERY_ERR_DICT_NON(200080502, "查询字典项信息失败，字典项 %s 不存在"),
    DICT_QUERY_ERR_CODE_NULL(200080503, "查询字典信息失败，字典编码为空"),
    DICT_QUERY_ERR_FIELD_NULL(200080504, "查询字典信息失败，字典表字段参数为空"),
    DICT_QUERY_ERR_FIELD_NON(200080505, "查询字典信息失败，字典表或者字段不存在"),
    DICT_QUERY_ERR_SQL_ILLEGAL(200080506, "查询字典信息失败，参数异常，请确定参数合法性"),
    DICT_TREE_QUERY_OK(200080507, "字典树信息查询成功"),
    DICT_TREE_QUERY_ERR(200080508, "查询字典树信息失败"),
    /**
     * 字典值添加     2000806
     */
    DICT_ITEM_ADD_OK(200080600, "字典值添加成功, 字典值名：%s"),
    DICT_ITEM_ADD_ERR(200080601, "字典值添加失败 字典值名：%s"),
    DICT_ITEM_ADD_ERR_TEXT_REPEAT(200080602, "字典值添加失败，字典值文本 %s 存在"),
    DICT_ITEM_ADD_ERR_TEXT_DISABLED(200080603, "字典值添加失败，字典值文本 %s 已停用"),
    DICT_ITEM_ADD_ERR_VALUE_REPEAT(200080602, "字典值添加失败，字典值值 %s 存在"),
    DICT_ITEM_ADD_ERR_VALUE_DISABLED(200080603, "字典值添加失败，字典值值 %s 已停用"),
    /**
     * 字典项删除     2000807
     */
    DICT_ITEM_DELETE_OK(200080700, "字典值删除成功, 字典值名：%s"),
    DICT_ITEM_DELETE_ERR(200080701, "字典值删除失败，字典值名：%s"),
    DICT_ITEM_DELETE_ERR_NON(200080702, "字典值删除失败，字典值不存在"),
    /**
     * 字典项更新     2000808
     */
    DICT_ITEM_QUERY_ERR(200080800, "查询字典值信息失败"),
    DICT_ITEM_UPDATE_OK(200080800, "更新字典值成功，字典值名：%s"),
    DICT_ITEM_UPDATE_ERR(200080801, "更新字典值失败，字典值名：%s"),
    DICT_ITEM_UPDATE_ERR_NON(200080802, "更新字典值失败，字典值 %s 不存在"),
    DICT_ITEM_UPDATE_ERR_REPEAT(200080804, "更新字典值失败，字典值 %s 存在"),
    DICT_ITEM_UPDATE_ERR_TEXT_REPEAT(200080805, "更新字典值失败，字典值文本 %s 存在"),
    DICT_ITEM_UPDATE_ERR_VALUE_REPEAT(200080806, "更新字典值失败，字典值值 %s 存在"),

    /**
     * 定时任务添加     2000901
     */
    QUARTZ_JOB_ADD_OK(200090100, "定时任务添加成功, 定时任务名：%s"),
    QUARTZ_JOB_ADD_ERR(200090101, "定时任务添加失败 定时任务名：%s"),
    QUARTZ_JOB_ADD_ERR_REPEAT(200090102, "定时任务添加失败，定时任务 %s 存在"),
    /**
     * 定时任务删除     2000902
     */
    QUARTZ_JOB_DELETE_OK(200090200, "定时任务删除成功, 定时任务名：%s"),
    QUARTZ_JOB_DELETE_ERR(200090201, "定时任务删除失败，定时任务名：%s"),
    QUARTZ_JOB_DELETE_ERR_NON(200090202, "定时任务删除失败，定时任务不存在"),
    QUARTZ_JOB_DELETE_ERR_ASSIGNED(200090203, "定时任务删除失败，%s 定时任务已存在于计划程序中不可直接删除"),
    /**
     * 定时任务更新     2000903
     */
    QUARTZ_JOB_QUERY_ERR(200090300, "定时任务查询失败"),
    QUARTZ_JOB_UPDATE_OK(200090301, "更新定时任务成功，定时任务名：%s"),
    QUARTZ_JOB_UPDATE_ERR(200090302, "更新定时任务失败，定时任务名：%s"),
    QUARTZ_JOB_UPDATE_ERR_NON(200090303, "更新定时任务失败，定时任务 %s 不存在"),
    QUARTZ_JOB_UPDATE_ERR_REPEAT(200090304, "更新定时任务失败，定时任务 %s 存在"),
    QUARTZ_JOB_UPDATE_ERR_ASSIGNED(200090305, "更新定时任务失败，定时任务 %s 已存在于计划程序中不可直接更新"),
    /**
     * 定时任务信息分页查询 2000904
     */
    QUARTZ_JOB_PAGE_QUERY_OK(200090400, "创建定时任务信息分页信息成功"),
    QUARTZ_JOB_PAGE_QUERY_ERR(200090401, "创建定时任务信息分页信息错误，错误信息：%s"),
    /**
     * 定时任务触发 2000905
     */
    QUARTZ_JOB_TRIGGER_OK(200090500, "{} 定时任务触发成功"),
    QUARTZ_JOB_TRIGGER_ERR(200090501, "%s 定时任务触发失败，错误信息：%s"),
    QUARTZ_JOB_TRIGGER_ERR_NON(200090502, "定时任务触发失败，定时任务不存在"),
    QUARTZ_JOB_RESUME_ERR_JOB_NON(200090503, "定时任务恢复失败，定时任务不存在"),
    QUARTZ_JOB_RESUME_ERR_JOB_STATE_ERR(200090504, "定时任务恢复失败，定时任务状态异常"),
    /**
     * 定时任务暂停 2000906
     */
    QUARTZ_JOB_PAUSE_OK(200090600, "{} 定时任务暂停成功"),
    QUARTZ_JOB_PAUSE_ERR(200090601, "定时任务暂停失败，错误信息：%s"),
    QUARTZ_JOB_PAUSE_ERR_NON(200090602, "定时任务暂停失败，定时任务不存在"),
    QUARTZ_JOB_PAUSE_ERR_STATE_ERR(200090603, "定时任务暂停失败，定时任务状态异常"),
    /**
     * 定时任务移除 2000907
     */
    QUARTZ_JOB_REMOVE_OK(200090700, "定时任务移除成功"),
    QUARTZ_JOB_REMOVE_ERR(200090701, "定时任务移除失败，错误信息：%s"),
    QUARTZ_JOB_REMOVE_ERR_JOB_NON(200090702, "定时任务移除失败，定时任务不存在"),
    QUARTZ_JOB_REMOVE_ERR_STATE_ERR(200090703, "定时任务移除失败，定时任务状态异常"),
    /**
     * 定时任务构建 2000908
     */
    QUARTZ_JOB_SCHEDULER_OK(200090800, "定时任务构建成功"),
    QUARTZ_JOB_SCHEDULER_ERR(200090801, "定时任务构建失败，错误信息：%s"),
    QUARTZ_JOB_SCHEDULER_ERR_JOB_NON(200090802, "定时任务构建失败，定时任务不存在"),
    QUARTZ_JOB_SCHEDULER_ERR_JOB_REPEAT(200090803, "定时任务构建失败，定时任务已存在于计划程序中"),
    QUARTZ_JOB_SCHEDULER_ERR_JOB_STATE_ERR(200090804, "定时任务构建失败，定时任务状态异常"),
    /**
     * 定时任务状态查询 2000909
     */
    QUARTZ_JOB_GET_STATE_OK(200090900, "定时任务状态查询成功"),
    QUARTZ_JOB_GET_STATE_ERR(200090901, "定时任务状态查询失败，错误信息：%s"),
    QUARTZ_JOB_GET_STAT_ERR_JOB_NON(200090802, "定时任务状态查询失败，定时任务不存在"),

    /**
     * WebSocket信息接口分页查询 2001001
     */
    WEBSOCKET_PAGE_QUERY_OK(200100100, "创建WebSocket信息分页成功"),
    WEBSOCKET_PAGE_QUERY_ERR(200100101, "创建WebSocket信息分页信息错误，错误信息：%s"),
    WEBSOCKET_QUERY_ERR(200100102, "WebSocket信息查询失败"),
    /**
     * WebSocket信息发送 2001002
     */
    WEBSOCKET_MESSAGE_SEND_OK(200100200, "WebSocket信息发送成功"),
    WEBSOCKET_MESSAGE_SEND_ERR(200100201, "WebSocket信息发送成功，错误信息：%s"),
    WEBSOCKET_MESSAGE_SEND_ERR_RICH_TEXT_SAVE_ERR(200100201, "WebSocket信息发送成功，富文本信息保存失败"),

    /**
     * 数据源分页查询 2001101
     */
    DATASOURCE_PAGE_QUERY_OK(200110100, "创建数据源信息分页信息成功"),
    DATASOURCE_PAGE_QUERY_ERR(200110101, "创建数据源信息分页信息错误，错误信息：%s"),
    /**
     * 数据源添加     2001102
     */
    DATASOURCE_ADD_OK(200110200, "数据源添加成功，数据源名：%s"),
    DATASOURCE_ADD_ERR(200110201, "数据源添加失败，数据源名：%s，请确认信息后重新添加"),
    DATASOURCE_ADD_ERR_REPEAT(200110202, "数据源添加失败，%s 已存在，请确认信息后重新添加"),
    DATASOURCE_ADD_ERR_FREEZE(200110203, "数据源添加失败，%s 已冻结，请确认信息后重新添加"),
    /**
     * 数据源冻结     2001103
     */
    DATASOURCE_FREEZE_OK(200110300, "数据源冻结成功，数据源标识：%s，操作人编号：%s"),
    DATASOURCE_FREEZE_ERR(200110301, "数据源冻结失败，数据源标识：%s"),
    DATASOURCE_FREEZE_ERR_DATASOURCE_NULL(200110302, "数据源冻结失败，数据源不存在，数据源标识：%s"),
    /**
     * 数据源解冻     2001104
     */
    DATASOURCE_UNFREEZE_OK(200110400, "数据源解冻成功，数据源标识：%s"),
    DATASOURCE_UNFREEZE_ERR_DATASOURCE_NULL(200110401, "数据源解结失败，数据源不存在，数据源标识：%s"),
    /**
     * 数据源信息更新   2001105
     */
    DATASOURCE_UPDATE_OK(200110500, "数据源信息更新成功，数据源标识：%s"),
    DATASOURCE_UPDATE_ERR(200110501, "数据源信息更新失败，数据源标识：%s"),
    DATASOURCE_UPDATE_ERR_NON(200110502, "数据源信息更新失败，数据源信息 %s 不存在"),
    DATASOURCE_UPDATE_ERR_NAME_REPEAT(200110503, "数据源信息更新失败，数据源名称 %s 存在"),
    DATASOURCE_UPDATE_ERR_USING(200110504, "数据源信息更新失败，%s 正在使用"),
    /**
     * 数据源删除     2001106
     */
    DATASOURCE_DELETE_OK(200110600, "数据源删除成功，数据源标识：%s"),
    DATASOURCE_DELETE_ERR(200110601, "数据源删除失败，数据源标识：%s"),
    DATASOURCE_DELETE_ERR_DATASOURCE_NULL(200110602, "数据源删除失败，数据源不存在，数据源标识：%s"),
    DATASOURCE_DELETE_ERR_USING(200110603, "数据源删除失败，%s 正在使用"),

    /**
     * 在线开发-数据库表    2001201
     */
    TABLE_PAGE_QUERY_OK(200120100, "创建数据库表信息分页信息成功"),
    TABLE_PAGE_QUERY_ERR(200120101, "创建数据库表信息分页信息错误，错误信息：%s"),

    /**
     * 在线开发-表单按钮-分页     2001301
     */
    FORM_BUTTON_PAGE_QUERY_OK(200130100, "创建表单按钮信息分页信息成功"),
    FORM_BUTTON_PAGE_QUERY_ERR(200130101, "创建表单按钮信息分页信息错误，错误信息：%s"),
    /**
     * 在线开发-表单按钮-添加     2001302
     */
    FORM_BUTTON_ADD_OK(200130200, "表单按钮添加成功，表单按钮名：%s"),
    FORM_BUTTON_ADD_ERR(200130201, "表单按钮添加失败，表单按钮名：%s，请确认信息后重新添加"),
    FORM_BUTTON_ADD_ERR_REPEAT(200130202, "表单按钮添加失败，表单按钮名称 %s 或者编号 %s 已存在，请确认信息后重新添加"),
    FORM_BUTTON_ADD_ERR_DEACTIVATE(200130203, "表单按钮添加失败，%s 已停用，请确认信息后重新添加"),
    /**
     * 在线开发-表单按钮-更新   2001303
     */
    FORM_BUTTON_UPDATE_OK(200130300, "表单按钮信息更新成功，表单按钮标识：%s"),
    FORM_BUTTON_UPDATE_ERR(200130301, "表单按钮信息更新失败，表单按钮标识：%s"),
    FORM_BUTTON_UPDATE_ERR_NON(200130302, "表单按钮信息更新失败，表单按钮信息 %s 不存在"),
    FORM_BUTTON_UPDATE_ERR_REPEAT(200130303, "表单按钮信息更新失败，表单按钮名称 %s 或者编号 %s 已存在"),
    FORM_BUTTON_UPDATE_ERR_USING(200130304, "表单按钮信息更新失败，%s 正在使用"),
    /**
     * 在线开发-表单按钮-删除     2001304
     */
    FORM_BUTTON_DELETE_OK(200130400, "表单按钮删除成功，表单按钮标识：%s"),
    FORM_BUTTON_DELETE_ERR(200130401, "表单按钮删除失败，表单按钮标识：%s"),
    FORM_BUTTON_DELETE_ERR_FORM_BUTTON_NULL(200130402, "表单按钮删除失败，表单按钮不存在，表单按钮标识：%s"),
    FORM_BUTTON_DELETE_ERR_USING(200130403, "表单按钮删除失败，%s 正在使用"),
    /**
     * 在线开发-表单按钮-查询     2001305
     */
    FORM_BUTTON_QUERY_OK(200130500, "表单按钮查询成功"),
    FORM_BUTTON_QUERY_ERR_FORM_BUTTON_NON(200130501, "表单按钮查询失败，编号为 %s 的表单按钮不存在"),

    /**
     * 在线开发-JavaScript 增强-分页     2001401
     */
    FORM_JAVA_SCRIPT_PAGE_QUERY_OK(200140100, "创建JavaScript 增强信息分页信息成功"),
    FORM_JAVA_SCRIPT_PAGE_QUERY_ERR(200140101, "创建JavaScript 增强信息分页信息错误，错误信息：%s"),
    /**
     * 在线开发-JavaScript 增强-添加     2001402
     */
    FORM_JAVA_SCRIPT_ADD_OK(200140200, "JavaScript 增强添加成功"),
    FORM_JAVA_SCRIPT_ADD_ERR(200140201, "JavaScript 增强添加失败，请确认信息后重新添加"),
    /**
     * 在线开发-JavaScript 增强-更新   2001403
     */
    FORM_JAVA_SCRIPT_UPDATE_OK(200140300, "JavaScript 增强信息更新成功，JavaScript 增强标识：%s"),
    FORM_JAVA_SCRIPT_UPDATE_ERR(200140301, "JavaScript 增强信息更新失败，JavaScript 增强标识：%s"),
    FORM_JAVA_SCRIPT_UPDATE_ERR_NON(200140302, "JavaScript 增强信息更新失败，JavaScript 增强标识 %s 不存在"),
    /**
     * 在线开发-JavaScript 增强-删除     2001404
     */
    FORM_JAVA_SCRIPT_DELETE_OK(200140400, "JavaScript 增强删除成功，JavaScript 增强标识：%s"),
    FORM_JAVA_SCRIPT_DELETE_ERR(200140401, "JavaScript 增强删除失败，JavaScript 增强标识：%s"),
    FORM_JAVA_SCRIPT_DELETE_ERR_FORM_JAVA_SCRIPT_NULL(200140402, "JavaScript 增强删除失败，JavaScript 增强不存在，JavaScript 增强标识：%s"),
    /**
     * 在线开发-JavaScript 增强-查询     2001405
     */
    FORM_JAVA_SCRIPT_QUERY_OK(200140500, "JavaScript 增强查询成功"),
    FORM_JAVA_SCRIPT_QUERY_ERR_FORM_JAVA_SCRIPT_NON(200140501, "JavaScript 增强查询失败，编号为 %s 的JavaScript 增强不存在"),

    /**
     * 在线开发-Java 增强-分页     2001501
     */
    FORM_JAVA_PAGE_QUERY_OK(200150100, "创建Java 增强信息分页信息成功"),
    FORM_JAVA_PAGE_QUERY_ERR(200150101, "创建Java 增强信息分页信息错误，错误信息：%s"),
    /**
     * 在线开发-Java 增强-添加     2001502
     */
    FORM_JAVA_ADD_OK(200150200, "Java 增强添加成功"),
    FORM_JAVA_ADD_ERR(200150201, "Java 增强添加失败，请确认信息后重新添加"),
    FORM_JAVA_ADD_ERR_CLASS_BEAN_NON(200150202, "Java 增强信息更新失败，判断 Class 或是 Spring 的 Bean 不存在"),
    /**
     * 在线开发-Java 增强-更新   2001503
     */
    FORM_JAVA_UPDATE_OK(200150300, "Java 增强信息更新成功，Java 增强标识：%s"),
    FORM_JAVA_UPDATE_ERR(200150301, "Java 增强信息更新失败，Java 增强标识：%s"),
    FORM_JAVA_UPDATE_ERR_NON(200150302, "Java 增强信息更新失败，Java 增强标识 %s 不存在"),
    FORM_JAVA_UPDATE_ERR_CLASS_BEAN_NON(200150303, "Java 增强信息更新失败，Class 或是 Spring 的 Bean 不存在"),
    /**
     * 在线开发-Java 增强-删除     2001504
     */
    FORM_JAVA_DELETE_OK(200150400, "Java 增强删除成功，Java 增强标识：%s"),
    FORM_JAVA_DELETE_ERR(200150401, "Java 增强删除失败，Java 增强标识：%s"),
    FORM_JAVA_DELETE_ERR_FORM_JAVA_NULL(200150402, "Java 增强删除失败，Java 增强不存在，Java 增强标识：%s"),
    /**
     * 在线开发-Java 增强-查询     2001505
     */
    FORM_JAVA_QUERY_OK(200150500, "Java 增强查询成功"),
    FORM_JAVA_QUERY_ERR_FORM_JAVA_NON(200150501, "Java 增强查询失败，编号为 %s 的Java 增强不存在"),

    /**
     * 在线开发-StructuredQueryLanguage 增强-分页     2001601
     */
    FORM_STRUCTURED_QUERY_LANGUAGE_PAGE_QUERY_OK(200160100, "创建StructuredQueryLanguage 增强信息分页信息成功"),
    FORM_STRUCTURED_QUERY_LANGUAGE_PAGE_QUERY_ERR(200160101, "创建StructuredQueryLanguage 增强信息分页信息错误，错误信息：%s"),
    /**
     * 在线开发-StructuredQueryLanguage 增强-添加     2001602
     */
    FORM_STRUCTURED_QUERY_LANGUAGE_ADD_OK(200160200, "StructuredQueryLanguage 增强添加成功"),
    FORM_STRUCTURED_QUERY_LANGUAGE_ADD_ERR(200160201, "StructuredQueryLanguage 增强添加失败，请确认信息后重新添加"),
    /**
     * 在线开发-StructuredQueryLanguage 增强-更新   2001603
     */
    FORM_STRUCTURED_QUERY_LANGUAGE_UPDATE_OK(200160300, "StructuredQueryLanguage 增强信息更新成功，StructuredQueryLanguage 增强标识：%s"),
    FORM_STRUCTURED_QUERY_LANGUAGE_UPDATE_ERR(200160301, "StructuredQueryLanguage 增强信息更新失败，StructuredQueryLanguage 增强标识：%s"),
    FORM_STRUCTURED_QUERY_LANGUAGE_UPDATE_ERR_NON(200160302, "StructuredQueryLanguage 增强信息更新失败，StructuredQueryLanguage 增强标识 %s 不存在"),
    /**
     * 在线开发-StructuredQueryLanguage 增强-删除     2001604
     */
    FORM_STRUCTURED_QUERY_LANGUAGE_DELETE_OK(200160400, "StructuredQueryLanguage 增强删除成功，StructuredQueryLanguage 增强标识：%s"),
    FORM_STRUCTURED_QUERY_LANGUAGE_DELETE_ERR(200160401, "StructuredQueryLanguage 增强删除失败，StructuredQueryLanguage 增强标识：%s"),
    FORM_STRUCTURED_QUERY_LANGUAGE_DELETE_ERR_FORM_STRUCTURED_QUERY_LANGUAGE_NULL(200160402, "StructuredQueryLanguage 增强删除失败，StructuredQueryLanguage 增强不存在，StructuredQueryLanguage 增强标识：%s"),
    /**
     * 在线开发-StructuredQueryLanguage 增强-查询     2001605
     */
    FORM_STRUCTURED_QUERY_LANGUAGE_QUERY_OK(200160500, "StructuredQueryLanguage 增强查询成功"),
    FORM_STRUCTURED_QUERY_LANGUAGE_QUERY_ERR_FORM_STRUCTURED_QUERY_LANGUAGE_NON(200160501, "StructuredQueryLanguage 增强查询失败，编号为 %s 的StructuredQueryLanguage 增强不存在"),

    /**
     * 在线开发 - 数据库表表信息 新增 2001701
     */
    TABLE_HEAD_ADD_ERR_01(200170101, "数据库表表信息复制新增，数据库表 %s 存在"),

    /**
     * 在线开发 - 数据库表表信息 更新 2001702
     */
    TABLE_HEAD_UPDATE_ERR_01(200170201, "数据库表表信息更新失败，数据库表不存在"),


//    结束符
    ;

    /**
     * 返回码
     */
    private Integer code;
    /**
     * 返回消息
     */
    private String message;

    @Override
    public String toString() {
        return '{' + "\"errorCode\":" + this.code + ",\"errorValue\":\"" + this.message + "\"}";
    }

}
