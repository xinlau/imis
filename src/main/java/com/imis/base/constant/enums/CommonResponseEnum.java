package com.imis.base.constant.enums;


import com.imis.base.globle.CommonExceptionAssert;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>
 * CommonResponseEnum<br>
 * 通用返回结果
 * </p>
 * 一般对应于{@link com.imis.base.globle.exception.BaseException}，系统封装的工具出现异常
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月31日 16:50
 */
@Getter
@AllArgsConstructor
public enum CommonResponseEnum implements CommonExceptionAssert {

    /**
     * 成功
     */
    SUCCESS(200, "SUCCESS"),
    /**
     * 服务器遇到错误，无法完成请求。服务器异常，无法识别的异常，尽可能对通过判断减少未定义异常抛出
     */
    ERROR_500(500, "ERROR"),
    /**
     * 无法获得服务 — 由于临时过载或维护，服务器无法处理请求。
     */
    ERROR_503(503, "无法获得服务"),

    /**
     * Shiro 相关
     */
    ERROR_NO_AUTHORIZATION(510, "访问权限认证未通过"),
    ERROR_NO_AUTHENTICATION(511, "身份验证未通过"),
    ERROR_NO_TOKEN(512, "Token失效，请重新登录"),

    ;

    /**
     * 返回码
     */
    private Integer code;

    /**
     * 返回消息
     */
    private String message;

}
