package com.imis.base.constant;

/**
 * <p>
 * CacheConstant<br>
 * 缓存常量
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public interface CacheConstant {

	/**
	 * redis
	 */
	String REDIS_CACHE_MANAGER = "redisCacheManager";
	/**
	 * ehCache
	 */
	String EHCACHE_CACHE_MAANGER = "ehCacheCacheManager";

	/**
	 * 登录用户Token缓存Key前缀
	 */
	String PREFIX_SYS_USER_ID_TOKEN = "sys_user_id_token";
	/**
	 * 客户端Session标识与客户端标识缓存Key前缀
	 */
	String SESSION_ID_CLIENT_ID = "sys:cache:webSocket";
	/**
	 * 客户端Session数量
	 */
	String SESSION_ONLINE_COUNT = "onlineCount";
	/**
	 * 字典信息缓存
	 */
	String SYS_DICT_CACHE = "sys:cache:dict";
	/**
	 * 表字典信息缓存
	 */
	String SYS_DICT_TABLE_CACHE = "sys:cache:dictTable";
	/**
	 * 字典信息缓存
	 */
	String PREFIX_SYS_DICT_CACHE = "sys:cache:dict";
	/**
	 * 表字典信息缓存
	 */
	String PREFIX_SYS_DICT_TABLE_CACHE = "sys:cache:dictTable";
	/**
	 * 数据权限配置缓存
	 */
	String PREFIX_SYS_DATA_PERMISSIONS_CACHE = "sys:cache:permissions:dataRules";

	/**
	 * 缓存用户信息
	 */
	String PREFIX_SYS_USERS_CACHE = "sys:cache:user:info";
	/**
	 * 用户角色缓存Key前缀
	 */
	String PREFIX_SYS_USER_ROLE = "sys:cache:user:role";
	/**
	 * 用户组织机构缓存Key前缀
	 */
	String PREFIX_SYS_USER_ORGANIZATION = "sys:cache:user:organization";

	/**
	 * 缓存角色信息
	 */
	String PREFIX_SYS_ROLE_CACHE = "sys:cache:role:info";
	/**
	 * 角色权限缓存Key前缀
	 */
	String PREFIX_SYS_ROLE_PERMISSIONS = "sys:cache:role:permissions";

	/**
	 * 缓存菜单信息
	 */
	String PREFIX_SYS_MENU_CACHE = "sys:cache:menu";

	/**
	 * 锁标识
	 */
	String PREFIX_LOCK_IDENTIFICATION = "lock_identification";

}
