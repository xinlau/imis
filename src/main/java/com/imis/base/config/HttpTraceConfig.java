package com.imis.base.config;

import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * HttpTraceConfig<br>
 * 显示 HTTP 跟踪信息
 * （默认情况下，最后 100 个 HTTP 请求-响应交换）
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月22日 11:14
 */
@Configuration
public class HttpTraceConfig {

    @Bean
    public HttpTraceRepository httpTraceRepository() {
        return new InMemoryHttpTraceRepository();
    }

}
