package com.imis.base.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * <p>
 * ServiceInfoConfig<br>
 * 控制程序在启动后输出内容
 *  <ul>
 *      <li>各组件的启动地址</li>
 *      <li>启动后的其他信息</li>
 *  </ul>
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月31日 16:50
 */
@Slf4j
@Configuration
public class ServiceInfoConfig implements ApplicationListener<WebServerInitializedEvent> {
    @Override
    public void onApplicationEvent(WebServerInitializedEvent webServerInitializedEvent) {
        if (webServerInitializedEvent.getApplicationContext().getParent() == null) {
            Environment env = webServerInitializedEvent.getApplicationContext().getEnvironment();
            String ip = null;
            try {
                ip = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                log.error("UnknownHostException:", e.getCause());
            }
            String port = env.getProperty("server.port");
            String path = env.getProperty("server.servlet.context-path");
            log.info("\n=========================程序已经启动完毕==========================\n\t" +
                    "\n-----------------------------------------------------------------\n\t" +
                    "本机运行地址: \t\thttp://localhost:" + port + path + "\n\t" +
                    "外部运行地址: \t\thttp://" + ip + ":" + port + path + "\n\t" +
                    "接口Doc(推荐): \t\thttp://" + ip + ":" + port + path + "doc.html\n" +
                    "-----------------------------------------------------------------");
        }
    }
}
