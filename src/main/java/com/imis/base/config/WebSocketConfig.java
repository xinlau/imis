package com.imis.base.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * <p>
 * WebSocketConfig<br>
 * WebSocket 配置类
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
@Configuration
public class WebSocketConfig {
    /**
     * 创建 ServerEndpointExporter 组件，交由 spring IOC 容器管理，
     * 它会自动扫描注册应用中所有的 @ServerEndpoint 注解声明的 Websocket endpoint
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

}
