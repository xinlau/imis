package com.imis.base.config;

import com.google.code.kaptcha.Constants;
import com.imis.base.constant.CommonConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * <p>
 * KaptchaConfig<br>
 * Kaptcha配置类 用于生成图形验证码
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月28日 10:02
 */
@Component
@Configuration
public class KaptchaConfig {

    /**
     * 字符验证码
     *
     * @return DefaultKaptcha
     */
    @Bean(name = "charVerificationCode")
    public com.google.code.kaptcha.impl.DefaultKaptcha getCharVerificationCode() {
        com.google.code.kaptcha.impl.DefaultKaptcha defaultKaptcha = new com.google.code.kaptcha.impl.DefaultKaptcha();
        // 1.配置属性参数
        java.util.Properties properties = new java.util.Properties();
        // 图片边框，合法值：yes , no
        properties.setProperty(Constants.KAPTCHA_BORDER, "yes");
        // 边框颜色，合法值： r,g,b (and optional alpha) 或者 white,black,blue.
        properties.setProperty(Constants.KAPTCHA_BORDER_COLOR, "black");
        // 边框厚度，合法值：>0
        properties.setProperty(Constants.KAPTCHA_BORDER_THICKNESS, "1");
        // 图片宽
        properties.setProperty(Constants.KAPTCHA_IMAGE_WIDTH, "150");
        // 图片高
        properties.setProperty(Constants.KAPTCHA_IMAGE_HEIGHT, "50");
        // 图片实现类
        properties.setProperty(Constants.KAPTCHA_PRODUCER_IMPL, "com.google.code.kaptcha.impl.DefaultKaptcha");
        // 文本实现类
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_IMPL, "com.google.code.kaptcha.text.impl.DefaultTextCreator");
        // 文本集合，验证码值从此集合中获取
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING, "abcde2345678gfymnpwx");
        // 验证码长度
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, "5");
        // 字体
        // properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_NAMES, "宋体,楷体,微软雅黑");
        // 字体大小
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_SIZE, "45");
        // 字体颜色，合法值： r,g,b  或者 white,black,blue
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_COLOR, "blue");
        // 文字间隔
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_SPACE, "2");
        // 干扰实现类
        properties.setProperty(Constants.KAPTCHA_NOISE_IMPL, "com.google.code.kaptcha.impl.DefaultNoise");
        // 干扰颜色，合法值： r,g,b 或者 white,black,blue.
        properties.setProperty(Constants.KAPTCHA_NOISE_COLOR, "black");
        // 图片样式：
        // 水纹com.google.code.kaptcha.impl.WaterRipple
        // 鱼眼com.google.code.kaptcha.impl.FishEyeGimpy
        // 阴影com.google.code.kaptcha.impl.ShadowGimpy
        properties.setProperty(Constants.KAPTCHA_OBSCURIFICATOR_IMPL, "com.google.code.kaptcha.impl.WaterRipple");
        // 背景实现类
        properties.setProperty(Constants.KAPTCHA_BACKGROUND_IMPL, "com.google.code.kaptcha.impl.DefaultBackground");
        // 背景颜色渐变，开始颜色
        // properties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_FROM, "light grey");
        // 背景颜色渐变， 结束颜色
        // properties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_TO, "white");
        // 文字渲染器
        properties.setProperty(Constants.KAPTCHA_WORDRENDERER_IMPL, "com.google.code.kaptcha.text.impl.DefaultWordRenderer");
        // session key
        properties.setProperty(Constants.KAPTCHA_SESSION_KEY, CommonConstant.KAPTCHA_SESSION_KEY);
        // session date
        properties.setProperty(Constants.KAPTCHA_SESSION_DATE, CommonConstant.KAPTCHA_SESSION_DATE);
        // 2.从属性文件中检索配置值，并在未指定值时指定默认值。
        com.google.code.kaptcha.util.Config config = new com.google.code.kaptcha.util.Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }

    /**
     * 运算验证码
     *
     * @return DefaultKaptcha
     */
    @Bean(name = "operationVerificationCode")
    public com.google.code.kaptcha.impl.DefaultKaptcha getOperationVerificationCode() {
        com.google.code.kaptcha.impl.DefaultKaptcha defaultKaptcha = new com.google.code.kaptcha.impl.DefaultKaptcha();
        // 1.配置属性参数
        java.util.Properties properties = new java.util.Properties();
        // 图片边框，合法值：yes , no
        properties.setProperty(Constants.KAPTCHA_BORDER, "yes");
        // 边框颜色，合法值： r,g,b (and optional alpha) 或者 white,black,blue.
        properties.setProperty(Constants.KAPTCHA_BORDER_COLOR, "black");
        // 边框厚度，合法值：>0
        properties.setProperty(Constants.KAPTCHA_BORDER_THICKNESS, "1");
        // 图片宽
        properties.setProperty(Constants.KAPTCHA_IMAGE_WIDTH, "170");
        // 图片高
        properties.setProperty(Constants.KAPTCHA_IMAGE_HEIGHT, "50");
        // 图片实现类
        properties.setProperty(Constants.KAPTCHA_PRODUCER_IMPL, "com.google.code.kaptcha.impl.DefaultKaptcha");
        // 文本实现类
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_IMPL, "com.imis.base.util.KaptchaTextCreatorUtil");
        // 文本集合，验证码值从此集合中获取
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING, "0123456789+-*/%");
        // 验证码长度
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, "9");
        // 字体
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_NAMES, "Arial,Courier");
        // 字体大小
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_SIZE, "35");
        // 字体颜色，合法值： r,g,b  或者 white,black,blue
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_COLOR, "blue");
        // 文字间隔
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_SPACE, "2");
        // 干扰实现类
        properties.setProperty(Constants.KAPTCHA_NOISE_IMPL, "com.google.code.kaptcha.impl.DefaultNoise");
        // 干扰颜色，合法值： r,g,b 或者 white,black,blue.
        properties.setProperty(Constants.KAPTCHA_NOISE_COLOR, "243,79,67");
        // 图片样式：
        // 水纹com.google.code.kaptcha.impl.WaterRipple
        // 鱼眼com.google.code.kaptcha.impl.FishEyeGimpy
        // 阴影com.google.code.kaptcha.impl.ShadowGimpy
        properties.setProperty(Constants.KAPTCHA_OBSCURIFICATOR_IMPL, "com.google.code.kaptcha.impl.WaterRipple");
        // 背景实现类
        properties.setProperty(Constants.KAPTCHA_BACKGROUND_IMPL, "com.google.code.kaptcha.impl.DefaultBackground");
        // 背景颜色渐变，开始颜色
        properties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_FROM, "234,172,236");
        // 背景颜色渐变， 结束颜色
        properties.setProperty(Constants.KAPTCHA_BACKGROUND_CLR_TO, "234,144,115");
        // 文字渲染器
        properties.setProperty(Constants.KAPTCHA_WORDRENDERER_IMPL, "com.google.code.kaptcha.text.impl.DefaultWordRenderer");
        // session key
        properties.setProperty(Constants.KAPTCHA_SESSION_KEY, CommonConstant.KAPTCHA_SESSION_KEY);
        // session date
        properties.setProperty(Constants.KAPTCHA_SESSION_DATE, CommonConstant.KAPTCHA_SESSION_DATE);
        // 2.从属性文件中检索配置值，并在未指定值时指定默认值。
        com.google.code.kaptcha.util.Config config = new com.google.code.kaptcha.util.Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }

}
