package com.imis.base.config;

import com.imis.base.util.SendSmsUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * StaticConfig<br>
 * 设置静态参数初始化
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年08月18日 15:23
 */
@Configuration
public class StaticConfig {

    /**
     * 主账号AccessKey的ID
     */
    @Value("${imis-boot.sms.accessKeyId}")
    private String accessKeyId;

    /**
     * 秘钥
     */
    @Value("${imis-boot.sms.accessKeySecret}")
    private String accessKeySecret;

    @Bean
    public void initStatic() {
        /**
         * 短信相关
         */
        SendSmsUtil.setAccessKeyId(accessKeyId);
        SendSmsUtil.setAccessKeySecret(accessKeySecret);
    }

}
