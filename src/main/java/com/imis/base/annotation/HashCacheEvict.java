package com.imis.base.annotation;

import com.imis.base.constant.CommonConstant;

import java.lang.annotation.*;

/**
 * <p>
 * HashCacheEvict<br>
 * 清除以HASH形式存储的缓存注解
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月25日 09:15
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface HashCacheEvict {
    /**
     * HASH标识
     *
     * @return String
     */
    String hashIdentification();

    /**
     * HASH内存放的Key
     *
     * @return String
     */
    String hashKey() default CommonConstant.EMPTY;
}
