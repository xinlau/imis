package com.imis.base.annotation;

import com.imis.base.constant.CommonConstant;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Dict<br>
 * 字典注解
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月24日 14:15
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dict {

    /**
     * 字典编号
     *
     * @return String
     */
    String dicCode();

    /**
     * 字典值
     *
     * @return String
     */
    String dicText() default CommonConstant.EMPTY;

    /**
     * 数据字典项
     *
     * @return String
     */
    String dictTable() default CommonConstant.EMPTY;
}
