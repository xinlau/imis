package com.imis.base.annotation;

import java.lang.annotation.*;

/**
 * <p>
 * HashCachePut<br>
 * 以HASH形式更新缓存注解
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月24日 14:15
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface HashCachePut {
    /**
     * HASH标识
     *
     * @return String
     */
    String hashIdentification();

    /**
     * HASH内存放的Key
     *
     * @return String
     */
    String hashKey();

    /**
     * HASH过期时间
     * 0 - 立即过期
     * -1 - 不会被使用
     * -2 - 永不过期
     *
     * @return int
     */
    int expireTime() default -2;
}
