package com.imis.base.annotation;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.enums.RepeatOperationLockEnum;

import java.lang.annotation.*;

/**
 * <p>
 * RepeatOperationLock<br>
 * 防止高并发重复请求
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年08月20日 14:54
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface RepeatOperationLock {

    /**
     * 标识
     *
     * @return String
     */
    String lockIdentification() default CommonConstant.EMPTY;

    /**
     * 锁类型
     * <ul>
     *     <li>1、基于参数</li>
     *     <li>2、基于方法与参数</li>
     *     <li>3、基于IP与参数（默认）</li>
     *     <li>4、基于TOKEN与参数</li>
     * </ul>
     *
     * @return String
     */
    RepeatOperationLockEnum type() default RepeatOperationLockEnum.IP;

    /**
     * 锁时长，默认500ms
     *
     * @return long
     */
    long timeOut() default 500;

    /**
     * 自动解锁
     * <ul>
     *     <li>1、false：执行完方法立即解锁</li>
     *     <li>2、true：等待锁时长结束解锁</li>
     * </ul>
     *
     * @return boolean
     */
    boolean automaticUnlocking() default false;

}
