package com.imis.base.annotation;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.enums.VerificationCodeTypeEnum;

import java.lang.annotation.*;

/**
 * <p>
 * VerificationCaptcha<br>
 * 验证码验证
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月31日 15:52
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface VerificationCaptcha {

    /**
     * 是否开启验证码验证策略
     *
     * @return boolean
     */
    boolean verification() default true;

    /**
     * 验证码类型
     *
     * @return String
     */
    VerificationCodeTypeEnum type() default VerificationCodeTypeEnum.OPERATION;

    /**
     * 验证码内容
     *
     * @return String
     */
    String captcha() default CommonConstant.EMPTY;

    /**
     * 验证码标识
     *
     * @return String
     */
    String verificationCodeIdentification();

}
