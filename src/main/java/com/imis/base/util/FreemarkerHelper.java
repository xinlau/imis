package com.imis.base.util;

import com.imis.base.constant.CommonConstant;
import com.imis.base.util.table.model.TableTemplateModel;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;

import java.io.StringWriter;

/**
 * <p>
 * FreemarkerHelper<br>
 * Freemarker 引擎工具类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月28日 17:36
 */
@Slf4j
public class FreemarkerHelper {

    private FreemarkerHelper(){
        throw new IllegalStateException("Utility class");
    }

    private static final Configuration TEMPLATE_CONFIG = new Configuration(Configuration.VERSION_2_3_28);

    static {
        // 模板对于数字超过1000，会自动格式为1,,000(禁止转换)
        TEMPLATE_CONFIG.setNumberFormat("0.#####################");
        TEMPLATE_CONFIG.setClassForTemplateLoading(FreemarkerHelper.class, "/");
    }

    /**
     * 解析模板
     *
     * @param templateName - 模板名
     * @param encoding     - 编码
     * @param paras        - 参数
     * @return String - parseTemplate
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/9/29 11:39
     */
    private static String parseTemplate(String templateName, String encoding, Object paras) {
        String parseTemplate = "";
        try {
            StringWriter stringWriter = new StringWriter();
            Template template = TEMPLATE_CONFIG.getTemplate(templateName, encoding);
            template.process(paras, stringWriter);
            parseTemplate = stringWriter.toString();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return parseTemplate;
    }

    /**
     * 获取数据库参数模版
     *
     * @param templateModel - 数据库表模板模型
     * @return String - 获取数据库参数模版
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/9/29 11:43
     */
    public static String parseTemplateForTable(TableTemplateModel templateModel) {
        return parseTemplate(CommonConstant.TABLE_TEMPLATE, CommonConstant.UTF_8, templateModel);
    }

}