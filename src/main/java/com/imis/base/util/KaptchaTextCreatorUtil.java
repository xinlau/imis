package com.imis.base.util;

/**
 * <p>
 * KaptchaTextCreatorUtil<br>
 * 运算验证码 生成器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月30日 10:32
 */
public class KaptchaTextCreatorUtil extends com.google.code.kaptcha.text.impl.DefaultTextCreator {

    /**
     * 运算表达式及结果值
     */
    StringBuilder result = new StringBuilder();

    /**
     * 获取运算验证码
     *
     * @return String - 返回运算验证码
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 10:35
     */
    @Override
    public String getText() {
        // 生成随机数
        java.util.Random random = new java.util.Random(System.currentTimeMillis());
        int x = random.nextInt(51);
        int y = random.nextInt(51);
        // 生成随机运算方式
        int operationalRules = random.nextInt(4);
        // 生成运算验证码
        switch (operationalRules) {
            // 加法运算
            case 0:
                add(x, y);
                break;
            // 减法运算
            case 1:
                subtract(x, y);
                break;
            // 除法、求余运算
            case 2:
                divide(x, y);
                break;
            // 乘法运算
            default:
                multiply(x, y);
                break;
        }
        return result.toString();
    }

    /**
     * 加法运算
     *
     * @param x - 变量x
     * @param y - 变量y
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 10:40
     */
    private void add(int x, int y) {
        result.append(x);
        result.append(" + ");
        result.append(y);
        result.append(" = ?@");
        result.append(x + y);
    }

    /**
     * 减法运算
     *
     * @param x - 变量x
     * @param y - 变量y
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 10:40
     */
    private void subtract(int x, int y) {
        int max = Math.max(x, y);
        int min = Math.min(x, y);
        result.append(max);
        result.append(" - ");
        result.append(min);
        result.append(" = ?@");
        result.append(max - min);
    }

    /**
     * 乘法运算
     *
     * @param x - 变量x
     * @param y - 变量y
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 10:40
     */
    private void multiply(int x, int y) {
        int value = x * y;
        result.append(x);
        result.append(value > 100 ? " + " : " * ");
        result.append(y);
        result.append(" = ?@");
        result.append(value > 100 ? x + y : x * y);
    }

    /**
     * 除法、求余运算
     *
     * @param x - 变量x
     * @param y - 变量y
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/30 10:40
     */
    private void divide(int x, int y) {
        int max = Math.max(x, y);
        int min = Math.min(x, y);
        if (min == 0) {
            multiply(max, min);
        } else if (max % min == 0) {
            result.append(max);
            result.append(" / ");
            result.append(min);
            result.append(" = ?@");
            result.append(max / min);
        } else {
            result.append(max);
            result.append(" % ");
            result.append(min);
            result.append(" = ?@");
            result.append(max % min);
        }
    }

}
