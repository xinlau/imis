package com.imis.base.util.table;

import com.imis.module.online.table.model.po.TableFields;

import java.util.Comparator;

/**
 * <p>
 * 字段 顺序  排序
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
public class FieldNumberComparator implements Comparator<TableFields> {

    @Override
    public int compare(TableFields tableFields1, TableFields tableFields2) {
        if (tableFields1 == null || tableFields1.getSortOrder() == null || tableFields2 == null || tableFields2.getSortOrder() == null) {
            return -1;
        }
        Integer thisVal = tableFields1.getSortOrder();
        Integer anotherVal = tableFields2.getSortOrder();
        return (thisVal < anotherVal ? -1 : (thisVal.equals(anotherVal) ? 0 : 1));
    }

}
