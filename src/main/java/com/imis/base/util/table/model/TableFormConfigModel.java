package com.imis.base.util.table.model;

import com.imis.module.online.table.model.po.TableFields;
import com.imis.module.online.table.model.po.TableIndex;
import lombok.Data;

/**
 * <p>
 * TableFormConfigModel<br>
 * 数据库表的表单配置模型
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月27日 16:04
 */
@Data
public class TableFormConfigModel {

    /**
     * 表格名称
     */
    private java.lang.String tableName;
    /**
     * 是否同步了数据库
     */
    private java.lang.Integer isSynchronize;
    /**
     * 功能注释
     */
    private java.lang.String tableDescription;
    /**
     * 表单版本
     */
    private java.lang.String tableVersion;
    /**
     * 表单类型
     */
    private java.lang.Integer tableType;
    /**
     * 表单主键策略  PrimaryKey
     */
    private java.lang.String primaryKeyType;
    /**
     * 表单主键策略-序列 (针对 Oracle 等数据库)
     */
    private java.lang.String primaryKeySequence;
    /**
     * 附表关联类型（0一对多;1 一对一）
     */
    private java.lang.Integer relationType;
    /**
     * 附表清单
     */
    private java.lang.String subTableChecklist;
    /**
     * 一对多 Table 顺序
     */
    private java.lang.Integer tableOrder;
    /**
     * 表格列属性
     */
    private java.util.List<TableFields> columns;
    /**
     * 索引
     */
    private java.util.List<TableIndex> indexes;
    /**
     * 树形列表 ID 列名
     */
    private java.lang.String treeIdFieldName;
    /**
     * 树形列表 菜单 列名
     */
    private java.lang.String treeFieldName;
    /**
     * 树形列表 父ID 列名
     */
    private java.lang.String treeParentIdFieldName;
    /**
     * 数据库数据源配置
     */
    private TableDataBaseConfig tableDataBaseConfig;

}
