package com.imis.base.util.table.model;

import lombok.Data;

/**
 * <p>
 * TableFieldTemplateModel<br>
 * 数据库表字段模板模型
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月29日 10:47
 */
@Data
public class TableFieldTemplateModel {

    /**
     * 字段名字
     */
    private String fieldName;
    /**
     * 字段长度
     */
    private Integer fieldLength;
    /**
     * 字段类型
     */
    private String fieldType;
    /**
     * 小数点长度
     */
    private Integer fieldDecimalDigits;
    /**
     * 字段默认值
     */
    private String fieldDefaultValue;
    /**
     * 字段是否允许为空 0否 1是
     */
    private Integer fieldIsNull;
    /**
     * 字段注释
     */
    private String fieldComment;

}
