package com.imis.base.util.table.model;

import lombok.Data;

/**
 * <p>
 * TableTemplateModel<br>
 * 数据库表模板模型
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月29日 10:47
 */
@Data
public class TableTemplateModel {

    /**
     * 数据库类型
     */
    private java.lang.String dataBaseType;
    /**
     * 表名称
     */
    private java.lang.String tableName;
    /**
     * 表单主键策略
     */
    private java.lang.String primaryKeyType;
    /**
     * 表单主键策略-序列(针对Oracle等数据库)
     */
    private java.lang.String primaryKeySequence;
    /**
     * 表字段
     */
    private java.util.List<TableFieldTemplateModel> tableFieldList;

}
