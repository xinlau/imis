package com.imis.base.util.table.service;

import com.imis.base.globle.exception.BusinessException;
import com.imis.base.util.table.model.ColumnMeta;
import com.imis.module.online.table.model.po.TableIndex;

/**
 * <p>
 * IDataBaseTableHandleService<br>
 * 数据库表处理服务
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月28日 17:36
 */
public interface IDataBaseTableHandleService {

    /**
     * 获取添加列（表字段）SQL
     *
     * @param columnMeta - 数据库表处理对象
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String getAddColumnSql(final ColumnMeta columnMeta);

    /**
     * 获取重命名列（表字段）SQL
     *
     * @param columnMeta - 数据库表处理对象
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String getReNameFieldName(final ColumnMeta columnMeta);

    /**
     * 获取更新列（表字段）SQL
     *
     * @param formColumnMeta - Form 的自动配置信息
     * @param dataColumnMeta - 数据库表处理对象
     * @return String - SQL语句
     * @throws BusinessException - 数据库异常
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String getUpdateColumnSql(final ColumnMeta formColumnMeta, final ColumnMeta dataColumnMeta) throws BusinessException;

    /**
     * 根据数据类型转换类类型
     *
     * @param dataType - 数据类型
     * @param digits   - 数字
     * @return String - 类类型
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String getMatchClassTypeByDataType(final String dataType, final int digits);

    /**
     * 获取删除指定表 SQL
     *
     * @param tableName - 数据库表名称
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String dropTableSql(final String tableName);

    /**
     * 获取删除指定表字段 SQL
     *
     * @param fieldName - 数据库表字段名称
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String getDropColumnSql(final String fieldName);

    /**
     * 获取列添加注释（表字段添加注释）SQL
     *
     * @param columnMeta - 数据库表处理对象
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String getColumnCommentSql(final ColumnMeta columnMeta);

    /**
     * 处理特殊 SQL
     *
     * @param formColumnMeta - Form 的自动配置信息
     * @param dataColumnMeta - 数据库表处理对象
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String getSpecialHandle(final ColumnMeta formColumnMeta, final ColumnMeta dataColumnMeta);

    /**
     * 获取创建指定表的指定索引 SQL
     *
     * @param tableIndex - 数据库表索引
     * @param tableName - 数据库表名称
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String creatIndexes(final TableIndex tableIndex, final String tableName);

    /**
     * 获取删除指定表的指定索引 SQL
     *
     * @param indexName - 索引名称
     * @param tableName - 数据库表名称
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String dropIndexes(final String indexName, final String tableName);

    /**
     * 获取指定表的指定索引 SQL
     *
     * @param indexName - 索引名称
     * @param tableName - 数据库表名称
     * @return String - SQL语句
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/3 9:57
     */
    String countIndex(final String indexName, final String tableName);

}
