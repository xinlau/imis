package com.imis.base.util.table;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * <p>
 * ExtendJsonConvert<br>
 * 扩展Json转换
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public class ExtendJsonConvert {

    /**
     * json转换中的系统保留字
     */
    protected static Map<String, String> syscode = new HashMap<String, String>();

    static {
        syscode.put("class", "clazz");
    }

    /**
     * 处理否含有json转换中的保留字
     * @param field -
     * @param flag - 1:转换 2:还原
     * @return String -
     * @author XinLau
     * @since 2020/9/30 9:16
     * @creed The only constant is change ! ! !
     */
    private static String dealSysCode(String field, int flag) {
        String change = field;
        Iterator it = syscode.keySet().iterator();
        while (it.hasNext()) {
            String key = String.valueOf(it.next());
            String value = String.valueOf(syscode.get(key));
            if (flag == 1) {
                change = field.replaceAll(key, value);
            } else if (flag == 2) {
                change = field.replaceAll(value, key);
            }
        }
        return change;
    }

}
