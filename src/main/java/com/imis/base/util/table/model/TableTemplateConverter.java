package com.imis.base.util.table.model;

import com.imis.module.online.table.model.po.TableFields;
import com.imis.module.online.table.model.po.TableHead;
import com.imis.module.system.model.vo.SysDataSourceVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 数据库表模板 Converter 转换器
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Mapper
public interface TableTemplateConverter {

    /**
     * 数据库表模板 转换器实例
     */
    TableTemplateConverter INSTANCE = Mappers.getMapper(TableTemplateConverter.class);

    /**
     * 数据库表模型转换
     *
     * @param tableFormConfigModel - 数据库表的表单配置模型
     * @param dataBaseType         - 数据库类型
     * @return TableTemplateModel - 数据库表模板模型
     */
    @Mappings({
            @Mapping(target = "dataBaseType", source = "dataBaseType"),
            // @Mapping(target = "tableFieldList", expression = "java(com.imis.base.util.table.model.TableTemplateConverter.INSTANCE.modelConvert(tableFormConfigModel.columns))")
            @Mapping(target = "tableFieldList", source = "tableFormConfigModel.columns")
    })
    TableTemplateModel modelConvert(TableFormConfigModel tableFormConfigModel, String dataBaseType);

    /**
     * 数据库表字段模型转换
     *
     * @param tableFieldList - 数据库表字段信息
     * @return List<TableFieldTemplateModel> - 数据库表字段模板模型
     */
    @Mappings({})
    List<TableFieldTemplateModel> modelConvert(List<TableFields> tableFieldList);

    /**
     * 数据库表字段模型转换
     *
     * @param tableFields - 数据库表字段信息
     * @return TableFieldTemplateModel - 数据库表字段模板模型
     */
    @Mappings({
            @Mapping(target = "fieldDecimalDigits", source = "fieldPointLength"),
            @Mapping(target = "fieldDefaultValue", expression = "java(com.imis.base.util.ConvertUtils.judgeIsNumber(tableFields.getFieldDefaultValue()))"),
            @Mapping(target = "fieldIsNull", source = "isNull"),
            @Mapping(target = "fieldComment", source = "fieldTxt")
    })
    TableFieldTemplateModel modelConvert(TableFields tableFields);

    /**
     * 数据库表字段模型转换
     *
     * @param tableFields - 数据库表字段信息
     * @return ColumnMeta - 数据库表处理对象
     */
    @Mappings({
            @Mapping(target = "tableName", ignore = true),
            @Mapping(target = "primaryKeyType", ignore = true),
            @Mapping(target = "columnId", source = "tableFields.id"),
            @Mapping(target = "columnName", expression = "java(tableFields.getFieldName().toLowerCase())"),
            @Mapping(target = "oldColumnName", expression = "java(tableFields.getFieldNameOld() != null ? tableFields.getFieldNameOld().toLowerCase() : null)"),
            @Mapping(target = "columnSize", source = "tableFields.fieldLength"),
            @Mapping(target = "columnType", expression = "java(tableFields.getFieldType().toLowerCase())"),
            @Mapping(target = "columnComment", source = "tableFields.fieldTxt"),
            @Mapping(target = "columnDefaultValue", expression = "java(com.imis.base.util.ConvertUtils.judgeIsNumber(tableFields.getFieldDefaultValue()))"),
            @Mapping(target = "decimalDigits", source = "tableFields.fieldPointLength"),
            @Mapping(target = "isNullable", expression = "java(tableFields.getIsNull() == 1 ? \"Y\" : \"N\")")
    })
    ColumnMeta tableFieldModelConvert(TableFields tableFields);

    /**
     * 获取 TableFormConfigModel 配置表数据列
     *
     * @param tableFormConfigModel - 数据库表的表单配置模型
     * @return Map<String, ColumnMeta> - 数据库表处理对象
     */
    default Map<String, ColumnMeta> modelConvert(TableFormConfigModel tableFormConfigModel) {
        List<TableFields> tableFieldsList = tableFormConfigModel.getColumns();
        Map<String, ColumnMeta> columnMap = new HashMap<String, ColumnMeta>(tableFieldsList.size());
        tableFieldsList.forEach((tableFields) -> {
            ColumnMeta columnMeta = tableFieldModelConvert(tableFields);
            columnMeta.setTableName(tableFormConfigModel.getTableName());
            columnMeta.setPrimaryKeyType(tableFormConfigModel.getPrimaryKeyType());
            columnMap.put(tableFields.getFieldName(), columnMeta);
        });
        return columnMap;
    }

    /**
     * 系统数据源管理 转 数据库表数据源配置
     *
     * @param sysDataSource - 数据库表数据源配置
     * @return
     */
    @Mappings({
            @Mapping(target = "url", expression = "java(com.imis.base.util.PasswordUtil.decrypt(sysDataSource.getUrl()))")
    })
    TableDataBaseConfig dataSourceToTableDataBaseConfig(SysDataSourceVO sysDataSource);

    /**
     * 获取 TableFormConfigModel 配置表数据列
     *
     * @param tableHead     - 在线开发-数据库表表信息 PO
     * @param sysDataSource - 数据库表数据源配置
     * @param fieldList     - 在线开发-数据库表字段信息 PO
     * @return TableFormConfigModel - 数据库表的表单配置模型
     */
    @Mappings({
            @Mapping(target = "tableName", source = "tableHead.tableName"),
            @Mapping(target = "tableDescription", source = "tableHead.tableDescription"),
            @Mapping(target = "isSynchronize", ignore = true),
            @Mapping(target = "tableVersion", ignore = true),
            @Mapping(target = "tableType", ignore = true),
            @Mapping(target = "primaryKeyType", source = "tableHead.idType"),
            @Mapping(target = "primaryKeySequence", source = "tableHead.idSequence"),
            @Mapping(target = "relationType", ignore = true),
            @Mapping(target = "subTableChecklist", ignore = true),
            @Mapping(target = "tableOrder", ignore = true),
            @Mapping(target = "columns", source = "fieldList"),
            @Mapping(target = "indexes", ignore = true),
            @Mapping(target = "treeIdFieldName", ignore = true),
            @Mapping(target = "treeFieldName", ignore = true),
            @Mapping(target = "treeParentIdFieldName", ignore = true),
            @Mapping(target = "tableDataBaseConfig", source = "sysDataSource")
    })
    TableFormConfigModel modelConvert(TableHead tableHead, SysDataSourceVO sysDataSource, List<TableFields> fieldList);

}
