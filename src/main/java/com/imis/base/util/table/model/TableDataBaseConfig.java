package com.imis.base.util.table.model;

import lombok.Data;

/**
 * <p>
 * TableDataBaseConfig<br>
 * 数据库表数据源配置
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月27日 16:04
 */
@Data
public class TableDataBaseConfig {

    /**
     * url
     */
    private String url;

    /**
     * username
     */
    private String username;

    /**
     * password
     */
    private String password;

    /**
     * driverClassName
     */
    private String driverClassName;

}
