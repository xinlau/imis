package com.imis.base.util;

import com.imis.base.constant.HttpHeadersConstants;
import com.imis.base.constant.enums.ArgumentResponseEnum;
import com.imis.base.constant.enums.CommonResponseEnum;
import com.imis.base.shiro.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * CurrentUserUtils<br>
 * 获取当前用户相关 工具类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年04月19日 10:08
 */
@Slf4j
@Component
public class CurrentUserUtils {

    /**
     * 获取request中的TOKEN
     *
     * @return String - 用户账号
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/10 13:30
     */
    public static String getTokenFormHttpServletRequest(){
        String accessToken = SpringContextUtils.getHttpServletRequest().getHeader(HttpHeadersConstants.X_ACCESS_TOKEN);
        CommonResponseEnum.ERROR_NO_TOKEN.assertNotEmpty(accessToken);
        return accessToken;
    }

    /**
     * 获取request中的用户编号
     *
     * @return Long - 用户编号
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/10 13:30
     */
    public static Long getUserIdFormHttpServletRequest(){
        String userId = SpringContextUtils.getHttpServletRequest().getHeader(HttpHeadersConstants.X_ACCESS_USER_ID);
        if (ConvertUtils.isNotEmpty(userId)){
            return Long.parseLong(userId);
        }
        return null;
    }

    /**
     * 获取request中的角色编号
     *
     * @return Long - 角色编号
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/10 13:30
     */
    public static Long getRoleIdFormHttpServletRequest(){
        String roleId = SpringContextUtils.getHttpServletRequest().getHeader(HttpHeadersConstants.X_ACCESS_ROLE_ID);
        if (ConvertUtils.isNotEmpty(roleId)){
            return Long.parseLong(roleId);
        }
        return null;
    }

    /**
     * 获取request中的组织机构编号
     *
     * @return Long - 组织机构编号
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/10 13:30
     */
    public static Long getOrganizationIdFormHttpServletRequest(){
        String organizationId = SpringContextUtils.getHttpServletRequest().getHeader(HttpHeadersConstants.X_ACCESS_ORGANIZATION_ID);
        if (ConvertUtils.isNotEmpty(organizationId)){
            return Long.parseLong(organizationId);
        }
        return null;
    }

    /**
     * 根据request中的TOKEN获取用户账号
     *
     * @return String - 用户账号
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/10 13:30
     */
    public static String getUserNameByToken(){
        String username = JwtUtil.getUsername(getTokenFormHttpServletRequest());
        ArgumentResponseEnum.USER_LOGIN_ERR_NON.assertNotEmpty(username);
        return username;
    }

    /**
     * 获取当前访问对象的IP地址
     *
     * @return String
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/3/10 13:30
     */
    public static String getUserIp() {
        // 获取 request
        HttpServletRequest request = SpringContextUtils.getHttpServletRequest();
        if (ConvertUtils.isEmpty(request)) {
            return null;
        }
        return IPUtils.getClientIpAddress(request);
    }

}
