package com.imis.base.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Locale;

/**
 * spring restTemplate 工具类
 */

/**
 * <p>
 * RestTemplateUtil<br>
 * Spring RestTemplate 工具类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年06月13日 18:17
 */
public final class RestTemplateUtil {

    /**
     * 用户代理
     */
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";

    /**
     * 客户端Http请求工厂
     */
    private static SimpleClientHttpRequestFactory requestFactory = null;

    static {
        requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(10000);
        requestFactory.setReadTimeout(10000);
    }

    private RestTemplateUtil() {
    }

    private static HttpHeaders getHttpHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add(HttpHeaders.USER_AGENT, USER_AGENT);
        headers.setAcceptLanguage(Locale.LanguageRange.parse(Locale.CHINA.getLanguage()));
        return headers;
    }

    /**
     * get 请求
     *
     * @param remoteUrl
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getRequest(String remoteUrl, Class<T> clazz) {
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        HttpEntity<String> entity = new HttpEntity<>("parameters", getHttpHeader());
        return restTemplate.exchange(remoteUrl, HttpMethod.GET, entity, clazz).getBody();
    }

    /**
     * post 请求
     *
     * @param remoteUrl
     * @param clazz
     * @param params    post参数
     * @param <T>       返回值
     * @return
     */
    public static <T> T postMethod(String remoteUrl, Class<T> clazz, MultiValueMap<String, String> params) {
        HttpHeaders httpHeader = getHttpHeader();
        // 表单提交方式
        httpHeader.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, httpHeader);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate.exchange(remoteUrl, HttpMethod.POST, requestEntity, clazz).getBody();
    }

    /**
     * post 请求
     *
     * @param remoteUrl
     * @param clazz
     * @param params    post参数
     * @param <T>       返回值
     * @return
     */
    public static <T> T postForEntity(String remoteUrl, Class<T> clazz, JSONObject params) {
        HttpHeaders httpHeader = getHttpHeader();
        // 表单提交方式
        httpHeader.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> formEntity = new HttpEntity<>(params.toString(), httpHeader);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate.postForEntity(remoteUrl, formEntity, clazz).getBody();
    }

}
