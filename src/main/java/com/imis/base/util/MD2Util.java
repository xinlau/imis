package com.imis.base.util;

import com.imis.base.constant.CommonConstant;
import lombok.extern.slf4j.Slf4j;

import java.security.MessageDigest;

/**
 * <p>
 * MD2Util<br>
 * MD2加密工具
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年07月21日 09:57
 */
@Slf4j
public class MD2Util {

    private static String byteArrayToHexString(byte[] b) {
        StringBuilder resultSb = new StringBuilder();
        for (byte value : b) {
            resultSb.append(byteToHexString(value));
        }
        return resultSb.toString();
    }

    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n += 256;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return String.valueOf(CommonConstant.HEX_DIGITS[d1]) + CommonConstant.HEX_DIGITS[d2];
    }

    /**
     * MD2加密
     *
     * @param origin      - 源
     * @param charsetName - 字符集名称
     * @return String -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/7/31 11:34
     */
    public static String md2Encode(String origin, String charsetName) {
        String resultString = null;
        try {
            resultString = origin;
            MessageDigest md = MessageDigest.getInstance(CommonConstant.MD2);
            if (ConvertUtils.isEmpty(charsetName)) {
                resultString = byteArrayToHexString(md.digest(resultString.getBytes()));
            } else {
                resultString = byteArrayToHexString(md.digest(resultString.getBytes(charsetName)));
            }
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
        return resultString;
    }

}
