package com.imis.base.util.form.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.ControlTypeConstant;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.form.jsonschema.BaseColumn;
import com.imis.base.util.form.jsonschema.BaseCommonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * LinkDownPropertyBase<br>
 * 级联下拉 属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LinkDownPropertyBase extends BaseCommonProperty {

    /**
     * 配置信息
     */
    String dictTable;

    /**
     * 级联下拉组件 的其他级联列
     */
    List<BaseColumn> otherColumns;

    public LinkDownPropertyBase() {
    }

    /**
     * 构造器
     */
    public LinkDownPropertyBase(String key, String title, String dictTable) {
        this.type = DataBaseConstant.STRING;
        this.view = ControlTypeConstant.LINK_DOWN;
        this.key = key;
        this.title = title;
        this.dictTable = dictTable;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(4);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        JSONObject temp = JSONObject.parseObject(this.dictTable);
        prop.put("config", temp);
        prop.put("others", otherColumns);
        map.put("prop", prop);
        return map;
    }
}
