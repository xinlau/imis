package com.imis.base.util.form.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.util.form.jsonschema.BaseCommonProperty;
import com.imis.module.system.model.vo.DictVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * SwitchPropertyBase<br>
 * 数字 属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NumberPropertyBase extends BaseCommonProperty {

    private static final long serialVersionUID = -558615331436437200L;

    /**
     * 倍数
     * 验证实例是否为此数值的倍数
     * “multipleOf”的值必须是一个数字，严格大于0。
     */
    private Integer multipleOf;
    /**
     * 小于等于
     * “maximum”的值必须是一个数字，表示数字实例的包含上限。
     * 如果实例是数字，则仅当实例小于或等于“最大”时，此关键字才会生效。
     */
    private Integer maximum;
    /**
     * 小于
     * “exclusiveMaximum”的值必须是数字，表示数字实例的独占上限。
     * 如果实例是数字，则实例仅在其值严格小于（不等于）“exclusiveMaximum”时才有效。
     */
    private Integer exclusiveMaximum;
    /**
     * 大于等于
     */
    private Integer minimum;
    /**
     * 大于等于
     */
    private Integer exclusiveMinimum;
    /**
     * 根据ECMA 262正则表达式方言，该字符串应该是有效的正则表达式。
     */
    private String pattern;

    public NumberPropertyBase() {
    }

    /**
     * 构造器
     *
     * @param key   字段名
     * @param title 字段备注
     * @param type  number和integer
     */
    public NumberPropertyBase(String key, String title, String type) {
        this.key = key;
        this.type = type;
        this.title = title;
        this.view = "number";
    }

    /**
     * 列表类型的走这个构造器  字典里存储的都是字符串 没法走这个构造器
     *
     * @param key
     * @param title
     * @param view    list-checkbox-radio
     * @param include
     */
    public NumberPropertyBase(String key, String title, String view, List<DictVO> include) {
        this.type = "integer";
        this.key = key;
        this.view = view;
        this.title = title;
        this.include = include;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(8);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        if (multipleOf != null) {
            prop.put("multipleOf", multipleOf);
        }
        if (maximum != null) {
            prop.put("maximum", maximum);
        }
        if (exclusiveMaximum != null) {
            prop.put("exclusiveMaximum", exclusiveMaximum);
        }
        if (minimum != null) {
            prop.put("minimum", minimum);
        }
        if (exclusiveMinimum != null) {
            prop.put("exclusiveMinimum", exclusiveMinimum);
        }
        if (pattern != null) {
            prop.put("pattern", pattern);
        }
        map.put("prop", prop);
        return map;
    }
}
