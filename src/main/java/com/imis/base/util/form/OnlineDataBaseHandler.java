package com.imis.base.util.form;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.DataBaseConstant;
import com.imis.module.online.table.model.po.TableFields;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * OnlineDataBaseHandler<br>
 * 数据库处理
 * </p>
 *
 * @author XinLau
 * @since 2020-03-12
 */
public class OnlineDataBaseHandler {

    /**
     * 判断是否是数值类型
     *
     * @param dataBaseType - 数据库类型
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/20 10:30
     */
    public static Boolean isNumberField(String dataBaseType) {
        // 新增判断是否是数值类型另一条件
        return DataBaseConstant.INT.equals(dataBaseType) || DataBaseConstant.DOUBLE.equals(dataBaseType) || DataBaseConstant.BIG_DECIMAL.equals(dataBaseType) || DataBaseConstant.INTEGER.equals(dataBaseType);
    }

    /**
     * 判断是否日期类型
     *
     * @param dataBaseType - 数据库类型
     * @return Boolean
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/20 10:30
     */
    public static boolean isDateField(String dataBaseType) {
        return DataBaseConstant.DATE.equalsIgnoreCase(dataBaseType) || DataBaseConstant.DATE_TIME_.equalsIgnoreCase(dataBaseType) || DataBaseConstant.TIMESTAMP.equalsIgnoreCase(dataBaseType);
    }

    /**
     * 获取 Mybatis 的 value 表达式 如 #{name,jdbcType=VARCHAR}
     *
     * @param dataBaseType - 数据库类型
     * @param tableFields  - 数据库表字段信息
     * @param requestJson  - JSONObject
     * @param params       - 参数
     * @return String - Mybatis 的 value 表达式
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/20 10:30
     */
    public static String initMybatisValueWithJdbcType(String dataBaseType, TableFields tableFields, JSONObject requestJson, Map<String, Object> params) {
        // string/int/double/Date/BigDecimal/Text/Blob
        String fieldType = tableFields.getFieldType();
        String fieldName = tableFields.getFieldName();
        // date/datetime/text/radio/checkbox/textarea....主要是处理date/datetime
        String fieldShowType = tableFields.getFieldShowType();
        // online表单编辑，值设置为空问题解决(主要时间、BigDecimal类型，设置值为 null )
        if (requestJson.get(fieldName) == null) {
            return "null";
        }
        if (DataBaseConstant.INT.equals(fieldType)) {
            // int
            params.put(fieldName, requestJson.getIntValue(fieldName));
            return DataBaseConstant.HASH_LEFT_BRACE + fieldName + ",jdbcType=INTEGER}";
        } else if (DataBaseConstant.DOUBLE.equals(fieldType)) {
            // double
            params.put(fieldName, requestJson.getDoubleValue(fieldName));
            return DataBaseConstant.HASH_LEFT_BRACE + fieldName + ",jdbcType=DOUBLE}";
        } else if (DataBaseConstant.BIG_DECIMAL.equals(fieldType)) {
            // BigDecimal
            params.put(fieldName, new BigDecimal(requestJson.getString(fieldName)));
            return DataBaseConstant.HASH_LEFT_BRACE + fieldName + ",jdbcType=DECIMAL}";
        } else if (DataBaseConstant.BLOB.equals(fieldType)) {
            // 支持 Blob 字段保存
            params.put(fieldName, requestJson.getString(fieldName) != null ? requestJson.getString(fieldName).getBytes() : null);
            return DataBaseConstant.HASH_LEFT_BRACE + fieldName + ",jdbcType=BLOB}";
        } else if (DataBaseConstant.DATE.equals(fieldType)) {
            // Date
            String fieldValue = requestJson.getString(fieldName);
            final String value = fieldValue.length() > 10 ? fieldValue.substring(0, 10) : fieldValue;
            if (DataBaseConstant.DB_TYPE_ORACLE.equals(dataBaseType)) {
                if (DataBaseConstant.DATE_.equals(fieldShowType)) {
                    params.put(fieldName, value);
                    return "to_date(#{" + fieldName + "},'yyyy-MM-dd')";
                } else {
                    params.put(fieldName, fieldValue.length() == 10 ? requestJson.getString(fieldName) + " 00:00:00" : fieldValue);
                    return "to_date(#{" + fieldName + "},'yyyy-MM-dd HH24:mi:ss')";
                }
            } else if (DataBaseConstant.DB_TYPE_POSTGRE_SQL.equals(dataBaseType)) {
                if (DataBaseConstant.DATE_.equals(fieldShowType)) {
                    params.put(fieldName, value);
                } else {
                    params.put(fieldName, fieldValue.length() == 10 ? requestJson.getString(fieldName) + " 00:00:00" : fieldValue);
                }
                return "CAST(#{" + fieldName + "} AS TIMESTAMP)";
            } else {
                params.put(fieldName, requestJson.getString(fieldName));
                return DataBaseConstant.HASH_LEFT_BRACE + fieldName + DataBaseConstant.RIGHT_BRACE;
            }
        } else {
            // String
            params.put(fieldName, requestJson.getString(fieldName));
            return DataBaseConstant.HASH_LEFT_BRACE + fieldName + ",jdbcType=VARCHAR}";
        }
    }

}
