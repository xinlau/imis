package com.imis.base.util.form.model;

import lombok.Data;

import java.util.Date;

/**
 * <p>
 * DataPermissionRuleModel<br>
 * 数据权限规则模型
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月16日 15:40
 */
@Data
public class DataPermissionRuleModel {

    /**
     * id
     */
    private String id;
    /**
     * 对应的菜单id
     */
    private String permissionId;
    /**
     * 规则名称
     */
    private String ruleName;
    /**
     * 字段
     */
    private String ruleColumn;
    /**
     * 条件
     */
    private String ruleConditions;
    /**
     * 规则值
     */
    private String ruleValue;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改人
     */
    private String updateBy;

}
