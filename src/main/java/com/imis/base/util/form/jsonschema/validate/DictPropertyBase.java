package com.imis.base.util.form.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.ControlTypeConstant;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.form.jsonschema.BaseCommonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * DictPropertyBase<br>
 * 字典 属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DictPropertyBase extends BaseCommonProperty {

    private static final long serialVersionUID = 3786503639885610767L;

    /**
     * 字典编码
     */
    private String dictCode;
    /**
     * 字典表
     */
    private String dictTable;
    /**
     * 字典字段
     */
    private String dictText;

    public DictPropertyBase() {
    }

    /**
     * 构造器
     */
    public DictPropertyBase(String key, String title, String dictTable, String dictCode, String dictText) {
        this.type = DataBaseConstant.STRING;
        this.view = ControlTypeConstant.SELECT_SEARCH;
        this.key = key;
        this.title = title;
        this.dictCode = dictCode;
        this.dictTable = dictTable;
        this.dictText = dictText;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(16);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        if (dictCode != null) {
            prop.put("dictCode", dictCode);
        }
        if (dictTable != null) {
            prop.put("dictTable", dictTable);
        }
        if (dictText != null) {
            prop.put("dictText", dictText);
        }
        map.put("prop", prop);
        return map;
    }

}
