package com.imis.base.util.form.model;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * LinkDown<br>
 * 下拉联动组件
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月01日 13:39
 */
@Data
public class LinkDown implements Serializable {

    private static final long serialVersionUID = 4740166316629191651L;

    /**
     * 字典code
     */
    private String dictField;

    /**
     * 字典表
     */
    private String dictTable;

    /**
     * 字典Text
     */
    private String dictText;

    /**
     *
     */
    private String linkField;

    /**
     * ID
     */
    private String idField;

    /**
     * 父ID
     */
    private String parentIdField;

    /**
     * 父ID值
     */
    private String parentIdValue;

    /**
     * 条件
     */
    private String conditionSql;

}
