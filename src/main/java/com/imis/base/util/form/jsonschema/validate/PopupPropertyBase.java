package com.imis.base.util.form.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.ControlTypeConstant;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.form.jsonschema.BaseCommonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * PopupPropertyBase<br>
 * 弹出框 属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PopupPropertyBase extends BaseCommonProperty {

    private static final long serialVersionUID = -3200493311633999539L;

    private String popupCode;

    private String destFields;

    private String orgFields;

    public PopupPropertyBase() {
    }

    public PopupPropertyBase(String key, String title, String popupCode, String destFields, String orgFields) {
        this.view = ControlTypeConstant.POPUP;
        this.type = DataBaseConstant.STRING;
        this.key = key;
        this.title = title;
        this.popupCode = popupCode;
        this.destFields = destFields;
        this.orgFields = orgFields;
    }


    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(5);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        if (popupCode != null) {
            prop.put("popupCode", popupCode);
        }
        if (destFields != null) {
            prop.put("destFields", destFields);
        }
        if (orgFields != null) {
            prop.put("orgFields", orgFields);
        }
        map.put("prop", prop);
        return map;
    }

}
