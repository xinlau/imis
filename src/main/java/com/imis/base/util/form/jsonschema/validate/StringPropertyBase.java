package com.imis.base.util.form.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.form.jsonschema.BaseCommonProperty;
import com.imis.module.system.model.vo.DictVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * SwitchPropertyBase<br>
 * 字符串 属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StringPropertyBase extends BaseCommonProperty {

    private static final long serialVersionUID = -3200493311633999539L;

    /**
     * 最大长度
     */
    private Integer maxLength;
    /**
     * 最小长度
     */
    private Integer minLength;
    /**
     * 根据ECMA 262正则表达式方言，该字符串应该是有效的正则表达式。
     */
    private String pattern;
    /**
     * 错误提示信息
     */
    private String errorInfo;

    public StringPropertyBase() {
    }

    /**
     * 一般字符串类型走这个构造器
     *
     * @param key       字段名
     * @param title     字段备注
     * @param view      展示控件
     * @param maxLength 数据库字段最大长度
     */
    public StringPropertyBase(String key, String title, String view, Integer maxLength) {
        this.maxLength = maxLength;
        this.key = key;
        this.view = view;
        this.title = title;
        this.type = DataBaseConstant.STRING;
    }

    /**
     * 列表类型的走这个构造器
     *
     * @param key       - 字段名
     * @param title     - 字段备注
     * @param view      - 展示控件 list-checkbox-radio
     * @param maxLength - 数据库字段最大长度
     * @param include   - 数据字典
     */
    public StringPropertyBase(String key, String title, String view, Integer maxLength, List<DictVO> include) {
        this.maxLength = maxLength;
        this.key = key;
        this.view = view;
        this.title = title;
        this.type = DataBaseConstant.STRING;
        this.include = include;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(6);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        if (maxLength != null) {
            prop.put("maxLength", maxLength);
        }
        if (minLength != null) {
            prop.put("minLength", minLength);
        }
        if (pattern != null) {
            prop.put("pattern", pattern);
        }
        if (errorInfo != null) {
            prop.put("errorInfo", errorInfo);
        }
        map.put("prop", prop);
        return map;
    }

}
