package com.imis.base.util.form.jsonschema;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.imis.module.system.model.vo.DictVO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * BaseCommonProperty<br>
 * 通用 属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Data
public abstract class BaseCommonProperty implements Serializable {

    private static final long serialVersionUID = -426159949502493187L;

    protected String key;

    /**
     * <p>此关键字的值必须是字符串或数组。如果它是一个数组，那么数组的元素必须是字符串，并且必须是唯一的。
     * <p>字符串值必须是六种基本类型之一（“null”，“boolean”，“object”，“array”，“number”或“string”），或“integer”，它匹配任何数字，零分数部分。
     * <p>当且仅当实例位于为此关键字列出的任何集合中时，实例才会验证。
     */
    protected String type;

    /**
     * 对应JsonSchema的enum
     * <p>该关键字的值必须是一个数组。这个数组应该至少有一个元素。数组中的元素应该是唯一的。如果实例的值等于此关键字的数组值中的某个元素，则实例将对此关键字成功验证。
     * 数组中的元素可以是任何值，包括null
     * <p>
     * {
     * "type": "string",
     * "enum": ["1", "2", "3"] 需要的话可以通过这个include转一下
     * }
     */
    protected List<DictVO> include;
    /**
     * 对应JsonSchema的const
     * <p>此关键字的值可以是任何类型，包括null。
     * 如果实例的值等于关键字的值，则实例将针对此关键字成功验证。
     */
    protected Object constant;
    /**
     * 展示类型
     */
    protected String view;
    /**
     * 数据库字段备注
     */
    protected String title;
    /**
     * 字段显示排序
     */
    protected Integer order;
    /**
     * 是否禁用
     */
    protected boolean disabled;
    /**
     * 字段默认值
     */
    protected String defaultValue;

    /**
     * 返回一个map有两个key
     * <P>key ---> Property JSON的key
     * <P>prop --> JSON object
     *
     * @return
     */
    public abstract Map<String, Object> getPropertyJson();

    public JSONObject getCommonJson() {
        JSONObject json = new JSONObject();
        json.put("type", type);
        if (include != null && include.size() > 0) {
            json.put("enum", include);
        }
        if (constant != null) {
            json.put("constant", constant);
        }
        if (title != null) {
            json.put("title", title);
        }
        if (order != null) {
            json.put("order", order);
        }
        if (view == null) {
            json.put("view", "input");
        } else {
            json.put("view", view);
        }
        if (disabled) {
            String str = "{\"widgetattrs\":{\"disabled\":true}}";
            JSONObject ui = JSONObject.parseObject(str);
            json.put("ui", ui);
        }
        if (StringUtils.isNotBlank(defaultValue)) {
            json.put("defaultValue", defaultValue);
        }
        return json;
    }


}
