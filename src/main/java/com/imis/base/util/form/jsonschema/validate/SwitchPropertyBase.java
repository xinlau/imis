package com.imis.base.util.form.jsonschema.validate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.ControlTypeConstant;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.form.jsonschema.BaseCommonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * SwitchPropertyBase<br>
 * 开关 属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SwitchPropertyBase extends BaseCommonProperty {

    /**
     * 扩展参数配置信息
     */
    private String extendString;

    public SwitchPropertyBase() {
    }

    /**
     * 构造器
     */
    public SwitchPropertyBase(String key, String title, String extendString) {
        this.type = DataBaseConstant.STRING;
        this.view = ControlTypeConstant.SWITCH;
        this.key = key;
        this.title = title;
        this.extendString = extendString;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(16);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        JSONArray array = new JSONArray();
        if (extendString != null) {
            array = JSONArray.parseArray(extendString);
            prop.put("extendOption", array);
        }
        map.put("prop", prop);
        return map;
    }

}
