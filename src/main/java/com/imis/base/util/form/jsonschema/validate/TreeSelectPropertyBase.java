package com.imis.base.util.form.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.ControlTypeConstant;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.form.jsonschema.BaseCommonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * LinkDownPropertyBase<br>
 * 树控件 属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TreeSelectPropertyBase extends BaseCommonProperty {

    private static final long serialVersionUID = 3786503639885610767L;
    /**
     * 表名,文本,id
     */
    private String dict;
    /**
     * 父级字段 默认 parent_id
     */
    private String parentField;
    /**
     * 父级节点的值 暂时没用到 默认为0
     */
    private String parentValue;
    /**
     * 子级字段
     */
    private String hasChildField;
    /**
     * 树形下拉保存text值的字段名
     */
    private String textField;
    /**
     * 是不是 parent 组件 1是 0否
     */
    private Integer parentComponent = 0;

    public TreeSelectPropertyBase() {
    }

    /**
     * 构造器 构造普通树形下拉
     */
    public TreeSelectPropertyBase(String key, String title, String dict, String parentField, String parentValue) {
        this.type = DataBaseConstant.STRING;
        this.view = ControlTypeConstant.SELECT_TREE;
        this.key = key;
        this.title = title;
        this.dict = dict;
        this.parentField = parentField;
        this.parentValue = parentValue;
    }

    /**
     * 分类字典下拉专用
     *
     * @param key
     * @param title
     * @param parentValue
     */
    public TreeSelectPropertyBase(String key, String title, String parentValue) {
        this.type = DataBaseConstant.STRING;
        this.view = ControlTypeConstant.DICT_TREE;
        this.key = key;
        this.title = title;
        this.parentValue = parentValue;
    }

    /**
     * 分类字典 支持存储text 下拉专用
     *
     * @param key
     * @param title
     * @param parentValue
     * @param textField
     */
    public TreeSelectPropertyBase(String key, String title, String parentValue, String textField) {
        this(key, title, parentValue);
        this.textField = textField;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(8);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        if (dict != null) {
            prop.put("dict", dict);
        }
        if (parentField != null) {
            prop.put("parentField", parentField);
        }
        if (parentValue != null) {
            prop.put("parentValue", parentValue);
        }
        if (textField != null) {
            prop.put("textField", textField);
        }
        if (hasChildField != null) {
            prop.put("hasChildField", hasChildField);
        }
        if (parentComponent != null) {
            prop.put("parentComponent", parentComponent);
        }
        map.put("prop", prop);
        return map;
    }

}
