package com.imis.base.util.form.jsonschema;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * JsonSchema 模式类</br>
 * < http://json-schema.org/draft-07/schema# >
 * </p>
 *
 * @author XinLau
 * @since 2020-10-03
 */
@Data
public class JsonSchemaDescription implements Serializable {

    private static final long serialVersionUID = 7682073117441544718L;

    /**
     * schema
     */
    private String schema = "http://json-schema.org/draft-07/schema#";
    /**
     * 用它给我们的模式提供了标题。
     */
    private String title;
    /**
     * 关于模式的描述。
     */
    private String description;
    /**
     * type 关键字在我们的 JSON 数据上定义了第一个约束：必须是一个 JSON 对象。 可以直接设置成 object
     */
    private String type;

    private List<String> required;

    public String getSchema() {
        return schema;
    }

    public JsonSchemaDescription() {
    }

    public JsonSchemaDescription(List<String> required) {
        this.description = "我是一个 jsonschema description";
        this.title = "我是一个 jsonschema title";
        this.type = "object";
        this.required = required;
    }

}
