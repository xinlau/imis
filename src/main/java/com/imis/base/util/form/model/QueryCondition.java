package com.imis.base.util.form.model;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * QueryCondition<br>
 * 查询条件对象
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年09月01日 13:39
 */
@Data
public class QueryCondition implements Serializable {

    private static final long serialVersionUID = 4740166316629191651L;

    /**
     * 字段
     */
    private String field;
    /**
     * 类型
     */
    private String type;
    /**
     * 规则
     */
    private String rule;
    /**
     * 值
     */
    private String value;


    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        if (field == null || "".equals(field)) {
            return "";
        }
        stringBuffer.append(this.field).append(" ").append(this.rule).append(" ").append(this.type).append(" ").append(this.value);
        return stringBuffer.toString();
    }
}
