package com.imis.base.util.form;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.CommonConstant;
import com.imis.base.globle.exception.JavaEnhanceException;
import com.imis.base.util.ConvertUtils;
import com.imis.base.util.CustomizeClassLoader;
import com.imis.base.util.SpringContextUtils;
import com.imis.module.online.form.enhance.JavaEnhanceInterface;
import com.imis.module.online.form.enhance.JavaEnhanceListInterface;
import com.imis.module.online.form.model.po.FormEnhanceJava;
import com.imis.module.online.table.model.po.TableHead;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * EnhanceJavaUtil<br>
 * JAVA 增强工具类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年11月20日 14:53
 */
@Slf4j
public class EnhanceJavaUtil {

    /**
     * 根据 FormEnhanceJava 对象获取 JAVA 增强对象
     *
     * @param enhance - 在线开发-Java增强 PO
     * @return Object - JAVA 增强对象
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    public static Object getJavaEnhanceInterface(FormEnhanceJava enhance) {
        if (enhance == null) {
            return null;
        }
        // 类型
        String javaType = enhance.getJavaType();
        // 类名
        String javaValue = enhance.getJavaValue();
        Object aClass = null;
        if (ConvertUtils.isNotEmpty(javaValue)) {
            if (CommonConstant.CLASS.equals(javaType)) {
                try {
                    aClass = CustomizeClassLoader.getClassByClassName(javaValue).newInstance();
                } catch (InstantiationException e) {
                    log.error(e.getMessage(), e);
                } catch (IllegalAccessException e) {
                    log.error(e.getMessage(), e);
                }
            } else if (CommonConstant.SPRING.equals(javaType)) {
                aClass = SpringContextUtils.getBean(javaValue);
            }
        }
        return aClass;
    }

    /**
     * 执行 JAVA 增强接口定义（批量数据处理）
     *
     * @param tableHead - 在线开发-数据库表表信息 PO
     * @param enhance   - 在线开发-Java增强 PO
     * @param json      - 数据
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    public static Integer executeJavaEnhanceInterface(final TableHead tableHead, final FormEnhanceJava enhance, final JSONObject json) throws JavaEnhanceException {
        Object javaEnhance = getJavaEnhanceInterface(enhance);
        if (javaEnhance != null && javaEnhance instanceof JavaEnhanceInterface) {
            // JAVA 增强接口定义
            JavaEnhanceInterface javaInter = (JavaEnhanceInterface) javaEnhance;
            // 执行
            return javaInter.execute(tableHead.getTableName(), json);
        }
        return CommonConstant.OL_IMPORT_INSERT;
    }

    /**
     * 执行 JAVA 增强接口定义（批量数据处理）
     *
     * @param tableHead - 在线开发-数据库表表信息 PO
     * @param enhance   - 在线开发-Java增强 PO
     * @param dataList  - 批量数据
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/6 11:07
     */
    public static Integer executeJavaEnhanceListInterface(final TableHead tableHead, final FormEnhanceJava enhance, final List<Map<String, Object>> dataList) throws JavaEnhanceException {
        Object javaEnhance = getJavaEnhanceInterface(enhance);
        if (javaEnhance != null && javaEnhance instanceof JavaEnhanceListInterface) {
            // JAVA 增强接口定义（批量数据处理）
            JavaEnhanceListInterface javaInter = (JavaEnhanceListInterface) javaEnhance;
            // 执行
            return javaInter.execute(tableHead.getTableName(), dataList);
        }
        return CommonConstant.OL_IMPORT_INSERT;
    }

}
