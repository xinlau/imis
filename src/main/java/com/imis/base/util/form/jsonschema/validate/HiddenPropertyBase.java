package com.imis.base.util.form.jsonschema.validate;

import com.alibaba.fastjson.JSONObject;
import com.imis.base.constant.ControlTypeConstant;
import com.imis.base.constant.DataBaseConstant;
import com.imis.base.util.form.jsonschema.BaseCommonProperty;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * HiddenPropertyBase<br>
 *  属性
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@EqualsAndHashCode(callSuper = true)
public class HiddenPropertyBase extends BaseCommonProperty {

    private static final long serialVersionUID = -8939298551502162479L;

    public HiddenPropertyBase() {
    }

    public HiddenPropertyBase(String key, String title) {
        this.type = DataBaseConstant.STRING;
        this.view = ControlTypeConstant.HIDDEN;
        this.key = key;
        this.title = title;
    }

    @Override
    public Map<String, Object> getPropertyJson() {
        Map<String, Object> map = new HashMap<>(3);
        map.put("key", getKey());
        JSONObject prop = getCommonJson();
        prop.put("hidden", true);
        map.put("prop", prop);
        return map;
    }

}
