package com.imis.base.util;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * SendSmsEnum<br>
 * 阿里短信模板枚举
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年04月15日 11:54
 */
public enum SendSmsEnum {

    /**
     * 登录短信模板
     */
    LOGIN_TEMPLATE_CODE("SMS_187747708", "泰安政企直通车平台", "code");

    /**
     * 短信模板ID。请在控制台模板管理页面模板CODE一列查看。
     */
    @Getter
    @Setter
    private String templateCode;
    /**
     * 短信签名名称。请在控制台签名管理页面签名名称一列查看。
     */
    @Getter
    @Setter
    private String signName;
    /**
     * 短信模板必需的数据名称，多个key以逗号分隔，此处配置作为校验
     */
    @Getter
    @Setter
    private String keys;

    SendSmsEnum(String templateCode, String signName, String keys) {
        this.templateCode = templateCode;
        this.signName = signName;
        this.keys = keys;
    }

    public static SendSmsEnum toEnum(String templateCode) {
        if (ConvertUtils.isEmpty(templateCode)) {
            return null;
        }
        for (SendSmsEnum item : SendSmsEnum.values()) {
            if (item.getTemplateCode().equals(templateCode)) {
                return item;
            }
        }
        return null;
    }

}

