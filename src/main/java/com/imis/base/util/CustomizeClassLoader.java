package com.imis.base.util;

import com.imis.base.constant.enums.CommonResponseEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * CustomizeClassLoader<br>
 * 自定义类加载器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年10月23日 17:28
 */
@Slf4j
public class CustomizeClassLoader extends ClassLoader {

    /**
     * 通过类名获取类
     *
     * @param className - 类名称
     * @return Class -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/23 17:30
     */
    public static Class getClassByClassName(String className) {
        Class aClass = null;
        try {
            aClass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage(), e);
            CommonResponseEnum.ERROR_500.assertFail(className + " not found!");
        }
        return aClass;
    }

    /**
     * 获得类的全名，包括包名
     *
     * @param object - 类
     * @return String - 类的全名
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/10/23 17:30
     */
    public static String getPackPath(Object object) {
        // 检查用户传入的参数是否为空
        CommonResponseEnum.ERROR_500.assertNotNull(object, "参数不能为空！");
        // 获得类的全名，包括包名
        return object.getClass().getName();
    }

}
