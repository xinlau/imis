package com.imis.base.util;

import com.imis.base.constant.CommonConstant;
import com.imis.base.constant.enums.BrowserTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * BrowserUtils<br>
 * 浏览器 工具类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
@Slf4j
public class BrowserUtils {

    private final static String IE11 = "rv:11.0";
    private final static String IE10 = "MSIE 10.0";
    private final static String IE9 = "MSIE 9.0";
    private final static String IE8 = "MSIE 8.0";
    private final static String IE7 = "MSIE 7.0";
    private final static String IE6 = "MSIE 6.0";
    private final static String MAXTHON = "Maxthon";
    private final static String QQ = "QQBrowser";
    private final static String GREEN = "GreenBrowser";
    private final static String SE360 = "360SE";
    private final static String FIREFOX = "Firefox";
    private final static String OPERA = "Opera";
    private final static String CHROME = "Chrome";
    private final static String SAFARI = "Safari";
    private final static String CAMINO = "Camino";
    private final static String OTHER = "其它";
    private final static String ZH = "zh";
    private final static String ZH_CN = "zh-cn";
    private final static String EN = "en";
    private final static String EN_US = "en";
    private static Map<String, String> langMap = new HashMap<>();

    static {
        langMap.put(ZH, ZH_CN);
        langMap.put(EN, EN_US);
    }

    /**
     * 判断是否是IE
     *
     * @param request -
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    public static Boolean isIe(HttpServletRequest request) {
        return (request.getHeader(HttpHeaders.USER_AGENT).toLowerCase().indexOf("msie") > 0 || request
                .getHeader(HttpHeaders.USER_AGENT).toLowerCase().indexOf("rv:11.0") > 0) ? Boolean.TRUE
                : Boolean.FALSE;
    }

    /**
     * 获取IE版本
     *
     * @param request -
     * @return Double -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    public static Double getIeVersion(HttpServletRequest request) {
        double version = 0.0;
        if (getBrowserTypeEnum(request, IE11)) {
            version = 11.0;
        } else if (getBrowserTypeEnum(request, IE10)) {
            version = 10.0;
        } else if (getBrowserTypeEnum(request, IE9)) {
            version = 9.0;
        } else if (getBrowserTypeEnum(request, IE8)) {
            version = 8.0;
        } else if (getBrowserTypeEnum(request, IE7)) {
            version = 7.0;
        } else if (getBrowserTypeEnum(request, IE6)) {
            version = 6.0;
        }
        return version;
    }

    /**
     * 获取浏览器类型
     *
     * @param request -
     * @return BrowserTypeEnum -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    public static BrowserTypeEnum getBrowserTypeEnum(HttpServletRequest request) {
        BrowserTypeEnum browserType = null;
        if (getBrowserTypeEnum(request, IE11)) {
            browserType = BrowserTypeEnum.IE11;
        }
        if (getBrowserTypeEnum(request, IE10)) {
            browserType = BrowserTypeEnum.IE10;
        }
        if (getBrowserTypeEnum(request, IE9)) {
            browserType = BrowserTypeEnum.IE9;
        }
        if (getBrowserTypeEnum(request, IE8)) {
            browserType = BrowserTypeEnum.IE8;
        }
        if (getBrowserTypeEnum(request, IE7)) {
            browserType = BrowserTypeEnum.IE7;
        }
        if (getBrowserTypeEnum(request, IE6)) {
            browserType = BrowserTypeEnum.IE6;
        }
        if (getBrowserTypeEnum(request, FIREFOX)) {
            browserType = BrowserTypeEnum.Firefox;
        }
        if (getBrowserTypeEnum(request, SAFARI)) {
            browserType = BrowserTypeEnum.Safari;
        }
        if (getBrowserTypeEnum(request, CHROME)) {
            browserType = BrowserTypeEnum.Chrome;
        }
        if (getBrowserTypeEnum(request, OPERA)) {
            browserType = BrowserTypeEnum.Opera;
        }
        if (getBrowserTypeEnum(request, CAMINO)) {
            browserType = BrowserTypeEnum.Camino;
        }
        return browserType;
    }

    /**
     * 获取浏览器类型
     *
     * @param request     -
     * @param browserType - 浏览器类型
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    private static Boolean getBrowserTypeEnum(HttpServletRequest request,
                                              String browserType) {
        return request.getHeader(HttpHeaders.USER_AGENT).toLowerCase()
                .indexOf(browserType) > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * 检查浏览器类型
     *
     * @param request -
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    public static String checkBrowse(HttpServletRequest request) {
        String userAgent = request.getHeader(HttpHeaders.USER_AGENT);
        if (regex(OPERA, userAgent)) {
            return OPERA;
        }
        if (regex(CHROME, userAgent)) {
            return CHROME;
        }
        if (regex(FIREFOX, userAgent)) {
            return FIREFOX;
        }
        if (regex(SAFARI, userAgent)) {
            return SAFARI;
        }
        if (regex(SE360, userAgent)) {
            return SE360;
        }
        if (regex(GREEN, userAgent)) {
            return GREEN;
        }
        if (regex(QQ, userAgent)) {
            return QQ;
        }
        if (regex(MAXTHON, userAgent)) {
            return MAXTHON;
        }
        if (regex(IE11, userAgent)) {
            return IE11;
        }
        if (regex(IE10, userAgent)) {
            return IE10;
        }
        if (regex(IE9, userAgent)) {
            return IE9;
        }
        if (regex(IE8, userAgent)) {
            return IE8;
        }
        if (regex(IE7, userAgent)) {
            return IE7;
        }
        if (regex(IE6, userAgent)) {
            return IE6;
        }
        return OTHER;
    }

    /**
     * 检查浏览器类型
     *
     * @param regex -
     * @param str   -
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    public static boolean regex(String regex, String str) {
        Pattern p = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher m = p.matcher(str);
        return m.find();
    }

    /**
     * 获取浏览器语言
     *
     * @param request -
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    public static String getBrowserLanguage(HttpServletRequest request) {
        String browserLang = request.getLocale().getLanguage();
        String browserLangCode = langMap.get(browserLang);
        if (browserLangCode == null) {
            browserLangCode = EN_US;
        }
        return browserLangCode;
    }

    /**
     * 判断请求是否来自电脑端
     *
     * @param request -
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    public static Boolean isDesktop(HttpServletRequest request) {
        return !isMobile(request);
    }

    /**
     * 判断请求是否来自移动端
     *
     * @param request -
     * @return Boolean -
     * @author XinLau
     * @creed The only constant is change ! ! !
     * @since 2020/8/31 17:27
     */
    public static Boolean isMobile(HttpServletRequest request) {
        String ua = request.getHeader(HttpHeaders.USER_AGENT).toLowerCase();
        Pattern pattern = Pattern.compile(CommonConstant.MOBILE_TYPE_REGULAR_EXPRESSION);
        return pattern.matcher(ua).find();
    }

}
