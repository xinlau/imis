package com.imis.base.interceptor;

import com.imis.base.globle.exception.BusinessException;
import com.imis.module.system.bus.SysDataSourceBus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * <p>
 * DataSourceInitialization<br>
 * 数据源初始化
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2021年01月04日 09:34
 */
@Slf4j
@Component
@Order(value = 1)
public class DataSourceInitialization implements CommandLineRunner {

    /**
     * 系统数据源管理 业务处理类
     */
    private SysDataSourceBus sysDataSourceBus;

    @Autowired
    public void setSysDataSourceBus(SysDataSourceBus sysDataSourceBus) {
        this.sysDataSourceBus = sysDataSourceBus;
    }

    /**
     * 数据源启动初始化
     * @param args - 参数
     * @throws Exception - 异常
     */
    @Override
    public void run(String... args) throws BusinessException {
        long startTime = System.currentTimeMillis();
        log.debug("数据源自动初始化......{}");
        sysDataSourceBus.initializeTheDataSource();
        long endTime = System.currentTimeMillis();
        log.debug("数据源自动初始化全部完成： 用时 {} - {} = {} 毫秒", startTime, endTime, endTime - startTime);
    }

}
