package com.imis.base.shiro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.imis.module.system.model.vo.SysRoleVO;
import com.imis.module.system.model.vo.SysUserOrganizationVO;
import com.imis.module.system.model.vo.SysUserVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * UserInfo<br>
 * 登录用户对象返回值
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年12月23日 17:13
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "登录用户对象返回值", description = "登录用户返回值 对象")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Token
     */
    @ApiModelProperty(value = "Token")
    private String token;

    /**
     * 用户基本信息
     */
    @ApiModelProperty(value = "用户基本信息")
    private SysUserVO user;

    /**
     * 角色基本信息
     */
    @ApiModelProperty(value = "角色基本信息")
    private List<SysRoleVO> roleList;

    /**
     * 负责的组织机构信息
     */
    @ApiModelProperty(value = "负责的组织机构信息")
    private List<SysUserOrganizationVO> organizationList;

    public UserInfo() {
    }

    public UserInfo(final String token, final SysUserVO user, final List<SysRoleVO> roleList, final List<SysUserOrganizationVO> organizationList) {
        this.token = token;
        this.user = user;
        this.roleList = roleList;
        this.organizationList = organizationList;
    }

}
