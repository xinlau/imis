package com.imis.base.shiro.util;

import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.imis.base.constant.CommonConstant;
import com.imis.base.util.SpringContextUtils;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * <p>
 * JwtUtil<br>
 * JWT工具类
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2019年03月26日 11:11
 */
public class JwtUtil {

    /**
     * Token缓存时间：3600000毫秒即一小时
     */
    public static final long EXPIRE_TIME = 60 * 60 * 1000;

    /**
     * 校验Token是否正确
     *
     * @param token  - 密钥
     * @param secret - 用户的密码
     * @return boolean - 是否正确
     */
    public static boolean verify(String token, String username, String secret) {
        try {
            // 根据密码生成JWT效验器
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm).withClaim("username", username).build();
            // 效验TOKEN
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            return false;
        }
    }

    /**
     * 获得Token中的信息无需secret解密也能获得
     * @param token - 密钥
     * @return Token中包含的用户名
     */
    public static String getUsername(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("username").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 生成签名,EXPIRE_TIME后过期
     *
     * @param username - 用户名
     * @param secret - 用户的密码
     * @return 加密的token
     */
    public static String sign(String username, String secret) {
        // 毫秒级的时间处理
        Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256(secret);
        // 附带 username信息
        return JWT.create().withClaim("username", username).withExpiresAt(date).sign(algorithm);
    }

    /**
     * 生成永不过期签名当做Token缓存Key使用
     *
     * @param username - 用户名
     * @param secret   - 密码
     * @return String
     */
    public static String getTokenKey(String username, String secret){
        // 附带 username 信息
        return JWT.create().withClaim("username", username).sign(Algorithm.HMAC256(secret));
    }

    /**
     * 从session中获取变量
     * @param key
     * @return String
     */
    public static String getSessionData(String key) {
        //${myVar}%
        //得到${} 后面的值
        String moshi = CommonConstant.EMPTY;
        if (key.contains(CommonConstant.RIGHT_BRACE)) {
            moshi = key.substring(key.indexOf(CommonConstant.RIGHT_BRACE) + 1);
        }
        String returnValue = null;
        if (key.contains(CommonConstant.HASH_LEFT_BRACE)) {
            key = key.substring(2, key.indexOf(CommonConstant.RIGHT_BRACE));
        }
        if (StrUtil.isNotEmpty(key)) {
            HttpSession session = SpringContextUtils.getHttpServletRequest().getSession();
            returnValue = (String) session.getAttribute(key);
        }
        //结果加上${} 后面的值
        if (returnValue != null) {
            returnValue = returnValue + moshi;
        }
        return returnValue;
    }

}
