package com.imis.base.shiro.authc.aop;

import com.imis.base.constant.HttpHeadersConstants;
import com.imis.base.constant.enums.CommonResponseEnum;
import com.imis.base.shiro.authc.JwtToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * JwtFilter<br>
 * 鉴权登录拦截器
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月26日 11:18
 */
@Slf4j
public class JwtFilter extends BasicHttpAuthenticationFilter {

	/**
	 * 执行登录认证
	 *
	 * @param request
	 * @param response
	 * @param mappedValue
	 * @return boolean
	 */
	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
		try {
			executeLogin(request, response);
			return true;
		} catch (Exception e) {
			throw new AuthenticationException(CommonResponseEnum.ERROR_NO_AUTHENTICATION.getMessage());
		}
	}

	/**
	 * 执行登录
	 */
	@Override
	protected boolean executeLogin(ServletRequest request, ServletResponse response) {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String token = httpServletRequest.getHeader(HttpHeadersConstants.X_ACCESS_TOKEN);
		JwtToken jwtToken = new JwtToken(token);
		// 提交给 realm 进行登入，如果错误他会抛出异常并被捕获
		getSubject(request, response).login(jwtToken);
		// 如果没有抛出异常则代表登入成功，返回true
		return true;
	}

	/**
	 * 对跨域提供支持
	 */
	@Override
	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		httpServletResponse.setHeader(HttpHeadersConstants.ACCESS_CONTROL_ALLOW_ORIGIN, httpServletRequest.getHeader(HttpHeadersConstants.ORIGIN));
		httpServletResponse.setHeader(HttpHeadersConstants.ACCESS_CONTROL_ALLOW_METHODS, "GET,POST,OPTIONS,PUT,DELETE");
		httpServletResponse.setHeader(HttpHeadersConstants.ACCESS_CONTROL_ALLOW_HEADERS, httpServletRequest.getHeader(HttpHeadersConstants.ACCESS_CONTROL_REQUEST_HEADERS));
		// 跨域时会首先发送一个 option 请求，这里我们给 option 请求直接返回正常状态
		if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
			httpServletResponse.setStatus(HttpStatus.OK.value());
			return false;
		}
		return super.preHandle(request, response);
	}
}
