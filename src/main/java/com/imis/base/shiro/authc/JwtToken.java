package com.imis.base.shiro.authc;
 
import org.apache.shiro.authc.AuthenticationToken;

/**
 * <p>
 * JwtToken<br>
 * 权限令牌
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年03月26日 11:18
 */
public class JwtToken implements AuthenticationToken {
	
	private static final long serialVersionUID = 1L;
	private String token;
 
    public JwtToken(String token) {
        this.token = token;
    }
 
    @Override
    public Object getPrincipal() {
        return token;
    }
 
    @Override
    public Object getCredentials() {
        return token;
    }
}
