package ${package.Controller};

import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import ${package.Controller?substring(0, package.Controller?last_index_of('.') + 1)}bus.${entity}Bus;

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Slf4j
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping(path = "<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
@Api(tags = {"${table.comment!}"})
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass}<${entity}Bus> {
<#else>
public class ${table.controllerName} {
</#if>

}