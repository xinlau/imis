package ${package.Controller?substring(0, package.Controller?last_index_of('.') + 1)}bus;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.imis.module.base.BaseBus;
import ${package.Service}.${table.serviceName};

/**
 * <p>
 * ${table.comment!} 业务处理类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Slf4j
@Service
public class ${entity}Bus extends BaseBus {

    /**
     * ${table.comment!} 服务类
     */
    private ${table.serviceName} serviceBy${table.serviceName};
    @Autowired
    public void set${table.serviceName}(${table.serviceName}  serviceBy${table.serviceName}) {
        this.serviceBy${table.serviceName} = serviceBy${table.serviceName};
    }

}