package ${package.Entity}.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * ${table.comment!} VO
 *
 * @author ${author}
 * @since ${date}
*/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="${entity}对象", description="${table.comment!}")
public class ${entity}VO implements Serializable {

    private static final long serialVersionUID = 1L;

<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    /**
     * ${field.comment}
     */
    @ApiModelProperty(value = "${field.comment}")
    <#if field.propertyType =='java.util.Date'>
        <#if field.fieldDbType =='date'>
    @JsonFormat(timezone = "GMT+8", DatePattern.NORM_DATE_PATTERN)
    @DateTimeFormat(pattern="yyyy-MM-dd")
        <#elseif field.fieldDbType =='datetime'>
    @JsonFormat(timezone = "GMT+8", DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
        </#if>
    <#elseif field.propertyType =='java.time.LocalDateTime'>
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @DateTimeFormat(DatePattern.NORM_DATETIME_PATTERN)
    </#if>
    private ${field.propertyType} ${field.propertyName};
</#list>
<#------------  END 字段循环遍历  ---------->
}
