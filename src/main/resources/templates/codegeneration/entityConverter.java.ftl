package ${package.Entity}.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.imis.base.constant.ConverterIgnoreConstant;
import ${package.Entity}.vo.${entity}VO;
import ${package.Entity}.ro.${entity}RO;
import ${package.Entity}.po.${entity};
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import java.util.List;

/**
 * <p>
 * ${table.comment!} Converter 转换器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Mapper
public interface ${entity}Converter {

    /**
     * ${table.comment!} 转换器实例
     */
    ${entity}Converter INSTANCE = Mappers.getMapper(${entity}Converter.class);

    /**
     * ${table.comment!} RO转PO
     *
     * @param entity - ${table.comment!} RO
     * @return  ${entity} - ${table.comment!} PO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.CREATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.CREATE_TIME, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_BY, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.UPDATE_TIME, ignore = true)
    })
    ${entity} getAddEntity(${entity}RO entity);

    /**
     * ${table.comment!} PO转VO
     *
     * @param entity - ${table.comment!} PO
     * @return ${entity}VO - ${table.comment!} VO
     */
    @Mappings({})
    ${entity}VO getReturnValue(${entity} entity);

    /**
     * ${table.comment!} PO转VO
     *
     * @param entityList - ${table.comment!} 数组PO
     * @return List<${entity}VO> - ${table.comment!} 数组VO
     */
    @Mappings({})
    List<${entity}VO> getReturnValue(List<${entity}> entityList);

    /**
     * ${table.comment!} 分页PO转分页VO
     *
     * @param entityPage - ${table.comment!} 分页PO
     * @return Page<${entity}VO> - ${table.comment!} 分页VO
     */
    @Mappings({
            @Mapping(target = ConverterIgnoreConstant.ASCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.ASC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESCS, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.DESC, ignore = true),
            @Mapping(target = ConverterIgnoreConstant.OPTIMIZE_COUNT_SQL, ignore = true)
     })
    Page<${entity}VO> getReturnValue(Page<${entity}> entityPage);

}
