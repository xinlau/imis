<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN" "http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd">
<hibernate-mapping>
    <class name="${tableName}" table="${tableName}" optimistic-lock="version">
        <#if tableFieldList?exists>
            <!-- 处理字段 start -->
            <#list tableFieldList as tableField>
                <#if tableField.fieldName == "id">
                    <!-- 处理主键字段 start -->
                    <#if primaryKeyType?if_exists?html=='UUID'>
                        <id name="id" type="java.lang.String" length="${tableField.fieldLength}" unsaved-value="null">
                            <generator class="uuid"/>
                        </id>
                    <#elseif primaryKeyType?if_exists?html=='NATIVE'>
                        <#if dataBaseType=='MYSQL'>
                            <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                                <generator class="identity"/>
                            </id>
                        <#elseif dataBaseType=='ORACLE'>
                            <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                                <generator class="native"/>
                            </id>
                        <#elseif dataBaseType=='SQLSERVER'>
                            <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                                <generator class="identity"/>
                            </id>
                        <#elseif dataBaseType=='POSTGRESQL'>
                            <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                                <generator class="native"/>
                            </id>
                        </#if>
                    <#elseif primaryKeyType?if_exists?html=='SEQUENCE'>
                        <#if dataBaseType=='MYSQL'>
                            <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                                <generator class="identity"/>
                            </id>
                        <#elseif dataBaseType=='ORACLE'>
                            <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                                <generator class="sequence">
                                    <param name="sequence">${primaryKeySequence}
                                    </param>
                                </generator>
                            </id>
                        <#elseif dataBaseType=='SQLSERVER'>
                            <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                                <generator class="identity"/>
                            </id>
                        <#elseif dataBaseType=='POSTGRESQL'>
                            <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                                <generator class="native"/>
                            </id>
                        </#if>
                    <#else>
                        <id name="id" type="java.lang.Long" length="${tableField.fieldLength}" unsaved-value="null">
                            <generator class="assigned"/>
                        </id>
                    </#if>
                <#else>
                    <!-- 处理 ${tableField.fieldComment}（${tableField.fieldName}） 字段 start -->
                    <property name="${tableField.fieldName}"
                            <#switch tableField.fieldType?lower_case>
                                <#case "string">
                                    type="java.lang.String"
                                    <#break>
                                <#case "text">
                                    <#if dataBaseType=='ORACLE'>
                                        type="clob"
                                    <#else>
                                        type="text"
                                    </#if>
                                    <#break>
                                <#case "int">
                                    type="java.lang.Integer"
                                    <#break>
                                <#case "double">
                                    <#if dataBaseType=='MYSQL'>
                                        type="java.lang.Double"
                                    <#elseif dataBaseType=='ORACLE'>
                                        type="java.math.BigDecimal"
                                    <#elseif dataBaseType=='POSTGRESQL'>
                                        type="java.math.BigDecimal"
                                    <#elseif dataBaseType=='SQLSERVER'>
                                        type="java.math.BigDecimal"
                                    </#if>
                                    <#break>
                                <#case "date">
                                    <#if dataBaseType=='MYSQL'>
                                        type="java.util.Date"
                                    <#elseif dataBaseType=='ORACLE'>
                                        type="java.sql.Timestamp"
                                    <#elseif dataBaseType=='POSTGRESQL'>
                                        type="java.util.Date"
                                    <#elseif dataBaseType=='SQLSERVER'>
                                        type="java.util.Date"
                                    </#if>
                                    <#break>
                                <#case "bigdecimal">
                                    <#if dataBaseType=='MYSQL'>
                                        type="java.math.BigDecimal"
                                    <#elseif dataBaseType=='ORACLE'>
                                        type="java.math.BigDecimal"
                                    <#elseif dataBaseType=='POSTGRESQL'>
                                        type="java.math.BigDecimal"
                                    <#elseif dataBaseType=='SQLSERVER'>
                                        type="java.math.BigDecimal"
                                    </#if>
                                    <#break>
                                <#case "blob">
                                    <#if dataBaseType=='MYSQL'>
                                        type="blob"
                                    <#elseif dataBaseType=='ORACLE'>
                                        type="blob"
                                    <#elseif dataBaseType=='POSTGRESQL'>
                                        type="binary"
                                    <#elseif dataBaseType=='SQLSERVER'>
                                        type="image"
                                    </#if>
                                    <#break>
                            </#switch> access="property">
                        <column name="${tableField.fieldName}" <#if dataBaseType=='SQLSERVER' && tableField.fieldType?lower_case="string"> sql-type="nvarchar(${tableField.fieldLength})" </#if>
                                <#if dataBaseType=='SQLSERVER' && tableField.fieldType?lower_case="text"> sql-type="ntext" </#if>
                                <#if tableField.fieldType=='double'||tableField.fieldType=='BigDecimal'>
                                    precision="${tableField.fieldLength}" scale="${tableField.fieldDecimalDigits}"<#else>length="${tableField.fieldLength}"</#if>
                                <#if tableField.fieldDefaultVal?exists&&tableField.fieldDefaultVal!=''>default="${tableField.fieldDefaultVal}"</#if>
                                not-null="<#if tableField.fieldIsNull == 1>false<#else>true</#if>" unique="false">
                            <comment>${tableField.fieldComment}</comment>
                        </column>
                    </property>
                </#if>
            </#list>
            <!-- 处理字段 end -->
        </#if>
    </class>
</hibernate-mapping>