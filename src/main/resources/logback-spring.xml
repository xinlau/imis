<?xml version="1.0" encoding="UTF-8"?>
<!-- 日志级别有（从高到低）：FATAL（致命），ERROR（错误），WARN（警告），INFO（信息），DEBUG（调试），TRACE（跟踪）或者 OFF（关闭） -->
<!-- Logback 没有 FATAL 致命级别。它被映射到 ERROR 错误级别，如果设置为 WARN，则低于 WARN 的信息都不会输出 -->
<!-- scan:当此属性设置为true时，配置文件如果发生改变，将会被重新加载，默认值为true -->
<!-- scanPeriod:设置监测配置文件是否有修改的时间间隔，如果没有给出时间单位，默认单位是毫秒。当scan为true时，此属性生效。默认的时间间隔为1分钟。 -->
<!-- debug:当此属性设置为true时，将打印出logback内部日志信息，实时查看logback运行状态。默认值为false。 -->
<configuration  scan="true" scanPeriod="10 seconds" debug="true">

	<!-- 默认配置 -->
	<include resource="org/springframework/boot/logging/logback/defaults.xml"/>
	<!-- 配置控制台(Console) -->
	<include resource="org/springframework/boot/logging/logback/console-appender.xml"/>

	<!-- name的值是变量的名称，value的值时变量定义的值。通过定义的值会被插入到logger上下文中。定义变量后，可以使“${}”来使用变量。 -->
	<!-- 应用名称 -->
	<property name="APP_NAME" value="imis"/>
	<!-- 日志文件保存路径 -->
	<property name="FILE_PATH" value="../logs" />

	<contextName>${APP_NAME}</contextName>

	<!-- 彩色日志 -->
	<!-- 彩色日志依赖的渲染类 -->
	<conversionRule conversionWord="clr" converterClass="org.springframework.boot.logging.logback.ColorConverter" />
	<conversionRule conversionWord="wex" converterClass="org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter" />
	<conversionRule conversionWord="wEx" converterClass="org.springframework.boot.logging.logback.ExtendedWhitespaceThrowableProxyConverter" />

	<!-- 彩色日志格式 -->
	<property name="CONSOLE_LOG_PATTERN" value="${CONSOLE_LOG_PATTERN:-%clr(%d{yyyy-MM-dd HH:mm:ss.SSS,CTT}){faint} %clr(${LOG_LEVEL_PATTERN:-%5p}) %clr(${PID:- }){magenta} %clr([%15.15t]){faint} %clr(%-40.40logger{100}){cyan} %clr(:){faint} %m%n${LOG_EXCEPTION_CONVERSION_WORD:-%wEx}}"/>
	<!-- LOG 文件日志格式 -->
	<property name="LOG_FILE_LOG_PATTERN" value="%d{yyyy-MM-dd HH:mm:ss.SSS,CTT} [%thread] %-5level %logger{100} - %msg%n"/>
	<!-- LOG 文件日志格式 -->
	<property name="HTML_FILE_LOG_PATTERN" value="%p%d%msg%M%F{100}%L"/>

	<!-- 指定日志的收集策略 输出到控制台 -->
	<!-- 时间滚动输出 level 为 DEBUG 日志 -->
	<appender name="CONSOLE_DEBUG" class="ch.qos.logback.core.ConsoleAppender">
		<!-- 此日志 appender 是为开发使用，只配置最底级别，控制台输出的日志级别是大于或等于此级别的日志信息 -->
		<filter class="ch.qos.logback.classic.filter.ThresholdFilter">
			<level>debug</level>
		</filter>
		<encoder>
			<Pattern>${CONSOLE_LOG_PATTERN}</Pattern>
			<!-- 设置字符集 -->
			<charset>UTF-8</charset>
		</encoder>
	</appender>
	<!-- 时间滚动输出 level 为 ERROR 日志 -->
	<appender name="CONSOLE_ERROR" class="ch.qos.logback.core.ConsoleAppender">
		<!-- 此日志 appender 是为开发使用，只配置最底级别，控制台输出的日志级别是大于或等于此级别的日志信息 -->
		<filter class="ch.qos.logback.classic.filter.ThresholdFilter">
			<level>error</level>
		</filter>
		<encoder>
			<Pattern>${CONSOLE_LOG_PATTERN}</Pattern>
			<!-- 设置字符集 -->
			<charset>UTF-8</charset>
		</encoder>
	</appender>

	<!-- 指定日志的收集策略 输出到 LOG 文件 -->
	<!-- 时间滚动输出 level 为 DEBUG 日志 -->
	<appender name="LOG_DEBUG_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<!-- 正在记录的日志文件的路径及文件名 -->
		<file>${FILE_PATH}/${APP_NAME}_log_debug.log</file>
		<!-- 日志文件输出格式 -->
		<encoder>
			<pattern>${LOG_FILE_LOG_PATTERN}</pattern>
			<!-- 此处设置字符集 -->
			<charset>UTF-8</charset>
		</encoder>
		<!-- 日志记录器的滚动策略，按日期，按大小记录 -->
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- 日志归档 -->
			<fileNamePattern>${FILE_PATH}/debug/${APP_NAME}_log-debug-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
			<timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
				<maxFileSize>100MB</maxFileSize>
			</timeBasedFileNamingAndTriggeringPolicy>
			<!--日志文件保留天数-->
			<maxHistory>7</maxHistory>
		</rollingPolicy>
		<!-- 此日志文件只记录 debug 级别的 -->
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>debug</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
	</appender>
	<!-- 时间滚动输出 level 为 INFO 日志 -->
	<appender name="LOG_INFO_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<!-- 正在记录的日志文件的路径及文件名 -->
		<file>${FILE_PATH}/${APP_NAME}_log_info.log</file>
		<!--日志文件输出格式-->
		<encoder>
			<pattern>${LOG_FILE_LOG_PATTERN}</pattern>
			<!-- 此处设置字符集 -->
			<charset>UTF-8</charset>
		</encoder>
		<!-- 日志记录器的滚动策略，按日期，按大小记录 -->
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- 每天日志归档路径以及格式 -->
			<fileNamePattern>${FILE_PATH}/info/${APP_NAME}_log-info-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
			<timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
				<maxFileSize>100MB</maxFileSize>
			</timeBasedFileNamingAndTriggeringPolicy>
			<!--日志文件保留天数-->
			<maxHistory>15</maxHistory>
		</rollingPolicy>
		<!-- 此日志文件只记录 info 级别的 -->
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>info</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
	</appender>
	<!-- 时间滚动输出 level 为 WARN 日志 -->
	<appender name="LOG_WARN_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<!-- 正在记录的日志文件的路径及文件名 -->
		<file>${FILE_PATH}/${APP_NAME}_log_warn.log</file>
		<!--日志文件输出格式-->
		<encoder>
			<pattern>${LOG_FILE_LOG_PATTERN}</pattern>
			<!-- 此处设置字符集 -->
			<charset>UTF-8</charset>
		</encoder>
		<!-- 日志记录器的滚动策略，按日期，按大小记录 -->
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<fileNamePattern>${FILE_PATH}/warn/${APP_NAME}_log-warn-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
			<timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
				<maxFileSize>100MB</maxFileSize>
			</timeBasedFileNamingAndTriggeringPolicy>
			<!--日志文件保留天数-->
			<maxHistory>15</maxHistory>
		</rollingPolicy>
		<!-- 此日志文件只记录 warn 级别的 -->
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>warn</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
	</appender>
	<!-- 时间滚动输出 level 为 ERROR 日志 -->
	<appender name="LOG_ERROR_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<!-- 正在记录的日志文件的路径及文件名 -->
		<file>${FILE_PATH}/${APP_NAME}_log_error.log</file>
		<!-- 日志文件输出格式 -->
		<encoder>
			<pattern>${LOG_FILE_LOG_PATTERN}</pattern>
			<!-- 此处设置字符集 -->
			<charset>UTF-8</charset>
		</encoder>
		<!-- 日志记录器的滚动策略，按日期，按大小记录 -->
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- 日志文件输出的文件名 -->
			<fileNamePattern>${FILE_PATH}/error/${APP_NAME}_log-error-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
			<timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
				<maxFileSize>100MB</maxFileSize>
			</timeBasedFileNamingAndTriggeringPolicy>
			<!--日志文件保留天数-->
			<maxHistory>30</maxHistory>
		</rollingPolicy>
		<!-- 此日志文件只记录 ERROR 级别的 -->
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>ERROR</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
	</appender>

	<!-- 指定日志的收集策略 输出到 HTML 文件 -->
	<!-- 时间滚动输出 level 为 DEBUG 日志 -->
	<appender name="HTML_DEBUG_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<!-- 正在记录的日志文件的路径及文件名 -->
		<file>${FILE_PATH}/html/${APP_NAME}_log_debug.html</file>
		<!-- 日志文件输出格式 -->
		<encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
			<layout class="ch.qos.logback.classic.html.HTMLLayout">
				<pattern>${HTML_FILE_LOG_PATTERN}</pattern>
			</layout>
		</encoder>
		<!-- 日志记录器的滚动策略，按日期，按大小记录 -->
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- 日志文件输出的文件名 -->
			<fileNamePattern>${FILE_PATH}/html/${APP_NAME}_log_debug-%d{yyyy-MM-dd}.%i.html</fileNamePattern>
			<timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
				<maxFileSize>100MB</maxFileSize>
			</timeBasedFileNamingAndTriggeringPolicy>
			<!-- 日志文件保留天数 -->
			<maxHistory>7</maxHistory>
		</rollingPolicy>
		<!-- 此日志文件只记录 DEBUG 级别的 -->
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>DEBUG</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
	</appender>
	<!-- 时间滚动输出 level 为 INFO 日志 -->
	<appender name="HTML_INFO_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<!-- 正在记录的日志文件的路径及文件名 -->
		<file>${FILE_PATH}/html/${APP_NAME}_log_info.html</file>
		<!-- 日志文件输出格式 -->
		<encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
			<layout class="ch.qos.logback.classic.html.HTMLLayout">
				<pattern>${HTML_FILE_LOG_PATTERN}</pattern>
			</layout>
		</encoder>
		<!-- 日志记录器的滚动策略，按日期，按大小记录 -->
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- 日志文件输出的文件名 -->
			<fileNamePattern>${FILE_PATH}/html/${APP_NAME}_log_info-%d{yyyy-MM-dd}.%i.html</fileNamePattern>
			<timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
				<maxFileSize>100MB</maxFileSize>
			</timeBasedFileNamingAndTriggeringPolicy>
			<!-- 日志文件保留天数 -->
			<maxHistory>7</maxHistory>
		</rollingPolicy>
		<!-- 此日志文件只记录 DEBUG 级别的 -->
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>INFO</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
	</appender>
	<!-- 时间滚动输出 level 为 WARN 日志 -->
	<appender name="HTML_WARN_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<!-- 正在记录的日志文件的路径及文件名 -->
		<file>${FILE_PATH}/html/${APP_NAME}_log_warn.html</file>
		<!-- 日志文件输出格式 -->
		<encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
			<layout class="ch.qos.logback.classic.html.HTMLLayout">
				<pattern>${HTML_FILE_LOG_PATTERN}</pattern>
			</layout>
		</encoder>
		<!-- 日志记录器的滚动策略，按日期，按大小记录 -->
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- 日志文件输出的文件名 -->
			<fileNamePattern>${FILE_PATH}/html/${APP_NAME}_log_warn-%d{yyyy-MM-dd}.%i.html</fileNamePattern>
			<timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
				<maxFileSize>100MB</maxFileSize>
			</timeBasedFileNamingAndTriggeringPolicy>
			<!-- 日志文件保留天数 -->
			<maxHistory>7</maxHistory>
		</rollingPolicy>
		<!-- 此日志文件只记录 DEBUG 级别的 -->
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>WARN</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
	</appender>
	<!-- 时间滚动输出 level 为 ERROR 日志 -->
	<appender name="HTML_ERROR_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<!-- 正在记录的日志文件的路径及文件名 -->
		<file>${FILE_PATH}/html/${APP_NAME}_log_error.html</file>
		<!-- 日志文件输出格式 -->
		<encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
			<layout class="ch.qos.logback.classic.html.HTMLLayout">
				<pattern>${HTML_FILE_LOG_PATTERN}</pattern>
			</layout>
		</encoder>
		<!-- 日志记录器的滚动策略，按日期，按大小记录 -->
		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- 日志文件输出的文件名 -->
			<fileNamePattern>${FILE_PATH}/html/${APP_NAME}_log_error-%d{yyyy-MM-dd}.%i.html</fileNamePattern>
			<timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
				<maxFileSize>100MB</maxFileSize>
			</timeBasedFileNamingAndTriggeringPolicy>
			<!-- 日志文件保留天数 -->
			<maxHistory>7</maxHistory>
		</rollingPolicy>
		<!-- 此日志文件只记录 DEBUG 级别的 -->
		<filter class="ch.qos.logback.classic.filter.LevelFilter">
			<level>ERROR</level>
			<onMatch>ACCEPT</onMatch>
			<onMismatch>DENY</onMismatch>
		</filter>
	</appender>

	<!--
        <logger>用来设置某一个包或者具体的某一个类的日志打印级别、
        以及指定<appender>。<logger>仅有一个name属性，
        一个可选的level和一个可选的addtivity属性。
        name:用来指定受此logger约束的某一个包或者具体的某一个类。
        level:用来设置打印级别，大小写无关：TRACE, DEBUG, INFO, WARN, ERROR, ALL 和 OFF，
              还有一个特俗值INHERITED或者同义词NULL，代表强制执行上级的级别。
              如果未设置此属性，那么当前logger将会继承上级的级别。
        addtivity:是否向上级logger传递打印信息。默认是true。
    -->
	<!--<logger name="org.springframework.web" level="info"/>-->
	<!--<logger name="org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor" level="INFO"/>-->
	<!--
        使用mybatis的时候，sql语句是debug下才会打印，而这里我们只配置了info，所以想要查看sql语句的话，有以下两种操作：
        第一种把<root level="info">改成<root level="DEBUG">这样就会打印sql，不过这样日志那边会出现很多其他消息
        第二种就是单独给dao下目录配置debug模式，代码如下，这样配置sql语句会打印，其他还是正常info级别：
     -->
	<!--
        root节点是必选节点，用来指定最基础的日志输出级别，只有一个level属性
        level:用来设置打印级别，大小写无关：TRACE, DEBUG, INFO, WARN, ERROR, ALL 和 OFF，
        不能设置为INHERITED或者同义词NULL。默认是DEBUG
        可以包含零个或多个元素，标识这个appender将会添加到这个logger。
    -->

	<!-- 最基础的日志输出级别 -->
	<root level="DEBUG">
		<appender-ref ref="CONSOLE_DEBUG" />
		<appender-ref ref="LOG_DEBUG_FILE" />
		<appender-ref ref="HTML_DEBUG_FILE" />
	</root>

</configuration>