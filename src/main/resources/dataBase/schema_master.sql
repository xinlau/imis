/*
 Navicat Premium Data Transfer

 Source Server         : 虚拟机_ContOS_7_64_3306
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 192.168.1.102:3306
 Source Schema         : imis_master

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 28/12/2020 11:17:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for onl_form_button
-- ----------------------------
DROP TABLE IF EXISTS `onl_form_button`;
CREATE TABLE `onl_form_button`
(
    `id`              bigint(20)                                             NOT NULL COMMENT '主键ID',
    `table_head_id`   bigint(20)                                             NOT NULL COMMENT '表单ID',
    `button_code`     varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NULL DEFAULT NULL COMMENT '按钮编码',
    `button_icon`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NULL DEFAULT NULL COMMENT '按钮图标',
    `button_name`     varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NULL DEFAULT NULL COMMENT '按钮名称',
    `button_status`   tinyint(2)                                             NULL DEFAULT NULL COMMENT '按钮状态',
    `button_type`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NULL DEFAULT NULL COMMENT '按钮类型',
    `button_style`    varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NULL DEFAULT NULL COMMENT '按钮样式',
    `button_position` tinyint(2)                                             NULL DEFAULT NULL COMMENT '按钮位置（1侧面 2底部）',
    `expression`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表达式',
    `sort_order`      double(2, 0)                                           NULL DEFAULT NULL COMMENT '排序',
    `create_by`       bigint(20)                                             NULL DEFAULT NULL COMMENT '创建人',
    `create_time`     datetime(0)                                            NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`       bigint(20)                                             NULL DEFAULT NULL COMMENT '更新人',
    `update_time`     datetime(0)                                            NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `index_formid` (`table_head_id`) USING BTREE,
    INDEX `index_button_code` (`button_code`) USING BTREE,
    INDEX `index_button_status` (`button_status`) USING BTREE,
    INDEX `index_button_order` (`sort_order`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '在线开发-表单按钮'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for onl_form_enhance_java
-- ----------------------------
DROP TABLE IF EXISTS `onl_form_enhance_java`;
CREATE TABLE `onl_form_enhance_java`
(
    `id`            bigint(20)                                              NOT NULL,
    `table_head_id` bigint(20)                                              NOT NULL COMMENT '表单ID',
    `button_code`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin   NULL     DEFAULT NULL COMMENT '按钮编码',
    `java_type`     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin   NOT NULL COMMENT '类型',
    `java_value`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '数值',
    `active_status` tinyint(2)                                              NULL     DEFAULT NULL COMMENT '生效状态',
    `event`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin   NOT NULL DEFAULT 'end' COMMENT '事件状态(end:结束，start:开始)',
    `description`   varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL     DEFAULT NULL COMMENT '备注',
    `create_by`     bigint(20)                                              NULL     DEFAULT NULL COMMENT '创建人',
    `create_time`   datetime(0)                                             NULL     DEFAULT NULL COMMENT '创建时间',
    `update_by`     bigint(20)                                              NULL     DEFAULT NULL COMMENT '更新人',
    `update_time`   datetime(0)                                             NULL     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `index_fmid` (`table_head_id`) USING BTREE,
    INDEX `index_buttoncode` (`button_code`) USING BTREE,
    INDEX `index_status` (`active_status`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '在线开发-Java增强'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for onl_form_enhance_java_script
-- ----------------------------
DROP TABLE IF EXISTS `onl_form_enhance_java_script`;
CREATE TABLE `onl_form_enhance_java_script`
(
    `id`                  bigint(20)                                              NOT NULL COMMENT '主键ID',
    `table_head_id`       bigint(20)                                              NOT NULL COMMENT '表单ID',
    `java_script_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci     NULL COMMENT 'JS增强内容',
    `java_script_type`    varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin   NULL DEFAULT NULL COMMENT '类型',
    `description`         varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
    `create_by`           bigint(20)                                              NULL DEFAULT NULL COMMENT '创建人',
    `create_time`         datetime(0)                                             NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`           bigint(20)                                              NULL DEFAULT NULL COMMENT '更新人',
    `update_time`         datetime(0)                                             NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `index_fmid` (`table_head_id`) USING BTREE,
    INDEX `index_jstype` (`java_script_type`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '在线开发-JavaScript 增强'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for onl_form_enhance_structured_query_language
-- ----------------------------
DROP TABLE IF EXISTS `onl_form_enhance_structured_query_language`;
CREATE TABLE `onl_form_enhance_structured_query_language`
(
    `id`                                bigint(20)                                              NOT NULL COMMENT '主键ID',
    `table_head_id`                     bigint(20)                                              NULL DEFAULT NULL COMMENT '表单ID',
    `button_code`                       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin   NULL DEFAULT NULL COMMENT '按钮编码',
    `structured_query_language_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci     NULL COMMENT 'SQL内容',
    `structured_query_language_name`    varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin   NULL DEFAULT NULL COMMENT 'SQL名称',
    `description`                       varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
    `create_by`                         bigint(20)                                              NULL DEFAULT NULL COMMENT '创建人',
    `create_time`                       datetime(0)                                             NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`                         bigint(20)                                              NULL DEFAULT NULL COMMENT '更新人',
    `update_time`                       datetime(0)                                             NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `index_formid` (`table_head_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '在线开发-SQL增强'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for onl_table_fields
-- ----------------------------
DROP TABLE IF EXISTS `onl_table_fields`;
CREATE TABLE `onl_table_fields`
(
    `id`                    bigint(20) UNSIGNED ZEROFILL                            NOT NULL COMMENT '主键ID',
    `table_head_id`         bigint(20)                                              NOT NULL COMMENT '表ID',
    `field_name`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '字段名字',
    `field_txt`             varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段备注',
    `field_name_old`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '原字段名',
    `is_key`                tinyint(1) UNSIGNED ZEROFILL                            NULL DEFAULT NULL COMMENT '是否主键（0否 1是）',
    `is_null`               tinyint(1) UNSIGNED ZEROFILL                            NULL DEFAULT NULL COMMENT '是否允许为空（0否 1是）',
    `field_type`            varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '数据库字段类型',
    `field_length`          int(11)                                                 NOT NULL COMMENT '数据库字段长度',
    `field_point_length`    int(11)                                                 NULL DEFAULT NULL COMMENT '小数点',
    `field_default_value`   varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '表字段默认值',
    `dict_field`            varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典code',
    `dict_table`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典表',
    `dict_text`             varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典Text',
    `field_show_type`       varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '表单控件类型',
    `field_href`            varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转URL',
    `form_control_length`   int(11)                                                 NULL DEFAULT NULL COMMENT '表单控件长度',
    `field_valid_type`      varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单字段校验规则',
    `field_must_input`      tinyint(1)                                              NULL DEFAULT NULL COMMENT '字段是否必填',
    `field_extend_json`     json                                                    NULL COMMENT '扩展参数JSON',
    `control_default_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件默认值，不同的表达式展示不同的结果。\r\n1. 纯字符串直接赋给默认值；\r\n2. #{普通变量}；\r\n3. {{ 动态JS表达式 }}；\r\n4. ${填值规则编码}；\r\n填值规则表达式只允许存在一个，且不能和其他规则混用。',
    `is_query`              tinyint(1) UNSIGNED ZEROFILL                            NULL DEFAULT NULL COMMENT '是否查询条件（0否 1是）',
    `is_show_form`          tinyint(1) UNSIGNED ZEROFILL                            NULL DEFAULT NULL COMMENT '表单是否显示（0否 1是）',
    `is_show_list`          tinyint(1)                                              NULL DEFAULT 0 COMMENT '列表是否显示（0否 1是）',
    `is_read_only`          tinyint(1)                                              NULL DEFAULT 0 COMMENT '是否是只读（1是 0否）',
    `query_mode`            varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '查询模式',
    `main_table`            varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键主表名',
    `main_field`            varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键主键字段',
    `sort_order`            int(11)                                                 NULL DEFAULT NULL COMMENT '排序',
    `converter`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义值转换器',
    `query_def_value`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '查询默认值',
    `query_dict_text`       varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询配置字典text',
    `query_dict_field`      varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询配置字典code',
    `query_dict_table`      varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询配置字典table',
    `query_show_type`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '查询显示控件',
    `query_config_flag`     tinyint(1)                                              NULL DEFAULT NULL COMMENT '是否启用查询配置（1是 0否）',
    `query_valid_type`      varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '查询字段校验类型',
    `query_must_input`      tinyint(1)                                              NULL DEFAULT NULL COMMENT '查询字段是否必填（1是 0否）',
    `sort_flag`             tinyint(1)                                              NULL DEFAULT NULL COMMENT '是否支持排序（1是 0否）',
    `create_by`             bigint(20)                                              NULL DEFAULT NULL COMMENT '创建人',
    `create_time`           datetime(0)                                             NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`             bigint(32)                                              NULL DEFAULT NULL COMMENT '修改人',
    `update_time`           datetime(0)                                             NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `inex_table_id` (`table_head_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '在线开发-数据库表字段信息'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for onl_table_head
-- ----------------------------
DROP TABLE IF EXISTS `onl_table_head`;
CREATE TABLE `onl_table_head`
(
    `id`                        bigint(20)                                               NOT NULL COMMENT '主键ID',
    `table_name`                varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NOT NULL COMMENT '表名',
    `table_type`                tinyint(1)                                               NOT NULL COMMENT '表类型（ 0单表、1主表、2附表）',
    `table_version`             int(11)                                                  NULL     DEFAULT 1 COMMENT '表版本',
    `table_description`         varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '表说明',
    `id_type`                   varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL     DEFAULT NULL COMMENT '主键类型',
    `id_sequence`               varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL     DEFAULT NULL COMMENT '主键生成序列',
    `is_checkbox`               tinyint(1)                                               NOT NULL COMMENT '是否带 checkbox',
    `is_synchronize`            varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci   NOT NULL DEFAULT 'N' COMMENT '同步数据库状态',
    `is_page`                   tinyint(1)                                               NOT NULL COMMENT '是否分页',
    `is_tree`                   tinyint(1)                                               NOT NULL COMMENT '是否是树',
    `query_mode`                varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci   NOT NULL COMMENT '查询模式',
    `relation_type`             tinyint(1)                                               NULL     DEFAULT NULL COMMENT '映射关系（0一对多  1一对一）',
    `sub_table_check_list`      varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL     DEFAULT NULL COMMENT '附表清单（数据库表名称）',
    `tab_sort_order`            int(11)                                                  NULL     DEFAULT NULL COMMENT '附表排序序号',
    `tree_id_field_name`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL     DEFAULT NULL COMMENT '树表主键字段',
    `tree_field_name`           varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL     DEFAULT NULL COMMENT '树开表单列字段',
    `tree_parent_id_field_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL     DEFAULT NULL COMMENT '树形表单父id',
    `form_category`             varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NOT NULL DEFAULT 'bdfl_ptbd' COMMENT '表单分类',
    `form_template`             varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL     DEFAULT NULL COMMENT 'PC表单模板',
    `form_template_mobile`      varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL     DEFAULT NULL COMMENT '表单模板样式(移动端)',
    `is_scroll`                 tinyint(1)                                               NULL     DEFAULT 0 COMMENT '是否有横向滚动条',
    `copy_version`              int(11)                                                  NULL     DEFAULT NULL COMMENT '复制版本号',
    `copy_type`                 int(3)                                                   NULL     DEFAULT 0 COMMENT '复制表类型（1为复制表 0为原始表）',
    `physic_id`                 bigint(20)                                               NULL     DEFAULT NULL COMMENT '原始表ID',
    `theme_template`            varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL     DEFAULT NULL COMMENT '主题模板',
    `create_by`                 bigint(20)                                               NOT NULL COMMENT '创建人',
    `create_time`               datetime(0)                                              NOT NULL COMMENT '创建时间',
    `update_by`                 bigint(20)                                               NULL     DEFAULT NULL COMMENT '修改人',
    `update_time`               datetime(0)                                              NULL     DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `index_onlineform_table_name` (`table_name`) USING BTREE,
    INDEX `index_form_templdate` (`form_template`) USING BTREE,
    INDEX `index_templdate_mobile` (`form_template_mobile`) USING BTREE,
    INDEX `index_onlineform_table_version` (`table_version`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '在线开发-数据库表表信息'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for onl_table_index
-- ----------------------------
DROP TABLE IF EXISTS `onl_table_index`;
CREATE TABLE `onl_table_index`
(
    `id`             bigint(20)                                                    NOT NULL COMMENT '主键',
    `table_head_id`  bigint(20)                                                    NOT NULL COMMENT '主表id',
    `index_name`     varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '索引名称',
    `index_field`    varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin        NULL DEFAULT NULL COMMENT '索引栏位',
    `index_type`     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '索引类型',
    `is_synchronize` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT 'N' COMMENT '是否同步数据库（N未同步；Y已同步）',
    `del_flag`       tinyint(1)                                                    NULL DEFAULT 0 COMMENT '是否删除（0未删除 1删除）',
    `description`    varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin       NULL DEFAULT NULL COMMENT '备注',
    `create_by`      bigint(20)                                                    NULL DEFAULT NULL COMMENT '创建人',
    `create_time`    datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建日期',
    `update_by`      bigint(20)                                                    NULL DEFAULT NULL COMMENT '更新人',
    `update_time`    datetime(0)                                                   NULL DEFAULT NULL COMMENT '更新日期',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `index_table_id` (`table_head_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '在线开发-数据库表索引'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`
(
    `id`          bigint(20)                                             NOT NULL,
    `dict_name`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '字典名称',
    `dict_code`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '字典编码',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
    `type`        tinyint(2) UNSIGNED ZEROFILL                           NULL DEFAULT 00 COMMENT '字典类型（0为string，1为number）',
    `del_flag`    tinyint(2)                                             NULL DEFAULT NULL COMMENT '删除状态（0正常 1已删除）',
    `create_by`   bigint(20)                                             NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime(0)                                            NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20)                                             NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0)                                            NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `indextable_dict_code` (`dict_code`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '字典项'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`
(
    `id`          bigint(20)                                             NOT NULL,
    `dict_id`     bigint(20)                                             NULL DEFAULT NULL COMMENT '字典id',
    `item_text`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字典项文本',
    `item_value`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字典项值',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
    `sort_order`  double(8, 2)                                           NULL DEFAULT NULL COMMENT '排序',
    `status`      tinyint(1)                                             NULL DEFAULT NULL COMMENT '状态（1启用 0不启用）',
    `create_by`   bigint(20)                                             NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime(0)                                            NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20)                                             NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0)                                            NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `index_table_dict_id` (`dict_id`) USING BTREE,
    INDEX `index_table_sort_order` (`sort_order`) USING BTREE,
    INDEX `index_table_dict_status` (`status`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '字典值'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`
(
    `id`          bigint(20)                                             NOT NULL COMMENT 'ID',
    `file_name`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '文件名称',
    `real_name`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件上传名',
    `file_type`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件类型',
    `file_url`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '文件地址',
    `file_size`   bigint(20)                                             NULL DEFAULT NULL COMMENT '文件大小',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件描述',
    `del_flag`    tinyint(2)                                             NULL DEFAULT NULL COMMENT '删除状态 （0正常 1已删除）',
    `create_by`   bigint(20)                                             NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime(0)                                            NOT NULL COMMENT '创建时间',
    `update_by`   bigint(20)                                             NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0)                                            NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '文件存放'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `id`          bigint(20) UNSIGNED ZEROFILL                           NOT NULL COMMENT '主键id',
    `parent_id`   bigint(20) UNSIGNED ZEROFILL                           NULL     DEFAULT NULL COMMENT '父记录id',
    `name`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL     DEFAULT NULL COMMENT '菜单名称',
    `url`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL     DEFAULT NULL COMMENT '路径',
    `component`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL     DEFAULT NULL COMMENT '组件',
    `menu_type`   tinyint(1)                                             NOT NULL COMMENT '菜单类型（0：一级菜单，1：子菜单，2按钮权限）',
    `perms`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL     DEFAULT NULL COMMENT '菜单权限编码',
    `perms_type`  tinyint(1)                                             NOT NULL DEFAULT 1 COMMENT '权限策略（1显示，2禁用）',
    `sort_no`     double(8, 2) UNSIGNED ZEROFILL                         NOT NULL COMMENT '菜单排序',
    `icon`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL     DEFAULT NULL COMMENT '菜单图标',
    `leaf_type`   tinyint(1)                                             NOT NULL COMMENT '是否叶子节点（1:是，0:不是）',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL     DEFAULT NULL COMMENT '菜单描述',
    `create_by`   bigint(20)                                             NOT NULL COMMENT '创建人',
    `create_time` datetime(0)                                            NOT NULL COMMENT '创建时间',
    `update_by`   bigint(20)                                             NULL     DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0)                                            NULL     DEFAULT NULL COMMENT '更新时间',
    `del_flag`    int(1)                                                 NOT NULL DEFAULT 0 COMMENT '删除状态（0正常，1已删除）',
    `status`      tinyint(1)                                             NULL     DEFAULT 1 COMMENT '按钮权限状态(0无效，1有效)',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '功能菜单表'
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for sys_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_organization`;
CREATE TABLE `sys_organization`
(
    `id`                bigint(20)                                             NOT NULL COMMENT '主键',
    `parent_id`         bigint(20)                                             NULL DEFAULT NULL COMMENT '父级编号',
    `organization_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '组织机构名称',
    `organization_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '组织机构编号',
    `description`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
    `sort_order`        double(8, 2) UNSIGNED ZEROFILL                         NOT NULL COMMENT '排序号',
    `del_flag`          tinyint(2)                                             NOT NULL COMMENT '删除状态(0-正常,1-已删除)',
    `create_by`         bigint(20)                                             NOT NULL COMMENT '创建人',
    `create_time`       datetime(0)                                            NOT NULL COMMENT '创建时间',
    `update_by`         bigint(20)                                             NULL DEFAULT NULL COMMENT '更新人',
    `update_time`       datetime(0)                                            NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '组织机构-数据权限组'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_quartz_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz_job`;
CREATE TABLE `sys_quartz_job`
(
    `id`              bigint(20)                                             NOT NULL COMMENT '主键id',
    `job_name`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
    `job_group`       bigint(20)                                             NOT NULL COMMENT '任务分组',
    `job_class_name`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '定时任务执行类类名',
    `job_data_map`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '定时任务参数',
    `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'cron表达式',
    `job_state`       tinyint(2)                                             NOT NULL COMMENT '任务状态',
    `description`     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
    `create_by`       bigint(20)                                             NULL DEFAULT NULL COMMENT '创建人',
    `create_time`     datetime(0)                                            NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`       bigint(20)                                             NULL DEFAULT NULL COMMENT '更新人',
    `update_time`     datetime(0)                                            NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '定时任务'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_rich_text
-- ----------------------------
DROP TABLE IF EXISTS `sys_rich_text`;
CREATE TABLE `sys_rich_text`
(
    `id`               bigint(20)                                         NOT NULL COMMENT 'ID',
    `text_type`        tinyint(2)                                         NULL DEFAULT NULL COMMENT '文本类型',
    `primary_table_id` bigint(20)                                         NULL DEFAULT NULL COMMENT '主表id',
    `content`          longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '文本内容',
    `create_by`        bigint(20)                                         NULL DEFAULT NULL COMMENT '创建人',
    `create_time`      datetime(0)                                        NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`        bigint(20)                                         NULL DEFAULT NULL COMMENT '更新人',
    `update_time`      datetime(0)                                        NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '富文本'
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `id`          bigint(20)                                             NOT NULL COMMENT '主键id',
    `role_name`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
    `role_code`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色编码',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
    `create_by`   bigint(20)                                             NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime(0)                                            NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20)                                             NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0)                                            NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `unique_rolename` (`role_name`) USING BTREE COMMENT '角色名唯一约束'
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '系统角色表-权限组'
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`
(
    `id`          bigint(20)  NOT NULL COMMENT 'ID',
    `role_id`     bigint(20)  NOT NULL COMMENT '角色id',
    `menu_id`     bigint(20)  NOT NULL COMMENT '权限id',
    `create_by`   bigint(20)  NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20)  NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '角色-菜单权限关联表'
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for sys_role_table_field
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_table_field`;
CREATE TABLE `sys_role_table_field`
(
    `id`             bigint(20)  NOT NULL COMMENT 'ID',
    `role_id`        bigint(20)  NOT NULL COMMENT '角色id',
    `table_id`       bigint(20)  NOT NULL COMMENT '数据库表id',
    `table_filed_id` bigint(20)  NOT NULL COMMENT '数据库表字段id',
    `create_by`      bigint(20)  NULL DEFAULT NULL COMMENT '创建人',
    `create_time`    datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`      bigint(20)  NULL DEFAULT NULL COMMENT '更新人',
    `update_time`    datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '角色-数据库表表字段关联表'
  ROW_FORMAT = COMPRESSED;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `id`          bigint(20)                                             NOT NULL COMMENT '主键',
    `username`    varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '登录账号',
    `realname`    varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '真实姓名',
    `password`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
    `salt`        varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT 'md5密码盐',
    `avatar`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '头像',
    `birthday`    date                                                   NULL     DEFAULT NULL COMMENT '生日',
    `sex`         tinyint(1)                                             NULL     DEFAULT 0 COMMENT '性别(0-默认未知,1-男,2-女)',
    `email`       varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NULL     DEFAULT NULL COMMENT '电子邮件',
    `phone`       varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NULL     DEFAULT NULL COMMENT '电话',
    `status`      tinyint(1)                                             NOT NULL DEFAULT 0 COMMENT '状态(1-正常,2-冻结)',
    `del_flag`    tinyint(1)                                             NOT NULL DEFAULT 0 COMMENT '删除状态(0-正常,1-已删除)',
    `create_by`   bigint(32)                                             NOT NULL COMMENT '创建人',
    `create_time` datetime(0)                                            NOT NULL COMMENT '创建时间',
    `update_by`   bigint(32)                                             NULL     DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0)                                            NULL     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `unique_username` (`username`) USING BTREE COMMENT '用户名唯一约束'
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '系统用户表'
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for sys_user_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_organization`;
CREATE TABLE `sys_user_organization`
(
    `id`              bigint(20)  NOT NULL COMMENT '主键',
    `user_id`         bigint(20)  NOT NULL COMMENT '用户编号',
    `organization_id` bigint(20)  NOT NULL COMMENT '组织机构编号',
    `is_responsible`  tinyint(2)  NULL DEFAULT NULL COMMENT '是否负责部门（0-不负责，1-负责）',
    `create_by`       bigint(20)  NULL DEFAULT NULL COMMENT '创建人',
    `create_time`     datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`       bigint(20)  NULL DEFAULT NULL COMMENT '更新人',
    `update_time`     datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '用户-组织机构关联表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`
(
    `id`          bigint(20)  NOT NULL COMMENT 'ID',
    `user_id`     bigint(20)  NOT NULL COMMENT '用户id',
    `role_id`     bigint(20)  NOT NULL COMMENT '角色id',
    `create_by`   bigint(20)  NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20)  NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = '用户角色关联表'
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for sys_websocket_message
-- ----------------------------
DROP TABLE IF EXISTS `sys_websocket_message`;
CREATE TABLE `sys_websocket_message`
(
    `id`            bigint(20)                                             NOT NULL COMMENT '主键id',
    `sender`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '发送方',
    `receiver`      varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '接收方',
    `text_id`       bigint(20)                                             NOT NULL COMMENT '信息内容id',
    `message_state` tinyint(2)                                             NOT NULL COMMENT '信息状态（0：发送成功，1：等待接收，2：发送异常）',
    `create_by`     bigint(20)                                             NULL DEFAULT NULL COMMENT '创建人',
    `create_time`   datetime(0)                                            NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`     bigint(20)                                             NULL DEFAULT NULL COMMENT '更新人',
    `update_time`   datetime(0)                                            NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT = 'WebSocket信息传递记录'
  ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
