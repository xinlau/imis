package com.imis.module.system.controller;

import com.imis.Application;
import com.imis.module.system.bus.SysDictBus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class SysDictControllerTest {

    @Autowired(required = false)
    protected SysDictBus service;

    @Test
    void pagingQuerySysDictListByParameter() {
    }

    @Test
    void addDict() {
    }

    @Test
    void deleteDict() {
    }

    @Test
    void queryDictById() {
    }

    @Test
    void updateDict() {
    }

    @Test
    void queryDictItemByDict() {
    }

    @Test
    void pagingQuerySysDictItemListByParameter() {
    }

    @Test
    void addDictItem() {
    }

    @Test
    void deleteDictItem() {
    }

    @Test
    void queryDictItemById() {
    }

    @Test
    void updateDictItem() {
    }
}