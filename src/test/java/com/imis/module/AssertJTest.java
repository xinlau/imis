package com.imis.module;

import com.imis.module.system.model.po.SysDict;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * <p>
 * AssertJTest<br>
 *
 * </p>
 *
 * @author XinLau
 * @version 1.0
 * @since 2020年08月21日 17:53
 */
public class AssertJTest {

    @Test
    void doAssertJTestForString() {
        // 字符串断言
        assertThat("test")
                // 字符串断言描述
                .as("字符串断言描述")
                // 是否为" "字符串
                .isNotBlank()
                // 是否为test1的一部分
                .isSubstringOf("test1")
                // 对象内元素是否相等
                .isSameAs("test1")
                // 是否为空字符串
                .isNotEmpty()
                // 是否相等
                .isEqualTo("test")
                // 是否相等（忽略大小写）
                .isEqualToIgnoringCase("Test")
                // 是否是实例
                .isExactlyInstanceOf(String.class)
                // 是否在列表中
                .isIn(Arrays.asList("test", "hello"))
                // 是否在参数列表中
                .isIn("test", "hello")
                // 是否是实例中任何一个
                .isInstanceOfAny(String.class, Integer.class)
                // 是否不为空
                .isNotNull()
                // 是否包含es子串
                .contains("es")
                // te开始
                .startsWith("te")
                // st结束
                .endsWith("st")
                // 是否匹配 .e.t 格式
                .matches(".e.t");
        assertThat("").isNullOrEmpty();
    }

    @Test
    void doAssertJTestForNumber() {
        // 数字断言
        assertThat(new Integer(100))
                // 数字断言描述
                .as("数字断言描述")
                // 是否相等
                .isEqualTo(100)
                // 是否在0，300之间
                .isBetween(0, 300)
                // 是否非空
                .isNotNull()
                // 是否不等于0
                .isNotZero()
                // 是否大约等于80
                .isGreaterThanOrEqualTo(80)
                // 是否小于200
                .isLessThan(200)
                // 是否是正数
                .isPositive()
                // 是否是非负数
                .isNotNegative()
                // 是否在列表中
                .isIn(Arrays.asList(100, 200))
                // 是否是Integer类型
                .isInstanceOf(Integer.class);
    }

    @Test
    void doAssertJTestForDate() {
        // 日期断言
        assertThat(new Date())
                .as("日期断言描述")
                .isAfter("2018-08-01")
                .isAfterYear(2017)
                .isBetween("2018-01-01", "2018-08-31")
                .isEqualToIgnoringHours(new Date().toString())
                .isExactlyInstanceOf(Date.class)
                .isInSameHourAs(new Date())
                .isInThePast()
                .isToday();
    }

    @Test
    void doAssertJTestForArraysList() {
        // 列表断言
        assertThat(Arrays.asList("world", "hello"))
                .as("列表断言描述")
                .isNotEmpty()
                .isNotNull()
                .isInstanceOf(List.class)
                .isSubsetOf("hello", "world")
                .contains("hello")
                .containsOnlyOnce("world")
                .startsWith("world")
                .endsWith("hello");
    }

    @Test
    void doAssertJTestForEntity() {

        SysDict sysDict = new SysDict();
        sysDict.setDelFlag(1);
        sysDict.setDescription("123");
        sysDict.setDictCode("123");
        sysDict.setDictName("name123");
        sysDict.setType(1);

        SysDict sysDict1 = new SysDict();
        sysDict1.setDelFlag(2);
        sysDict1.setDescription("456");
        sysDict1.setDictCode("456");
        sysDict1.setDictName("name456");
        sysDict1.setType(2);

        SysDict sysDict2 = sysDict;

        // 对象断言
        assertThat(sysDict)
                .as("对象断言描述")
                // sysDict 的每个字段是否与 sysDict1 相同
                .isEqualToComparingFieldByField(sysDict1).as("不相同")
                // sysDict 是否是 SysDict 类的对象
                .isExactlyInstanceOf(SysDict.class).as("不是SysDict对象")
                // 是否是同一个对象
                .isSameAs(sysDict2).as("不是一个对象")
                // 是否非空
                .isNotNull()
                // 是否含有 dictName 字段
                .hasFieldOrProperty("dictName")
                // 是否含有 dictCode 字段，且值为 "123"
                .hasFieldOrPropertyWithValue("dictCode", 123);
    }

}
