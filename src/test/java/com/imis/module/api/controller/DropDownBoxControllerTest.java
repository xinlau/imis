package com.imis.module.api.controller;

import com.imis.Application;
import com.imis.module.api.bus.DropDownBoxBus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class DropDownBoxControllerTest {

    @Autowired(required = false)
    protected DropDownBoxBus service;

    @Test
    void queryDictItemsByDictCode() {
        // service.queryDictItemsByDictCode();
    }

    @Test
    void queryTableDictItemsByParameter() {
        // service.queryTableDictItemsByParameter();
    }

    @Test
    void queryTableDictItemsByCode() {
        // service.queryTableDictItemsByCode();
    }

    @Test
    void queryOrganizationTreeByOrganizationName() {
        // service.queryOrganizationTreeByOrganizationName();
    }

    @Test
    void queryMenuTreeByMenuName() {
        // service.queryMenuTreeByMenuName();
    }

    @Test
    void queryDictTreeListByParameter() {
        // service.queryDictTreeListByParameter();
    }

    @Test
    void queryQuartzJobClassName() {
        service.queryQuartzJobClassName();
    }

    @Test
    void queryQuartzJobParameterName() {
        // service.queryQuartzJobParameterName();
    }

    @Test
    void queryPermissionsValueConstantField() {
        service.queryPermissionsValueConstantField("user");
    }

    @Test
    void queryDictCodeConstantField() {
        service.queryDictCodeConstantField();
    }

}