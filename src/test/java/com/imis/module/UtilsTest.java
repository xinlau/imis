package com.imis.module;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.google.common.base.Joiner;
import com.imis.base.constant.CommonConstant;
import com.imis.base.util.*;
import org.junit.jupiter.api.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

class UtilsTest {

    @Test
    void addLog() {
        String logContent = "错误代码:%s,错误内容:%s.";

        String errCode = "13215";
        String errMsg = "登录错误";

        String s = addLogFormat(logContent, errCode, errMsg);

        System.out.println(s);

    }

    String addLogFormat(String logContent, Object... args) {
        return String.format(logContent, args);
    }

    @Test
    void SendSmsTest() {
        String phone = "18705486858";
        String[] paramName = {"code"};
        String[] paramValue = {"987654"};
        // SendSmsUtil.doSendSms(SendSmsEnum.LOGIN_TEMPLATE_CODE, phone, new SendSmsUtil.TemplateParam(paramName, paramValue));
    }

    @Test
    void sendEmailTest() {
        MailAccount account = new MailAccount();
        account.setHost("smtp.qq.com");
        account.setPort(25);
        account.setAuth(true);
        account.setFrom("xiaotaiye1996@qq.com");
        account.setUser("xiaotaiye1996@qq.com");
        account.setPass("");
        MailUtil.send(account, CollUtil.newArrayList("1347679271@qq.com"), "邮箱验证码", "邮件来自 IMIS-BOOT 测试", false);
        String A = String.valueOf(account);
        System.out.println(A);
    }

    @Test
    void getYaml() {
        Yaml yaml = new Yaml();
        URL url = UtilsTest.class.getClassLoader().getResource("application-dev.yml");

        if (url != null) {
            try {
                LinkedHashMap<String, LinkedHashMap> linkedHashMap = yaml.load(new FileInputStream(url.getFile()));
                Map<String, Object> object = yaml.load(new FileInputStream(url.getFile()));
                Map<String, Object> spring = (Map<String, Object>) object.get("spring");
                Map<String, Object> datasource = (Map<String, Object>) spring.get("datasource");
                Map<String, Object> dynamic = (Map<String, Object>) datasource.get("dynamic");
                Map<String, Object> datasource2 = (Map<String, Object>) dynamic.get("datasource");
                Map<String, Object> master = (Map<String, Object>) datasource2.get("master");
                for (Object s : master.values()) {
                    System.out.println(s);
                }
                linkedHashMap.get("spring").get("datasource");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


    private static final String CLASS_ROUTE = "/src/main/java/com/zzdz/platform/module/";

    public static String scanner(String string) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("请输入一个正确的" + string + "：");
        System.out.println(stringBuilder.toString());
        if (scanner.hasNext()) {
            String name = scanner.next();
            if (StringUtils.isNotBlank(name)) {
                return name;
            }
        }
        throw new MybatisPlusException("请输入正确的" + string + "！！！");
    }

    @Test
    void TestScanner() {
        String string = CLASS_ROUTE + scanner("模块名") + "/";
        System.out.println(string);
    }

    @Test
    void TestSerialCodeUtil() {
        String code = "A001Z999";
        String nextSerialCode = SerialCodeUtil.getNextSerialCode(code);
        System.out.println(nextSerialCode);
    }

    @Test
    void TestClazzNameUtil() {
        List<String> className = PackageClassUtils.getClazzName(CommonConstant.QUARTZ_CLASS_NAME, false);
        className.remove("com.imis.module.quartz.QuartzJobInitialization");
        System.out.println(Joiner.on(",").join(className));
    }

    @Test
    void doTestForRestTemplateUtil() {
        String remoteUrl = "http://172.16.102.3:8019/Api/Login/CheckLogin";
        JSONObject params = new JSONObject();
        params.put("account", "13207570768");
        params.put("password", "7a2bd812b9d2297fc32704b126445df7");
        params.put("mac", "LeiJinRong");

        JSONObject jsonObject = RestTemplateUtil.postForEntity(remoteUrl, JSONObject.class, params);

        System.out.println(jsonObject.toJSONString());
    }

    @Test
    void doTestForMD5AndMD2() {
        String string = "MD";
        String md5 = MD5Util.md5Encode(string, CommonConstant.UTF_8);

        System.out.println(md5);

        String md2 = MD2Util.md2Encode(string, CommonConstant.UTF_8);

        System.out.println(md2);
    }

    @Test
    void doTestForAESUtil() {
        String string = "MD";
        // 加密
        String encode = AESUtil.encode(string);
        System.out.println(encode);
        // 解密
        String decode = AESUtil.decode(encode);
        System.out.println(decode);

    }

    @Test
    void doTestForMacAndDisk() {
        System.out.println(getSerialNumber("C"));
    }

    public static String getSerialNumber(String drive) {
        String result = "";
        try {
            File file = File.createTempFile("damn", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);
            String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                    + "Set colDrives = objFSO.Drives\n"
                    + "Set objDrive = colDrives.item(\""
                    + drive
                    + "\")\n"
                    + "Wscript.Echo objDrive.SerialNumber";
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;

            }
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.trim();
    }

    @Test
    void doTestForComputerUniqueIdentificationUtil() {
        System.out.println("当前计算机操作系统名称：" + ComputerUniqueIdentificationUtil.getOsName());
        System.out.println("当前计算机的 CPU 序列号：" + ComputerUniqueIdentificationUtil.getCpuIdentification());
        System.out.println("当前计算机网卡的 MAC 地址：" + ComputerUniqueIdentificationUtil.getMacAddress());
        System.out.println("当前计算机主板序列号：" + ComputerUniqueIdentificationUtil.getMainBoardSerialNumber());
        System.out.println("当前计算机唯一标识：" + ComputerUniqueIdentificationUtil.getComputerUniqueIdentificationString());
    }

    @Test
    void doTestForDate() {
        String y1 = "2016-02-01";// 开始时间
        String y2 = "2019-12-01";// 结束时间

        Map<String, String> map = new HashMap<>();

        try {
            Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(y1);
            Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(y2);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            // 获取开始年份和开始月份
            int startYear = calendar.get(Calendar.YEAR);
            int startMonth = calendar.get(Calendar.MONTH);
            // 获取结束年份和结束月份
            calendar.setTime(endDate);
            int endYear = calendar.get(Calendar.YEAR);
            int endMonth = calendar.get(Calendar.MONTH);
            //
            List<String> list = new ArrayList<String>();
            for (int i = startYear; i <= endYear; i++) {
                String date = CommonConstant.EMPTY;
                if (startYear == endYear) {
                    for (int j = startMonth; j <= endMonth; j++) {
                        if (j < 9) {
                            date = i + "-0" + (j + 1);
                        } else {
                            date = i + "-" + (j + 1);
                        }
                        list.add(date);
                        map.put(date.substring(date.indexOf("-") + 1), date);
                    }

                } else {
                    if (i == startYear) {
                        for (int j = startMonth; j < 12; j++) {
                            if (j < 9) {
                                date = i + "-0" + (j + 1);
                            } else {
                                date = i + "-" + (j + 1);
                            }
                            list.add(date);
                            map.put(date.substring(date.indexOf("-") + 1), date);
                        }
                    } else if (i == endYear) {
                        for (int j = 0; j <= endMonth; j++) {
                            if (j < 9) {
                                date = i + "-0" + (j + 1);
                            } else {
                                date = i + "-" + (j + 1);
                            }
                            list.add(date);
                            map.put(date.substring(date.indexOf("-") + 1), date);
                        }
                    } else {
                        for (int j = 0; j < 12; j++) {
                            if (j < 9) {
                                date = i + "-0" + (j + 1);
                            } else {
                                date = i + "-" + (j + 1);
                            }
                            list.add(date);
                            map.put(date.substring(date.indexOf("-") + 1), date);
                        }
                    }
                }
            }

            // 所有的月份已经准备好
            //System.out.println(list);
            for (int i = 0; i < list.size(); i++) {
                // System.out.println(list.get(i));
            }

            System.out.println(JSONObject.toJSONString(map));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void doTestForLocalDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();

        String timeString = DateUtil.formatLocalDateTime(localDateTime);
        Date date = DateUtil.parse(timeString);
        LocalDateTime start = DateUtil.toLocalDateTime(DateUtil.beginOfMonth(date));
        LocalDateTime end = DateUtil.toLocalDateTime(DateUtil.endOfMonth(date));

        System.out.println(start);
        System.out.println(end);
    }

    @Test
    void doTestForPath() {
        String resource = this.getClass().getClassLoader().getResource("").getPath();
        System.out.println(resource);
    }

    @Test
    void doTestForArray() {
        String resource = null;
        String[] strings = resource.split(CommonConstant.COMMA);
        System.out.println(JSONObject.toJSONString(strings));
    }

    @Test
    void doTestForCryptoUtils() {
        String password = "root";
        String encodePassword = PasswordUtil.encrypt(password);
        System.out.println(encodePassword);
        password = PasswordUtil.decrypt(encodePassword);
        System.out.println(password);
    }

    /**
     * 获取默认停止Shell脚本
     * @param processName 进程名称
     * @return 返回默认stopShell脚本内容
     */
    private String stopDefaultShell(String processName){
        StringBuilder stringBuilder = new StringBuilder();
        //判断进程是否存在,如果进程存在则删除进程
        stringBuilder.append("echo \"Stop Procedure : "+processName+"\"").append("\n");
        stringBuilder.append("pid=`ps -ef |grep java|grep "+processName+"|awk '{print $2}'`").append("\n");
        stringBuilder.append("echo 'old Procedure pid:'$pid").append("\n");
        stringBuilder.append("if [ -n \"$pid\" ]").append("\n");
        stringBuilder.append("then").append("\n");
        stringBuilder.append("kill $pid").append("\n");
        stringBuilder.append("fi");
        return stringBuilder.toString();
    }

    @Test
    void doTestForStopShell(){
        System.out.println(stopDefaultShell("11111"));
    }

    @Test
    void doTestForCamelToUnderline(){
        String a = "userName,password,userCode";
        String b = ConvertUtils.camelToUnderline(a);
        System.out.println(b);
        String c = ConvertUtils.camelNameCapFirst(b);
        System.out.println(c);
        String d = ConvertUtils.camelName(b);
        System.out.println(d);
    }

}