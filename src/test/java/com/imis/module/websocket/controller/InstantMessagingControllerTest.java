package com.imis.module.websocket.controller;

import com.imis.Application;
import com.imis.module.websocket.bus.InstantMessagingBus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class InstantMessagingControllerTest {

    @Autowired(required = false)
    protected InstantMessagingBus service;

    @Test
    void queryOnlineNumber() {
        service.queryOnlineNumber();
    }

    @Test
    void pagingQueryHistoricalMassages() {
        // service.pagingQueryHistoricalMassages();
    }

    @Test
    void sendMassage() {
        // service.sendMassage();
    }
}