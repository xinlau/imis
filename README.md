![IMIS](doc/image/imis.png)      ![IDEA](doc/image/jetbrains.png)

# IMIS-Boot 快速开发平台V1.0 #

> [当前最新版本](https://github.com/XinLau1996/imis) ： 1.0.0（发布日期：2020-06-28）

> Java Framework For Enterprise Web Applications

> 面向企业web应用的Java框架

## 框架介绍

### 1. 后端选用技术

| 序号 | 名字 | 描述 |
| :---: | :--- | :--- |
|  | JDK1.8 | JAVA版本 |
|  | spring-boot-starter-parent 2.2.2.RELEASE | Spring-Boot |
|  | mybatis-plus-boot-starter | MP数据库交互组件 |
|  | dynamic-datasource-spring-boot-starter | MP动态数据源组件 |
|  | druid-spring-boot-starter | 数据库连接池 监控 插件 |
|  | spring-boot-starter-freemarker | 页面渲染引擎 |
|  | shiro-spring-boot-starter | shiro组件 |
|  | org.crazycake shiro-redis | 权限redis缓存组件 |
|  | java-jwt | token组件 |
|  | spring-boot-starter-websocket | websocket消息组件 |
|  | spring-data-redis | redis缓存组件 |
|  | spring-boot-starter-quartz | Quartz定时任务 |
|  | spring-boot-starter-test | 单元测试组件 |
|  | spring-boot-devtools springboot | 热部署插件 |
|  | knife4j-spring-boot-starter | Swagger接口文档组件 |
|  | org.projectlombok | lombok 简化开发插件 |
|  | hutool-all | 常用工具类组件 |
|  | kaptcha | 验证码 |
|  | aliyun-java-sdk-core | 阿里云SDK |
|  | fastjson | JSON组件 |
|  | assertj-core | 断言工具组件 |
|  | mapstruct-jdk8 | 对象映射组件 |
|  | mockito-core | mock测试组件 |
|  | spring-boot-actuator | 服务器性能监测组件 |

### 2. 后端开发工具以及插件

#### 开发工具：  

| 序号 | 名字 | 描述 |
| :---: | :--- | :--- |
|  | IDEA | IntelliJ IDEA | 
|  | Navicat | Navicat Premium 15 |  
|  | Redis Desktop Manager | Redis Desktop Manager | 
|  | Notepad++ | Notepad++ | 

#### 开发插件：  

| 序号 | 名字 | 描述 |
| :---: | :--- | :--- |
|  | Lombok | 实体类 | 
|  | JRebel and XRebel | 热部署不重启自动刷新 | 
|  | JRebel MybatisPlus extension | JRebel热部署不重启自动刷新Xml文件 | 
|  | MyBatisCodeHelper-Pro | MyBatis付费插件 |  
|  | Grep Console | 控制台日志 |

### 3.项目结构图

```text
├─doc
│  └─image
└─src
  └─main
    ├─java
    │  └─com
    │      └─imis
    │          ├─base
    │          │  ├─annotation
    │          │  ├─aspect
    │          │  ├─config
    │          │  ├─constant
    │          │  │  ├─enums
    │          │  │  └─permissions
    │          │  ├─filter
    │          │  ├─globle
    │          │  ├─shiro
    │          │  │  ├─authc
    │          │  │  │  └─aop
    │          │  │  └─util
    │          │  └─util
    │          └─module
    │              ├─api
    │              │  ├─bus
    │              │  ├─controller
    │              │  └─model
    │              │    ├─ro
    │              │    └─vo
    │              ├─base
    │              │  └─impl
    │              ├─online
    │              │   ├─form
    │              │   │   ├─bus
    │              │   │   ├─controller
    │              │   │   ├─dao
    │              │   │   │  └─xml
    │              │   │   ├─model
    │              │   │   │  ├─converter
    │              │   │   │  ├─po
    │              │   │   │  ├─ro
    │              │   │   │  └─vo
    │              │   │   └─service
    │              │   │      └─impl
    │              │   └─table
    │              │       ├─bus
    │              │       ├─controller
    │              │       ├─dao
    │              │       │  └─xml
    │              │       ├─model
    │              │       │  ├─converter
    │              │       │  ├─po
    │              │       │  ├─ro
    │              │       │  └─vo
    │              │       └─service
    │              │          └─impl  
    │              ├─quartz
    │              ├─system
    │              │   ├─bus
    │              │   ├─controller
    │              │   ├─dao
    │              │   │  └─xml
    │              │   ├─model
    │              │   │  ├─converter
    │              │   │  ├─po
    │              │   │  ├─ro
    │              │   │  └─vo
    │              │   └─service
    │              │      └─impl
    │              └─websocket
    │                  ├─bus
    │                  ├─controller
    │                  └─dto
    └─resources
        ├─dataBase
        ├─deployHelperConfig
        ├─logbackConfig
        ├─static
        │  ├─json
        │  ├─json
        │  └─template
        └─templates
            ├─codegeneration
            └─table


```

### 4.数据库表

![IMIS](doc/image/IMIS-BOOT.png)

### 5.感谢

> [感谢 IntelliJ IDEA 对开源的支持](https://www.jetbrains.com/?from=IMIS-Boot快速开发平台)

> [感谢 knife4j 对开源的支持](https://doc.xiaominfo.com/)

> [感谢 MyBatis-Plus 对开源的支持](https://baomidou.com/)

### 6.捐赠

如果觉得还不错，请作者喝杯咖啡吧 ☺☺☺ 

![收钱码](doc/image/收钱码.jpg)
