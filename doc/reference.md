![IMIS](image/imis.png) 

# IMIS-Boot 快速开发平台v1.0

> 待开发：短信模版、邮件模版、消息模版维护

## 注意事项：

### 1、关于数据库：
   <ul>
       <li>注意有关于SysLog的需要手动指定数据源</li>
       <li>代码生成器数据库tinyint(1)转换为Integer类型的操作：请把字段长度修改成2个长度，代码生成完成后再改回一个长度</li>
   </ul>

### 2、关于MyBatis的XML文件：
   <ul>
       <li>在mybatis的XML中如果拿传入的时间类型参数与空字符串''进行对比判断则会引发异常，这里需要注意字符串类型一定要与空字符串''进行对比判断</li>
       <li>XML Sql文件中 使用 小于 &lt; 大于 &gt; 关键字</li>
         
```text
    <![CDATA[<= ]]>
    <![CDATA[>= ]]>
```

   <li>XML Sql文件中 使用LIKE 关键字: bind标签 </li>
    
```text
<bind name="organizationName" value="'%' + sysOrganizationSelect.organizationName + '%'"></bind>
```

   <li>XML Sql文件中 使用LIKE 关键字: CONCAT语法 </li>

```text
organization_name LIKE CONCAT('%', #{sysOrganizationSelect.organizationName}, '%')
```
        
   </ul>

### 3 常见问题
   <ul>
       <li>可以使用equals()方法检测两个字符串是否相等; 一定不要使用 == 运算符检测两个字符串是否相等！</li>
       <li>如果想要忽略大小写的区别，可以使用equalsIgnoreCase()方法。</li>
       <li>由于枚举类型确保JVM中仅存在一个常量实例，因此我们可以安全地使用“ ==”运算符比较两个变量；此外，“==” 运算符可提供编译时和运行时的安全性</li>
       <li>VO对象内强制不出现Long、long类型的字段，转成字符串返回给前端，避免出现精度丢失问题</li>
       <li>注意使用 try-with-resource 语法实现对资源（文件、数据库连接、网络连接等）的关闭</li>
       <li>接口幂等性：接口幂等性就是用户对于同一操作发起的一次请求或者多次请求的结果是一致的，不会因为多次点击而产生了副作用。</li>
       <li>PUT请求：如果两个请求相同，后一个请求会把第一个请求覆盖掉。（PUT用来改资源）</li>
       <li>Post请求：后一个请求不会把第一个请求覆盖掉。（Post用来增资源）</li>
       <li>当@Transactional遇到@CacheEvict，你的代码是不是有Bug https://cloud.tencent.com/developer/article/1551858</li>
   </ul>








 
 <li>查看被占用端口对应的PID：netstat -aon|findstr "端口号"</li>
 <li>查看是哪个进程或者程序占用了端口：tasklist|findstr "PID号"</li>
 <li>结束该进程：taskkill /f /t /im 进程名字</li>
 <li>用生产环境运行：--spring.profiles.active=prod</li>

 <li>解决DIC注解造成的 Type definition error: [simple type, class cn.hutool.json.JSONNull]; 问题， Hutool会使用JSONNull来表示空值，而SpringCloud Feign默认使用的序列化是Jackson，在远程调用的过程中使用了Map，直接传入了Hutool的JSONObject，而该Map存在空值，所以存在JSONNull，最终导致错误。添加如下配置</li>


``` text
spring:
    jackson:
      default-property-inclusion: non_null  
```

<li>彩色日志控制台输出</li>

```text
-Dspring.output.ansi.enabled=ALWAYS
```

## Spring Boot参考指南

[参考](https://www.springcloud.cc/spring-boot.html)

## 自定义注解：
| 注解名字 | 描述 | 示例 |
| :---: | :---: | :--- |
| @Dic | 字典注解 | @Dic private Int sex; |
| @HashCache | Hash缓存新增注解 | @HashCache(hashIdentification = CacheConstant.PREFIX_SYS_USERS_CACHE, hashKey = "'getUserByName_' + #username" expireTime = "1000 * 60 * 60 * 24") |
| @HashCachePut | Hash缓存修改注解 | @HashCachePut(hashIdentification = CacheConstant.PREFIX_SYS_USERS_CACHE, hashKey = "'getUserByName_' + #username" expireTime = "1000 * 60 * 60 * 24") |
| @HashCacheEvict | Hash缓存删除注解 | @HashCacheEvict(hashIdentification = CacheConstant.PREFIX_SYS_USERS_CACHE, hashKey = "'getUserByName_' + #username") |
| @VerificationCaptcha | 验证码验证注解 | @VerificationCaptcha(verification = false, type = VerificationCodeTypeEnum.OPERATION, captcha = "#sysLoginRO.captcha") |
| @RepeatOperationLock | 防止高并发重复请求注解 | @RepeatOperationLock(lockIdentification = "", type = RepeatOperationLockEnum.IP, timeOut = "500", automaticUnlocking = false) |

## SQL 执行顺序解析：

![QUARTZ](image/SQLExecutionOrder.jpg) 

## SPEL表达式：

| 名字 | 位置 | 描述 | 示例 |
| :---: | :---: | :--- | :---: |
| methodName | root object | 当前被调用的方法名 | #root.methodName |
| method | root object | 当前被调用的方法 | #root.method.name |
| target | root object | 当前被调用的目标对象 | #root.target |
| targetClass | root object | 当前被调用的目标对象类 | #root.targetClass |
| args | root object | 当前被调用的方法的参数列表 | #root.args[0] |
| caches | root object | 当前方法调用使用的缓存列表（如@Cacheable(value={"cache1", "cache2"})），则有两个cache | #root.caches[0].name |
| argument name | evaluation context | 方法参数的名字. 可以直接 #参数名 ，也可以使用 #p0或#a0 的形式，0代表参数的索引； | #id 、 #a0 、 #p0 |
| result | evaluation context | 方法执行后的返回值（仅当方法执行之后的判断有效，如‘unless’，’cache put’的表达式 ’cache evict’的表达式,'cacheable'中不可用’'beforeInvocation=false） | #result |

## JdbcType类型和Java类型的对应关系：

| 序号 | JDBC Type | Java Type |
| :---: | :---: | :---: |         
| | CHAR | String  
| | VARCHAR | String  
| | LONGVARCHAR| String  
| | NUMERIC | java.math.BigDecimal  
| | DECIMAL | java.math.BigDecimal  
| | BIT | boolean  
| | BOOLEAN | boolean  
| | TINYINT | byte  
| | SMALLINT | short  
| | INTEGER | Integer  
| | BIGINT | long  
| | REAL | float  
| | FLOAT | double  
| | DOUBLE | double  
| | BINARY | byte[]  
| | VARBINARY | byte[]  
| | LONGVARBINARY | byte[]  
| | DATE | java.sql.Date  
| | TIME | java.sql.Time  
| | TIMESTAMP | java.sql.Timestamp  
| | CLOB | Clob  
| | BLOB | Blob  
| | ARRAY | Array  
| | DISTINCT | mapping of underlying type  
| | STRUCT | Struct  
| | REF | Ref
| | DATALINK | java.net.URL

## Cron表达式

### cron说明

```text
cron一定有七位数,最后一位是年
```

| 序号 | 位数 | 含义 | 取值范围 |
| :---: | :---: | :---: | :--- |
| | 第一位 | 表示秒 | 取值是0 ~ 59
| | 第二位 | 表示分 | 取值是0 ~ 59
| | 第三位 | 表示小时 | 取值是0 ~ 23
| | 第四位 | 表示天/日 | 取值是0 ~ 31
| | 第五位 | 表示月份 | 取值是1 ~ 12
| | 第六位 | 表示星期 | 取值是1 ~ 7, 星期一，星期二...， 还有 1 表示星期日
| | 第七位 | 表示年份 | 可以留空, 取值是1970 ~ 2099


### cron中,还有一些特殊的符号,含义如下:

```text
ron表达式对特殊字符的大小写不敏感，对代表星期的缩写英文大小写也不敏感。
```

| 序号 | 符号 | 含义 |
| :---: | :---: | :--- |
| | (*)星号 | 可以理解为每的意思，每秒、每分、每天、每月、每年...。 |
| | (?)问号 | 问号只能出现在日期和星期这两个位置，表示这个位置的值不确定，每天 3 点执行，因此第六位星期的位置，是不需要关注的，就是不确定的值；同时，日期和星期是两个相互排斥的元素，通过问号来表明不指定值，比如 1 月 10 日是星期一，如果在星期的位置另指定星期二，就前后冲突矛盾了。 |
| | (-)减号 | 表达一个范围，如在小时字段中使用“10 - 12”，则表示从 10 到 12 点，即 10、11、12。 |
| | (,)逗号 | 表达一个列表值，如在星期字段中使用“1,2,4”，则表示星期一、星期二、星期四。 |
| | (/)斜杠 | 如 x/y，x 是开始值，y 是步长，比如在第一位（秒），0/15 就是从 0 秒开始，每隔 15 秒执行一次，最后就是 0、15、30、45、60，另 */y，等同于 0/y。 |
| | (#)井号 | 该字符只能在星期字段中使用，表示当月某个工作日。如6#3表示当月的第三个星期五(6表示星期五，#3表示当前的第三个)，而4#5表示当月的第五个星期三，假设当月没有第五个星期三，忽略不触发。 |
| | L | 该字符只在日期和星期字段中使用，代表“Last”的意思，但它在两个字段中意思不同。L在日期字段中，表示这个月份的最后一天，如一月的31号，非闰年二月的28号；如果L用在星期中，则表示星期六，等同于7。但是，如果L出现在星期字段里，而且在前面有一个数值X，则表示“这个月的最后X天”，例如，6L表示该月的最后星期五 |
| | W | 该字符只能出现在日期字段里，是对前导日期的修饰，表示离该日期最近的工作日。例如15W表示离该月15号最近的工作日，如果该月15号是星期六，则匹配14号星期五；如果15日是星期日，则匹配16号星期一；如果15号是星期二，那结果就是15号星期二。但必须注意关联的匹配日期不能够跨月，如你指定1W，如果1号是星期六，结果匹配的是3号星期一，而非上个月最后的那天。W字符串只能指定单一日期，而不能指定日期范围。 |
| | LW | 在日期字段可以组合使用LW，它的意思是当月的最后一个工作日。 |
| | C | 该字符只在日期和星期字段中使用，代表“Calendar”的意思。它的意思是计划所关联的日期，如果日期没有被关联，则相当于日历中所有日期。例如5C在日期字段中就相当于日历5日以后的第一天。1C在星期字段中相当于星期日后的第一天。 |

###  举几个例子熟悉一下: 

[参考](https://zhuanlan.zhihu.com/p/35629505)

| 序号 | 表达式 | 说明 |
| :---: | :---: | :--- |         
| | 0 0 12 * * ? | 每天12点  
| | 0 15 10 ? * * | 每天10点15  
| | 0 15 10 * * ? | 每天10点15
| | 0 15 10 * * ? * | 每天10点15
| | 0 15 10 * * ? 2020 | 2020年的每天10点15
| | 0 * 14 * * ? | 每天14点到15点之间每1分钟运行一次，开始于14：00，结束于14：59
| | 0 0/5 14 * * ? | 每天14点到15点之间每5分钟运行一次，开始于14：00，结束于14：55
| | 0 0/5 14,18 * * ? | 每天14点到15点之间、每天18点到19点每5分钟运行一次
| | 0 0-5 14 * * ? | 每天14:00到14:05之间、每1分钟运行一次
| | 0 10,44 14 ? 3 WED | 三月的每周三的14：10到14：44每分钟运行一次
| | 0 15 10 ? * MON-FRI | 每周一、二、三、四、五的10：15运行一次
| | 0 15 10 15 * ? | 每月的15日10：15运行
| | 0 15 10 L * ? | 每月的最后一天10：15运行
| | 0 15 10 ? * 6L | 每月的最后一个星期五10：15运行
| | 0 15 10 ? * 6L 2020-2030 | 2020年到2030年每月的最后一个星期五10：15运行
| | 0 15 10 ? * 6#3 | 每月的第三个星期五10：15运行

## mapstruct-jdk8 常用注解

[参考](https://mapstruct.org/documentation/stable/reference/html/)

| 序号 | 注解名字 | 说明 | 参数及含义 |
| :---: | :---: | :--- | :--- | 
| | @Mapper | 只有在接口加上这个注解， MapStruct 才会去实现该接口 | @Mapper 里有个 componentModel 属性，主要是指定实现类的类型，一般用到两个 <br> default：默认，可以通过 Mappers.getMapper(Class) 方式获取实例对象 <br> spring：在接口的实现类上自动添加注解 @Component，可通过 @Autowired 方式注入
| | @Mapping | 属性映射，若源对象属性与目标对象名字一致，会自动映射对应属性 | source：源属性 <br> target：目标属性 <br> dateFormat：String 到 Date 日期之间相互转换，通过 SimpleDateFormat，该值为 SimpleDateFormat 的日期格式 <br> ignore: 忽略这个字段 |
| | @Mappings | 配置多个@Mapping |  |   
| | @MappingTarget | 用于更新已有对象 |  |   
| | @InheritConfiguration | 用于继承配置 |  |   

## 定时任务业务分析

### 操作

![QUARTZ](image/quartz.png) 

### 时序图

![QUARTZ](image/quartzSequencediagram.jpg) 

### 状态分析

```text
    /**
     * 不存在：编辑、删除、构建
     */
    NONE(1, "NONE"),
    /**
     * 正常：运行、暂停
     */
    NORMAL(2, "NORMAL"),
    /**
     * 暂停：恢复、移除
     */
    PAUSED(3, "PAUSED"),
    /**
     * 完成：移除、运行
     */
    COMPLETE(4, "COMPLETE"),
    /**
     * 出错：恢复、移除
     */
    ERROR(5, "ERROR"),
    /**
     * 阻塞：运行
     */
    BLOCKED(6, "BLOCKED");
```

## 流式断言器AssertJ

##### 所谓的流式断言就是相较于Assert的单个校验点断言，支持一条断言语句对实际值同时断言多个校验点。

[参考](https://sq.163yun.com/blog/article/193927614897795072)

[参考](https://blog.csdn.net/w97531/article/details/82260555?utm_medium=distribute.pc_relevant.none-task-blog-baidulandingword-5&spm=1001.2101.3001.4242)

## JSR303校验注解

### Null检查
```
@Null(message = "")       验证对象是否为null
@NotNull(message = "")    验证对象是否不为null, 无法查检长度为0的字符串
@NotBlank(message = "")   检查约束字符串是不是Null还有被Trim的长度是否大于0,只对字符串,且会去掉前后空格.
@NotEmpty(message = "")  检查约束元素是否为NULL或者是EMPTY.
```
### Booelan检查
```
@AssertTrue(message = "")     验证 Boolean 对象是否为 true  
@AssertFalse(message = "")    验证 Boolean 对象是否为 false  
```
 
### 长度检查
```
@Size(min=, max=,message = "") 验证对象（Array,Collection,Map,String）长度是否在给定的范围之内  
@Length(min=, max=,message = "") Validates that the annotated string is between min and max included.
```

### 日期检查
```
@Past(message = "")           验证 Date 和 Calendar 对象是否在当前时间之前  
@Future(message = "")     	验证 Date 和 Calendar 对象是否在当前时间之后  
@Pattern(regexp = "",message = "")    	验证 String 对象是否符合正则表达式的规则
```

### 数值检查
> 建议使用在包装类型（Integer这类），不建议使用在int类型上， 因为表单值为“”时无法转换为int，但可以转换为Stirng为"",Integer为null

```
@Min(message = "")     验证 Number 和 String 对象是否大等于指定的值  
@Max(message = "")     验证 Number 和 String 对象是否小等于指定的值  
@DecimalMax(message = "") 值不大于约束中指定的最大值. 这个约束的参数是一个通过BigDecimal定义的最大值的字符串表示.小数存在精度
@DecimalMin(message = "") 值必须不小于约束中指定的最小值. 这个约束的参数是一个通过BigDecimal定义的最小值的字符串表示.小数存在精度
@Digits(message = "")     验证 Number 和 String 的构成是否合法  
@Digits(integer=,fraction=,message = "") 验证字符串是否是符合指定格式的数字，interger指定整数精度，fraction指定小数精度。
 
@Range(min=, max=,message = "") 检查数字是否介于min和max之间.

@Valid  递归的对关联对象进行校验, 如果关联对象是个集合或者数组,那么对其中的元素进行递归校验,如果是一个map,则对其中的值部分进行校验.(是否进行递归验证)
    
@CreditCardNumber(message = "") 信用卡验证
@Email(message = "")  验证是否是邮件地址，如果为null,不进行验证，算通过验证。
@ScriptAssert(lang= ,script=, alias=,message = "")
    
@URL(protocol=,host=, port=,regexp=, flags=,message = "")
```

## JavaMail 邮件接收、解析说明

![JavaMail](image/Java邮件接收.png) 

## 实现功能：

### 系统日志
<li>系统日志分页查询</li>

### 字典管理
<li>分页查询字典项接口</li>
<li>字典项添加接口</li>
<li>字典项删除接口</li>
<li>字典项修改接口</li>
<li>根据字典项获取字典值</li>
<li>字典值添加接口</li>
<li>字典值删除接口</li>
<li>字典值修改接口</li>

### 用户信息
<li>用户信息分页查询</li>
<li>用户信息添加</li>
<li>用户信息修改</li>
<li>用户冻结</li>
<li>用户解冻</li>
<li>用户分配角色</li>
<li>用户取消角色</li>

### 角色信息
<li>角色信息分页查询</li>
<li>角色信息添加</li>
<li>角色信息修改</li>
<li>角色信息删除</li>
<li>角色分配功能菜单</li>
<li>角色取消功能菜单</li>
<li>分页查询已授权角色</li>
<li>分页查询未授权角色</li>
<li>为角色授权功能菜单权限</li>
<li>撤销角色功能菜单权限</li>

### 功能菜单
<li>分页查询功能菜单接口</li>
<li>查询功能菜单树接口</li>
<li>功能菜单添加接口</li>
<li>功能菜单删除接口</li>
<li>功能菜单修改接口</li>

### 组织机构
<li>分页查询组织机构接口</li>
<li>查询组织机构树接口</li>
<li>组织机构添加接口</li>
<li>组织机构删除接口</li>
<li>组织机构修改接口</li>
<li>根据父组织机构标识查询组织机构树接口</li>
<li>根据用户标识查询用户拥有的组织机构接口</li>
<li>用户授权组织机构</li>
<li>取消用户组织机构授权</li>

### 验证码
<li>验证码开关</li>
<li>验证码生成策略调整</li>

### 定时任务
<li>分页查询定时任务接口</li>
<li>定时任务添加接口</li>
<li>定时任务删除接口</li>
<li>定时任务修改接口</li>
<li>定时任务调度接口</li>
<li>定时任务执行接口</li>
<li>定时任务暂停接口</li>
<li>定时任务恢复接口</li>
<li>定时任务移除接口</li>

## 权限标签：

> 参考 JAVA 常量类 

```
src/main/java/com/imis/base/constant/permissions/PermissionsValueConstant.java
``` 

## Spring 如何实现 AOP ?

![AOP](image/AOP实现.png)